﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class BanksController : Controller
    {
        private FundContext db = new FundContext();

        // GET: /Banks/
        public ActionResult Index()
        {
            return View(db.Banks.ToList());
        }

        // GET: /Banks/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = db.Banks.Find(id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // GET: /Banks/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.BankTypes = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("FINORGTYPE") && l.Activity == ActivityType.Active), "TypeId", "TypeName");

            return View();
        }

        // POST: /Banks/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(Bank bank, ICollection<BankBranch> branches)
        {
            try
            {
                if (db.Banks.Where(l => (l.BankName.Equals(bank.BankName) || l.ShortName.Equals(bank.ShortName)) && l.BankId != bank.BankId).Count() == 0)
                {
                    if (bank.BankId == 0)
                    {
                        var newBank = new Bank(bank);
                        if (branches != null)
                        {
                            var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                            foreach (var branch in branches)
                            {
                                branch.BranchCode = "BR" + (maxcid++).ToString("000");
                                branch.Activity = ActivityType.Active;
                                newBank.BankBranchs.Add(new BankBranch(branch));
                            }
                        }

                        db.Banks.Add(newBank);
                    }
                    else
                    {
                        var obank = db.Banks.Find(bank.BankId);
                        obank = (Bank)bank.ConvertNotNull(obank);
                        db.Entry(obank).State = EntityState.Modified;

                        if(branches != null)
                        {
                            var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                            foreach(var branch in branches)
                            {
                                if(branch.BranchId == 0)
                                {
                                    branch.BranchCode = "BR" + (maxcid++).ToString("000");
                                    branch.Activity = ActivityType.Active;
                                    db.BankBranchs.Add(new BankBranch(branch));
                                }
                                else
                                {
                                    var obranch = db.BankBranchs.Find(branch.BranchId);
                                    obranch = (BankBranch)branch.ConvertNotNull(obranch);
                                    db.Entry(obranch).State = EntityState.Modified; ;
                                }
                            }

                            var pbranches = db.BankBranchs.Where(c => c.BankId == bank.BankId);
                            for (var rIndex = 0; rIndex < pbranches.Count(); rIndex++)
                            {
                                var branch = pbranches.ToList()[rIndex];
                                if (db.Entry(branch).State == EntityState.Unchanged)
                                    db.Entry(branch).State = EntityState.Deleted;
                            }
                        }
                    }

                    db.SaveChanges();

                    return Json(new { status = "Success", bname = bank.BankName }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = "Error", message = "Duplicate bank name." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Banks/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = db.Banks.Include(c => c.BankBranchs).Where(l => l.BankId == id).FirstOrDefault();
            if (bank == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankTypes = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("FINORGTYPE") && l.Activity == ActivityType.Active), "TypeId", "TypeName", bank.BankType);
            return View("Create", bank);
        }

        // GET: /Banks/Delete/5
        [CAuthorize]
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Bank bank = db.Banks.Find(id);
            if (bank == null)
            {
                return HttpNotFound();
            }
            return View(bank);
        }

        // POST: /Banks/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(short id)
        {
            Bank bank = db.Banks.Find(id);
            db.Banks.Remove(bank);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult BranchTemplate(BankBranch branch)
        {
            return PartialView(branch);
        }

        // GET: /Bank/RowTemplate
        [HttpPost]
        public ActionResult RowTemplate(BankBranch branch)
        {
            return PartialView(branch);
        }
    }
}
