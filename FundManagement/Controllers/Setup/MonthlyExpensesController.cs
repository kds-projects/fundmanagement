﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class MonthlyExpensesController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /MonthlyExpenses/
        public ActionResult Index()
        {
            return View(db.MonthlyExpenses.ToList());
        }

        // GET: /MonthlyExpenses/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyExpense monthlyExpense = db.MonthlyExpenses.Find(id);
            if (monthlyExpense == null)
            {
                return HttpNotFound();
            }
            return View(monthlyExpense);
        }

        // GET: /MonthlyExpenses/Create
        public ActionResult Create()
        {
            var year = new[] { new { TypeName = "2018", TypeId = 1 }, new { TypeName = "2019", TypeId = 2 }, new { TypeName = "2020", TypeId = 3 } };
            ViewBag.YearList = new SelectList(year, "TypeName", "TypeName");
            ViewBag.MonthList = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("MONTHNAME")), "RankId", "TypeName");
            
            return View();
        }

        // POST: /MonthlyExpenses/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(MonthlyExpense monthlyExpense)
        {
            try
            {
                db.MonthlyExpenses.Add(monthlyExpense);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", month = monthlyExpense.ExpenseMonthName, year = monthlyExpense.ExpenseYear }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /MonthlyExpenses/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyExpense monthlyExpense = db.MonthlyExpenses.Find(id);
            if (monthlyExpense == null)
            {
                return HttpNotFound();
            }
            return View(monthlyExpense);
        }

        // POST: /MonthlyExpenses/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ExpenseId,ExpenseYear,ExpenseMonth,CashDisbursementCTG,CashDisbursementDHK,CreditorsPayment,Salaries,Royalties,PrfessionalFees,Utilities,UtilitiyRemarks,ProjectPayment,ProjectRemarks,AssetsAcquisiation,InsurancePremium,Tax,TaxRemarks,Miscellaneous,MiscellaneousRemarks,DirectorsPayment,DirectorRemarks,Devidend,Complaince,Others,OtherRemarks,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] MonthlyExpense monthlyExpense)
        {
            if (ModelState.IsValid)
            {
                db.Entry(monthlyExpense).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(monthlyExpense);
        }

        // GET: /MonthlyExpenses/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            MonthlyExpense monthlyExpense = db.MonthlyExpenses.Find(id);
            if (monthlyExpense == null)
            {
                return HttpNotFound();
            }
            return View(monthlyExpense);
        }

        // POST: /MonthlyExpenses/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            MonthlyExpense monthlyExpense = db.MonthlyExpenses.Find(id);
            db.MonthlyExpenses.Remove(monthlyExpense);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
