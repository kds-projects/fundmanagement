﻿using FundManagement.Models;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FundManagement.Controllers.Setup
{
    public class WharfRentController : ContextList
    {
      
        public ActionResult Index()
        {
            var wharfRentlist = db.Wharfrents.Include(x => x.LCMain).Select(x => new WhrafRentViewModel
            {

             LcNo=x.LCMain.LCNo,
             LcDate=x.LCMain.LCDate,
             LotNo=x.LotNo,
             BillDate=x.BillDate,
             BreathingDate=x.BrethingDate,
             PortLandingDate=x.PortLandingDate,
             DeliveryDate=x.DeliveryDate,
             FromDate=x.WhrafRentFromDate,
             ToDate=x.WhrafRentToDate,
             WhrafRentAmount=x.WhrafRentAmount,
             DetentionCharge=x.ContainerDetentionCharge,
             Remarks=x.Remarks,
             LcFileId=x.LcFileId,
             WharfrentId=x.WharfrentId,
             DetentionFromDate=x.DetentionFromDate,
             DetentionToDate=x.DetentionToDate

            }).ToList();


            return View(wharfRentlist);
        }

        public ActionResult Create()
        {

            var LcListExistInWhrfRent = db.Wharfrents.Select(x => x.LcFileId).ToList();
            var piplinelist = dbIMS.PipelineMains.ToList();
            var lclist = db.LCMains.ToList();

            var ListOfLc = (from a in lclist
                          join
    b in piplinelist on a.LCFileId equals b.LCFileId
                          select new LCMain
                          {
                              LCFileId = a.LCFileId,
                              LCNo = a.LCNo
                          }).ToList();
            /*var LcList = db.LCMains.Where(x => !LcListExistInWhrfRent.Contains(x.LCFileId)).ToList();*/
            ViewBag.LcList = new SelectList(ListOfLc, "LCFileId", "LCNo");

            return View();
        }


        public ActionResult Edit(int WharfrentId)
        {
            var WfrafRent = db.Wharfrents.Find(WharfrentId);

            var LcList = db.LCMains.ToList();
            ViewBag.LcList = new SelectList(LcList, "LCFileId", "LCNo");

            return View(WfrafRent);
        }


        public ActionResult LoadLotNo(int lcno)
        {
           var lotNo= dbIMS.PipelineMains.Where(x => x.LCFileId == lcno).Select(x =>  new  
           { 
              Text=x.LotNo,
              Value=x.LotNo

           }).ToList();

            ViewBag.lotNo = new SelectList(lotNo, "Value", "Text");

            return PartialView("_GetLotNo");
        
        }

        public JsonResult GetFactoryLocation(int LcFileId)
        {
            int locationId = 1;
            locationId = (from a in db.LCWisePIs join b in db.PIMains 
                        on a.PIFileId equals b.PIFileId join 
                        c in db.PISubs on b.PIFileId equals
                        c.PIFileId join d in db.FactoryLoacations
                        on c.FactoryId equals d.FactoryId
                        select new
                        { 
                          locationId=d.LocationId
                        }).ToList().Select(x=>x.locationId).FirstOrDefault();
                        
            return Json(locationId, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult Save(Wharfrent wharfrent)
        {
            try
            {

                var rent = new Wharfrent(wharfrent);

                db.Wharfrents.Add(rent);
                db.SaveChanges();
                return Json(new { status = "Success", chkid = wharfrent.WharfrentId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }

        }

        [HttpPost]
        public ActionResult Edit(Wharfrent wharfrent)
        {

            try
            {
                var rent = new Wharfrent(wharfrent);
                db.Entry(rent).State = EntityState.Modified;
                db.SaveChanges();
                return Json(new { status = "Success", chkid = wharfrent.WharfrentId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }




    }
}