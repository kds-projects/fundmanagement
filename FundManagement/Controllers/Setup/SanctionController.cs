﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public class SanctionController : Controller
    {
        private FundContext db = new FundContext();

        // GET: /Sanction/
        public ActionResult Index()
        {
            var sanctions = db.SanctionMains;
            return View(sanctions.ToList());
        }

        // GET: /Sanction/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SanctionDetails sanctionDetails = db.SanctionDetails.Find(id);
            if (sanctionDetails == null)
            {
                return HttpNotFound();
            }
            return View(sanctionDetails);
        }

        // GET: /Sanction/Create
        public ActionResult Create()
        {
            ViewBag.Bank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.Branch = new SelectList(db.BankBranchs.Where(l => l.BankId == 0), "BranchId", "BranchName");

            return View();
        }

        // POST: /Sanction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(SanctionMain sanction, ICollection<SanctionDetails> sdetails)
        {
            try
            {
                //db.SanctionMains.Where(l => l.BankId == sanction.BankId && l.BranchId == sanction.BranchId
                //    && l.SanctionType == sanctionDetails.SanctionType && l.SanctionYear == sanctionDetails.SanctionYear
                if (sanction.SanctionId == 0)
                {
                    if (db.SanctionMains.Where(l => l.SanctionNo.Equals(sanction.SanctionNo)).Count() == 0)
                    {
                        var newSanction = new SanctionMain(sanction);
                        if (sdetails != null)
                        {
                            foreach (var sub in sdetails)
                            {
                                newSanction.SanctionDetails.Add(new SanctionDetails(sub));
                            }
                        }
                        db.SanctionMains.Add(newSanction);

                    }
                    else
                        return Json(new { status = "Error", message = "Saction no is already exist." }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    if (db.SanctionMains.Where(l => l.SanctionNo.Equals(sanction.SanctionNo) && l.SanctionId != sanction.SanctionId).Count() == 0)
                    {
                        var osanction = db.SanctionMains.Find(sanction.SanctionId);
                        osanction = (SanctionMain)sanction.ConvertNotNull(osanction);
                        db.Entry(osanction).State = EntityState.Modified;

                        if (sdetails != null)
                        {
                            //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                            foreach (var details in sdetails)
                            {
                                if (details.SanctionDetailsId == 0)
                                {
                                    details.Status = 1;
                                    db.SanctionDetails.Add(new SanctionDetails(details));
                                }
                                else
                                {
                                    var odetails = db.SanctionDetails.Find(details.SanctionDetailsId);
                                    odetails = (SanctionDetails)details.ConvertNotNull(odetails);
                                    db.Entry(odetails).State = EntityState.Modified;
                                }
                            }

                            var pdetails = db.SanctionDetails.Where(c => c.SanctionId == sanction.SanctionId);
                            for (var rIndex = 0; rIndex < pdetails.Count(); rIndex++)
                            {
                                var detail = pdetails.ToList()[rIndex];
                                if (db.Entry(detail).State == EntityState.Unchanged)
                                    db.Entry(detail).State = EntityState.Deleted;
                            }

                        }
                    }
                    else
                        return Json(new { status = "Error", message = "Saction no is already exist." }, JsonRequestBehavior.AllowGet);
                }

                db.SaveChanges();

                return Json(new { status = "Success", sno = sanction.SanctionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Sanction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SanctionMain sanction = db.SanctionMains.Find(id); //.Include(c => c.SanctionDetails).Where(l => l.SanctionId == id).FirstOrDefault();
            if (sanction == null)
            {
                return HttpNotFound();
            }

            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", sanction.BankId);
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.BankId == sanction.BankId), "BranchId", "BranchName", sanction.BranchId);

            return View("Create", sanction);
        }

        // POST: /Sanction/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "SanctionId,BranchId,BankId,SanctionType,SanctionYear,SanctionLimit,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] SanctionDetails sanctionDetails)
        {
            if (ModelState.IsValid)
            {
                db.Entry(sanctionDetails).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", sanctionDetails.BankId);
            //ViewBag.BranchId = new SelectList(db.BankBranchs, "BranchId", "BranchCode", sanctionDetails.BranchId);
            ViewBag.SanctionType = new SelectList(db.CustomTypes, "TypeId", "TypeName", sanctionDetails.SanctionType);
            return View(sanctionDetails);
        }

        // GET: /Sanction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            SanctionDetails sanctionDetails = db.SanctionDetails.Find(id);
            if (sanctionDetails == null)
            {
                return HttpNotFound();
            }
            return View(sanctionDetails);
        }

        // POST: /Sanction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            SanctionDetails sanctionDetails = db.SanctionDetails.Find(id);
            db.SanctionDetails.Remove(sanctionDetails);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult DetailsTemplate(SanctionDetails details)
        {
            ViewBag.Type = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return PartialView(details);
        }

        // GET: /Bank/RowTemplate
        public ActionResult RowTemplate(SanctionDetails details)
        {
            if (details.SanctionType == null)
                details.SanctionType = db.CustomTypes.Find(details.SanctionTypeId);

            return PartialView(details);
        }
    }
}
