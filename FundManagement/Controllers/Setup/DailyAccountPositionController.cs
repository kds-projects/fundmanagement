﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public class DailyAccountPositionController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /DailyAccountPosition/
        public ActionResult Index()
        {
            return View(db.DailyAccountPositions.ToList());
        }

        // GET: /DailyAccountPosition/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyAccountPosition dailyAccountPosition = db.DailyAccountPositions.Find(id);
            if (dailyAccountPosition == null)
            {
                return HttpNotFound();
            }
            return View(dailyAccountPosition);
        }

        // GET: /DailyAccountPosition/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: /DailyAccountPosition/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(DailyAccountPosition dailyAccountPosition)
        {
            try
            {
                db.DailyAccountPositions.Add(dailyAccountPosition);
                db.SaveChanges();

                return Json(new { status = "Success", date = dailyAccountPosition.DailyDate.ToString("dd-MMM-yyyy") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /DailyAccountPosition/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyAccountPosition dailyAccountPosition = db.DailyAccountPositions.Find(id);
            if (dailyAccountPosition == null)
            {
                return HttpNotFound();
            }
            return View(dailyAccountPosition);
        }

        // POST: /DailyAccountPosition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="DAPId,DailyDate,CurrentAccount,DLAccount,ODAccount,ReceivedAmount,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] DailyAccountPosition dailyAccountPosition)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dailyAccountPosition).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dailyAccountPosition);
        }

        // GET: /DailyAccountPosition/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyAccountPosition dailyAccountPosition = db.DailyAccountPositions.Find(id);
            if (dailyAccountPosition == null)
            {
                return HttpNotFound();
            }
            return View(dailyAccountPosition);
        }

        // POST: /DailyAccountPosition/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            DailyAccountPosition dailyAccountPosition = db.DailyAccountPositions.Find(id);
            db.DailyAccountPositions.Remove(dailyAccountPosition);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
