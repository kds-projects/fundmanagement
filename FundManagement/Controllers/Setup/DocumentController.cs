﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public class DocumentController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Document/
        [CAuthorize]
        public ActionResult Index()
        {
            return View(db.ReceivableDocuments.ToList());
        }

        // GET: /Document/Details/5
        [CAuthorize]
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceivableDocument receivableDocument = db.ReceivableDocuments.Find(id);
            if (receivableDocument == null)
            {
                return HttpNotFound();
            }
            return View(receivableDocument);
        }

        // GET: /Document/Create
        [CAuthorize]
        public ActionResult Create()
        {
            //ViewBag.CompanyList = new SelectList(new List<CustomType>() { new CustomType { TypeId = 1, TypeName = "KDS Accessories Ltd." },
            //    new CustomType { TypeId = 2, TypeName = "KDS Thread Ltd." }, new CustomType { TypeId = 3, TypeName = "KDS Poly Industry Ltd." }, 
            //    new CustomType { TypeId = 5, TypeName = "KDS Accessories Ltd (Unit-2)." }, new CustomType { TypeId = 6, TypeName = "KDS Thread Ltd (Unit-2)." }
            //}, "TypeId", "TypeName");
            ViewBag.CompanyList = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1), "CompanyId", "ShortName");
            ViewBag.DocumentTypes = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("RECEIVABLETYPE")), "TypeId", "TypeName");
            ViewBag.AccountList = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec PO_NEW.dbo.prcSupplierCS").ToList().Select(c => new { SupplierId = c.Supp_ID, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Supp_Name.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");

            return View();
        }

        // POST: /Document/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(ReceivableDocument doc_info)
        {
            try
            {
                if (doc_info.DocumentId == 0 && db.ReceivableDocuments.Where(l => l.DocumentNo.Equals(doc_info.DocumentNo)).Count() == 0)
                {
                    var newDocument = new ReceivableDocument(doc_info);
                    newDocument.Activity = ActivityType.Active;
                    db.ReceivableDocuments.Add(newDocument);
                    db.SaveChanges();

                    return Json(new { status = "Success", docno = doc_info.DocumentNo }, JsonRequestBehavior.AllowGet);
                }
                else if (doc_info.DocumentId > 0 && db.ReceivableDocuments.Where(l => l.DocumentNo.Equals(doc_info.DocumentNo) && l.DocumentId != doc_info.DocumentId).Count() == 0)
                {
                    var oldDocument = db.ReceivableDocuments.Find(doc_info.DocumentId);
                    oldDocument = (ReceivableDocument)doc_info.ConvertNotNull(oldDocument);
                    db.Entry(oldDocument).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { status = "Success", docno = doc_info.DocumentNo }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = "Error", message = "Duplicate document info." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Document/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceivableDocument receivableDocument = db.ReceivableDocuments.Find(id);
            if (receivableDocument == null)
            {
                return HttpNotFound();
            }
            //ViewBag.CompanyList = new SelectList(new List<CustomType>() { new CustomType { TypeId = 1, TypeName = "KDS Accessories Ltd." },
            //    new CustomType { TypeId = 2, TypeName = "KDS Thread Ltd." }, new CustomType { TypeId = 3, TypeName = "KDS Poly Industry Ltd." }, 
            //    new CustomType { TypeId = 5, TypeName = "KDS Accessories Ltd (Unit-2)." }, new CustomType { TypeId = 6, TypeName = "KDS Thread Ltd (Unit-2)." }
            //}, "TypeId", "TypeName", receivableDocument.CompanyId);
            ViewBag.CompanyList = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1), "CompanyId", "ShortName", receivableDocument.CompanyId);
            ViewBag.DocumentTypes = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("RECEIVABLETYPE")), "TypeId", "TypeName", receivableDocument.DocumentTypeId);
            ViewBag.AccountList = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec PO_NEW.dbo.prcSupplierCS").ToList().Select(c => new { SupplierId = c.Supp_ID, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Supp_Name.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour", receivableDocument.PayableId.ToString());

            return View("Create", receivableDocument);
        }

        // POST: /Document/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="DocumentId,DocumentNo,DocumentDate,ReceiveDate,DocumentType,PayableId,PayableAmount,Activity,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] ReceivableDocument receivableDocument)
        {
            if (ModelState.IsValid)
            {
                db.Entry(receivableDocument).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(receivableDocument);
        }

        // GET: /Document/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReceivableDocument receivableDocument = db.ReceivableDocuments.Find(id);
            if (receivableDocument == null)
            {
                return HttpNotFound();
            }
            return View(receivableDocument);
        }

        // POST: /Document/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            ReceivableDocument receivableDocument = db.ReceivableDocuments.Find(id);
            db.ReceivableDocuments.Remove(receivableDocument);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
