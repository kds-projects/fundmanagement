﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{

    public class ChequePaymentController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /ChequePayment/
        public ActionResult Index()
        {
            var chequepayments = db.ChequePayments.Include(c => c.ChequeInfo);
            return View(chequepayments.ToList());
        }

        // GET: /ChequePayment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequePayment chequePayment = db.ChequePayments.Find(id);
            if (chequePayment == null)
            {
                return HttpNotFound();
            }
            return View(chequePayment);
        }

        // GET: /ChequePayment/
        [CAuthorize]
        public ActionResult PendingInstruction()
        {
            var chequeissueinstructions = db.ChequeIssueInstructions.Include(c => c.UrgencyType).Where(c => c.InstructionStatus == 0 && c.ApprovedType == 1);
            return View(chequeissueinstructions.ToList());
        }

        // GET: /ChequePayment/Create
        [CAuthorize]
        public ActionResult PaymentByInstruction(int id)
        {
            var instruction = db.ChequeIssueInstructions.Single(c => c.InstructionId == id);
            if (instruction == null || instruction.InstructionStatus != 0)
            {
                return HttpNotFound();
            }

            var payment = new List<ChequeIssueInstruction>() { instruction }.ToList().Select(c => new ChequePayment()
            {
                IssueDate = clsMain.getCurrentTime().Date,
                InstructionId = c.InstructionId,
                IssueTo = c.IssuedAccountId.ToString() + (c.IssuedAccountId == 0 ? ">>" + c.IssuedAccountName : ""),
                ChequeAmount = c.ChequeAmount,
                ChequeDate = c.ChequeDate,
                Purpose = c.Purpose,
                IssuedAccountId = c.IssuedAccountId,
                IssuedAccountNo = (c.IssuedAccount != null ? c.IssuedAccount.AccountNo + " - " + c.IssuedAccount.Bank.BankName : "")
            }).FirstOrDefault();

            ViewBag.BankId = new SelectList(db.Banks.Where(c => c.Activity == ActivityType.Active), "BankId", "BankName", instruction.PaymentFrom.BankId);
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == instruction.PaymentFrom.BranchId), "AccountId", "AccountNo", instruction.BankAccountId);
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BankId == instruction.PaymentFrom.BankId), "BranchId", "BranchName", instruction.PaymentFrom.BranchId);
            ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.Activity == ActivityType.Active && c.AccountId == instruction.BankAccountId && c.Status == 1 && c.PaymentModeId == instruction.PaymentModeId), "ChequeId", "ChequeNo");
            ViewBag.UrgencyType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("URGTYPE")), "TypeId", "TypeName");
            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");
            ViewBag.ChequeModeId = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName", instruction.ChequeModeId);
            ViewBag.PaymentMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");

            var aclist = (from p in db.BankAccount.AsEnumerable()
                          join q in db.Banks.AsEnumerable() on p.BankId equals q.BankId
                          join r in dbProd.Companies.AsEnumerable() on p.CompanyId equals r.CompanyId
                          select new { AccountId = p.AccountId.ToString(), AccountNo = p.AccountNo + " >> " + q.ShortName + " >> " + r.ShortName }
                         ).Distinct().ToList()
                         .Union(db.ChequePayments.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssueTo, AccountNo = c.IssueTo + " >> " }).Distinct().ToList())
                         .Union(db.ChequeIssueInstructions.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssuedAccountName, AccountNo = c.IssuedAccountName + " >> " }).Distinct().ToList());
            ViewBag.IssueTo = new SelectList(aclist.Distinct(), "AccountId", "AccountNo", payment.IssueTo);

            var totalpayment = db.ChequePayments.Where(c => c.InstructionId == instruction.InstructionId).Select(c => c.ChequeAmount).DefaultIfEmpty(0).Sum();
            payment.ChequeAmount = instruction.ChequeAmount - totalpayment;

            ViewBag.PaymentModeId = instruction.PaymentModeId;

            return View(payment);
        }

        //// GET: /ChequePayment/Create
        //[CAuthorize]
        //public ActionResult Create()
        //{
        //    ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
        //    ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.AccountId >= 0), "AccountId", "AccountNo");
        //    ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.BranchId >= 0), "BranchId", "BranchName");
        //    ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.ChequeId == 0), "ChequeId", "ChequeNo");
        //    ViewBag.Issuer = new SelectList(db.ChequePayments.Select(c => new { IssueTo = c.IssueTo }).Distinct(), "IssueTo", "IssueTo");
        //    ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");
        //    ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName");

        //    return View();
        //}

        // POST: /ChequePayment/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        [CAuthorize]
        public ActionResult Save(ChequePayment chequePayment)
        {
            try
            {
                var cheque = db.ChequeInfos.Find(chequePayment.ChequeId);
                chequePayment.PaymentNo = "CPV" + (from p in db.ChequePayments.Where(c => c.IssueDate.Year == chequePayment.IssueDate.Year).AsEnumerable() select new { SLNo = p.SerialNo + 1 }).Select(c => c.SLNo).DefaultIfEmpty(1).Max().ToString("0000") + "/" + chequePayment.IssueDate.ToString("yy");

                if (chequePayment.InstructionId > 0)
                {
                    var instruction = db.ChequeIssueInstructions.Find(chequePayment.InstructionId);
                    chequePayment.IssueTo = instruction.IssuedAccountName;

                    var totalpayment = chequePayment.ChequeAmount + db.ChequePayments.Where(c => c.InstructionId == chequePayment.InstructionId).Select(c => c.ChequeAmount).DefaultIfEmpty(0).Sum();
                    var balance = instruction.ChequeAmount - totalpayment;
                    if (balance == 0)
                        instruction.InstructionStatus = 1;

                    db.Entry(instruction).State = EntityState.Modified;
                }

                if (cheque != null)
                {
                    cheque.Status = 0;
                    db.Entry(cheque).State = EntityState.Modified;
                }

                db.ChequePayments.Add(new ChequePayment(chequePayment));

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", chkid = chequePayment.ChequeId, chqno = cheque.ChequeNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /ChequePayment/
        [CAuthorize]
        public ActionResult PendingAdjustment()
        {
            var ins_list = (from p in db.ChequeIssueInstructions.AsEnumerable()
                            join q in db.AdvanceAdjustments.GroupBy(c => c.AdjustedInstructionId).Select(c => new { InstructionId = c.Key, AdjustedAmount = c.Sum(l => l.AdjustAmount) }).AsEnumerable() on p.InstructionId equals q.InstructionId into r
                            from s in r.DefaultIfEmpty()
                            where p.AdjustAmount > 0 && p.AdjustAmount - (s == null ? 0 : s.AdjustedAmount) > 0
                            select p
                           ).Distinct().ToList();
            var chequeissueinstructions = ins_list;
            return View(chequeissueinstructions.ToList());
        }

        // GET: /ChequePayment/Adjustment
        [CAuthorize]
        public ActionResult Adjustment(int id = 0)
        {
            var instruction = db.ChequeIssueInstructions.Find(id);
            if (instruction == null || instruction.AdjustAmount == 0)
            {
                return HttpNotFound();
            }
            var BillForwardId = instruction.ChequeIssueSubs.FirstOrDefault().LPBillForwardId;
            var payable_id = db.Database.SqlQuery<short>("Select Cast(Supp_ID As smallint) As Supp_ID From PO_New.dbo.Credit_Purchase_Bill_Main Where CPBPlanNo = " + BillForwardId.ToString()).FirstOrDefault();

            var payment = new List<ChequeIssueInstruction>() { instruction }.ToList().Select(c => new AdvanceAdjustment()
            {
                AdjustmentDate = clsMain.getCurrentTime().Date,
                InstructionId = c.InstructionId,
                ChequeIssueInstruction = instruction,
                //IssueTo = c.IssuedAccountId.ToString() + (c.IssuedAccountId == 0 ? ">>" + c.IssuedAccountName : ""),
                AdjustAmount = c.AdjustAmount,
                //ChequeDate = c.ChequeDate,
                Purpose = c.Purpose,
                //IssuedAccountId = c.IssuedAccountId,
                //IssuedAccountNo = (c.IssuedAccount != null ? c.IssuedAccount.AccountNo + " - " + c.IssuedAccount.Bank.BankName : "")
            }).FirstOrDefault();

            var inlist = (from p in db.ChequeIssueInstructions.AsEnumerable()
                          join q in db.ChequeIssueSubs.AsEnumerable() on p.InstructionId equals q.InstructionId
                          join r in db.ReceivableDocuments.AsEnumerable() on q.WorkOrderId equals r.DocumentId
                          join s in db.AdvanceAdjustments.GroupBy(c => c.InstructionId).Select(c => new { InstructionId = c.Key, AdjustAmount = c.Sum(l => l.AdjustAmount) }).AsEnumerable() on p.InstructionId equals s.InstructionId into t
                          from u in t.DefaultIfEmpty()
                          where p.PaymentAgainstId == 224 && r.PayableId == payable_id && ((p.AdjustAmount + p.ChequeAmount) - (u == null ? 0 : u.AdjustAmount)) > 0
                          select new { p.InstructionNo, p.InstructionId }
                         ).Distinct().ToList();

            ViewBag.Instructions = new SelectList(inlist.Distinct(), "InstructionId", "InstructionNo");

            var totalpayment = db.AdvanceAdjustments.Where(c => c.InstructionId == instruction.InstructionId).Select(c => c.AdjustAmount).DefaultIfEmpty(0).Sum();
            payment.AdjustAmount = instruction.AdjustAmount - totalpayment;

            payment.AdjustmentNo = "ADJ" + (from p in db.AdvanceAdjustments.Where(c => c.AdjustmentDate.Year == DateTime.Today.Year).AsEnumerable() select new { SLNo = p.SerialNo + 1 }).Select(c => c.SLNo).DefaultIfEmpty(1).Max().ToString("0000") + "/" + DateTime.Today.ToString("yy");

            return View(payment);
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult SaveAdjustment(AdvanceAdjustment _adjustment)
        {
            try
            {
                //var adjustment = db.AdvanceAdjustments.Find(_adjustment.AdjustmentId);
                _adjustment.AdjustmentNo = "ADJ" + (from p in db.AdvanceAdjustments.Where(c => c.AdjustmentDate.Year == _adjustment.AdjustmentDate.Year).AsEnumerable() select new { SLNo = p.SerialNo + 1 }).Select(c => c.SLNo).DefaultIfEmpty(1).Max().ToString("0000") + "/" + _adjustment.AdjustmentDate.ToString("yy");
                var instruction = db.ChequeIssueInstructions.Find(_adjustment.InstructionId);

                //if (adjustment.InstructionId > 0)
                //{
                //    var instruction = db.ChequeIssueInstructions.Find(adjustment.InstructionId);
                //    chequePayment.IssueTo = instruction.IssuedAccountName;

                //    var totalpayment = chequePayment.ChequeAmount + db.ChequePayments.Where(c => c.InstructionId == chequePayment.InstructionId).Select(c => c.ChequeAmount).DefaultIfEmpty(0).Sum();
                //    var balance = instruction.ChequeAmount - totalpayment;
                //    if (balance == 0)
                //        instruction.InstructionStatus = 1;

                //    db.Entry(instruction).State = EntityState.Modified;
                //}

                //if (cheque != null)
                //{
                //    cheque.Status = 0;
                //    db.Entry(cheque).State = EntityState.Modified;
                //}

                db.AdvanceAdjustments.Add(new AdvanceAdjustment(_adjustment));

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", insid = _adjustment.InstructionId, insno = instruction.InstructionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /ChequePayment/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequePayment payment = db.ChequePayments.Find(id);
            if (payment == null)
            {
                return HttpNotFound();
            }

            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", payment.ChequeInfo.BankId);
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.BankId == payment.ChequeInfo.BankId && c.BranchId == payment.ChequeInfo.BranchId), "AccountId", "AccountNo", payment.ChequeInfo.AccountId);
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.BankId == payment.ChequeInfo.BankId), "BranchId", "BranchName", payment.ChequeInfo.BranchId);
            ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.AccountId == payment.ChequeInfo.AccountId && (c.Status == 1 || c.ChequeId == payment.ChequeId)), "ChequeId", "ChequeNo", payment.ChequeId);
            ViewBag.Issuer = new SelectList(db.ChequePayments.Select(c => new { IssueTo = c.IssueTo }).Distinct(), "IssueTo", "IssueTo", payment.IssueTo);
            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName", payment.Signatories.Split(','));
            ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName", payment.ChequeModeId);

            return View(payment);
        }

        // POST: /ChequePayment/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include = "PaymentId,ChequeId,IssueDate,ChequeDate,IssueTo,ChequeAmount,Purpose,Signatories,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] ChequePayment chequePayment)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chequePayment).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.ChequeId = new SelectList(db.ChequeInfos, "ChequeId", "ChequeNo", chequePayment.ChequeId);
            return View(chequePayment);
        }

        // GET: /ChequePayment/Details/5
        public ActionResult Print(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequePayment chequePayment = db.ChequePayments.Find(id);
            if (chequePayment == null)
            {
                return HttpNotFound();
            }
            return View(chequePayment);
        }

        // GET: /ChequePayment/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequePayment chequePayment = db.ChequePayments.Find(id);
            if (chequePayment == null)
            {
                return HttpNotFound();
            }
            return View(chequePayment);
        }

        // POST: /ChequePayment/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChequePayment chequePayment = db.ChequePayments.Find(id);
            db.ChequePayments.Remove(chequePayment);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult getAdjustAmount(int id)
        {
            var adv_amt = (from p in db.ChequeIssueInstructions.AsEnumerable()
                           join q in db.ChequeIssueSubs.AsEnumerable() on p.InstructionId equals q.InstructionId
                           join r in db.ReceivableDocuments.AsEnumerable() on q.WorkOrderId equals r.DocumentId
                           join s in db.AdvanceAdjustments.GroupBy(c => c.InstructionId).Select(c => new { InstructionId = c.Key, AdjustAmount = c.Sum(l => l.AdjustAmount) }).AsEnumerable() on p.InstructionId equals s.InstructionId into t
                           from u in t.DefaultIfEmpty()
                           where p.InstructionId == id && ((p.AdjustAmount + p.ChequeAmount) - (u == null ? 0 : u.AdjustAmount)) > 0
                           select new { AdvanceAmount = ((p.AdjustAmount + p.ChequeAmount) - (u == null ? 0 : u.AdjustAmount)) }
                          ).FirstOrDefault().AdvanceAmount;
            return Json(new { adv_amt = adv_amt }, JsonRequestBehavior.AllowGet);
        }
    }
}
