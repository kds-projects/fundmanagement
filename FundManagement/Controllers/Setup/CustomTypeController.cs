﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    [CAuthorize]
    public class CustomTypeController : Controller
    {
        private FundContext db = new FundContext();

        // GET: /CustomType/
        public ActionResult Index()
        {
            return View(db.CustomTypes.ToList());
        }

        // GET: /CustomType/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }
            return View(customType);
        }

        // GET: /CustomType/Create
        public ActionResult Create()
        {
            ViewBag.flags = new SelectList(db.CustomTypes.Select(c => new { Flag = c.Flag }).Distinct(), "Flag", "Flag");

            return View();
        }

        // POST: /CustomType/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(CustomTypeView customType)
        {
            try
            {
                if (customType.TypeId == 0)
                {
                    var type = new CustomType(customType);
                    type.IPAddress = "-";
                    type.PCName = "-";
                    db.CustomTypes.Add(type);
                }
                else
                {
                    var type = db.CustomTypes.Find(customType.TypeId);
                    type = (CustomType)customType.ConvertNotNull(type);
                    db.Entry(type).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Success", tname = customType.TypeName }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /CustomType/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }

            var type = (CustomTypeView)customType.Convert(new CustomTypeView());
            ViewBag.flags = new SelectList(db.CustomTypes.Select(c => new { Flag = c.Flag }).Distinct(), "Flag", "Flag", customType.Flag);
            return View("Create", type);
        }

        // POST: /CustomType/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="TypeId,TypeName,ShortName,Flag,Activity,RankId,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] CustomType customType)
        {
            if (ModelState.IsValid)
            {
                db.Entry(customType).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(customType);
        }

        // GET: /CustomType/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CustomType customType = db.CustomTypes.Find(id);
            if (customType == null)
            {
                return HttpNotFound();
            }
            return View(customType);
        }

        // POST: /CustomType/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            CustomType customType = db.CustomTypes.Find(id);
            db.CustomTypes.Remove(customType);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
