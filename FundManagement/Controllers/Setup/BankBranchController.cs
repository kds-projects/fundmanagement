﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{

    public class BankBranchController : Controller
    {
        private FundContext db = new FundContext();

        // GET: /BankBranch/
        public ActionResult Index()
        {
            var bankbranchs = db.BankBranchs.Include(b => b.Bank);
            return View(bankbranchs.ToList());
        }

        // GET: /BankBranch/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBranch bankBranch = db.BankBranchs.Find(id);
            if (bankBranch == null)
            {
                return HttpNotFound();
            }
            return View(bankBranch);
        }

        // GET: /BankBranch/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            return View();
        }

        // POST: /BankBranch/Create
        [HttpPost, CAuthorize]
        public ActionResult Create([Bind(Include="BankId,BranchCode,BranchName,ShortName,Address,Activity")] BankBranch bankBranch)
        {
            //if (ModelState.IsValid)
            //{
                db.BankBranchs.Add(bankBranch);
                db.SaveChanges();
                return RedirectToAction("Index");
            //}

            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", bankBranch.BankId);
            return View(bankBranch);
        }

        // GET: /BankBranch/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBranch bankBranch = db.BankBranchs.Find(id);
            if (bankBranch == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", bankBranch.BankId);
            return View(bankBranch);
        }

        // GET: /BranchList/
        [HttpPost]
        public ActionResult BranchList(int id, string ctlname)
        {
            var bankbranchs = db.BankBranchs.Include(b => b.Bank).Where(l => l.BankId == id);
            ViewBag.Branches = new SelectList(bankbranchs, "BranchId", "BranchName");
            ViewBag.ctlname = ctlname;

            return PartialView();
        }

        // GET: /BankBranch/Delete/5
        [CAuthorize]
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankBranch bankBranch = db.BankBranchs.Find(id);
            if (bankBranch == null)
            {
                return HttpNotFound();
            }
            return View(bankBranch);
        }

        // POST: /BankBranch/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(short id)
        {
            BankBranch bankBranch = db.BankBranchs.Find(id);
            db.BankBranchs.Remove(bankBranch);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
