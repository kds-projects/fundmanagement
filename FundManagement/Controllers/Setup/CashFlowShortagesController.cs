﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class CashFlowShortagesController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /CashFlowShortages/
        public ActionResult Index()
        {
            return View(db.CashFlowShortages.ToList());
        }

        // GET: /CashFlowShortages/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowShortage cashFlowShortage = db.CashFlowShortages.Find(id);
            if (cashFlowShortage == null)
            {
                return HttpNotFound();
            }
            return View(cashFlowShortage);
        }

        // GET: /CashFlowShortages/Create
        public ActionResult Create()
        {
            var year = new[] { new { TypeName = "2018", TypeId = 1 }, new { TypeName = "2019", TypeId = 2 }, new { TypeName = "2020", TypeId = 3 } };
            ViewBag.YearList = new SelectList(year, "TypeName", "TypeName");
            ViewBag.MonthList = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("MONTHNAME")), "RankId", "TypeName");
            
            return View();
        }

        // POST: /CashFlowShortages/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(CashFlowShortage cashFlow)
        {
            try
            {
                db.CashFlowShortages.Add(cashFlow);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", month = cashFlow.CFSMonthName, year = cashFlow.CFSYear }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /CashFlowShortages/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowShortage cashFlowShortage = db.CashFlowShortages.Find(id);
            if (cashFlowShortage == null)
            {
                return HttpNotFound();
            }
            return View(cashFlowShortage);
        }

        // POST: /CashFlowShortages/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="CFSId,CFSYear,CFSMonth,DiscountingBDT,DiscountingUSD,LTL,Revolving,STL_DL,SisterConcern,SisterConcernRemarks,ProjectLoan,ProjectRemarks,Others,OtherRemarks,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] CashFlowShortage cashFlowShortage)
        {
            if (ModelState.IsValid)
            {
                db.Entry(cashFlowShortage).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(cashFlowShortage);
        }

        // GET: /CashFlowShortages/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowShortage cashFlowShortage = db.CashFlowShortages.Find(id);
            if (cashFlowShortage == null)
            {
                return HttpNotFound();
            }
            return View(cashFlowShortage);
        }

        // POST: /CashFlowShortages/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            CashFlowShortage cashFlowShortage = db.CashFlowShortages.Find(id);
            db.CashFlowShortages.Remove(cashFlowShortage);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
