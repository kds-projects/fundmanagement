﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;
using System.Transactions;
using System.Globalization;

namespace FundManagement.Controllers.Setup
{
    [Route("instruction")]
    public class ChequeInstructionController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /ChequeInstruction/
        public ActionResult Index()
        {
            var chequeissueinstructions = db.ChequeIssueInstructions.Include(c => c.UrgencyType);
            return View(chequeissueinstructions.ToList());
        }

        // GET: /ChequeInstruction/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeIssueInstruction chequeIssueInstruction = db.ChequeIssueInstructions.Find(id);
            if (chequeIssueInstruction == null)
            {
                return HttpNotFound();
            }
            return View(chequeIssueInstruction);
        }

        // GET: /ChequeInstruction/Create
        [CAuthorize]
        public ActionResult Create(string id = null)
        {
            ViewBag.GroupId = new SelectList(dbProd.GroupOfCompanies.Where(c => c.IsActive == 1), "GroupId", "GroupName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1), "CompanyId", "ShortName");
            ViewBag.BankId = new SelectList(db.Banks.Where(c => c.Activity == ActivityType.Active && c.BankId == 0), "BankId", "BankName");
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.AccountId == 0), "AccountId", "AccountNo");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BranchId == 0), "BranchId", "BranchName");

            ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.Activity == ActivityType.Active && c.ChequeId == 0), "ChequeId", "ChequeNo");

           ViewBag.UrgencyType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("URGTYPE")), "TypeId", "TypeName", (string.IsNullOrEmpty(id) ? 0 : 7));

            ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName", (string.IsNullOrEmpty(id) ? 0 : 83));

            ViewBag.PaymentMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYTYPE") && c.TypeId != 9 && c.TypeId != 78), "TypeId", "TypeName", (string.IsNullOrEmpty(id) ? 0 : 8));
            
            var aclist = (from p in db.BankAccount.AsEnumerable()
                          join q in db.Banks.AsEnumerable() on p.BankId equals q.BankId
                          join r in dbProd.Companies.AsEnumerable() on p.CompanyId equals r.CompanyId
                          select new { AccountId = p.AccountId.ToString(), AccountNo = p.AccountNo + " >> " + q.ShortName + " >> " + r.ShortName }
                         ).Distinct().ToList().Union(db.ChequePayments.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssueTo, AccountNo = c.IssueTo + " >> " }).Distinct().ToList())
                         .Union(db.ChequeIssueInstructions.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssuedAccountName, AccountNo = c.IssuedAccountName + " >> " }).Distinct().ToList());

            
            ViewBag.AccountList = new SelectList(aclist.Distinct(), "AccountId", "AccountNo");

            ViewBag.ConfirmId = new SelectList(db.Database.SqlQuery<OIConfirmView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'CONFIRMLIST', 0").ToList(), "OiId", "OIConfirmNo");

            if (string.IsNullOrEmpty(id))
            {
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE")), "TypeId", "TypeName", 226);

                return View(new ChequeIssueInstruction());
            }
            else if (id.ToLower().Equals("instructionagainstbill"))
            {
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE") && c.TypeId != 227), "TypeId", "TypeName", 226);
                ViewBag.BillForwardId = new SelectList(new List<string>() { }.Select(c => new { CPBPlanNo = c, CPB_Plan_ID = c }).Distinct(), "CPBPlanNo", "CPB_Plan_ID");
                ViewBag.AccountList = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'BILLFORWARDDETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
                ViewBag.DocumentList = new SelectList(db.ReceivableDocuments.Where(c => c.DocumentId == 0), "DocumentId", "DocumentNo");

                return View("InstructionAgainstBill", new ChequeIssueInstruction());
            }
            else if (id.ToLower().Equals("instructionagainstsbill"))
            {
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE") && c.TypeId != 227), "TypeId", "TypeName", 226);
                ViewBag.SBillForwardId = new SelectList(new List<string>() { }.Select(c => new { CPBPlanNo = c, CPB_Plan_ID = c }).Distinct(), "CPBPlanNo", "CPB_Plan_ID");
                ViewBag.AccountList = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'SBILLFORWARDDETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
                ViewBag.DocumentList = new SelectList(db.ReceivableDocuments.Where(c => c.DocumentId == 0), "DocumentId", "DocumentNo");

                return View("InstructionAgainstSBill", new ChequeIssueInstruction());
            }
            else if (id.ToLower().Equals("instructionagainstpo"))
            {
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE")), "TypeId", "TypeName", 227);
                ViewBag.PaymentAgainst = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("ADVANCEFOR")), "TypeId", "TypeName", 222);
                ViewBag.WorkOrderId = new SelectList(new List<string>() { }.Select(c => new { CPBPlanNo = c, CPB_Plan_ID = c }).Distinct(), "CPBPlanNo", "CPB_Plan_ID");

                var ac_list = db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'PODETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour.ToUpper() }).Distinct();
                //ac_list.Union(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'WOBDETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour.ToUpper() }).Distinct());

                ViewBag.AccountList = new SelectList(ac_list.Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");

                //ViewBag.PaymentList = new SelectList((from p in db.ChequeIssueInstructions.AsEnumerable()
                //                                     join q in db.ChequePayments.AsEnumerable() on p.InstructionId equals q.InstructionId
                //                                     select p
                //                                    ).Distinct().ToList().Take(10), "InstructionId", "InstructionNo");

                return View("InstructionAgainstPO", new ChequeIssueInstruction());
            }
            else if (id == "instructionagainstoi")
            {
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE")), "TypeId", "TypeName", 226);

                return View("InstructionAgainstOI", new ChequeIssueInstruction());
            }
            else
            {

                var companyIds = new List<int> { 1, 2, 3 };

                /*  var query = from company in dbContext.Company
                              where companyIds.Contains(company.CompanyId)
                              select company;*/

                var companyList = dbProd.Companies.Where(x => companyIds.Contains(x.CompanyId)).ToList();

                ViewBag.Company = new SelectList(companyList, "CompanyId", "CompanyName");
                ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE")), "TypeId", "TypeName", 226);

                var list = new List<SelectListItem>
                           {
                              new SelectListItem{ Text="Mr.Taslim Meah", Value = "2000" },
                              new SelectListItem{ Text="Mr.Pronoy Kanti Barua", Value = "20001" },
                              new SelectListItem{ Text="Option 3", Value = "3", Selected = true },
                            };

                ViewBag.AccountList = list;

                var PayForlist = new List<SelectListItem>()
                {
                  new SelectListItem { Text="TDS", Value="1" },
                  new SelectListItem { Text="VDS", Value="2" }
                };
                ViewBag.PayForlist = PayForlist;

                return View("InstructionAgainstTDSVDS", new ChequeIssueInstructionTDSVDSVM());
            }
        }

        // POST: /ChequeInstruction/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [CAuthorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult Save(ICollection<ChequeIssueInstruction> chequeInstructions, ICollection<ChequeIssueSub> chequeIssueSubs)
        {
            if(chequeInstructions == null || chequeInstructions.ToList().Count == 0)
                return Json(new { status = "Error", message = "No instruction available for save." }, JsonRequestBehavior.AllowGet);
            try
            {
                int max_instruction_id = db.ChequeIssueInstructions.AsEnumerable().Where(c => c.InstructionDate.Year == chequeInstructions.FirstOrDefault().InstructionDate.Year).Select(c => Convert.ToInt32((c.InstructionNo == null || c.InstructionNo.Length < 7 ? "INS0000/" : c.InstructionNo).Substring(3, 4)) + 1).DefaultIfEmpty(1).Max();
                //using (var scope = new TransactionScope(TransactionScopeOption.Required))
                //{
                //using (var db = new FundContext())
                //{
               
                    foreach (var chequeInstruction in chequeInstructions)
                    {
                        ChequeIssueInstruction instruction = null; 
                        using (var _scope = new TransactionScope(TransactionScopeOption.Suppress))
                        {
                            chequeInstruction.InstructionNo = "INS" + (max_instruction_id++).ToString("0000") + "/" + chequeInstruction.InstructionDate.ToString("yy");
                            instruction = new ChequeIssueInstruction(chequeInstruction);

                            if (instruction.IssuedAccountId > 0)
                            {
                                instruction.IssuedAccountName = instruction.IssuedAccount.CompanyAccount.CompanyName + " - " + instruction.IssuedAccount.AccountNo;
                            }

                            if (chequeIssueSubs != null)
                            {
                                foreach (var _sub in chequeIssueSubs)
                                {
                                    var sub = new ChequeIssueSub(_sub);

                                    if (sub.VAT + sub.Tax > 0)
                                    {
                                        var pl = new ChequeIssuePipeline(sub);
                                        sub.ChequeIssuePipelines.Add(pl);
                                    }

                                    instruction.ChequeIssueSubs.Add(sub);
                                }
                            }
                        }
                        db.ChequeIssueInstructions.Add(instruction);

                        db.SaveChanges();
                        //}

                        using (var _dbMail = new Models.DBFirst.EMailEntities())
                        {
                            var row = (from p in dbMail.EmailLibraries.AsEnumerable()
                                       join q in dbMail.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                       where p.EmailTypeId == 1
                                       select new
                                       {
                                           p.TypeName,
                                           p.EmailBody,
                                           q.EmailGroupFrom,
                                           q.EmailGroupTo,
                                           q.EmailGroupCC,
                                           q.EmailGroupBCC,
                                           p.Subject
                                       }).FirstOrDefault();

                            Models.DBFirst.EMailCollection mail = new Models.DBFirst.EMailCollection(new Models.DBFirst.EMailCollection());
                            mail.EmailType = row.TypeName;
                            mail.ModuleName = "FUND";
                            mail.EmailFrom = clsMain.getEmail(row.EmailGroupFrom ?? "Fund Management System <fms.cube@kdsgroup.net>");
                            mail.EmailTo = clsMain.getEmail(row.EmailGroupTo ?? "");
                            mail.EmailCC = clsMain.getEmail(row.EmailGroupCC ?? "");
                            mail.EmailBCC = clsMain.getEmail(row.EmailGroupBCC ?? "");
                            mail.EmailSubject = row.Subject.Replace("@InstructionNo", instruction.InstructionNo);
                            mail.EmailBody = row.EmailBody.Replace("@InstructionNo", instruction.InstructionNo)
                                .Replace("@BeneficiaryName", instruction.IssuedAccountName)
                                .Replace("@Amount", instruction.ChequeAmount.ToString("##,##0.00"))
                                .Replace("@ApprovalLink", @Url.Action("Approval", "ChequeInstruction", new { id = instruction.InstructionId }, Request.Url.Scheme));
                            mail.EmailAttachmentLink = "";
                            mail.CreateAttachment = 0;
                            mail.CreatedBy = clsMain.getCurrentUser();

                            dbMail.EMailCollections.Add(mail);

                            dbMail.SaveChanges();
                        }
                    }

                    // Commit tx-scope
                //    scope.Complete();
                //}

                //return View(pimain);
                return Json(new { status = "Success", ino = chequeInstructions.FirstOrDefault().InstructionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        [CAuthorize]
        [HttpPost]
        //[ValidateAntiForgeryToken]
        public ActionResult SaveTDSVDS(ICollection<ChequeIssueInstruction> chequeInstructions, ICollection<ChequeIssueTDSVDS> chequeIssueSubs)
        {
            if (chequeInstructions == null || chequeInstructions.ToList().Count == 0)
                return Json(new { status = "Error", message = "No instruction available for save." }, JsonRequestBehavior.AllowGet);
            try
            {
                int max_instruction_id = db.ChequeIssueInstructions.AsEnumerable().Where(c => c.InstructionDate.Year == chequeInstructions.FirstOrDefault().InstructionDate.Year).Select(c => Convert.ToInt32((c.InstructionNo == null || c.InstructionNo.Length < 7 ? "INS0000/" : c.InstructionNo).Substring(3, 4)) + 1).DefaultIfEmpty(1).Max();
                
                foreach (var chequeInstruction in chequeInstructions)
                {
                    ChequeIssueInstruction instruction = null;
                    using (var _scope = new TransactionScope(TransactionScopeOption.Suppress))
                    {
                        chequeInstruction.InstructionNo = "INS" + (max_instruction_id++).ToString("0000") + "/" + chequeInstruction.InstructionDate.ToString("yy");
                        instruction = new ChequeIssueInstruction(chequeInstruction);

                        if (instruction.IssuedAccountId > 0)
                        {
                            instruction.IssuedAccountName = instruction.IssuedAccount.CompanyAccount.CompanyName + " - " + instruction.IssuedAccount.AccountNo;
                        }
 
                    }
                    db.ChequeIssueInstructions.Add(instruction);

                    db.SaveChanges();

                    foreach (var ChequeIssueTDSVDS  in chequeIssueSubs)
                    {
                        ChequeIssueTDSVDS.ParentInstructionId = instruction.InstructionId;
                        db.ChequeIssueTDSVDS.Add(ChequeIssueTDSVDS);
                    }

                    db.SaveChanges();
                    //}

                    using (var _dbMail = new Models.DBFirst.EMailEntities())
                    {
                        /*var row = (from p in dbMail.EmailLibraries.AsEnumerable()
                                   join q in dbMail.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                   where p.EmailTypeId == 1
                                   select new
                                   {
                                       p.TypeName,
                                       p.EmailBody,
                                       q.EmailGroupFrom,
                                       q.EmailGroupTo,
                                       q.EmailGroupCC,
                                       q.EmailGroupBCC,
                                       p.Subject
                                   }).FirstOrDefault();

                        Models.DBFirst.EMailCollection mail = new Models.DBFirst.EMailCollection(new Models.DBFirst.EMailCollection());
                        mail.EmailType = row.TypeName;
                        mail.ModuleName = "FUND";
                        mail.EmailFrom = clsMain.getEmail(row.EmailGroupFrom ?? "Fund Management System <fms.cube@kdsgroup.net>");
                        mail.EmailTo = clsMain.getEmail(row.EmailGroupTo ?? "");
                        mail.EmailCC = clsMain.getEmail(row.EmailGroupCC ?? "");
                        mail.EmailBCC = clsMain.getEmail(row.EmailGroupBCC ?? "");
                        mail.EmailSubject = row.Subject.Replace("@InstructionNo", instruction.InstructionNo);
                        mail.EmailBody = row.EmailBody.Replace("@InstructionNo", instruction.InstructionNo)
                            .Replace("@BeneficiaryName", instruction.IssuedAccountName)
                            .Replace("@Amount", instruction.ChequeAmount.ToString("##,##0.00"))
                            .Replace("@ApprovalLink", @Url.Action("Approval", "ChequeInstruction", new { id = instruction.InstructionId }, Request.Url.Scheme));
                        mail.EmailAttachmentLink = "";
                        mail.CreateAttachment = 0;
                        mail.CreatedBy = clsMain.getCurrentUser();

                        dbMail.EMailCollections.Add(mail);

                        dbMail.SaveChanges();*/
                    }
                }

                // Commit tx-scope
                //    scope.Complete();
                //}

                //return View(pimain);
                return Json(new { status = "Success", ino = chequeInstructions.FirstOrDefault().InstructionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }


        // GET: /ChequePayment/
        [CAuthorize]
        public ActionResult ApprovalPending()
        {
            var chequeissueinstructions = db.ChequeIssueInstructions.Include(c => c.UrgencyType).Where(c => c.ApprovedType == 0);
            return View(chequeissueinstructions.ToList());
        }

        // GET: /ChequeInstruction/Approval/5
        [CAuthorize]
        public ActionResult Approval(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeIssueInstruction instruction = db.ChequeIssueInstructions.Find(id);
            if (instruction == null)
            {
                return HttpNotFound();
            }

            ViewBag.CompanyId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1), "CompanyId", "ShortName", instruction.PaymentFrom.CompanyId);
            ViewBag.BankId = new SelectList(db.BankAccount.Include(c => c.Bank).Where(c => c.Activity == ActivityType.Active && c.CompanyId == instruction.PaymentFrom.CompanyId).Select(c => new { BankId = c.BankId, BankName = c.Bank.BankName }).Distinct(), "BankId", "BankName", instruction.PaymentFrom.BankId);
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == instruction.PaymentFrom.BranchId && c.CompanyId == instruction.PaymentFrom.CompanyId), "AccountId", "AccountNo", instruction.BankAccountId);
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BankId == instruction.PaymentFrom.BankId), "BranchId", "BranchName", instruction.PaymentFrom.BranchId);
            ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.Activity == ActivityType.Active && c.AccountId == instruction.BankAccountId && c.Status == 1 && c.PaymentModeId == instruction.PaymentModeId), "ChequeId", "ChequeNo");
            ViewBag.UrgencyType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("URGTYPE")), "TypeId", "TypeName");
            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");
            ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName", instruction.ChequeMode.TypeId);
            ViewBag.PaymentMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYTYPE") && c.TypeId != 9 && c.TypeId != 78).OrderBy(c => c.RankId), "TypeId", "TypeName");

            short pay_type_id = 226;

            if (instruction.PaymentTypeId == 0)
            {
                if (instruction.OIBillConfirmId > 0)
                    pay_type_id = 226;
                else if (instruction.ChequeIssueSubs.Where(c => c.LPBillForwardId > 0).Count() > 0)
                    if (instruction.ChequeAmount > 0)
                        pay_type_id = 226;
                    else
                        pay_type_id = 228;
                else if (instruction.ChequeIssueSubs.Where(c => c.WorkOrderId > 0).Count() > 0)
                    pay_type_id = 227;
            }
            else
            {
                pay_type_id = instruction.PaymentTypeId;
            }

            ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYMODE")), "TypeId", "TypeName", pay_type_id);
            
            var aclist = (from p in db.BankAccount.AsEnumerable()
                          join q in db.Banks.AsEnumerable() on p.BankId equals q.BankId
                          join r in dbProd.Companies.AsEnumerable() on p.CompanyId equals r.CompanyId
                          select new { AccountId = p.AccountId.ToString(), AccountNo = p.AccountNo + " >> " + q.ShortName + " >> " + r.ShortName }
                         ).Distinct().ToList().Union(db.ChequePayments.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssueTo, AccountNo = c.IssueTo + " >> " }).Distinct().ToList())
                         .Union(db.ChequeIssueInstructions.Where(c => c.IssuedAccountId == 0).Select(c => new { AccountId = "0>>" + c.IssuedAccountName, AccountNo = c.IssuedAccountName + " >> " }).Distinct().ToList()); ;
            ViewBag.IssuedAccountId = new SelectList(aclist.Distinct(), "AccountId", "AccountNo", instruction.IssuedAccountId.ToString() + (instruction.IssuedAccountId == 0 ? ">>" + instruction.IssuedAccountName : ""));


            return View(instruction);
        }

        // GET: /ChequeInstruction/Details/5
        [HttpPost]
        [CAuthorize]
        public ActionResult Approval(ChequeIssueInstruction instruction)
        {
            try
            {
                if (instruction == null)
                {
                    return Json(new { status = "Error", message = "No instruction has sent for approval" }, JsonRequestBehavior.AllowGet);
                }

                TextInfo _characterCasing = new CultureInfo("en-US", false).TextInfo;
                ChequeIssueInstruction Instruction = db.ChequeIssueInstructions.Find(instruction.InstructionId);
                var user = clsMain.getCurrentUserInfo();
                
                //if (Instruction == null)
                //{
                //    return Json(new { status = "Error", message = "Invalid instruction has sent for approval" }, JsonRequestBehavior.AllowGet);
                //}

                var WorkOrderDetail = new BillForwardView();

                if (Instruction.InstructionTypeId == 212)
                {
                    if (Instruction.PaymentAgainstId == 222)
                    {
                        var WorkOrderId = Instruction.ChequeIssueSubs.First().WorkOrderId;
                        WorkOrderDetail = db.Database.SqlQuery<BillForwardView>("Select A.PO_Plan_ID As CPB_Plan_ID, Cast(Sum(B.PO_Total_Price) As Numeric(24, 2)) As CPB_AdjustAmount From PO_New.dbo.PO_Main As A Inner Join PO_New.dbo.PO_Main_Details As B On B.POPlanNo = A.POPlanNo Where A.POPlanNo = " + WorkOrderId + " Group By A.PO_Plan_ID").FirstOrDefault();
                    } 
                    else if (Instruction.PaymentAgainstId == 230)
                    {
                        var WorkOrderId = Instruction.ChequeIssueSubs.First().ServiceWorkOrderId;
                        WorkOrderDetail = db.Database.SqlQuery<BillForwardView>("Select A.Service_Plan_ID As CPB_Plan_ID, Cast(Sum(A.PO_Total_Price) As Numeric(24, 2)) As CPB_AdjustAmount From PO_New.dbo.PO_Service_Main As A Where A.ServicePlanNo = " + WorkOrderId + " Group By A.Service_Plan_ID").FirstOrDefault();
                    }
                }

                //using (var scope = new TransactionScope(TransactionScopeOption.Required))
                //{
                    //using (var _db = new FundContext())
                    //{
                        Instruction.ApprovedType = instruction.ApprovedType;

                        if (instruction.ApprovedType == 2) 
                            Instruction.DisapprovedReason = instruction.DisapprovedReason;

                        Instruction.ApprovedBy = clsMain.getCurrentUser();
                        Instruction.ApprovedTime = clsMain.getCurrentTime();
                        Instruction.Purpose = instruction.Purpose;

                        if (instruction.ApprovedType == 1)
                        {
                            Instruction.IssuedAccountId = instruction.IssuedAccountId;
                            Instruction.BankAccountId = instruction.BankAccountId;
                            Instruction.ChequeDate = instruction.ChequeDate;
                            Instruction.PaymentModeId = instruction.PaymentModeId;
                            Instruction.ChequeModeId = instruction.ChequeModeId;
                            Instruction.UrgencyTypeId = instruction.UrgencyTypeId;
                            Instruction.ChequeAmount = instruction.ChequeAmount;

                            if (instruction.IssuedAccountId > 0)
                            {
                                Instruction.IssuedAccountName = instruction.IssuedAccount.CompanyAccount.CompanyName + " - " + instruction.IssuedAccount.AccountNo;
                            }
                            else
                            {
                                Instruction.IssuedAccountName = instruction.IssuedAccountName;
                            }
                        }

                        db.Entry(Instruction).State = EntityState.Modified;

                        db.SaveChanges();
                    //}

                    //if (Instruction.InstructionTypeId == 212)
                    //{
                    //    var total_amt = Instruction.ChequeIssueSubs.First().BillAmount;
                    //    var vat_tax = Instruction.ChequeIssueSubs.First().ChequeIssuePipelines.FirstOrDefault();
                    //    if (vat_tax != null)
                    //    {
                    //        total_amt += vat_tax.Tax + vat_tax.VAT;
                    //    }
                    //    db.Database.ExecuteSqlCommand(string.Format("Update PO_New.dbo.PO_Main Set POAdvanceAmount = {1} Where POPlanNo = {0}", WorkOrderDetail.WorkOrderId, total_amt));
                    //}

                    var PaymentAgainst = db.CustomTypes.Find(Instruction.PaymentAgainstId);

                    using (var _dbMail = new Models.DBFirst.EMailEntities())
                    {
                        var row = (from p in _dbMail.EmailLibraries.AsEnumerable()
                                   join q in _dbMail.EmailFrameworks.AsEnumerable() on p.EmailTypeId equals q.EmailTypeId
                                   where p.EmailTypeId == 2
                                   select new
                                   {
                                       p.TypeName,
                                       p.EmailBody,
                                       q.EmailGroupFrom,
                                       q.EmailGroupTo,
                                       q.EmailGroupCC,
                                       q.EmailGroupBCC,
                                       p.Subject
                                   }).FirstOrDefault();

                        Models.DBFirst.EMailCollection mail = new Models.DBFirst.EMailCollection(new Models.DBFirst.EMailCollection());
                        mail.EmailType = row.TypeName;
                        mail.ModuleName = "FUND";
                        mail.EmailFrom = _characterCasing.ToTitleCase(user.UserTitle.ToLower()) + "<" + clsMain.getCurrentEmail() + ">" ?? "Syed Mahmudul Hasan Finance & Accounts <mahmudul.hasan@kdsgroup.net>";
                        mail.EmailTo = clsMain.getEmail(row.EmailGroupTo ?? "");
                        mail.EmailCC = clsMain.getEmail(row.EmailGroupCC ?? "") + (Instruction.PaymentFrom.CompanyId == 3 ? "; feroj.ahmed@kdsgroup.net" : Instruction.PaymentFrom.CompanyId == 2 ? "; tauquir.ahmed@kdsgroup.net" : "");
                        mail.EmailBCC = clsMain.getEmail(row.EmailGroupBCC ?? "");
                        mail.EmailSubject = row.Subject.Replace("@InstructionNo", Instruction.InstructionNo);
                        mail.EmailBody = row.EmailBody.Replace("@InstructionNo", Instruction.InstructionNo)
                            .Replace("@ApprovalText", (Instruction.ApprovedType == 1 ? "Below is the approved instruction of making payment.<br />Please arrange to pay as per below instruction." : "Below instruction has been disapproved due to <span style=\"color:red; font-weight:bold;\">" + Instruction.DisapprovedReason + "</span>"))
                            .Replace("@IssuerName", Instruction.PaymentFrom.Bank.BankName + " - " + Instruction.PaymentFrom.AccountNo)
                            .Replace("@BeneficiaryName", Instruction.IssuedAccountName)
                            .Replace("@Amount", Instruction.ChequeAmount.ToString("##,##0.00"))
                            .Replace("@AdjustAmount", Instruction.AdjustAmount.ToString("##,##0.00"))
                            .Replace("@TotalAmount", Instruction.TotalAmount.ToString("##,##0.00"))
                            .Replace("@RefNo", Instruction.ReferenceNo)
                            //.Replace("@PaymentType", Instruction.PaymentTypeId)
                            .Replace("@PaymentType", Instruction.PaymentMode.TypeName + (Instruction.PaymentModeId == 8 ? " (" + Instruction.ChequeMode.TypeName + ")" : ""))
                            .Replace("@PaymentAgainst", Instruction.InstructionTypeId == 212 ? PaymentAgainst.TypeName : Instruction.InstructionType.TypeName)
                            .Replace("@TypeWiseData", (Instruction.InstructionTypeId == 212 ? String.Format("<TR><TD ><b>PO/WO No:</b> {0} <br /></TD></TR><TR><TD ><b>PO/WO Value:</b> {1} <br /></TD></TR>", WorkOrderDetail.CPB_Plan_ID, WorkOrderDetail.CPB_AdjustAmount.ToString("##,##0.00")) : ""))
                            .Replace("@Purpose", Instruction.Purpose)
                            .Replace("@LinkText", (Instruction.ApprovedType == 1 ? String.Format("Follow the below link to pay: <a href=\"{0}\">Click here to pay</a>", @Url.Action("PaymentByInstruction", "ChequePayment", new { id = Instruction.InstructionId }, Request.Url.Scheme)) : ""));
                        mail.EmailAttachmentLink = "";
                        mail.CreateAttachment = 0;
                        mail.CreatedBy = clsMain.getCurrentUser();

                        //Models.DBFirst.EMailCollection email = new Models.DBFirst.EMailCollection(mail);
                        dbMail.EMailCollections.Add(mail);

                        dbMail.SaveChanges();
                    }

                //    // Commit tx-scope
                //    scope.Complete();
                //}

                return Json(new { status = "Success", ino = Instruction.InstructionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /ChequeInstruction/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeIssueInstruction chequeIssueInstruction = db.ChequeIssueInstructions.Find(id);
            if (chequeIssueInstruction == null)
            {
                return HttpNotFound();
            }
            //ViewBag.ChequeId = new SelectList(db.ChequeInfos, "ChequeId", "ChequeNo", chequeIssueInstruction.ChequeId);
            ViewBag.IssuedAccountId = new SelectList(db.BankAccount, "AccountId", "AccountCode", chequeIssueInstruction.IssuedAccountId);
            ViewBag.UrgencyTypeId = new SelectList(db.CustomTypes, "TypeId", "TypeName", chequeIssueInstruction.UrgencyTypeId);
            return View(chequeIssueInstruction);
        }

        // POST: /ChequeInstruction/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="InstructionId,ChequeId,InstructionDate,ChequeDate,IssuedAccountId,ChequeAmount,Purpose,UrgencyTypeId,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] ChequeIssueInstruction chequeIssueInstruction)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chequeIssueInstruction).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            //ViewBag.ChequeId = new SelectList(db.ChequeInfos, "ChequeId", "ChequeNo", chequeIssueInstruction.ChequeId);
            ViewBag.IssuedAccountId = new SelectList(db.BankAccount, "AccountId", "AccountCode", chequeIssueInstruction.IssuedAccountId);
            ViewBag.UrgencyTypeId = new SelectList(db.CustomTypes, "TypeId", "TypeName", chequeIssueInstruction.UrgencyTypeId);
            return View(chequeIssueInstruction);
        }

        // GET: /ChequeInstruction/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeIssueInstruction chequeIssueInstruction = db.ChequeIssueInstructions.Find(id);
            if (chequeIssueInstruction == null)
            {
                return HttpNotFound();
            }
            return View(chequeIssueInstruction);
        }

        // POST: /ChequeInstruction/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChequeIssueInstruction chequeIssueInstruction = db.ChequeIssueInstructions.Find(id);
            db.ChequeIssueInstructions.Remove(chequeIssueInstruction);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult ChequeList(int id, short? type)
        {
            if (type == null)
                ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.Activity == ActivityType.Active && c.AccountId == id && c.Status == 1), "ChequeId", "ChequeNo");
            else
                ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.Activity == ActivityType.Active && c.AccountId == id && c.Status == type), "ChequeId", "ChequeNo");

            return PartialView();
        }

        [HttpPost]
        public ActionResult BankList(int? id, short groupid = 0)
        {
            if (id == null)
            {
                var comlist = dbProd.Companies.AsNoTracking().Where(c => c.GroupId == groupid).Select(c => c.CompanyId).ToList();
                ViewBag.BankList = new SelectList(db.BankAccount.Include(c => c.Bank).Where(c => c.Activity == ActivityType.Active && comlist.Contains(c.CompanyId)).Select(c => new { BankId = c.BankId, BankName = c.Bank.BankName }).Distinct(), "BankId", "BankName");
            }
            else
            {
                ViewBag.BankList = new SelectList(db.BankAccount.Include(c => c.Bank).Where(c => c.Activity == ActivityType.Active && c.CompanyId == id).Select(c => new { BankId = c.BankId, BankName = c.Bank.BankName }).Distinct(), "BankId", "BankName");
            }

            return PartialView();
        }

        [HttpPost]
        public ActionResult BAccountList(int id, short comid = 0, short groupid = 0)
        {
            if (comid == 0)
            {
                var comlist = dbProd.Companies.AsNoTracking().Where(c => c.GroupId == groupid).Select(c => c.CompanyId).ToList();
                var ba = db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == id && comlist.Contains(c.CompanyId)).Select(c => c.AccountId).ToList();
                ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == id && comlist.Contains(c.CompanyId)), "AccountId", "AccountNo");
            }
            else
            {
                ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == id && c.CompanyId == comid), "AccountId", "AccountNo");
            }

            return PartialView();
        }

        [HttpPost]
        public ActionResult BForwardList(string id)
        {
            ViewBag.BillForwardId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'BILLFORWARDDETAILS', 0, 0, '" + id + "'").ToList().Select(c => new { CPB_Plan_ID = c.CPB_Plan_ID, CPBPlanNo = c.CPBPlanNo }).Distinct().OrderBy(c => c.CPB_Plan_ID), "CPBPlanNo", "CPB_Plan_ID");

            return PartialView();
        }

        [HttpPost]
        public ActionResult SBForwardList(string id)
        {
            ViewBag.SBillForwardId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'SBILLFORWARDDETAILS', 0, 0, '" + id + "'").ToList().Select(c => new { CPB_Plan_ID = c.CPB_Plan_ID, CPBPlanNo = c.CPBPlanNo }).Distinct().OrderBy(c => c.CPB_Plan_ID), "CPBPlanNo", "CPB_Plan_ID");

            return PartialView();
        }

        [HttpPost]
        public ActionResult IssueAccountList(short id)
        {
            if (id == 222)
                ViewBag.IssuedAccountId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'PODETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
            else if (id == 224 || id == 225)
                ViewBag.IssuedAccountId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec PO_NEW.dbo.prcSupplierCS").ToList().Select(c => new { SupplierId = c.Supp_ID, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Supp_Name.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
            else if (id == 230)
                ViewBag.IssuedAccountId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'SWODETAILS', 0, 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = /*"[" + c.SupplierId + "] " + */c.Payment_Favour }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
            
            return PartialView();
        }

        [HttpPost]
        public ActionResult WorkOrderList(string id, short type_id = 222)
        {
            if (type_id == 222)
                ViewBag.WorkOrderId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'PODETAILS', 0, 0, '" + id + "'").ToList().Select(c => new { WorkOrderId = c.WorkOrderId, CPB_Plan_ID = c.CPB_Plan_ID }).Distinct().OrderBy(c => c.CPB_Plan_ID), "WorkOrderId", "CPB_Plan_ID");
            else if (type_id == 224)
            {
                ViewBag.WorkOrderId = new SelectList(db.ReceivableDocuments.Where(c => c.PayableId.ToString() == id).Select(c => new { WorkOrderId = c.DocumentId, CPB_Plan_ID = c.DocumentNo }).Distinct().OrderBy(c => c.CPB_Plan_ID), "WorkOrderId", "CPB_Plan_ID");
            }
            else if (type_id == 225)
            {
                ViewBag.WorkOrderId = new SelectList(db.ReceivableDocuments.Where(c => c.PayableId.ToString() == id).Select(c => new { WorkOrderId = c.DocumentId, CPB_Plan_ID = c.DocumentNo }).Distinct().OrderBy(c => c.CPB_Plan_ID), "WorkOrderId", "CPB_Plan_ID");
            }
            else if (type_id == 230)
                ViewBag.WorkOrderId = new SelectList(db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'SWODETAILS', 0, 0, '" + id + "'").ToList().Select(c => new { WorkOrderId = c.WorkOrderId, CPB_Plan_ID = c.CPB_Plan_ID }).Distinct().OrderBy(c => c.CPB_Plan_ID), "WorkOrderId", "CPB_Plan_ID");
            

            return PartialView();
        }

        [HttpPost]
        public ActionResult DocumentList(int id)
        {
            var doc_list = (from p in db.ReceivableDocuments.AsEnumerable()
                            join q in db.ChequeIssueInstructions.GroupBy(c => c.DocumentId).Select(c => new { DocumentId = c.Key, BillAmount = c.Sum(l => l.ChequeAmount), AdjustAmount = c.Sum(l => l.AdjustAmount) }).AsEnumerable() on p.DocumentId equals q.DocumentId into r
                            from s in r.DefaultIfEmpty()
                            join t in db.ChequeIssueSubs.Where(c => c.ChequeIssueInstruction.PaymentAgainstId == 224).GroupBy(c => c.WorkOrderId).Select(c => new { DocumentId = c.Key, AdvanceAmount = c.Sum(l => l.BillAmount) }).AsEnumerable() on p.DocumentId equals t.DocumentId into u
                            from v in u.DefaultIfEmpty()
                            where p.PayableId == id && (p.PayableAmount - (s == null ? 0 : s.BillAmount) - (v == null ? 0 : v.AdvanceAmount) > 0 || (v == null ? 0 : v.AdvanceAmount) - (s == null ? 0 : s.AdjustAmount) > 0)
                            select p
                           ).Distinct().ToList();
            ViewBag.DocumentId = new SelectList(doc_list, "DocumentId", "DocumentNo");
            
            return PartialView();
        }

        [HttpPost]
        public ActionResult DocumentDetails(int id)
        {
            var doc_info = (from p in db.ReceivableDocuments.AsEnumerable()
                            join q in db.ChequeIssueInstructions.GroupBy(c => c.DocumentId).Select(c => new { DocumentId = c.Key, BillAmount = c.Sum(l => l.ChequeAmount), AdjustAmount = c.Sum(l => l.AdjustAmount) }).AsEnumerable() on p.DocumentId equals q.DocumentId into r
                            from s in r.DefaultIfEmpty()
                            join t in db.ChequeIssueSubs.Where(c => c.ChequeIssueInstruction.PaymentAgainstId == 224).GroupBy(c => c.WorkOrderId).Select(c => new { DocumentId = c.Key, AdvanceAmount = c.Sum(l => l.BillAmount) }).AsEnumerable() on p.DocumentId equals t.DocumentId into u
                            from v in u.DefaultIfEmpty()
                            where p.DocumentId == id //&& p.PayableAmount - (s == null ? 0 : s.BilAmount) > 0
                            select new { ProjectAmount = p.PayableAmount, BillBalance = p.PayableAmount - (s == null ? 0 : s.BillAmount) - (v == null ? 0 : v.AdvanceAmount), AdvanceBalance = (v == null ? 0 : v.AdvanceAmount) - (s == null ? 0 : s.AdjustAmount) }
                           ).Distinct().FirstOrDefault();

            return Json(new { project_amount = doc_info.ProjectAmount.ToString("##,##0.00"), bill_balance = doc_info.BillBalance.ToString("##,##0.00"), adjust_balance = doc_info.AdvanceBalance.ToString("##,##0.00") }, JsonRequestBehavior.AllowGet);
        }

        // GET: /Bank/RowTemplate
        //[HttpPost]
        public ActionResult RowTemplate(string id, string type = "OI", short type_id = 222)
        {
            if (type.ToUpper().Equals("BILL"))
            {
                var instruction_list = db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'BILLFORWARDDETAILS', 0, " + id).ToList();

                return PartialView("RowTemplate_BILL", instruction_list);
            }
            else if (type.ToUpper().Equals("SBILL"))
            {
                var instruction_list = db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'SBILLFORWARDDETAILS', 0, " + id).ToList();

                return PartialView("RowTemplate_SBILL", instruction_list);
            }
            else if (type.ToUpper().Equals("PO"))
            {
                var _type = "PODETAILS";
                if (type_id == 224)
                    _type = "WOBDETAILS";
                else if (type_id == 230)
                    _type = "SWODETAILS";

                var instruction_list = db.Database.SqlQuery<BillForwardView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, " + _type + ", 0, " + id).ToList();

                return PartialView("RowTemplate_PO", instruction_list);
            }
            else
            {
                var instruction_list = db.Database.SqlQuery<OIConfirmView>("Exec dbo.prcgetPendingListOfOIChequePayment 0, 0, 0, 'CONFIRMDETAILS', " + id).ToList();               
                return PartialView(instruction_list);
            }
        }


        /*     url="@Url.Action("RowTemplateTDSVDS")",
                             data: ({ FromDate: FromDate, ToDate: ToDate, Flag: Flag
         }),*/


        public ActionResult RowTemplateTDSVDS(DateTime? fromdate, DateTime? todate, int flag, int companyid)
        {
            var instruction_list = db.Database.SqlQuery<PendingTDSVDS>("Exec dbo.prcgetPendingListOfTaxVat '"+ fromdate + "' ,'"+ todate + "' ,'"+ flag + "','"+companyid+"' ").ToList();
            return PartialView("RowTemplateTDSVDS", instruction_list);

        }

    }
}
