﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public class BudgetController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Budget/
        public ActionResult Index()
        {
            return View(db.ImportBudgets.ToList());
        }

        // GET: /Budget/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImportBudget importBudget = db.ImportBudgets.Find(id);
            if (importBudget == null)
            {
                return HttpNotFound();
            }
            return View(importBudget);
        }

        // GET: /Budget/Create
        [CAuthorize]
        public ActionResult Manage(string id = "amount")
        {
            short index = 1;
            short current_year = Convert.ToInt16(DateTime.Today.Year);

            var budget = new BudgetEntryView(id, current_year);

            var year = new List<clsMonthList>();
            while (true)
            {
                year.Add(new clsMonthList()
                {
                    MonthId = index++,
                    MonthName = current_year.ToString()
                });

                current_year -= 1;
                if (current_year <= 2016)
                    break;
            }

            //var year = new[] { new { TypeName = "2020", TypeId = 1 }, new { TypeName = "2020", TypeId = 1 }, new { TypeName = "2019", TypeId = 2 }, new { TypeName = "2018", TypeId = 3 }, new { TypeName = "2017", TypeId = 4 } };
            ViewBag.YearList = new SelectList(year, "MonthName", "MonthName");
            ViewBag.MonthList = new SelectList(dbProd.CustomTypes.Where(l => l.Flag.Equals("MONTHNAME")), "RankId", "TypeName");
            ViewBag.FactoryList = new SelectList(dbProd.Factories, "FactoryId", "ShortName");
            ViewBag.QuantityUnit = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("QUNIT")), "TypeId", "TypeName", budget.QuantityUnitId);
            
            return View("Create", budget);
        }

        // POST: /Budget/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(ICollection<BudgetView> importBudget)
        {
            try
            {
                if (importBudget !=null)
                {
                    foreach (var budget in importBudget)
                    {
                        var pbudget = db.ImportBudgets.Where(c => c.BudgetYear == budget.BudgetYear && c.BudgetMonth == budget.BudgetMonth && c.FactoryId == budget.FactoryId).FirstOrDefault();
                        if (pbudget == null)
                        {
                            db.ImportBudgets.Add(new ImportBudget(budget));
                        }
                        else
                        {
                            pbudget = (ImportBudget)budget.ConvertNotNull(pbudget);
                            db.Entry(pbudget).State = EntityState.Modified;
                        }
                    }

                    db.SaveChanges();
                }

                return Json(new { status = "Success", year = importBudget.FirstOrDefault().BudgetYear }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Budget/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImportBudget importBudget = db.ImportBudgets.Find(id);
            if (importBudget == null)
            {
                return HttpNotFound();
            }
            return View(importBudget);
        }

        // GET: /Budget/Delete/5
        [CAuthorize]
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ImportBudget importBudget = db.ImportBudgets.Find(id);
            if (importBudget == null)
            {
                return HttpNotFound();
            }
            return View(importBudget);
        }

        // POST: /Budget/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(short id)
        {
            ImportBudget importBudget = db.ImportBudgets.Find(id);
            db.ImportBudgets.Remove(importBudget);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: /Budget/Create
        [CAuthorize]
        public ActionResult BudgetDetails(string id = "amount", short year = 2020, short qunit = 41)
        {
            var budget = new BudgetEntryView(id, year, qunit);

            return PartialView(budget);
        }
    }
}
