﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public  class ChequeReceiveController : ContextList
    {
        // GET: ChequeReceive
        public ActionResult Index()
        {
            var ChequeRecivelst = db.ChequeReceives.Select(x => x.ChequeId).ToList();

            var chequereceive = db.ChequePayments.Include(x => x.ChequeInfo).Include(x => x.ChequeInfo.BankAccount).Include(x => x.ChequeInfo.Bank).Include(x => x.ChequeInfo.BankBranch).Where(x => ChequeRecivelst.Contains(x.ChequeId)).Select(x => new ChequeReceiveViewModel
            {
                AccountId = x.IssuedAccountId,
                AccountNo = x.ChequeInfo.BankAccount.AccountNo,
                BankId = x.ChequeInfo.Bank.BankId,
                BankName = x.ChequeInfo.Bank.BankName,
                BranchId = x.ChequeInfo.BankBranch.BranchId,
                BankBranchName = x.ChequeInfo.BankBranch.BranchName,
                ChequeId = x.ChequeId,
                ChequeNo = x.ChequeInfo.ChequeNo,
                ChequeDate = x.ChequeDate,
                IssueDate = x.IssueDate,
                IssueTo = x.IssueTo,
                ChequeAmount = x.ChequeAmount,
                Purpose = x.Purpose,
                ReceiveDate = db.ChequeReceives.Where(m => m.ChequeId == x.ChequeId).Select(m => m.ReceiveDate).FirstOrDefault()


            }).ToList().OrderBy(x=>x.ChequeNo).ThenBy(x=>x.ChequeDate).OrderBy(x=>x.IssueDate);

            return View(chequereceive);
        }

        public ActionResult PendingChequeReceive()
        {

            var ChequeRecivelst = db.ChequeReceives.Select(x => x.ChequeId).ToList();

            var Representative = (from a in db.ChequePayments
                                 join
                                 b in db.ChequeIssueSubs on
                                 a.InstructionId equals b.InstructionId
                                 where b.WorkOrderId != 0 || b.ServiceBillForwardId != 0
                                 || b.LPBillForwardId != 0 || b.ServiceWorkOrderId != 0
                                 select new
                                 {
                                     Id = 0,
                                     Name = a.IssueTo

                                 }).ToList().Distinct();
            ViewBag.Representative = new SelectList(Representative, "Id", "Name");

            return View();
        
        }

        public ActionResult RowTemplatePendingChequeReceive(DateTime? fromdate, DateTime? todate, string issueTo)
        {
            var ChequeRecivelst = db.ChequeReceives.Select(x => x.ChequeId).ToList();

            var PendingChequereceiveList = db.ChequePayments.Include(x => x.ChequeInfo).Include(x => x.ChequeInfo.BankAccount).Include(x => x.ChequeInfo.Bank).Include(x => x.ChequeInfo.BankBranch).Select(x => new ChequeReceiveViewModel
            {
                AccountId = x.IssuedAccountId,
                AccountNo = x.ChequeInfo.BankAccount.AccountNo,
                BankId = x.ChequeInfo.Bank.BankId,
                BankName = x.ChequeInfo.Bank.BankName,
                BranchId = x.ChequeInfo.BankBranch.BranchId,
                BankBranchName = x.ChequeInfo.BankBranch.BranchName,
                ChequeId = x.ChequeId,
                ChequeNo = x.ChequeInfo.ChequeNo,
                ChequeDate = x.ChequeDate,
                IssueDate = x.IssueDate,
                IssueTo = x.IssueTo,
                ChequeAmount = x.ChequeAmount,
                Purpose = x.Purpose

            }).ToList().Where(x => !ChequeRecivelst.Contains(x.ChequeId) && x.IssueDate.Year == DateTime.Now.Year && x.IssueDate>=fromdate
             && x.IssueDate<=todate && x.IssueTo== issueTo);

            return PartialView("RowTemplatePendingChequeReceive", PendingChequereceiveList);

        }

        public ActionResult PendingChequeReceiveDetails(int Id)
        {
            var chequereceive = db.ChequePayments.Include(x => x.ChequeInfo).Include(x => x.ChequeInfo.BankAccount).Include(x => x.ChequeInfo.Bank).Include(x => x.ChequeInfo.BankBranch).Select(x => new ChequeReceiveViewModel
            {
                AccountId = x.ChequeInfo.AccountId,
                AccountNo = x.ChequeInfo.BankAccount.AccountNo,
                BankId = x.ChequeInfo.Bank.BankId,
                BankName = x.ChequeInfo.Bank.BankName,
                BranchId = x.ChequeInfo.BankBranch.BranchId,
                BankBranchName = x.ChequeInfo.BankBranch.BranchName,
                ChequeId = x.ChequeId,
                ChequeNo = x.ChequeInfo.ChequeNo,
                ChequeDate = x.ChequeDate,
                IssueDate = x.IssueDate,
                IssueTo = x.IssueTo,
                ChequeAmount = x.ChequeAmount,
                Purpose = x.Purpose

            }).ToList().Where(x=>x.ChequeId==Id).FirstOrDefault();

            return View(chequereceive);
        
        }


        [HttpPost]
        public ActionResult Save(ChequeReceiveViewModel ChequeReceive)
        {
            try
            {

                var ChequeRcv = new ChequeReceive(ChequeReceive);
                  
                db.ChequeReceives.Add(ChequeRcv);
                db.SaveChanges();               
                return Json(new { status = "Success", chkid = ChequeReceive.ChequeId, chqno = ChequeReceive.ChequeNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }

        }



    }
}