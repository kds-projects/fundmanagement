﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Setup
{
    public class ChequeController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Cheque/
        public ActionResult Index()
        {
            var chequeinfos = db.ChequeInfos.Include(c => c.Bank).Include(c => c.BankAccount).Include(c => c.BankBranch).Include(c => c.CurrencyType);
            return View(chequeinfos.ToList());
        }

        // GET: /Cheque/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeInfo chequeInfo = db.ChequeInfos.Find(id);
            if (chequeInfo == null)
            {
                return HttpNotFound();
            }
            return View(chequeInfo);
        }

        // GET: /Cheque/Create
        [CAuthorize]
        public ActionResult Requisition()
        {
            ViewBag.BankList = new SelectList(db.Banks.Where(c => c.Activity == ActivityType.Active), "BankId", "BankName");
            ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.AccountId == 0), "AccountId", "AccountNo");
            ViewBag.BranchList = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BranchId >= 0), "BranchId", "BranchName");
            ViewBag.CurrencyList = new SelectList(db.CustomTypes, "TypeId", "TypeName");
            ViewBag.PaymentMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");
            ViewBag.RequisitionNo = "CBR" + db.ChequeBookRequisitions.Where(l => l.Year == DateTime.Today.Year).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "/" + DateTime.Today.ToString("yy");

            return View();
        }

        // POST: /Cheque/Requisition_Save
        [HttpPost]
        [CAuthorize]
        public ActionResult Requisition_Save(ChequeBookRequisition req_info, ICollection<ChequeBookRequisitionSub> req_subs)
        {
            try
            {
                LandedCosting s = new LandedCosting();

                var requisition = new ChequeBookRequisition(req_info);
                requisition.RequisitionNo = "CBR" + db.ChequeBookRequisitions.Where(l => l.Year == req_info.RequisitionDate.Year).Select(c => c.SerialNo + 1).Max().GetValueOrDefault(Convert.ToInt16("1")).ToString("0000") + "/" + req_info.RequisitionDate.ToString("yy");
                
                if (req_subs != null)
                {
                    foreach (var sub in req_subs)
                    {
                        var _sub = new ChequeBookRequisitionSub(sub);
                        req_info.ChequeBookRequisitionSubs.Add(_sub);
                    }
                }

                db.ChequeBookRequisitions.Add(requisition);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", reqno = requisition.RequisitionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /ChequePayment/
        [CAuthorize]
        public ActionResult ApprovalPending()
        {
            var requisitions = db.ChequeBookRequisitions.Where(c => c.ApprovalType == 0);
            return View(requisitions.ToList());
        }

        // GET: /ChequeInstruction/Approval/5
        [CAuthorize]
        public ActionResult Approval(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeBookRequisition requisition = db.ChequeBookRequisitions.Find(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankList = new SelectList(db.Banks.Where(c => c.Activity == ActivityType.Active), "BankId", "BankName", requisition.BankId);
            ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == requisition.BranchId), "AccountId", "AccountNo", requisition.AccountId);
            ViewBag.BranchList = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BankId == requisition.BankId), "BranchId", "BranchName", requisition.BranchId);

            return View(requisition);
        }

        // GET: /ChequeInstruction/Approval/5
        [CAuthorize]
        public ActionResult Approval_Save(int id, short approval_type, string disapproved_reason)
        {
            ChequeBookRequisition requisition = db.ChequeBookRequisitions.Find(id);
            if (requisition == null)
            {
                return HttpNotFound();
            }
            if (requisition.ApprovalType == 1)
            {
                return Json(new { status = "Error", message = "Already approved." }, JsonRequestBehavior.AllowGet);
            }
            else if (requisition.ApprovalType == 2)
            {
                return Json(new { status = "Error", message = "Already disapproved." }, JsonRequestBehavior.AllowGet);
            }

            try
            {
                requisition.ApprovalType = approval_type;

                if (requisition.ApprovalType == 2)
                    requisition.DisapprovedReason = disapproved_reason;
                else
                    requisition.DisapprovedReason = "";

                requisition.ApprovedBy = clsMain.getCurrentUser();
                requisition.ApprovedTime = clsMain.getCurrentTime();

                db.Entry(requisition).State = EntityState.Modified;
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", reqno = requisition.RequisitionNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Cheque/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.BankList = new SelectList(db.Banks.Where(c => c.Activity == ActivityType.Active), "BankId", "BankName");
            ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.AccountId == 0), "AccountId", "AccountNo");
            ViewBag.BranchList = new SelectList(db.BankBranchs.Where(c => c.Activity == ActivityType.Active && c.BranchId >= 0), "BranchId", "BranchName");
            ViewBag.CurrencyList = new SelectList(db.CustomTypes, "TypeId", "TypeName");
            ViewBag.PaymentMode = new SelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");
            ViewBag.RequisitionList = new SelectList(db.ChequeBookRequisitions.Where(c => c.RequisitionId == 0), "RequisitionId", "RequisitionNo");

            return View();
        }

        // POST: /Cheque/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(ChequeInfo chequeInfo, ICollection<ChequeBookInfo> chequebooks)
        {
            try
            {
                if (chequeInfo.RequisitionId > 0)
                {
                    var req_info = db.ChequeBookRequisitions.Find(chequeInfo.RequisitionId);
                    req_info.Status = 0;
                    db.Entry(req_info).State = EntityState.Modified;
                }

                string SerialRange = "", comma = "";
                foreach (var book in chequebooks)
                {
                    var chqRange = book.SerialRange.Split('-').Select(c => new { SLNo = Convert.ToInt32(c.Trim()), Length = c.Trim().Length }).ToList();
                    for (var chqno = chqRange[0].SLNo; chqno <= chqRange[1].SLNo; chqno++)
                    {
                        chequeInfo.ChequeNo = book.Prefix + chqno.ToString("0000000000000".Substring(0, chqRange[0].Length));
                        chequeInfo.Status = Convert.ToInt16(chequeInfo.Activity == ActivityType.Active ? 1 : 0);
                        chequeInfo.SerialRange = book.SerialRange;
                        chequeInfo.Prefix = book.Prefix;

                        if (db.ChequeInfos.Any(c => c.ChequeNo.Equals(chequeInfo.ChequeNo) && c.AccountId == chequeInfo.AccountId))
                            return Json(new { status = "Error", message = "Duplicate cheque no." }, JsonRequestBehavior.AllowGet);

                        db.ChequeInfos.Add(new ChequeInfo(chequeInfo));
                    }

                    SerialRange += comma + book.SerialRange;
                    comma = ", ";
                }

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", chqno = SerialRange }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Cheque/Edit/5
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeInfo chequeInfo = db.ChequeInfos.Find(id);
            if (chequeInfo == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", chequeInfo.BankId);
            ViewBag.AccountId = new SelectList(db.BankAccount, "AccountId", "AccountCode", chequeInfo.AccountId);
            ViewBag.BranchId = new SelectList(db.BankBranchs, "BranchId", "BranchCode", chequeInfo.BranchId);
            ViewBag.CurrencyId = new SelectList(db.CustomTypes, "TypeId", "TypeName", chequeInfo.CurrencyId);
            return View(chequeInfo);
        }

        // POST: /Cheque/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ChequeId,AccountId,BranchId,BankId,ChequeNo,Prefix,CurrencyId,SerialRange,Activity,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] ChequeInfo chequeInfo)
        {
            if (ModelState.IsValid)
            {
                db.Entry(chequeInfo).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", chequeInfo.BankId);
            ViewBag.AccountId = new SelectList(db.BankAccount, "AccountId", "AccountCode", chequeInfo.AccountId);
            ViewBag.BranchId = new SelectList(db.BankBranchs, "BranchId", "BranchCode", chequeInfo.BranchId);
            ViewBag.CurrencyId = new SelectList(db.CustomTypes, "TypeId", "TypeName", chequeInfo.CurrencyId);
            return View(chequeInfo);
        }

        // GET: /ChequePayment/Create
        [CAuthorize]
        public ActionResult Cancel()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.AccountId >= 0), "AccountId", "AccountNo");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.BranchId >= 0), "BranchId", "BranchName");
            ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.ChequeId == 0), "ChequeId", "ChequeNo");
            ViewBag.Issuer = new SelectList(db.ChequePayments.Select(c => new { IssueTo = c.IssueTo }).Distinct(), "IssueTo", "IssueTo");
            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");
            ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName");

            return View();
        }

        // GET: /ChequePayment/Create
        [CAuthorize]
        [ActionName("history")]
        public ActionResult ChequeHistory()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.AccountId = new SelectList(db.BankAccount.Where(c => c.AccountId >= 0), "AccountId", "AccountNo");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(c => c.BranchId >= 0), "BranchId", "BranchName");
            //ViewBag.ChequeList = new SelectList(db.ChequeInfos.Where(c => c.ChequeId == 0), "ChequeId", "ChequeNo");
            ViewBag.Issuer = new SelectList(db.ChequePayments.Select(c => new { IssueTo = c.IssueTo }).Distinct(), "IssueTo", "IssueTo");
            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");
            ViewBag.ChequeMode = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")).Distinct(), "TypeId", "TypeName");

            var chklist = (from p in db.BankAccount.AsEnumerable()
                          join q in db.Banks.AsEnumerable() on p.BankId equals q.BankId
                          join r in dbProd.Companies.AsEnumerable() on p.CompanyId equals r.CompanyId
                          join s in db.ChequeInfos.AsEnumerable() on p.AccountId equals s.AccountId
                          join t in db.ChequePayments.AsEnumerable() on s.ChequeId equals t.ChequeId //into v
                          //from t in v.DefaultIfEmpty()
                          select new { ChequeId = s.ChequeId, ChequeNo = s.ChequeNo + " >> " + p.AccountNo + " >> " + q.ShortName + " >> " + r.ShortName + " >> " + (t == null ? "" : t.IssueTo), PaymentDate = t.IssueDate, ChequeDate = t.ChequeDate }
                         );
            ViewBag.ChequeId = new SelectList(chklist.OrderBy(c => c.PaymentDate).Distinct(), "ChequeId", "ChequeNo");


            return View();
        }

        // POST: /Cheque/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult CancelSave(ChequeInfo chequeInfo)
        {
            try
            {
                var cheque = db.ChequeInfos.Find(chequeInfo.ChequeId);
                cheque.CancelDate = chequeInfo.CancelDate;
                cheque.Status = chequeInfo.Status;
                cheque.CancelReason = chequeInfo.CancelReason;
                cheque.CancelEntryDate = clsMain.getCurrentTime();
                cheque.CancelledBy = clsMain.getCurrentUser();

                db.Entry(cheque).State = EntityState.Modified;

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", chqno = cheque.ChequeNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Cheque/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ChequeInfo chequeInfo = db.ChequeInfos.Find(id);
            if (chequeInfo == null)
            {
                return HttpNotFound();
            }
            return View(chequeInfo);
        }

        // POST: /Cheque/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ChequeInfo chequeInfo = db.ChequeInfos.Find(id);
            db.ChequeInfos.Remove(chequeInfo);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult AccountList(int id)
        {
            ViewBag.AccountList = new SelectList(db.BankAccount.Where(c => c.Activity == ActivityType.Active && c.BranchId == id), "AccountId", "AccountNo");

            return PartialView();
        }

        [HttpPost]
        public ActionResult AccountDetails(int id)
        {
            var ac = db.BankAccount.Find(id);

            return Json(new { curid = ac.CurrencyId }, JsonRequestBehavior.AllowGet);
        }

        [HttpPost]
        public ActionResult StockDetails(int id)
        {
            var stock_details = db.ChequeInfos.Where(c => c.AccountId == id && c.PaymentModeId == 8).GroupBy(c => c.SerialRange).Select(c => new { SerialRange = c.Key, TotalStock = c.Count(), StockAvailable = c.Count(l => l.Status == 1) }).Where(c => c.StockAvailable > 0).Distinct().Select(c => new StockPosition() {
                SerialRange = c.SerialRange,
                TotalStock = c.TotalStock,
                StockAvailable = c.StockAvailable
            });

            return PartialView(stock_details);
        }

        //[HttpPost, HttpGet]
        public ActionResult StockDetailsFT(ICollection<ChequeBookRequisitionSub> reqSubs)
        {
            var stock_details = reqSubs.Select(c => new StockPosition()
            {
                RequisitionSubId = c.RequisitionSubId,
                SerialRange = c.SerialRange,
                TotalStock = c.TotalCheque,
                StockAvailable = c.StockAvailable
            });

            return PartialView("StockDetails", stock_details);
        }

        [HttpPost]
        public ActionResult RequisitionList(int id)
        {
            ViewBag.RequisitionList = new SelectList(db.ChequeBookRequisitions.Where(c => c.AccountId == id && c.Status == 1), "RequisitionId", "RequisitionNo");

            return PartialView();
        }
    }
}
