﻿using FundManagement.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FundManagement.Controllers.Setup
{
    public class PayorderController : ContextList
    {
        // GET: Payorder
        public ActionResult Index()
        {
            var payorderlist = (from a in db.PayorderInfos
                                join
                                b in db.PayorderDetails on a.PayorderId
                                 equals b.PayOrderId
                                group new { a, b } by new
                                {
                                    a.PayorderId,
                                    a.ReferenceNo,
                                    a.AccountId,
                                    a.PayorderDate
                                } into gr
                                select new 
                                {
                                    Reference = gr.Key.ReferenceNo,
                                    PayorderDate =gr.Key.PayorderDate,
                                    BankAccount = db.BankAccount.Where(x => x.AccountId == gr.Key.AccountId).Select(x => x.AccountNo).FirstOrDefault(),
                                    Amount = gr.Sum(x => x.b.PayorderAmount)
                                }).ToList();


            var payorderVM = payorderlist.Select(x => new PayorderListVM { Amount = x.Amount, BankAccount = x.BankAccount, PayorderDate = x.PayorderDate.ToString("dd-MM-yyyy"), Reference = x.Reference });

            return View(payorderVM);
        }


        [CAuthorize]
        public ActionResult Create()
        {
           
            ViewBag.CompanyId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1), "CompanyId", "ShortName");

            ViewBag.BankId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1 && c.CompanyId==0), "CompanyId", "ShortName");

            ViewBag.BranchId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1 && c.CompanyId == 0), "CompanyId", "ShortName");

            ViewBag.AccountId = new SelectList(dbProd.Companies.Where(c => c.IsActive == 1 && c.CompanyId == 0), "CompanyId", "ShortName");

            ViewBag.Signature = new MultiSelectList(db.CustomTypes.Where(c => c.Activity == ActivityType.Active && c.Flag.Equals("CHQSIG")).Distinct(), "TypeName", "TypeName");

            var accountlist = (from a in db.BankAccount.AsEnumerable()
                               join b in db.Banks.AsEnumerable()
                               on a.BankId equals b.BankId
                               join c in dbProd.Companies.AsEnumerable() on a.CompanyId
                               equals c.CompanyId
                               select new { AccountId = a.AccountId.ToString(), AccountNo = a.AccountNo + " >> " + b.ShortName + " >> " + c.ShortName }).Distinct().ToList().Union
                             (
                                db.ChequePayments.Where(x => x.IssuedAccountId == 0).Select
                                (
                                    c => new { AccountId = "0>>" + c.IssueTo, AccountNo = c.IssueTo + ">>" }
                                ).Distinct().ToList()
                             ).Union
                             (
                                  db.ChequeIssueInstructions.Where(x => x.IssuedAccountId == 0).Select
                                (
                                    c => new { AccountId = "0>>" + c.IssuedAccountName, AccountNo = c.IssuedAccountName + ">>" }
                                ).Distinct().ToList()
                             );
                             
            ViewBag.IssuedAccountId = new SelectList(accountlist.Distinct(), "AccountId", "AccountNo");

            return View(new PayorderInfo() { PayorderDate=DateTime.Now,ReferenceNo="KDSA/CTG/F&A/" });
            
        }

        public ActionResult RowTemplate( string IssueTo, int slNo)
        {
            ViewBag.IssueTo = IssueTo;
            ViewBag.slNo = slNo;
            return PartialView();
        }

        [HttpGet]
        public JsonResult IsExists( string referenceno)
        {
            bool isExsists = false;

            if (db.PayorderInfos.Where(x=>x.ReferenceNo== referenceno).ToList().Any() )
            {
                isExsists = true;
            }

            return Json(isExsists,JsonRequestBehavior.AllowGet);
        }

        
        [CAuthorize]
        [HttpPost]      
        public ActionResult Save(PayorderInfo payorderInfo, ICollection<PayorderDetails> payorderDetails)
        {
            if (payorderInfo == null || payorderDetails.ToList().Count == 0)
                return Json(new { status = "Error", message = "No Date available for save." }, JsonRequestBehavior.AllowGet);
            try
            {
                        payorderInfo.CreatedTime = DateTime.Now;
                       var payorder = new PayorderInfo(payorderInfo);
                        
                        if (payorderDetails != null)
                        {
                            foreach (var _sub in payorderDetails)
                            {
                             var sub = new PayorderDetails(_sub);
                             payorder.payorderDetails.Add(sub);
                            }
                        }

                    db.PayorderInfos.Add(payorder);
                    db.SaveChanges();
                   
                return Json(new { status = "Success", ino = payorderDetails.ToList().Count, chkid = payorder.PayorderId }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
              
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }




    }
}