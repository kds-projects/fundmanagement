﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class BankAccountController : ContextList
    {
        private FundContext db = new FundContext();

        // GET: /BankAccount/
        public ActionResult Index()
        {
            var bankaccount = db.BankAccount.Include(b => b.Bank).Include(b => b.BankBranch).Include(b => b.CategoryType).Include(b => b.CurrencyType);
            return View(bankaccount.ToList());
        }

        // GET: /BankAccount/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = db.BankAccount.Find(id);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // GET: /BankAccount/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.Bank = new SelectList(db.Banks.OrderBy(c => c.BankName), "BankId", "BankName");
            ViewBag.Branch = new SelectList(db.BankBranchs.Where(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.Category = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ACCCAT")), "TypeId", "TypeName");
            ViewBag.Currency = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CURCODE")).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.Type = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ACCTYPE")).OrderBy(c => c.TypeId), "TypeId", "TypeName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");

            return View();
        }

        // POST: /BankAccount/Create
        [HttpPost, CAuthorize]
        public ActionResult Save(BankAccountView bankAccount)
        {
            try
            {
                if (bankAccount.AccountId == 0 && db.BankAccount.Where(l => l.AccountNo.Equals(bankAccount.AccountNo) && l.BankId == bankAccount.BankId).Count() == 0)
                {
                    var newAccount = new BankAccount(bankAccount);
                    newAccount.AccountCode = "BA0001";
                    db.BankAccount.Add(newAccount);
                    db.SaveChanges();

                    return Json(new { status = "Success", acno = bankAccount.AccountNo }, JsonRequestBehavior.AllowGet);
                }
                else if (bankAccount.AccountId > 0 && db.BankAccount.Where(l => l.AccountNo.Equals(bankAccount.AccountNo) && l.BankId == bankAccount.BankId && l.AccountId != bankAccount.AccountId).Count() == 0)
                {
                    var oldAccount = db.BankAccount.Find(bankAccount.AccountId);
                    oldAccount = (BankAccount)bankAccount.ConvertNotNull(oldAccount);
                    db.Entry(oldAccount).State = EntityState.Modified;
                    db.SaveChanges();

                    return Json(new { status = "Success", acno = bankAccount.AccountNo }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = "Error", message = "Duplicate account info." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /BankAccount/Edit/5
        [CAuthorize]
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = db.BankAccount.Find(id);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }

            ViewBag.Bank = new SelectList(db.Banks.OrderBy(c => c.BankName), "BankId", "BankName", bankAccount.BankId);
            ViewBag.Branch = new SelectList(db.BankBranchs.Where(c => c.BankId == bankAccount.BankId), "BranchId", "BranchName", bankAccount.BranchId);
            ViewBag.Category = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ACCCAT")), "TypeId", "TypeName", bankAccount.AccountTypeId);
            ViewBag.Currency = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CURCODE")).OrderBy(c => c.RankId), "TypeId", "TypeName", bankAccount.CurrencyId);
            ViewBag.Type = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ACCTYPE")), "TypeId", "TypeName", bankAccount.CategoryId);
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", bankAccount.CompanyId);

            return View("Create", bankAccount);
        }

        // GET: /BankAccount/Delete/5
        [CAuthorize]
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            BankAccount bankAccount = db.BankAccount.Find(id);
            if (bankAccount == null)
            {
                return HttpNotFound();
            }
            return View(bankAccount);
        }

        // POST: /BankAccount/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(short id)
        {
            BankAccount bankAccount = db.BankAccount.Find(id);
            db.BankAccount.Remove(bankAccount);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
