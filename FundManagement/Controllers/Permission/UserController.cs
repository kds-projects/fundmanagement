﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;
using System.Globalization;

namespace FundManagement.Controllers.Permission
{
    [CAuthorize(Roles="Admin")]
    public class UserController : ContextList
    {
        // GET: /User/
        public ActionResult Index()
        {
            return View(db.UserInfos.ToList());
        }

        // GET: /User/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            if (sys_User_Name == null)
            {
                return HttpNotFound();
            }
            return View(sys_User_Name);
        }

        // GET: /User/Create
        [CAuthorize]
        public ActionResult Register()
        {
            return View();
        }

        // POST: /User/Create
        [HttpPost]
        public ActionResult Register([Bind(Include = "UserId,UserName,UserPWD,IsAdmin")] UserInfo sys_User_Name, string ConfirmPassword)
        {
            if (ModelState.IsValid)
            {
                if (ConfirmPassword.Length > 0 && sys_User_Name.UserPWD.Equals(ConfirmPassword))
                {
                    sys_User_Name.UserPWD = fncEncryptWord(sys_User_Name.UserPWD);
                    db.UserInfos.Add(sys_User_Name);
                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    ViewBag.errormessage = "Password mismatch.";
            }

            return View(sys_User_Name);
        }

        // GET: /User/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            if (sys_User_Name == null)
            {
                return HttpNotFound();
            }
            return View(sys_User_Name);
        }

        // POST: /User/Edit/5
        [HttpPost]
        public ActionResult Edit([Bind(Include = "UserId,UserPWD")] UserInfo sys_User_Name, string ConfirmPassword)
        {
            if (ModelState.IsValid)
            {
                if (sys_User_Name.UserPWD.Equals(ConfirmPassword))
                {
                    db.Entry(sys_User_Name).State = EntityState.Modified;
                    if (ConfirmPassword.Length > 0)
                        sys_User_Name.UserPWD = fncEncryptWord(sys_User_Name.UserPWD);

                    db.SaveChanges();
                    return RedirectToAction("Index");
                }
                else
                    ViewBag.errormessage = "Password mismatch.";
            }

            return View(sys_User_Name);
        }

        // GET: /User/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            if (sys_User_Name == null)
            {
                return HttpNotFound();
            }
            return View(sys_User_Name);
        }

        // POST: /User/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            db.UserInfos.Remove(sys_User_Name);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        //
        // GET: /User/Login
        [AllowAnonymous]
        public ActionResult PartialLogin(string returnUrl)
        {
            ViewBag.ReturnUrl = returnUrl;
            return PartialView(new UserView());
        }

        //
        // GET: /User/Login
        [AllowAnonymous]
        public ActionResult Login(string returnurl)
        {
            ViewBag.returnurl = returnurl;
            return PartialView(new UserView());
        }

        //
        // POST: /User/Login
        [HttpPost]
        [AllowAnonymous]
        //[ValidateAntiForgeryToken]
        public ActionResult Login(UserView userView, string returnurl, bool isAjaxCall = false)
        {
            int userId = 0;
            //Session["UserId"] = 0;
            //Session["UserName"] = "";
            //Session["UserTitle"] = "";

            //Session["UserId"] = 1;
            //Session["UserName"] = "XHQ1029-11";
            //Session["UserTitle"] = "Noman";
            Session["UsereMail"] = "";

            //if (!ModelState.IsValid)
            //{
            //    return PartialView(model);
            //}

            // This doen't count login failures towards lockout only two factor authentication
            // To enable password failures to trigger lockout, change to shouldLockout: true
            var result = LoginValidation(userView, ref userId);
            switch (result)
            {
                case UserStatus.Success:
                    var textInfo = new CultureInfo("en-US", false).TextInfo;
                    UserInfo user = db.UserInfos.Find(userId);
                    Session["UserId"] = user.UserId;
                    Session["UserName"] = textInfo.ToTitleCase(user.UserName);
                    Session["UserTitle"] = textInfo.ToTitleCase(user.UserName);
                    //Session.Timeout = 200;
                    //Session["UsereMail"] = user.eMail;

                    if (isAjaxCall)
                        return Json("Success", JsonRequestBehavior.AllowGet);
                    else
                    {
                        if (returnurl != null && returnurl.Length > 0)
                            return Redirect(returnurl);
                        else
                            return RedirectToAction("Index", "Home");
                    }
                case UserStatus.LockedOut:
                    return View("Lockout");
                //case SignInStatus.RequiresVerification:
                //    return RedirectToAction("SendCode", new { ReturnUrl = returnUrl });
                case UserStatus.Failure:
                    if (isAjaxCall)
                        return Json("Failure", JsonRequestBehavior.AllowGet);
                    else
                        return RedirectToAction("Login", new { returnurl = returnurl });
                default:
                    ModelState.AddModelError("", "Invalid login attempt.");
                    return PartialView(userView);
            }
        }

        [AllowAnonymous]
        public UserStatus LoginValidation(UserView userView, ref int userId)
        {
            string Pass = userView.UserPWD; //fncEncryptWord();
            var user = db.UserInfos.Where(l => l.UserName == userView.UserName && l.UserPWD == Pass && l.IsActive == 1);

            if (user != null && user.Count() > 0)
            {
                userId = user.FirstOrDefault().UserId;
                return UserStatus.Success;
            }
            else
            {
                userId = 0;
                return UserStatus.Failure;
            }
        }

        [AllowAnonymous]
        public ActionResult LogOff()
        {
            Session["UserId"] = 0;
            Session["UserName"] = "";
            Session["UserTitle"] = "";

            return RedirectToAction("Index", "Home");
        }

        [AllowAnonymous]
        public ActionResult RetrivePassword(string id)
        {
            var user = db.UserInfos.Where(l => l.UserName.Equals(id)).FirstOrDefault();

            if (user == null)
            {
                return Json(new { status = "error", message = "Invalid user." }, JsonRequestBehavior.AllowGet);
            }
            else if (user.eMail == null || !user.eMail.Contains('@') || !user.eMail.Contains('.'))
            {
                return Json(new { status = "error", message = "Invalid email address." }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                try
                {
                    //EMailCollectionView MailCollection = new EMailCollectionView();
                    //MailCollection.EmailType = "PasswordRetrive";
                    //MailCollection.ModuleName = "IPMS";
                    //MailCollection.EmailFrom = "Inventory & Procurement Management System<ipms.cube@kdsgroup.net>";
                    //MailCollection.EmailTo = user.eMail;
                    //MailCollection.EmailCC = "";
                    //MailCollection.EmailBCC = "";
                    //MailCollection.EmailSubject = "IPMS User Password";
                    //MailCollection.EmailBody = "User Id: " + user.UserName + "<br>Password: " + user.UserPWD;
                    //MailCollection.EmailAttachmentLink = "";
                    //MailCollection.CreateAttachment = 0;
                    //MailCollection.CreatedBy = clsMain.getCurrentUser();

                    //EMailCollection mail = new EMailCollection(MailCollection);

                    //dbf.EMailCollections.Add(mail);

                    //dbEM.SaveChanges();

                    return Json(new { status = "success", message = "Password has been sent to " + user.eMail + "." }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    return Json(new { status = "error", message = "Message can't sent due to some error." }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        [CAuthorize]
        public ActionResult getUserId()
        {
            if (Session == null || Session["UserId"] == null)
            {
                return RedirectToAction("Login", new { returnurl = Request.Url });
            }
            else
            {
                return Content(Session["UserId"].ToString());
            }
        }

        //Encryption Method
        //[AllowAnonymous]
        public string fncEncryptWord(string s)
        {
            int crack;
            crack = 11;

            char T1; string T2;
            T2 = "";

            for (int i = 0; i < s.Length; i++)
            {
                if (s.Substring(i, 1) == @"\\")
                {
                    T2 = T2 + @"\\";
                }
                else
                {
                    T1 = Convert.ToChar(s.Substring(i, 1));
                    T2 = T2 + Convert.ToChar(T1 + crack);
                }
            }
            return T2;
        }


        //Decryption Method
        public string fncDecryptWord(string s)
        {
            int crack;
            crack = 11;

            char T1; string T2;
            T2 = "";

            for (int i = 0; i < s.Length; i++)
            {
                if (s.Substring(i, 1) == @"\\")
                {
                    T2 = T2 + @"\\";
                }
                else
                {
                    T1 = Convert.ToChar(s.Substring(i, 1));
                    T2 = T2 + Convert.ToChar(T1 - crack);
                }
            }
            return T2;
        }

        // GET: /User/ChangePassword/5
        [CAuthorize]
        public ActionResult ChangePassword(short? id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            if (sys_User_Name == null)
            {
                return HttpNotFound();
            }

            UserView userView = new UserView(sys_User_Name);
            userView.UserPWD = "";

            ViewBag.rurl = returnUrl;
            return View(userView);
        }

        // POST: /User/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [CAuthorize]
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult ChangePassword([Bind(Include = "UserId,UserPWD,ConfirmPassword,OldPassword")] UserView userView, string returnUrl)
        {
            UserInfo sys_User_Name = db.UserInfos.Find(userView.UserId);

            string Pass = fncEncryptWord(userView.OldPassword);
            if (userView.OldPassword == null || userView.OldPassword.Length == 0)
                ViewBag.errormessage = "Old password is blank.";
            else if (db.UserInfos.Where(u => u.UserId == userView.UserId && u.UserPWD == Pass).Count() == 0)
                ViewBag.errormessage = "Old password is invalid.";
            else if (userView.ConfirmPassword == null || userView.ConfirmPassword.Length == 0)
                ViewBag.errormessage = "Confrim password is blank.";
            else if (userView.UserPWD == null || userView.UserPWD.Length == 0)
                ViewBag.errormessage = "New password is blank.";
            else if (!userView.ConfirmPassword.Equals(userView.UserPWD))
                ViewBag.errormessage = "Pasword mismatch.";
            else
            {
                db.Entry(sys_User_Name).State = EntityState.Modified;
                sys_User_Name.UserPWD = fncEncryptWord(userView.UserPWD);
                db.SaveChanges();
                return Redirect(returnUrl);
            }

            userView.UserName = sys_User_Name.UserName;
            return View(userView);
        }

        [CAuthorize]
        public ActionResult Profile(short? id, string returnUrl)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            UserInfo sys_User_Name = db.UserInfos.Find(id);
            if (sys_User_Name == null)
            {
                return HttpNotFound();
            }

            ViewBag.rurl = returnUrl;
            return View(sys_User_Name);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        //// GET: Role/Create
        //[CAuthorize]
        //public ActionResult UserAccess(short? id)
        //{
        //    ViewBag.UserId = new SelectList(db.UserInfos.Select(c => new { UserId = c.UserId, UserName = c.UserName + " (" + c.UserTitle + ")" }), "UserId", "UserName");
        //    ViewBag.type = new SelectList((new[] { new { tid = "Role" }, new { tid = "User" } }).ToList(), "tid", "tid", "Role");
        //    ViewBag.RoleId = new MultiSelectList(db.User_Role.Select(c => new { c.RoleId, c.RoleName }), "RoleId", "RoleName");
        //    var mtlist = db.MenuTypes.ToList();

        //    return View(mtlist);
        //}

        //// GET: Role/Create
        //[HttpPost, CAuthorize]
        //public ActionResult UserAccessSave(string type, ICollection<UserPermission> userPermission)
        //{
        //    try
        //    {
        //        var userid = userPermission.First().UserId;
        //        var ups = db.User_Permission.Where(l => l.UserId == userid).ToList();

        //        foreach (var up in userPermission)
        //        {
        //            if (type.Equals("User"))
        //            {
        //                if (ups.Where(l => l.ActionId == up.ActionId).Count() > 0)
        //                {
        //                    var urd = ups.Where(l => l.ActionId == up.ActionId).First();
        //                    db.Entry(urd).State = EntityState.Modified;
        //                }
        //                else
        //                {
        //                    User_Permission urd = new User_Permission(db.User_Action.Find(up.ActionId));
        //                    urd.UserId = up.UserId;
        //                    db.User_Permission.Add(urd);
        //                }
        //            }
        //            else
        //            {
        //                if (ups.Where(l => l.RoleId == up.RoleId).Count() > 0)
        //                {
        //                    var urd = ups.Where(l => l.RoleId == up.RoleId).First();
        //                    db.Entry(urd).State = EntityState.Modified;
        //                }
        //                else
        //                {
        //                    User_Permission urd = new User_Permission(up);
        //                    urd.IPAddress = clsMain.GetUser_IP();
        //                    urd.PCName = clsMain.GetUser_PCName();
        //                    db.User_Permission.Add(urd);
        //                }
        //            }
        //        }

        //        for (int ri = 0; ri < ups.Count; ri++)
        //        {
        //            if (db.Entry(ups[ri]).State == EntityState.Unchanged)
        //                db.Entry(ups[ri]).State = EntityState.Deleted;
        //        }

        //        db.SaveChanges();

        //        return Json(new { status = "Successful" }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
        //    }
        //}

        //// GET: Role/Create
        //[CAuthorize]
        //public ActionResult UserRoleDetails(short? id)
        //{
        //    try
        //    {
        //        var pemission = db.User_Permission.Where(l => l.UserId == id);

        //        var RoleId = pemission.OrderByDescending(c => c.RoleId).First().RoleId;
        //        var RoleList = pemission.Select(c => c.RoleId).Distinct().ToArray();
        //        //var RoleList = db.User_Role.Where(l => l.RoleId < 5).Select(c => c.RoleId).ToArray();
        //        var type = RoleId >= 1 ? "Role" : "User";

        //        //var alist = new List<short>();

        //        //if (RoleId == 0)
        //        //    alist = pemission.Select(c => c.ActionId).ToList();
        //        //else
        //        //    alist = db.User_Role_Details.Where(c => c.RoleId == RoleId).Select(c => c.ActionId).ToList();


        //        if (RoleId == 0)
        //            return Json(new { RoleId = RoleId, type = type, alist = pemission.Select(c => c.ActionId).ToList() }, JsonRequestBehavior.AllowGet);
        //        else
        //            return Json(new { RoleId = RoleList, type = type, alist = db.User_Role_Details.Where(c => c.RoleId == RoleId).Select(c => c.ActionId).ToList() }, JsonRequestBehavior.AllowGet);
        //    }
        //    catch (Exception ex)
        //    {
        //        return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
        //    }
        //}
    }
}
