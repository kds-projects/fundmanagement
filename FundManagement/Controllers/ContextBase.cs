﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FundManagement.Models;
using System.Web.Mvc;
using FundManagement.Models.DBFirst;

namespace FundManagement.Controllers
{
    public class ContextList : Controller
    {
        public FundContext db = new FundContext();
        public IMSContext dbIMS = new IMSContext();
        public LCContext dbLC = new LCContext();
        public ProductContext dbProd = new ProductContext();
        public EMailEntities dbMail = new EMailEntities();
    }
}