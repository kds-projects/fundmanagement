﻿using FundManagement.Models;
using System;
using System.Data;
using System.Data.Entity;
using System.IO;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Stimulsoft.Base;
using Stimulsoft.Report;
using Stimulsoft.Report.Dictionary;
using Stimulsoft.Report.Mvc;
using System.Data.SqlClient;

namespace FundManagement.Controllers
{
    [CAuthorize]
    public class ReportController : ContextList
    {
        public ActionResult CSProfile()
        {
            var cslist = (from p in db.PIMains.AsEnumerable()
                          join q in db.CSMains.AsEnumerable() on p.PIFileId equals q.PIFileId
                          select new { p.PIFileId, p.PINo, Year = q.CSDate.Year, CSNo = q.CSNo + " >> " + p.PINo + " >> " + p.RequistionNo, q.CSId }
                       ).Distinct().OrderBy(c => c.CSNo);
            ViewBag.YearList = new SelectList(cslist, "Year", "Year");
            ViewBag.CSList = new SelectList(cslist, "CSId", "CSNo");

            return View();
        }

        public ActionResult PIInHand()
        {
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.FactoryId = new SelectList(dbProd.Factories, "FactoryId", "ShortName");

            return View();
        }

        public ActionResult PIForward()
        {
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.FactoryId = new SelectList(dbProd.Factories, "FactoryId", "ShortName");

            return View();
        }

        public ActionResult Bank()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");

            return View();
        }

        public ActionResult LC()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");

            var lclist = (from p in db.LCMains.AsEnumerable()
                          join q in db.LCAcceptances.AsEnumerable() on p.LCFileId equals q.LCFileId into x
                          from r in x.DefaultIfEmpty()
                          join s in db.LoanMains.AsEnumerable() on (r == null ? -1 : r.LCAcceptanceId) equals s.LCAcceptanceId into y
                          from t in y.DefaultIfEmpty()
                          where p.LCFileId != 0
                          select new { p.LCFileId, LCNo = p.LCNo + (r == null ? "" : " > " + r.AcceptanceNo) + (t == null ? "" : " > " + t.LoanNo) }
                         ).Distinct().ToList();

            ViewBag.LC = new SelectList(lclist, "LCFileId", "LCNo");

            return View();
        }

        public ActionResult WhrafRentRegister()
        {

           /* var lclist = (from p in db.LCMains.AsEnumerable()
                         join q in db.Wharfrents.AsEnumerable() on p.LCFileId equals q.LcFileId
                         select new
                         {
                             LCFileId = p.LCFileId,
                             LcNo = p.LCNo
                         }).ToList();*/


            var bondList = db.Database.SqlQuery<BondVM>("exec prcLoadBond").ToList();
                          
            ViewBag.LC = new SelectList(bondList, "BondId", "LicenseNumber");

            return View();
        }


        public ActionResult LCInfo()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.LC = new SelectList(db.LCMains.Where(l => l.LCFileId != 0), "LCFileId", "LCNo");

            return View();
        }

        public ActionResult LCLiability()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");

            var lclist = (from p in db.LCMains.AsEnumerable()
                          join q in db.LCAcceptances.AsEnumerable() on p.LCFileId equals q.LCFileId into x
                          from r in x.DefaultIfEmpty()
                          join s in db.LoanMains.AsEnumerable() on (r == null ? -1 : r.LCAcceptanceId) equals s.LCAcceptanceId into y
                          from t in y.DefaultIfEmpty()
                          where p.LCFileId != 0
                          select new{ p.LCFileId, LCNo = p.LCNo + (r == null ? "" : " > " + r.AcceptanceNo) + (t == null ? "" : " > " + t.LoanNo) }
                         ).Distinct().ToList();

            ViewBag.LC = new SelectList(lclist, "LCFileId", "LCNo");

            return View();
        }

        public ActionResult Acceptance()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");

            return View();
        }

        public ActionResult PaymentRegister()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.PaymentFor = new SelectList((new string[]{"LOAN", "ACCEPTANCE"}).Select(c => new { GroupName = c }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View(new ReportView());
        }

        public ActionResult AdjustmentRegister()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.AdjustFor = new SelectList((new string[] { "LOAN", "ACCEPTANCE" }).Select(c => new { GroupName = c }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View(new ReportView());
        }

        public ActionResult LoanDetails()
        {
            //ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            //ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            //ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            //ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            //ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");
            ViewBag.Loan = new SelectList(db.LoanMains.Where(l => l.ScheduleTypeId != 62), "LoanId", "LoanNo");

            return View();
        }

        public ActionResult LoanRegister()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult LoanPosition()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult PaymentForecasting()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult LoanRepayment()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && (c.GroupName.Equals("FUNDED") || c.TypeId == 37)), "TypeId", "TypeName");

            return View();
        }

        public ActionResult InterestDetails()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && (c.GroupName.Equals("FUNDED"))), "TypeId", "TypeName");
            ViewBag.LoanId = new SelectList(db.LoanMains.Where(c => c.LoanId == 0), "LoanId", "LoanNo");

            return View();
        }

        public ActionResult SanctionRegister()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.SanctionYear = new SelectList(db.SanctionMains.Select(c => new { c.SanctionYear }).Distinct(), "SanctionYear", "SanctionYear");
            ViewBag.SanctionId = new SelectList(db.SanctionMains, "SanctionId", "SanctionNo");

            return View();
        }

        public ActionResult ChequeRegister()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.BankAccountId = new SelectList(db.BankAccount, "AccountId", "AccountNo");
            ViewBag.GroupId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ShortName");
            ViewBag.ChequeModeId = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")), "TypeId", "TypeName");
            ViewBag.PaymentModeId = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");
            ViewBag.IssuedAccountName = new SelectList(db.ChequePayments.Select(c => new { IssuedAccountName = c.IssueTo }).Distinct().ToList(), "IssuedAccountName", "IssuedAccountName");
            ViewBag.DateType = new SelectList((new[] { new { Value = 1, Text = "Issue Date" }, new { Value = 2, Text = "Cheque Date" }, new { Value = 3, Text = "Cancel Date" } }).AsEnumerable().Select(c => new { Value = c.Value, Text = c.Text }), "Value", "Text", 1);

            return View();
        }

        public ActionResult TotalChequeDue()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.ChequeModeId = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CHQMODE")), "TypeId", "TypeName");
            ViewBag.DateType = new SelectList((new[] { new { Value = 1, Text = "Issue Date" }, new { Value = 2, Text = "Cheque Date" } }).AsEnumerable().Select(c => new { Value = c.Value, Text = c.Text }), "Value", "Text", 1);

            return View();
        }

        public ActionResult BusinessPerformance()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && (c.GroupName.Equals("FUNDED") || c.TypeId == 37)), "TypeId", "TypeName");

            return View();
        }

        public ActionResult CashFlowForecast()
        {
            return View();
        }

        public ActionResult LiabilityReconciliation()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult DailyFundPosition()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.AccountType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ACCTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult LiabilityCompare()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.GroupId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult CostReconciliation()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");

            var lclist = (from p in db.LCMains.AsEnumerable()
                          join q in db.LCAcceptances.AsEnumerable() on p.LCFileId equals q.LCFileId
                          join ps in db.PaymentSubs.AsEnumerable() on q.LCAcceptanceId equals ps.LCAcceptanceId into z
                          from r in z.DefaultIfEmpty()
                          join s in db.LoanMains.AsEnumerable() on q.LCAcceptanceId equals s.LCAcceptanceId into y
                          from t in y.DefaultIfEmpty()
                          where (r != null && r.LoanId == 0) || t != null
                          select new { p.LCFileId, LCNo = p.LCNo + " > " + q.AcceptanceNo }
                         ).Distinct().ToList();

            ViewBag.LC = new SelectList(lclist, "LCFileId", "LCNo");

            return View();
        }

        public ActionResult RatioAnalysis()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.GroupId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ShortName");
            ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");

            return View();
        }

        public ActionResult UnUtilizeInsurance()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");

            return View();
        }

        public ActionResult LandedCosting()
        {
            //ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            //ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            //ViewBag.GroupId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ShortName");
            //ViewBag.LiabilityGroup = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("LIABILITYTYPE")).Select(c => new { GroupName = c.GroupName }).Distinct(), "GroupName", "GroupName");
            //ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");
            //ViewBag.LCAcceptanceId = new SelectList(db.LCAcceptances.Include(c => c.LCMain).Select(p => new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }), "LCAcceptanceId", "AcceptanceNo");
            //ViewBag.AcceptList = new SelectList(
            //    db.LCAcceptances.Include(c => c.LCMain).Join(dbIMS.PipelineMains, c => c.PLEId, p => p.PLEId,
            //    (accept, pipeline) => new { accept = accept, pipeline = pipeline })
            //    .Where(c => c.accept.LandedCostingStatus == 0)
            //    .Select(p => new { p.accept.LCAcceptanceId, AcceptanceNo = p.accept.AcceptanceNo + " >> " + p.accept.LCMain.LCNo + " (" + p.pipeline.LotNo + ")" }), "LCAcceptanceId", "AcceptanceNo");
            ViewBag.LCAcceptanceId = new SelectList(
                (from p in db.LCAcceptances.Include(c => c.LCMain).AsEnumerable()
                 join q in dbIMS.PipelineMains.AsEnumerable() on p.PLEId equals q.PLEId
                 where 
                 p.LandedCostingStatus == 0 || p.LandedCostingStatus==1
                 select new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo + " (" + q.LotNo + ")" }
                ).ToList(), "LCAcceptanceId", "AcceptanceNo");
            

            return View();
        }

        public ActionResult LocalPurchasePayable()
        {
            ViewBag.CompanyId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ProductType");
            ViewBag.SupplierId = new SelectList(db.Database.SqlQuery<BillForwardView>("Select Distinct A.Supp_ID As SupplierId, A.Supp_Name As Payment_Favour From PO_New.dbo.Supplier As A Left Outer Join PO_New.dbo.Credit_Purchase_Bill_Main As B On A.Supp_ID = B.Supp_ID Left Outer Join PO_New.dbo.Service_Bill_Main As D On A.Supp_ID = D.Supp_ID Left Outer Join FundManagement.dbo.ChequeIssueSub As C On C.LPBillForwardId = B.CPBPlanNo Or C.ServiceBillForwardId = D.SRVPlanNo Where C.InstructionId Is Not Null").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = c.Payment_Favour.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
            //ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");
            //ViewBag.LCAcceptanceId = new SelectList(db.LCAcceptances.Include(c => c.LCMain).Select(p => new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }), "LCAcceptanceId", "AcceptanceNo");

            return View();
        }

        public ActionResult LocalPurchaseAdvance()
        {
            ViewBag.CompanyId = new SelectList(dbProd.GroupOfCompanies, "GroupId", "ProductType");
            ViewBag.SupplierId = new SelectList(db.Database.SqlQuery<BillForwardView>("Select Distinct A.Supp_ID As SupplierId, A.Supp_Name As Payment_Favour From PO_New.dbo.Supplier As A Left Outer Join PO_New.dbo.PO_Main As B On A.Supp_ID = B.Supp_ID Left Outer Join dbo.ReceivableDocument As C On C.PayableId = A.Supp_ID Where IsNull(C.PayableAmount, 0) + IsNull(B.POAdvanceAmount, 0) > 0").ToList().Select(c => new { SupplierId = c.SupplierId, Payment_Favour = c.Payment_Favour.ToUpper() }).Distinct().OrderBy(c => c.Payment_Favour), "SupplierId", "Payment_Favour");
            //ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");
            //ViewBag.LCAcceptanceId = new SelectList(db.LCAcceptances.Include(c => c.LCMain).Select(p => new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }), "LCAcceptanceId", "AcceptanceNo");

            return View();
        }

        public ActionResult BudgetPosition()
        {
            ViewBag.FactoryId = new SelectList(dbProd.Factories, "FactoryId", "ShortName");

            return View();
        }

        public ActionResult ProjectProfile()
        {
            ViewBag.DocumentId = new SelectList(db.ReceivableDocuments, "DocumentId", "DocumentName");

            return View();
        }

        public ActionResult CBRequisitionProfile()
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Select(c => c.BankId == 0), "BranchId", "BranchName");
            ViewBag.CompanyId = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.AccountId = new SelectList(db.BankAccount, "AccountId", "AccountNo");
            ViewBag.RequisitionId = new SelectList(db.ChequeBookRequisitions.Where(c => c.ApprovalType == 1).OrderByDescending(c => c.RequisitionDate), "RequisitionId", "RequisitionNo");

            return View();
        }

        [ActionName("ShowReport")]
        public ActionResult ShowReport()
        {
            if (Request.QueryString["title"] != null && Request.QueryString["title"].ToString().Length > 1)
                ViewBag.Title = Request.QueryString["title"].ToString();

            return PartialView();
        }

        public ActionResult GetReportSnapshot()
        {
            //try
            //{
                //System.Threading.Thread.Sleep(5000);
                DateTime? AsOnDate = null; string CreatedBy = "", rptFileName = "", Caption = "";
                //Session["UserName"] = "Noman"; Session["UserId"] = "1";
                if (AsOnDate == null)
                    AsOnDate = DateTime.Today.AddMonths(-1);
                if (CreatedBy == "" && Session["UserName"] != null)
                    CreatedBy = Session["UserName"].ToString();
                else if (CreatedBy == null)
                    CreatedBy = "";

                //if (Request.QueryString["rptFileName"] != null)
                //    rptFileName = Request.QueryString["rptFileName"];
                //else
                //rptFileName = "rptCategoryList";

                rptFileName = Request.QueryString["fn"].ToString();
                if (rptFileName.Length > 8 && rptFileName.Substring(rptFileName.Length - 6).All(char.IsDigit))
                    rptFileName = rptFileName.Substring(0, rptFileName.Length - 6);

                // Create the report object
                StiReport report = new StiReport();

                string reportPath = Server.MapPath("~/Reports/" + rptFileName + ".mrt");

                if (!System.IO.File.Exists(reportPath))
                {
                    Response.Write("The specified report does not exist\n" + reportPath + " \n");
                }

                report.Load(reportPath);
                //report.Compile();
                //report.Dictionary.Variables["_ItemId"].Value = "0";
                //report.Dictionary.Variables["_FDate"].Value = "01-Jan-2018";
                //report.Dictionary.Variables["_TDate"].Value = "01-Jan-2019";
                //report.Dictionary.Variables["_FactoryId"].Value = "";
                //report.Dictionary.Variables["_CategoryId"].Value = "0";
                //report.Dictionary.Variables["_UserId"].Value = CreatedBy.ToString();

                List<string> qs = Request.QueryString.Keys.Cast<string>().ToList();
                foreach (StiVariable pf in report.Dictionary.Variables)
                {
                    if (qs.Contains(pf.Name.Replace("_", "")))
                    {
                        if (!String.IsNullOrEmpty(Request.QueryString[pf.Name.Replace("_", "")].ToString()))
                        {
                            if (pf.Type == typeof(string))
                            {
                                report.Dictionary.Variables[pf.Name].Value = "" + Request.QueryString[pf.Name.Replace("_", "")].ToString() + "";
                            }
                            else if (pf.Type.BaseType == typeof(System.ValueType))
                            {
                                if (pf.Type == typeof(DateTime))
                                {
                                    report.Dictionary.Variables[pf.Name].Value = "" + DateTime.Parse(Request.QueryString[pf.Name.Replace("_", "")].ToString()).ToString("yyyy-MM-dd") + "";
                                }
                                else
                                {
                                    report.Dictionary.Variables[pf.Name].Value = Request.QueryString[pf.Name.Replace("_", "")].ToString();
                                }
                            }
                        }
                        else
                        {
                            if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                            {
                                report.Dictionary.Variables[pf.Name].Value = "0";
                            }
                            else if (pf.Type == typeof(string))
                            {
                                report.Dictionary.Variables[pf.Name].Value = "";
                            }
                        }
                    }
                    else
                    {
                        if (pf.Type.BaseType == typeof(ValueType) && pf.Type != typeof(DateTime))
                        {
                            report.Dictionary.Variables[pf.Name].Value = "0";
                        }
                        else if (pf.Type == typeof(string))
                        {
                            report.Dictionary.Variables[pf.Name].Value = "";
                        }
                    }
                }

                if (report.Dictionary.Variables["_CreditUserId"] != null)
                    report.Dictionary.Variables["_CreditUserId"].Value = Session["UserId"].ToString();

                if (report.Dictionary.Variables["_UserId"] != null)
                    report.Dictionary.Variables["_UserId"].Value = Session["UserName"].ToString();

                StiSqlDatabase db = (StiSqlDatabase)report.Dictionary.Databases["FUND"];

                if (db == null)
                    db = (StiSqlDatabase)report.Dictionary.Databases["IPMS"];

                if (db == null)
                {
                    db = (StiSqlDatabase)report.Dictionary.Databases["FUND_Prod"];

                    db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=ProductCube;User Id=sa;Password=dbsrv170@sdk;Connect Timeout=300";
                    db.PromptUserNameAndPassword = false;
                }
                //else if (db == null)
                //{
                //    db = (StiSqlDatabase)report.Dictionary.Databases["FUND_Prod"];

                //    db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=ProductCube;User Id=sa;Password=dbsrv170@sdk;Connect Timeout=300";
                //    db.PromptUserNameAndPassword = false;
                //}
                else
                {
                //StiSqlDatabase db = (StiSqlDatabase)report.Dictionary.Databases["FUND"];
                string connectionString = db.ConnectionString;

                SqlConnectionStringBuilder builder = new SqlConnectionStringBuilder(connectionString);
                string databaseName = builder.InitialCatalog;

               /* if (databaseName == "FundTest")
                {
                    db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=FundTest;User Id=sa;Password=dbsrv170@sdk;Persist Security Info=True;Integrated Security=False;Connect Timeout=300";
                    //((StiSqlSource)report.Dictionary.DataSources["IPMS"]).CommandTimeout = 6000;
                    db.PromptUserNameAndPassword = false;
                }
                else
                {*/

                    db.ConnectionString = "Data Source=192.168.0.181;Initial Catalog=FundManagement;User Id=sa;Password=dbsrv170@sdk;Persist Security Info=True;Integrated Security=False;Connect Timeout=300";
                    //((StiSqlSource)report.Dictionary.DataSources["IPMS"]).CommandTimeout = 6000;
                    db.PromptUserNameAndPassword = false;
                //}
               

                }

                report.Render();

                return StiMvcViewer.GetReportSnapshotResult(HttpContext, report);
            //}
            //catch(Exception ex)
            //{
            //    throw ex;
            //}
        }

        public ActionResult ViewerEvent()
        {
            return StiMvcViewer.ViewerEventResult();
        }

        public ActionResult PrintReport()
        {
            StiReport report = StiMvcViewer.GetReportObject();

            return StiMvcViewer.PrintReportResult();
        }

        public ActionResult ExportReport()
        {

            var settings = new Stimulsoft.Report.Export.StiPdfExportSettings();
            var service = new Stimulsoft.Report.Export.StiPdfExportService();

            return StiMvcViewer.ExportReportResult();
        }

        public ActionResult LCList(short? id)
        {
            var lclist = (from p in db.LCMains.AsEnumerable()
                          join q in db.LCAcceptances.AsEnumerable() on p.LCFileId equals q.LCFileId into x
                          from r in x.DefaultIfEmpty()
                          join s in db.LoanMains.AsEnumerable() on r.LCAcceptanceId equals s.LCAcceptanceId into y
                          from t in y.DefaultIfEmpty()
                          where p.LCFileId != 0
                          select new { p.LCFileId, p.IssuerBankId, LCNo = p.LCNo + r == null ? "" : " > " + r.AcceptanceNo + t == null ? "" : " > " + t.LoanNo }
                         ).Distinct().ToList();

            if (id != null && id > 0)
            {
                ViewBag.LC = new SelectList(lclist.Where(l => l.IssuerBankId == id), "LCFileId", "LCNo");
            }
            else
            {
                ViewBag.LC = new SelectList(lclist, "LCFileId", "LCNo");
            }

            return PartialView();
        }

        public ActionResult LoanList(short? id = 0)
        {
            var loanlist = (from p in db.LoanMains.AsEnumerable()
                            join q in db.Banks.AsEnumerable() on p.IssuerBankId equals q.BankId
                            join r in db.BankBranchs.AsEnumerable() on p.IssuerBranchId equals r.BranchId
                            select new { p.LoanId, p.LoanTypeId, LoanNo = p.LoanNo + " > " + q.ShortName + ", " + r.ShortName }
                            ).Distinct().ToList().Union(
                            (from p in db.ODMains.AsEnumerable()
                              join q in db.Banks.AsEnumerable() on p.IssuerBankId equals q.BankId
                              join r in db.BankBranchs.AsEnumerable() on p.IssuerBranchId equals r.BranchId
                              select new { LoanId = p.ODId, p.LoanTypeId, LoanNo = p.ODNo + " > " + q.ShortName + ", " + r.ShortName }
                            ).Distinct().ToList());

            if (id != null && id > 0)
            {
                ViewBag.Loans = new SelectList(loanlist.Where(l => l.LoanTypeId == id), "LoanId", "LoanNo");
            }
            else
            {
                ViewBag.Loans = new SelectList(loanlist, "LoanId", "LoanNo");
            }

            return PartialView();
        }

        public ActionResult GetLoanType(string lcat)
        {
            if (lcat == null || lcat.Length == 0)
            {
                ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName");
            }
            else
            {
                ViewBag.LoanType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && c.GroupName.Equals(lcat)), "TypeId", "TypeName");
            }

            return PartialView();
        }

        public ActionResult GetFactoryList(short? id)
        {
            if (id == null || id == 0)
            {
                ViewBag.FactoryId = new SelectList(dbProd.Factories, "FactoryId", "ShortName");
            }
            else
            {
                ViewBag.FactoryId = new SelectList(dbProd.Factories.Where(c => c.CompanyId == id), "FactoryId", "ShortName");
            }

            return PartialView();
        }
    }
}