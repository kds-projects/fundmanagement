﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;
using FundManagement.Models.DBFirst;

namespace FundManagement.Controllers.LC
{
    public class LCAcceptanceController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /LCAcceptance/
        public ActionResult Index()
        {
            var lcacceptances = db.LCAcceptances.Include(l => l.AcceptanceCurrency).Include(l => l.LCMain);
            return View(lcacceptances.ToList());
        }

        // GET: /LCAcceptance/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LCAcceptance lCAcceptance = db.LCAcceptances.Find(id);
            if (lCAcceptance == null)
            {
                return HttpNotFound();
            }
            return View(lCAcceptance);
        }

        // GET: /LCAcceptance/Create
        [CAuthorize]
        public ActionResult Create()
        {
            var lotlist = (from p in dbIMS.PipelineMains.AsEnumerable()
                           join t in dbIMS.PipelineSubs.AsEnumerable().Join(db.PISubs.AsEnumerable(), ps => ps.PIFileSubId, ls => ls.PIFileSubId, (ps, ls) => new { ps = ps, ls = ls }).AsEnumerable().GroupBy(c => c.ps.PLEId).Select(c => new { PLEId = c.Key, LotAmount = c.Sum(l => l.ps.StockInTransit * l.ls.UnitPrice) }) on p.PLEId equals t.PLEId
                           join q in db.LCAcceptances.AsEnumerable().GroupBy(c => c.PLEId).Select(c => new { PLEId = c.Key, AcceptAmount = c.Sum(l => l.AcceptanceAmount) }) on p.PLEId equals q.PLEId into r
                           from s in r.DefaultIfEmpty()
                           join u in db.LCMains.AsEnumerable() on p.LCFileId equals u.LCFileId
                           where (t.LotAmount - (s == null ? 0 : s.AcceptAmount)) > 0
                           select new { u.LCFileId, u.LCNo }
                          ).Distinct().ToList();

            ViewBag.Currency = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("CURCODE")), "TypeId", "TypeName");
            ViewBag.LCFile = new SelectList(lotlist, "LCFileId", "LCNo");
            ViewBag.LotList = new SelectList(dbIMS.PipelineMains.Where(c => c.LCFileId == 0), "PLEId", "LotNo");
            ViewBag.IsEdit = false;

            return View();
        }

        // POST: /LCAcceptance/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(LCAcceptanceView acceptanceView)
        {
            try
            {
                if (acceptanceView.LCAcceptanceId == 0)
                {
                    LCAcceptance acceptance = new LCAcceptance(acceptanceView);
                    //var lc = db.LCMains.Find(acceptance.LCFileId);
                    //acceptance.LoanTenure = lc.BankTenure;

                    db.LCAcceptances.Add(acceptance);
                }
                else
                {
                    LCAcceptance acceptance = db.LCAcceptances.Find(acceptanceView.LCAcceptanceId);
                    acceptance = (LCAcceptance)acceptanceView.ConvertNotNull(acceptance);
                    //var lc = db.LCMains.Find(acceptance.LCFileId);
                    //acceptance.LoanTenure = lc.BankTenure;

                    db.Entry(acceptance).State = EntityState.Modified;
                }

                db.SaveChanges();

                return Json(new { status = "Success", acceptno = acceptanceView.AcceptanceNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /LCAcceptance/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            ViewBag.Currency = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("CURCODE")), "TypeId", "TypeName");
            //ViewBag.LCFile = new SelectList(lotlist, "LCFileId", "LCNo");
            //ViewBag.LotList = new SelectList(dbIMS.PipelineMains.Where(c => c.LCFileId == 0), "PLEId", "LotNo");
            ViewBag.AcceptList = new SelectList(db.LCAcceptances.Include(c => c.LCMain).Select(p => new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }), "LCAcceptanceId", "AcceptanceNo");
            ViewBag.IsEdit = true;

            return View("Create");
        }

        // GET: /LCAcceptance/LandedCosting/5
        [CAuthorize]
        public ActionResult LandedCosting(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}
            //LCAcceptance lCAcceptance = db.LCAcceptances.Find(id);
            //if (lCAcceptance == null)
            //{
            //    return HttpNotFound();
            //}

            //ViewBag.Currency = new SelectList(db.CustomTypes.Where(l => l.Flag.Equals("CURCODE")), "TypeId", "TypeName");
            ////ViewBag.LCFile = new SelectList(lotlist, "LCFileId", "LCNo");
            ////ViewBag.LotList = new SelectList(dbIMS.PipelineMains.Where(c => c.LCFileId == 0), "PLEId", "LotNo");
            var landedcosingids = new List<int>() { };// db.LandedCostings.Select(c => c.LCAcceptanceId).ToList();
            //ViewBag.AcceptList = new SelectList(db.LCAcceptances.Include(c => c.LCMain).Where(c => !landedcosingids.Contains(c.LCAcceptanceId)).Select(p => new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }), "LCAcceptanceId", "AcceptanceNo");
            //ViewBag.AcceptList = new SelectList(
            //    db.LCAcceptances.Include(c => c.LCMain).Join(dbIMS.PipelineMains, accept => accept.PLEId, pipeline => pipeline.PLEId,
            //    (accept, pipeline) => new { accept = accept, pipeline = pipeline })
            //    .Where(c => c.accept.LandedCostingStatus == 0)
            //    .Select(p => new { p.accept.LCAcceptanceId, AcceptanceNo = p.accept.AcceptanceNo + " >> " + p.accept.LCMain.LCNo + " (" + p.pipeline.LotNo + ")" }), "LCAcceptanceId", "AcceptanceNo");
            //ViewBag.IsEdit = true;
            ViewBag.AcceptList = new SelectList(
                (from p in db.LCAcceptances.Include(c => c.LCMain).AsEnumerable()
                 join q in dbIMS.PipelineMains.AsEnumerable() on p.PLEId equals q.PLEId
                 where p.LandedCostingStatus == 0
                 select new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo + " (" + q.LotNo + ")" }
                ).ToList(), "LCAcceptanceId", "AcceptanceNo");

            return View(new LandedCosting());
        }

        [CAuthorize, HttpPost]
        public ActionResult LandedCostingSave(int pleid, LandedCosting cost, string mrrno, DateTime ardate, string cino, DateTime cidate, ICollection<PipelineView> pldet)
        {
            try
            {
                cost.CurrencyId = 23;
                var accept = db.LCAcceptances.Find(cost.LCAcceptanceId);
                cost.LCFileId = accept.LCFileId; cost.PLEId = accept.PLEId;
                if (cost.LandedCostingId == 0)
                {
                    LandedCosting costing = new LandedCosting(cost);

                    db.LandedCostings.Add(costing);
                }
                else
                {
                    LandedCosting costing = db.LandedCostings.Find(cost.LandedCostingId);
                    costing = (LandedCosting)cost.ConvertNotNull(costing);

                    db.Entry(costing).State = EntityState.Modified;
                }

                var plemain = dbIMS.PipelineMains.Find(pleid);
                plemain.MRRNo = mrrno; plemain.ActualReceiveDate = ardate;
                plemain.CommercialInvoiceNo = cino; plemain.CommercialInvoiceDate = cidate;

                dbIMS.Entry(plemain).State = EntityState.Modified;

                foreach(var sub in pldet)
                {
                    var plsub = dbIMS.PipelineSubs.Find(sub.PLESubId);
                    plsub.ActualReceivedQuantity = sub.ActualReceivedQuantity;

                    dbIMS.Entry(plsub).State = EntityState.Modified;
                }

                dbIMS.SaveChanges();

                db.SaveChanges();

                return Json(new { status = "Success", lcostno = accept.AcceptanceNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: LC
        [HttpPost, CAuthorize]
        public ActionResult CostingStatusUpdate(int id)
        {
            try
            {
                var accept = db.LCAcceptances.Find(id);
                accept.LandedCostingStatus = 1;
                db.Entry(accept).State = EntityState.Modified;

                var pipeline = dbIMS.PipelineMains.Find(accept.PLEId);

                var grr_subs = (from p in dbIMS.GRRSubs.Where(c => c.GRRMain.PLEId == accept.PLEId).AsEnumerable()
                                join q in db.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                                where p.GRRQuantity > 0
                                select new { GRRSubId = p.GRRSubId, GRRQuantity = p.GRRQuantity, GRRValue = p.GRRQuantity * q.UnitPrice, UnitPrice = q.UnitPrice, p.GRRMain.GRRDate, p.ProductId }
                               ).ToList();

                if (grr_subs != null)
                {
                    decimal acp_com_rate = 0, lc_com_rate = 0, libor_rate = 0, handling_rate = 0;
                    decimal total_grr_qty = 0, total_grr_value = 0;
                    var cost = db.LandedCostings.Where(l => l.LCAcceptanceId == id).First();
                    var pi_list = db.LCWisePIs.Where(c => c.LCFileId == accept.LCMain.LCFileId).Select(c => c.PIFileId).ToList();
                    var saction = db.CSSubs.Where(c => pi_list.Contains(c.CSMain.PIFileId)).First();

                    libor_rate = accept.LiborInterestRate;
                    handling_rate = accept.HCInterestRate;

                    if (accept.LCMain.IssuerBankId == saction.BankId1)
                    {
                        acp_com_rate = saction.AcceptanceCommissionRate1;
                        lc_com_rate = saction.LCCommissionRate1;
                        if (libor_rate == 0)
                        {
                            libor_rate = saction.Libor1;
                        }
                        if (handling_rate == 0)
                        {
                            handling_rate = saction.Handling1;
                        }
                    }
                    else if (accept.LCMain.IssuerBankId == saction.BankId2)
                    {
                        acp_com_rate = saction.AcceptanceCommissionRate2;
                        lc_com_rate = saction.LCCommissionRate2;
                        if (libor_rate == 0)
                        {
                            libor_rate = saction.Libor2;
                        }
                        if (handling_rate == 0)
                        {
                            handling_rate = saction.Handling2;
                        }
                    }
                    else if (accept.LCMain.IssuerBankId == saction.BankId3)
                    {
                        acp_com_rate = saction.AcceptanceCommissionRate3;
                        lc_com_rate = saction.LCCommissionRate3;
                        if (libor_rate == 0)
                        {
                            libor_rate = saction.Libor3;
                        }
                        if (handling_rate == 0)
                        {
                            handling_rate = saction.Handling3;
                        }
                    }

                    total_grr_qty = grr_subs.Select(c => c.GRRQuantity).Sum();
                    total_grr_value = grr_subs.Select(c => c.GRRValue).Sum();

                    //foreach (var _sub in grr_subs.Select(c => c.GRRQuantity).Sum())
                    //{
                    //    total_grr_qty += _sub.GRRQuantity;
                    //    total_grr_value += _sub.GRRValue;
                    //}

                    var sub_ids = grr_subs.Select(c => c.GRRSubId).ToList();

                    var accept_tenure = accept.AcceptanceTenure; // accept.AcceptanceTenure >= 180 ? 180 : accept.AcceptanceTenure;

                    foreach (var sub_id in sub_ids)
                    {
                        var sub = dbIMS.GRRSubs.Find(sub_id);
                        var _sub = grr_subs.Where(c => c.GRRSubId == sub_id).FirstOrDefault();
                        var CIValue = (cost.CostValue * _sub.GRRValue) / total_grr_value;
                        var TotalLandedCosting = cost.LCOpeningCommission + cost.LCOpeningCommissionVAT + cost.SwiftCharges + cost.StampAndStationery + cost.PostageAndCourier +
                                            cost.AmendmentCharges + cost.CreditReportCharges + cost.ShippingGuaranteeCommission + cost.CNFCost + cost.InsurancePremium +
                                            cost.AcceptanceCommission + cost.Duty + cost.AIT + (((((accept.AcceptanceAmount * libor_rate) / 100) * accept_tenure) / 360) * accept.BDTRate) +
                                            (((((accept.AcceptanceAmount * handling_rate) / 100) * accept_tenure) / 360) * accept.BDTRate) +
                                            cost.FreightCharges + cost.Margin + cost.SwiftChargesAcceptanceCommission;
                        sub.LandedPriceBDT = Convert.ToDecimal((((CIValue) + ((TotalLandedCosting * CIValue) / cost.CostValue)) / _sub.GRRQuantity).ToString("##0.0000"));
                        sub.ImportPriceFC = _sub.UnitPrice;
                        sub.CurrencyId = accept.CurrencyId;

                        dbIMS.Entry(sub).State = EntityState.Modified;
                        dbIMS.SaveChanges();
                    }
                }

                var grr_date = grr_subs.FirstOrDefault().GRRDate;
                var product_list = grr_subs.Select(c => c.ProductId.ToString()).Aggregate((i, j) => i + ", " + j);

                db.SaveChanges();

                dbIMS.Database.ExecuteSqlCommand("Exec prcUpdateOnwardsRate 0, '" + grr_date.ToString() + "', '" + product_list + "'");

                return Json(new { status = "Success", lcostno = accept.LCMain.LCNo + " (" + pipeline.LotNo + ")" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /LCAcceptance/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LCAcceptance lCAcceptance = db.LCAcceptances.Find(id);
            if (lCAcceptance == null)
            {
                return HttpNotFound();
            }
            return View(lCAcceptance);
        }

        // POST: /LCAcceptance/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            LCAcceptance lCAcceptance = db.LCAcceptances.Find(id);
            db.LCAcceptances.Remove(lCAcceptance);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: LC
        public ActionResult LotList(int id, string type = "create")
        {
            if (type.ToLower().Equals("edit"))
            {
                var idlist = (from p in db.LCAcceptances.AsEnumerable()
                              join q in dbIMS.PipelineMains.AsEnumerable() on p.PLEId equals q.PLEId
                              select p
                             ).Distinct();

                ViewBag.LotList = new SelectList(db.LCAcceptances.Where(l => l.LCFileId == id), "PLEId", "LotNo");

                return PartialView();
            }
            else
            {
                //var lotlist = new[] { new { PLEId = id, LotNo = "FULL" } };
                var lotlist = (from p in dbIMS.PipelineMains.AsEnumerable()
                               join t in dbIMS.PipelineSubs.AsEnumerable().Join(db.PISubs.AsEnumerable(), ps => ps.PIFileSubId, ls => ls.PIFileSubId, (ps, ls) => new { ps = ps, ls = ls }).AsEnumerable().GroupBy(c => c.ps.PLEId).Select(c => new { PLEId = c.Key, LotAmount = c.Sum(l => l.ps.StockInTransit * l.ls.UnitPrice) }) on p.PLEId equals t.PLEId
                               join q in db.LCAcceptances.AsEnumerable().GroupBy(c => c.PLEId).Select(c => new { PLEId = c.Key, AcceptAmount = c.Sum(l => l.AcceptanceAmount) }) on p.PLEId equals q.PLEId into r
                               from s in r.DefaultIfEmpty()
                               where p.LCFileId == id && (t.LotAmount - (s == null ? 0 : s.AcceptAmount)) > 0
                               select p
                              ).Distinct();
                ViewBag.PLEId = new SelectList(lotlist, "PLEId", "LotNo");

                return PartialView();
            }
        }

        // GET: LC
        public ActionResult LotDetails(int id)
        {
            try
            {
                //var pipeline = db.LCMains.Find(id);
                //var acceptval = db.LCAcceptances.Where(l => l.LCFileId == pipeline.LCFileId).Select(c => c.AcceptanceAmount).DefaultIfEmpty(0).Sum();

                //if (pipeline == null)
                //    throw new Exception("Invalid lot no.");

                //return Json(new { status = "Success", plamt = (pipeline.LCAmount - acceptval).ToString("##,##0.00"), bldate = DateTime.Today.ToString("dd-MMM-yyyy"), 
                //    bedate = DateTime.Today.ToString("dd-MMM-yyyy"), blno = "-", beno = "-", curid = pipeline.CurrencyId }, JsonRequestBehavior.AllowGet);

                var pipeline = dbIMS.PipelineMains.Find(id);
                var lc = db.LCMains.Find(pipeline.LCFileId);
                var acceptval = db.LCAcceptances.AsEnumerable().Where(l => l.PLEId == pipeline.PLEId).Select(c => c.AcceptanceAmount).DefaultIfEmpty(0).Sum();
                var pi_ids = db.LCWisePIs.Where(c => c.LCFileId == lc.LCFileId).Select(c => c.PIFileId).Distinct();
                var cs_details = db.CSSubs.Where(c => pi_ids.Contains(c.CSMain.PIFileId)).FirstOrDefault();

                CSSub cs_rate = new CSSub();
                if (cs_details.BankId1 == lc.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor1, Handling1 = cs_details.Handling1, UPASS1 = cs_details.UPASS1, PostUPASS1 = cs_details.PostUPASS1 };
                else if (cs_details.BankId2 == lc.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor2, Handling1 = cs_details.Handling2, UPASS1 = cs_details.UPASS2, PostUPASS1 = cs_details.PostUPASS2 };
                else if (cs_details.BankId3 == lc.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor3, Handling1 = cs_details.Handling3, UPASS1 = cs_details.UPASS3, PostUPASS1 = cs_details.PostUPASS3 };

                var pl = (from p in dbIMS.PipelineSubs.AsEnumerable()
                          join q in db.PISubs.AsEnumerable() on p.PIFileSubId equals q.PIFileSubId
                          where p.PLEId == id
                          select new { p.StockInTransit, q.UnitPrice }
                    ).Sum(c => c.StockInTransit * c.UnitPrice);

                if (pipeline == null)
                    throw new Exception("Invalid lot no.");

                var libor = (pl * cs_rate.Libor1 * cs_rate.UPASS1) / 36000;
                var handling = (pl * cs_rate.Handling1 * cs_rate.UPASS1) / 36000;

                return Json(new { status = "Success", plamt = (pl - acceptval).ToString("##,##0.00"), bldate = pipeline.BLDate.ToString("dd-MMM-yyyy"), grdate = pipeline.LotReceivedDate.ToString("dd-MMM-yyyy"),
                    bedate = pipeline.BillOfEntryDate.ToString("dd-MMM-yyyy"), blno = "-", beno = pipeline.BillOfEntryNo ?? "-", curid = lc.CurrencyId, LoanTenure = lc.BankTenure, libor = libor, 
                    handling = handling, libor_rate = cs_rate.Libor1, handling_rate = cs_rate.Handling1, upass = cs_rate.UPASS1, post_upass = cs_rate.PostUPASS1 }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: LC
        public ActionResult AcceptanceDetails(int id)
        {
            try
            {
                var accept = (from p in db.LCAcceptances.AsEnumerable()
                              join r in dbIMS.PipelineMains.AsEnumerable() on p.PLEId equals r.PLEId into s
                              from q in s.DefaultIfEmpty()
                              where p.LCAcceptanceId == id
                              select new { p, LotNo = q == null ? "-" : q.LotNo, BENo = q == null ? "-" : q.BillOfEntryNo, 
                                  BEDate = q == null ? DateTime.Parse("01-Jan-1900") : q.BillOfEntryDate, 
                                  BLDate = q == null ? DateTime.Parse("01-Jan-1900") : q.BLDate, BLNo = "-" }
                             ).Distinct().FirstOrDefault();

                var lcfileid = accept.p.LCFileId;
                var pi_ids = db.LCWisePIs.Where(c => c.LCFileId == lcfileid).Select(c => c.PIFileId).Distinct();
                var cs_details = db.CSSubs.Where(c => pi_ids.Contains(c.CSMain.PIFileId)).FirstOrDefault();

                CSSub cs_rate = new CSSub();
                if (cs_details.BankId1 == accept.p.LCMain.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor1, Handling1 = cs_details.Handling1, UPASS1 = cs_details.UPASS1, PostUPASS1 = cs_details.PostUPASS1 };
                else if (cs_details.BankId2 == accept.p.LCMain.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor2, Handling1 = cs_details.Handling2, UPASS1 = cs_details.UPASS2, PostUPASS1 = cs_details.PostUPASS2 };
                else if (cs_details.BankId3 == accept.p.LCMain.IssuerBankId)
                    cs_rate = new CSSub() { Libor1 = cs_details.Libor3, Handling1 = cs_details.Handling3, UPASS1 = cs_details.UPASS3, PostUPASS1 = cs_details.PostUPASS3 };

                return Json(new
                {
                    status = "Success",
                    acp = new
                    {
                        LCFileId = accept.p.LCFileId,
                        LCNo = accept.p.LCMain.LCNo,
                        PLEId = accept.p.PLEId,
                        LotNo = accept.LotNo,
                        AcceptanceNo = accept.p.AcceptanceNo,
                        TenureStartDate = accept.p.TenureStartDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.TenureStartDate.ToString("dd-MMM-yyyy"),
                        AcceptanceDate = accept.p.AcceptanceDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.AcceptanceDate.ToString("dd-MMM-yyyy"),
                        ExpireDate = accept.p.ExpireDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.ExpireDate.ToString("dd-MMM-yyyy"),
                        AcceptanceTenure = accept.p.AcceptanceTenure,
                        DocumentRetirementDate = accept.p.DocumentRetirementDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.DocumentRetirementDate.ToString("dd-MMM-yyyy"),
                        BankReceiveDate = accept.p.BankReceiveDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.BankReceiveDate.ToString("dd-MMM-yyyy"),
                        HandoverToCNFDate = accept.p.HandoverToCNFDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.HandoverToCNFDate.ToString("dd-MMM-yyyy"),
                        HandoverToSourcingDate = accept.p.HandoverToSourcingDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.p.HandoverToSourcingDate.ToString("dd-MMM-yyyy"),
                        BLNo = accept.BLNo,
                        BLDate = accept.BLDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.BLDate.ToString("dd-MMM-yyyy"),
                        BENo = accept.BENo,
                        BEDate = accept.BEDate.ToString("dd-MMM-yyyy").Equals("01-Jan-1900") ? "" : accept.BEDate.ToString("dd-MMM-yyyy"),
                        CurrencyId = accept.p.CurrencyId,
                        AcceptanceAmount = accept.p.AcceptanceAmount,
                        LoanTenure = accept.p.LoanTenure,
                        BDTRate = accept.p.BDTRate,
                        AmountInBDT = accept.p.AmountInBDT,
                        USDRate = accept.p.USDRate,
                        AmountInUSD = accept.p.AmountInUSD,
                        LiborAmount = accept.p.LiborAmount,  // (accept.p.AcceptanceAmount * cs_rate.Libor1 * accept.p.AcceptanceTenure) / 36000,
                        LiborBDTRate = accept.p.BDTRate,
                        LiborBDT = accept.p.BDTRate * accept.p.LiborAmount,  // ((accept.p.AcceptanceAmount * cs_rate.Libor1 * accept.p.AcceptanceTenure) / 36000),
                        HandlingCharge = accept.p.HandlingCharge,  // (accept.p.AcceptanceAmount * cs_rate.Handling1 * accept.p.AcceptanceTenure) / 36000,
                        HCBDTRate = accept.p.BDTRate,
                        HCBDT = accept.p.BDTRate * accept.p.HandlingCharge,  // ((accept.p.AcceptanceAmount * cs_rate.Handling1 * accept.p.AcceptanceTenure) / 36000),
                        libor_rate = accept.p.LiborInterestRate, 
                        handling_rate = accept.p.HCInterestRate,
                        Remarks = accept.p.Remarks
                    }
                }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: LC
        public ActionResult LandedCostingDetails(int id)
        {
            try
            {
                var accept = (from p in db.LCAcceptances.AsEnumerable()
                              join r in db.LandedCostings.AsEnumerable() on p.LCAcceptanceId equals r.LCAcceptanceId into s
                              from q in s.DefaultIfEmpty()
                              where p.LCAcceptanceId == id
                              select q
                             ).Distinct().FirstOrDefault();

                var pleid = db.LCAcceptances.Find(id).PLEId;
                var plmain = dbIMS.PipelineMains.Find(pleid);

                if (accept == null)
                {
                    return Json(new
                    {
                        status = "Success",
                        cino = plmain.CommercialInvoiceNo,
                        cidate = plmain.CommercialInvoiceDate,
                        arno = plmain.MRRNo,
                        ardate = plmain.ActualReceiveDate,
                        attachments = plmain.Attachments, 
                        acp = new LandedCosting()
                        {
                            LandedCostingId = 0,
                            LCFileId = db.LCAcceptances.Find(id).LCFileId,
                            PLEId = db.LCAcceptances.Find(id).PLEId
                        }
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new
                    {
                        status = "Success",
                        cino = plmain.CommercialInvoiceNo,
                        cidate = plmain.CommercialInvoiceDate.ToString("dd-MMM-yyyy"),
                        arno = plmain.MRRNo,
                        ardate = plmain.ActualReceiveDate.ToString("dd-MMM-yyyy"),
                        attachments = plmain.Attachments, 
                        acp = new LandedCosting()
                        {
                            LandedCostingId = accept.LandedCostingId,
                            LCFileId = accept.LCFileId,
                            PLEId = accept.PLEId,
                            AmendmentCharges = accept.AmendmentCharges,
                            AIT = accept.AIT,
                            AcceptanceCommission = accept.AcceptanceCommission,
                            SwiftChargesAcceptanceCommission = accept.SwiftChargesAcceptanceCommission,
                            BerthOperatorCharge = accept.BerthOperatorCharge,
                            CNFAIT = accept.CNFAIT,
                            //CNFCost = accept.CNFCost,
                            CostValue = accept.CostValue,
                            CreditReportCharges = accept.CreditReportCharges,
                            CustomJettyExpenses = accept.CustomJettyExpenses,
                            DonationFees = accept.DonationFees,
                            Duty = accept.Duty,
                            ExcessGoods = accept.ExcessGoods,
                            FreightCharges = accept.FreightCharges,
                            LIBOR = accept.LIBOR,
                            HandlingCharge = accept.HandlingCharge,
                            HystarCharge = accept.HystarCharge,
                            InsurancePremium = accept.InsurancePremium,
                            LabourCharge = accept.LabourCharge,
                            LCOpeningCommission = accept.LCOpeningCommission,
                            LCOpeningCommissionVAT = accept.LCOpeningCommissionVAT,
                            Margin = accept.Margin,
                            MiscellaneousExpenses = accept.MiscellaneousExpenses,
                            OnlineCharge = accept.OnlineCharge,
                            PortCharges = accept.PortCharges,
                            PostageAndCourier = accept.PostageAndCourier,
                            RCCCharge = accept.RCCCharge,
                            ScanningCharge = accept.ScanningCharge,
                            ServiceCharge = accept.ServiceCharge,
                            ShippingAgentCharges = accept.ShippingAgentCharges,
                            ShippingGuaranteeCommission = accept.ShippingGuaranteeCommission,
                            StampAndStationery = accept.StampAndStationery,
                            SwiftCharges = accept.SwiftCharges,
                            TruckFare = accept.TruckFare,
                            VAT = accept.VAT,
                            Wharfrent = accept.Wharfrent,
                            Remarks = accept.Remarks,
                            Depocharges=accept.Depocharges,
                            ContainerRepairCharges=accept.ContainerRepairCharges,
                            CusTomDuty=accept.CusTomDuty,
                            RegularDuty=accept.RegularDuty,
                            SupplementaryDuty=accept.SupplementaryDuty,
                            ValueAddedTax=accept.ValueAddedTax,
                            AdvanceTax=accept.AdvanceTax                          

            }
                    }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        public ActionResult RowTemplate(int pleid)
        {
            var items = (from p in db.PISubs.AsEnumerable()
                         join q in dbProd.ItemInfoes.AsEnumerable() on p.ProductId equals q.ItemId
                         join r in dbProd.viewQuantityUnits.AsEnumerable() on p.QuantityUnitId equals r.QuantityUnitId
                         join s in dbIMS.PipelineSubs.AsEnumerable() on p.PIFileSubId equals s.PIFileSubId
                         join u in dbIMS.PRSubs.AsEnumerable() on p.PRSubId equals u.PRSubId
                         where s.PLEId == pleid
                         select new { p, q, r, s, u.FactoryETA }
                        ).Select(c => new PipelineView()
                        {
                            CategoryId = c.q.CategoryId,
                            ItemId = c.q.ItemId,
                            ItemName = c.q.ItemName,
                            MRRNo = c.s.PipelineMain.MRRNo,
                            ActualReceiveDate = c.s.PipelineMain.ActualReceiveDate,
                            ActualReceivedQuantity = c.s.ActualReceivedQuantity,
                            QuantityUnit = c.r.QuantityUnit,
                            QuantityUnitId = c.r.QuantityUnitId,
                            StockInTransit = c.s.StockInTransit,
                            UnitPrice = c.p.UnitPrice,
                            PipelineValue = c.s.StockInTransit * c.p.UnitPrice,
                            PIFileSubId = c.p.PIFileSubId,
                            PLEId = c.s.PLEId,
                            PLESubId = c.s.PLESubId
                        }).ToList();

            return PartialView(items);
        }

        public ActionResult ReceiveDetails(int pleid)
        {
            var pl = dbIMS.PipelineMains.Find(pleid);

            return Json(new
            {
                rcvdate = pl.ActualReceiveDate,
                mrrno = pl.MRRNo,
                tqty = pl.PipelineSubs.Sum(c => c.ActualReceivedQuantity).ToString("##,##0.00")
            }, JsonRequestBehavior.AllowGet);
        }

        public ActionResult ReasonTemplate()
        {
            var plist = (from p in dbIMS.PipelineMains.AsEnumerable()
                         join q in db.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                         join r in db.LCAcceptances.AsEnumerable() on p.PLEId equals r.PLEId into s
                         from t in s.DefaultIfEmpty()
                         where p.IsMailSentForBankRetirement == 1 && t == null
                         select new { pleid = p.PLEId, pleno = q.LCNo + " (" + p.LotNo + ")" }
                        ).Distinct();

            ViewBag.PipelineId = new SelectList(plist, "pleid", "pleno");

            return PartialView();
        }

        public ActionResult ReasonDetails(int pleid)
        {
            var pl = dbIMS.PipelineMains.Find(pleid);

            return Json(new
            {
                reason = pl.ReasonForLateBankRetirement
            }, JsonRequestBehavior.AllowGet);
        }

        [CAuthorize]
        public ActionResult UpdateReason(int pleid, string reason = "")
        {
            var pl = dbIMS.PipelineMains.Find(pleid);

            pl.ReasonForLateBankRetirement = reason;

            dbIMS.Entry(pl).State = EntityState.Modified;

            dbIMS.SaveChanges();

            return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
        }
    }
}
