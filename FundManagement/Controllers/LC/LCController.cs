﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;
using System.Web.Script.Serialization;
using Newtonsoft.Json;

namespace FundManagement.Controllers
{
    public class LCController : ContextList
    {
        // GET: /LCMain/
        public ActionResult Index()
        {
            //var lcmains = db.LCMains.Include(l => l.BTenureType).Include(l => l.Currency).Include(l => l.IssuerBank).Include(l => l.IssuerBranch).Include(l => l.NegotiationBank).Include(l => l.STenureType).Include(l => l.SupplierBank);
            var lcmains = (from p in db.LCMains.AsEnumerable()
                           join q in db.Banks.AsEnumerable() on p.IssuerBankId equals q.BankId
                           join r in db.BankBranchs.AsEnumerable() on p.IssuerBranchId equals r.BranchId
                           join s in dbIMS.Suppliers.AsEnumerable() on p.SupplierId equals s.SupplierId
                           join t in dbProd.Companies.AsEnumerable() on p.BeneficiaryId equals t.CompanyId
                           join u in dbIMS.Countries.AsEnumerable() on p.OriginCountryId equals u.CountryId
                           join v in dbLC.Ports.AsEnumerable() on p.POLId equals v.PortId
                           join w in db.CustomTypes.AsEnumerable() on p.CurrencyId equals w.TypeId
                           select new { p, q, r, s, t, u, v, w }
                          ).Select(c => new LCMainView()
                          {
                              LCFileId = c.p.LCFileId,
                              LCFileNo = c.p.LCFileNo,
                              LCNo = c.p.LCNo,
                              LCDate = c.p.LCDate,
                              IssuerBank = c.q.ShortName,
                              IssuerBranch = c.r.ShortName,
                              Applicant = c.t.ShortName,
                              Supplier = c.s.ShortName,
                              Origin = c.u.CountryName,
                              PortName = c.v.PortName,
                              ShipmentDate = c.p.ShipmentDate,
                              ExpireDate = c.p.ExpireDate,
                              LCAmount = c.p.LCAmount,
                              CoverNoteNo = c.p.CoverNoteNo,
                              CurrencyType = c.w.TypeName
                          }).Distinct();

            return View(lcmains.ToList());
        }

        // GET: /LCMain/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LCMain lCMain = db.LCMains.Find(id);
            if (lCMain == null)
            {
                return HttpNotFound();
            }
            return View(lCMain);
        }

        // GET: /LCMain/Create
        [CAuthorize]
        public ActionResult Create()
        {
            var tt = db.CustomTypes.Where(c => c.Flag.Equals("TENURETYPE"));
            ViewBag.BTenureType = new SelectList(tt, "TypeId", "TypeName", 52);
            ViewBag.Currency = new SelectList(db.CustomTypes, "TypeId", "TypeName");
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs, "BranchId", "BranchName");
            ViewBag.NegotiatingBankList = new SelectList(db.LCMains.Select(c => new { c.NegotiatingBank }).Distinct(), "NegotiatingBank", "NegotiatingBank");
            ViewBag.STenureType = new SelectList(tt, "TypeId", "TypeName");
            ViewBag.BeneficiaryBankList = new SelectList(db.LCMains.Select(c => new { c.BeneficiaryBank }).Distinct(), "BeneficiaryBank", "BeneficiaryBank");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.Supplier = new SelectList(dbIMS.Suppliers, "SupplierId", "SupplierName");
            ViewBag.Agent = new SelectList(db.LocalAgents, "AgentId", "AgentName");
            ViewBag.Country = new SelectList(dbIMS.Countries, "CountryId", "CountryName");
            ViewBag.POL = new SelectList(db.Ports, "PortId", "PortName");
            ViewBag.PIFileList = new MultiSelectList(db.PIMains.Where(l => l.IsLCOpen == 0 && l.IsForwardToBank == 1), "PIFileId", "PINo");
            //ViewBag.PIFileList = new MultiSelectList(dbLC.PIMains.Where(l => l.PIFileNo.Equals("PI0331/17") || l.PIFileNo.Equals("PI0355/17")), "PIFileId", "PINo");

            return View();
        }

        // POST: /LCMain/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(LCMainView lcMainView, LCSub lcSub)
        {
            try
            {
                LCMain lcmain = new LCMain(lcMainView);
                lcmain.LCFileNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('LC', '" + lcmain.LCDate.Year + "', '" + lcmain.LCDate.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();
                lcmain.LCType = "IMPORT";
                LCSub lcsub = new LCSub();
                lcsub = (LCSub)lcSub.Convert(lcsub);
                lcsub.AmendmentNo = "-";
                lcsub.AmendmentDate = DateTime.Parse("01-Jan-1900");
                
                if (lcMainView.PIList != null)
                {
                    foreach (var id in lcMainView.PIList)
                    {
                        LCWisePI lcpi = new LCWisePI();
                        lcpi.PIFileId = int.Parse(id);
                        lcsub.LCWisePIs.Add(lcpi);

                        PIMain pimain = db.PIMains.Find(lcpi.PIFileId);
                        pimain.IsLCOpen = 1; 
                        pimain.LCOpenDate = lcmain.LCDate;
                        db.Entry(pimain).State = EntityState.Modified;
                    }
                }

                lcmain.LCSubs.Add(lcsub);

                db.LCMains.Add(lcmain);

                db.SaveChanges();
                
                //return View(pimain);
                return Json(new { status = "Success", lcno = lcmain.LCFileNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /LCMain/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LCMain lCMain = db.LCMains.Find(id);
            if (lCMain == null)
            {
                return HttpNotFound();
            }

            var pilist = lCMain.LCWisePIs.Select(c => c.PIFileId).ToList();

            var tt = db.CustomTypes.Where(c => c.Flag.Equals("TENURETYPE"));
            ViewBag.BTenureType = new SelectList(tt, "TypeId", "TypeName", lCMain.BTenureTypeId);
            ViewBag.Currency = new SelectList(db.CustomTypes, "TypeId", "TypeName", lCMain.CurrencyId);
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName", lCMain.IssuerBankId);
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs, "BranchId", "BranchName", lCMain.IssuerBranchId);
            ViewBag.NegotiatingBankList = new SelectList(db.Banks, "BankId", "BankName", lCMain.NegotiatingBank);
            ViewBag.STenureType = new SelectList(tt, "TypeId", "TypeName", lCMain.STenureTypeId);
            ViewBag.BeneficiaryBankList = new SelectList(db.Banks, "BankId", "BankName", lCMain.BeneficiaryBank);
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", lCMain.BeneficiaryId);
            ViewBag.Supplier = new SelectList(dbIMS.Suppliers, "SupplierId", "SupplierName", lCMain.SupplierId);
            ViewBag.Agent = new SelectList(db.LocalAgents, "AgentId", "AgentName", lCMain.LocalAgentId);
            ViewBag.Country = new SelectList(dbIMS.Countries, "CountryId", "CountryName", lCMain.OriginCountryId);
            ViewBag.POL = new SelectList(db.Ports, "PortId", "PortName", lCMain.POLId);
            ViewBag.PIFileList = new MultiSelectList(db.PIMains.Where(l => (l.IsLCOpen == 0 && l.IsForwardToBank == 1) || pilist.Contains(l.PIFileId)), "PIFileId", "PINo", pilist.ToArray());

            ViewBag.LCSub = lCMain.LCSubs.Select(c => new { LCFileId = c.LCFileId, LCSubId = c.LCSubId, AmendmentTypeId = c.AmendmentTypeId.ToString(), AmendmentNo = c.AmendmentNo, AmendmentDate = c.AmendmentDate.ToString("dd-MMM-yyyy") });
            
            var lcview = (LCMainView)lCMain.Convert(new LCMainView());
            return View("Create", lcview);
        }

        // GET: /LCMain/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LCMain lCMain = db.LCMains.Find(id);
            if (lCMain == null)
            {
                return HttpNotFound();
            }
            return View(lCMain);
        }

        // POST: /LCMain/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            LCMain lCMain = db.LCMains.Find(id);
            db.LCMains.Remove(lCMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        public ActionResult PIDetails(string[] id)
        {
            if (id != null && id.Count() > 0)
            {
                List<int> PIList = id.Select(c => int.Parse(c.Trim())).ToList();
                short i = 0;
                var pidetails = (from p in db.PIMains.AsEnumerable()
                                //join q in dbIMS.Suppliers.AsEnumerable() on p.SupplierId equals q.SupplierId
                                //join r in dbProd.Companies.AsEnumerable() on p.BeneficiaryId equals r.CompanyId
                                //join s in dbLC.LocalAgents.AsEnumerable() on p.LocalAgentId equals s.AgentId
                                //join t in dbLC.Ports.AsEnumerable() on p.POLId equals t.PortId
                                //join u in dbLC.Countries.AsEnumerable() on p.OriginCountryId equals u.CountryId
                                where PIList.Contains(p.PIFileId)
                                select new { p.BeneficiaryId, p.SupplierId, p.LocalAgentId, p.POLId, p.OriginCountryId, p.ForwardBankId, p.ForwardBranchId }
                               ).Distinct();

                if(pidetails.Count() == 1)
                {
                    var pimain = pidetails.FirstOrDefault();
                    return Json(new { status = "Success", BeneficiaryId = pimain.BeneficiaryId, SupplierId = pimain.SupplierId, LocalAgentId = pimain.LocalAgentId, POLId = pimain.POLId, OriginCountryId = pimain.OriginCountryId, BankId = pimain.ForwardBankId, BranchId = pimain.ForwardBranchId }, JsonRequestBehavior.AllowGet);
                }
                
                return Json(new { status = "Error", message = "Different benificiary or supplier." }, JsonRequestBehavior.AllowGet);
            }
            else
                return PartialView();
        }

        public ActionResult PIProductList(string[] id)
        {
            if (id != null && id.Count() > 0)
            {
                List<int> PIList = id.Select(c => int.Parse(c.Trim())).ToList();
                short i = 0;
                var itemList = (from p in db.PIMains.AsEnumerable()
                                join q in db.PISubs.AsEnumerable() on p.PIFileId equals q.PIFileId
                                join r in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals r.ItemId
                                join s in dbIMS.PRSubs.AsEnumerable() on q.PRSubId equals s.PRSubId
                                join t in dbProd.Variant_Value.AsEnumerable() on q.QuantityUnitId equals t.VarValueId
                                join u in db.CustomTypes.AsEnumerable() on p.CurrencyId equals u.TypeId
                                where PIList.Contains(p.PIFileId)
                                select new { p, q, r, PRNo = s.PRMain.PRNo, s.FactoryETA, QuantityUnit = t.VariantValue, CurrencyId = u.TypeId }
                               ).Select(c => new PIWiseItemDetails()
                               {
                                   PRNo = c.PRNo,
                                   ItemName = c.r.ItemName,
                                   HSCode = c.r.HSCode,
                                   FactoryETA = c.FactoryETA,
                                   Quantity = c.q.PIQuantity,
                                   QuantityUnit = c.QuantityUnit,
                                   UnitPrice = c.q.UnitPrice,
                                   CurrencyId = c.CurrencyId,
                                   Currency = c.p.CurrencyType,
                                   SLNo = ++i
                               }).ToList();

                return PartialView(itemList);
            }
            else
                return PartialView();
        }
    }
}
