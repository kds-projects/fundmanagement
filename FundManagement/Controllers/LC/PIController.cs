﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class PIController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /PI/
        public ActionResult Index()
        {
            return View(db.PIMains.Include(c => c.Currency).ToList());
        }

        [CAuthorize]
        public ActionResult PIInHand()
        {
            //var pimains = db.PIMains.Where(l => l.IsForwardToBank == 0).ToList();
            return View(db.PIMains.Include(c => c.Currency).Where(l => l.IsForwardToBank == 0));
        }

        [CAuthorize]
        public ActionResult CSPending()
        {
            //var pimains = db.PIMains.Where(l => l.IsForwardToBank == 0).ToList();
            return View(db.PIMains.Include(c => c.Currency).Where(l => l.IsForwardToBank == 0 && l.IsCSDone == 0));
        }

        [CAuthorize]
        public ActionResult CSForLC(int? id)
        {
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "ShortName");
            ViewBag.PIList = new SelectList(db.PIMains.Include(c => c.Currency).Where(l => l.IsForwardToBank == 0), "PIFileId", "PINo", id);

            return View(db.PIMains.Find(id) ?? new PIMain());
        }

        [CAuthorize]
        public ActionResult ForwardPending()
        {
            //var pimains = db.PIMains.Where(l => l.IsForwardToBank == 0).ToList();
            return View(db.PIMains.Include(c => c.Currency).Where(l => l.IsForwardToBank == 0 && l.IsCSDone == 1));
        }

        [CAuthorize]
        public ActionResult ForwardToBank(int id)
        {
            PIMain pimain = db.PIMains.Find(id);
            pimain.ForwardToBankDate = clsMain.getCurrentTime();

            if (pimain == null && pimain.IsCSDone != 1)
            {
                return HttpNotFound();
            }

            var banks = db.CSMains.Where(c => c.PIFileId == id).First().CSSubs.First();
            var bids = new List<short>();
            if (banks.BankId1 > 0)
                bids.Add(banks.BankId1);
            if (banks.BankId2 > 0)
                bids.Add(banks.BankId2);
            if (banks.BankId3 > 0)
                bids.Add(banks.BankId3);

            ViewBag.BankId = new SelectList(db.Banks.Where(c => bids.Contains(c.BankId)), "BankId", "BankName");
            ViewBag.BranchId = new SelectList(db.BankBranchs.Where(l => l.BankId == 0), "BranchId", "BranchName");

            return View(pimain);
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult Save(PIMain pimain)
        {
            try
            {
                PIMain fpimain = db.PIMains.Find(pimain.PIFileId);
                if (fpimain == null)
                {
                    return Json(new { status = "Error", message = "Invalid pi number" }, JsonRequestBehavior.AllowGet);
                }

                fpimain.ForwardToBankDate = pimain.ForwardToBankDate;
                fpimain.IsForwardToBank = 1;
                fpimain.ForwardUserId = clsMain.getCurrentUser();
                fpimain.ForwardBankId = pimain.ForwardBankId;
                fpimain.ForwardBranchId = pimain.ForwardBranchId;

                db.Entry(fpimain).State = EntityState.Modified;
                db.SaveChanges();

                return Json(new { status = "Success", pino = fpimain.PIFileNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult CSSave(CSMain csmain, CSSub cssub)
        {
            try
            {
                if (csmain != null && cssub != null)
                {
                    CSMain cs = new CSMain(csmain);
                    var pi = db.PIMains.Find(cs.PIFileId);

                    cs.CSNo = "CS/" + pi.BeneficiaryId.ToString("00") + "/" + db.CSMains.Where(c => c.PIMain.BeneficiaryId == pi.BeneficiaryId && c.Year == cs.Year).Select(c => c.SerialNo + 1).DefaultIfEmpty(1).Max().Value.ToString("0000") + "/" + cs.Year.Value.ToString().Substring(2, 2);

                    cs.CSSubs.Add(new CSSub(cssub));

                    db.CSMains.Add(cs);

                    pi.CSDate = clsMain.getCurrentTime();
                    pi.IsCSDone = 1; pi._editStatus = 2;
                    db.Entry(pi).State = EntityState.Modified;

                    db.SaveChanges();

                    return Json(new { status = "Success", pino = pi.PIFileNo }, JsonRequestBehavior.AllowGet);
                }
                else
                    return Json(new { status = "Error", message = "Invalid json file has sent for save." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        public ActionResult SanctionDetails(short id, DateTime date)
        {
            var sanction =  db.SanctionDetails.Where(c => c.SanctionTypeId == 40 && c.SanctionMain.BankId == id && c.SanctionMain.ExpireDate >= date).OrderBy(c => c.SanctionMain.ExpireDate).FirstOrDefault() ?? new SanctionDetails();
            var lccom = sanction.LCCommissionRate;
            var acpcom = db.SanctionDetails.Where(c => c.SanctionTypeId == 37 && c.SanctionMain.BankId == id && c.SanctionMain.ExpireDate >= date).OrderBy(c => c.SanctionMain.ExpireDate).Select(l => l.AcceptanceCommissionRate).DefaultIfEmpty(0).First();
            var ocharge = sanction.OtherChargeRate;
            var pupasr = sanction.InterestRate;

            return Json(new { lccom, acpcom, ocharge, pupasr }, JsonRequestBehavior.AllowGet);
        }

        //[HttpGet, HttpPost]
        public ActionResult PIDetails(int id)
        {
            var pi = db.PIMains.Find(id) ?? new PIMain();

            var item = (from p in pi.PISubs.AsEnumerable()
                        join q in dbIMS.PRSubs.AsEnumerable() on p.PRSubId equals q.PRSubId
                        join r in dbProd.ItemInfoes.AsEnumerable() on q.ProductId equals r.ItemId
                        join s in dbProd.ItemCategories.AsEnumerable() on r.ItemCode.Substring(0, 2) equals s.CategoryCode
                        select s.CategoryName
                       ).Distinct().ToList();
            ViewBag.item = item == null || item.Count == 0 ? "" : item.Aggregate((i, j) => i + ", " + j);
            
            //return Json(new { pino = pi.PINo, pidate = pi.PIDate.ToString("dd-MMM-yyyy"), prno = pi.RequistionNo, prdate = pi.PRDate, company = pi.Company.CompanyName, supplier = pi.Supplier.SupplierName, item = item == null ? "" : item.Aggregate((i, j) => i + ", " + j) }, JsonRequestBehavior.AllowGet);
            return PartialView(pi);
        }
    }
}
