﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Others
{
    public class DailyFundPositionController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /DailyFundPosition/
        public ActionResult Index()
        {
            return View(db.DailyFundMains.ToList());
        }

        // GET: /DailyFundPosition/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyFundMain dailyFundMain = db.DailyFundMains.Find(id);
            if (dailyFundMain == null)
            {
                return HttpNotFound();
            }
            return View(dailyFundMain);
        }

        // GET: /DailyFundPosition/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.AccountId = new SelectList(db.CaptionDetails, "AccountId", "CaptionCode");
            return View();
        }

        // POST: /DailyFundPosition/Save
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(DailyFundMain dfMain, ICollection<DailyFundSub> dfdetails)
        {
            try
            {
                //if (db.CashFlowMains.Where(l => l.CFMonth == dfMain.CFMonth && l.CFYear == dfMain.CFYear).Count() == 0)
                //{
                if (dfMain.DFId == 0)
                {
                    var dfmain = new DailyFundMain(dfMain);
                    if (dfdetails != null)
                    {
                        //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                        foreach (var dfdet in dfdetails)
                        {
                            //cfdet.BranchCode = "BR" + (maxcid++).ToString("000");
                            dfmain.DailyFundSubs.Add(new DailyFundSub(dfdet));
                        }
                    }

                    db.DailyFundMains.Add(dfmain);
                }
                else
                {
                    var dfmain = db.DailyFundMains.Find(dfMain.DFId);
                    dfmain = (DailyFundMain)dfMain.ConvertNotNull(dfmain);
                    db.Entry(dfmain).State = EntityState.Modified;

                    if (dfdetails != null)
                    {
                        //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                        foreach (var dfdet in dfdetails)
                        {
                            if (dfdet.DFSubId == 0)
                            {
                                //branch.BranchCode = "BR" + (maxcid++).ToString("000");
                                db.DailyFundSubs.Add(new DailyFundSub(dfdet));
                            }
                            else
                            {
                                var odfdet = db.DailyFundSubs.Find(dfdet.DFSubId);
                                odfdet = (DailyFundSub)dfdet.ConvertNotNull(odfdet);
                                db.Entry(odfdet).State = EntityState.Modified; ;
                            }
                        }

                        var pcfdet = db.DailyFundSubs.Where(c => c.DFId == dfMain.DFId);
                        for (var rIndex = 0; rIndex < pcfdet.Count(); rIndex++)
                        {
                            var dfdet = dfdetails.ToList()[rIndex];
                            if (db.Entry(dfdet).State == EntityState.Unchanged)
                                db.Entry(dfdet).State = EntityState.Deleted;
                        }
                    }
                }

                db.SaveChanges();

                return Json(new { status = "Success", dfdate = dfMain.DFDate.ToString("dd-MMM-yyyy") }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //    return Json(new { status = "Error", message = "Duplicate bank name." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /DailyFundPosition/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyFundMain dailyFundMain = db.DailyFundMains.Find(id);
            if (dailyFundMain == null)
            {
                return HttpNotFound();
            }
            return View(dailyFundMain);
        }

        // POST: /DailyFundPosition/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="DFId,DFYear,DFMonth,DFDate,Remarks,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] DailyFundMain dailyFundMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(dailyFundMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            return View(dailyFundMain);
        }

        // GET: /DailyFundPosition/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            DailyFundMain dailyFundMain = db.DailyFundMains.Find(id);
            if (dailyFundMain == null)
            {
                return HttpNotFound();
            }
            return View(dailyFundMain);
        }

        // POST: /DailyFundPosition/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            DailyFundMain dailyFundMain = db.DailyFundMains.Find(id);
            db.DailyFundMains.Remove(dailyFundMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult CashTemplate(CashFlowDetails cfdet)
        {
            //if (cfdet != null && cfdet.AccountId > 0)
            //{
            //    var caption = db.CaptionDetails.Find(cfdet.AccountId);
            //    ViewBag.TypeId = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == 0), "AccountId", "CaptionName", caption.ParentCaption.ParentCaption.AccountId);
            //    ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == caption.ParentCaption.ParentCaption.AccountId), "AccountId", "CaptionName", caption.ParentCaption.AccountId);
            //    ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == caption.ParentCaption.AccountId), "AccountId", "CaptionName", caption.AccountId);
            //}
            //else
            //{
            //    ViewBag.TypeId = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == 0), "AccountId", "CaptionName");
            //    ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.AccountId == 0), "AccountId", "CaptionName");
            //    ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.AccountId == 0), "AccountId", "CaptionName");
            //}

            return PartialView(cfdet);
        }

        // GET: /Bank/RowTemplate
        [HttpPost]
        public ActionResult RowTemplate(ICollection<DailyFundSub> dfdet)
        {
            //cfdet.AccountHead = db.CaptionDetails.Find(cfdet.AccountId);
            dfdet.ToList().ForEach(c => c.BankAccount = db.BankAccount.Find(c.AccountId));

            return PartialView(dfdet);
        }

        [HttpPost]
        public ActionResult AccountList(short id, string type)
        {
            if (type == "cat")
                ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == id), "AccountId", "CaptionName");
            else
                ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == id), "AccountId", "CaptionName");

            ViewBag.type = type;
            return PartialView();
        }

        [HttpPost]
        public ActionResult DFDetails(DateTime date)
        {
            var acdetails = db.DailyFundSubs.Include(c => c.DailyFundMain).Where(c => c.DailyFundMain.DFDate <= date).GroupBy(c => c.AccountId).Select(c => new { AccountId = c.Key, DFDate = c.Max(l => l.DailyFundMain.DFDate) });
            var funddetails = (from p in db.DailyFundSubs.Include(c => c.DailyFundMain).AsEnumerable()
                               join q in acdetails.AsEnumerable() on new { p.AccountId, DFDate = p.DailyFundMain.DFDate } equals new { q.AccountId, q.DFDate }
                               select p
                              ).Distinct();

            var subs = (from p in db.BankAccount.Include(c => c.Bank).Include(c => c.BankBranch).Include(c => c.AccountType).AsEnumerable()
                        //join t in funddetails.AsEnumerable() on p.AccountId equals t.AccountId
                        join q in db.DailyFundSubs.Include(c => c.DailyFundMain).Where(c => c.DailyFundMain.DFDate == date).AsEnumerable() on new { p.AccountId } equals new { q.AccountId } into r
                        from s in r.DefaultIfEmpty()
                        join u in funddetails.AsEnumerable() on p.AccountId equals u.AccountId into v
                        from t in v.DefaultIfEmpty()
                        select new { p, s, t }
                       ).Select(c => new DailyFundSub()
                       {
                           DFId = (c.s != null && c.s.DailyFundMain.DFDate == date ? c.s.DFId : 0),
                           DFSubId = (c.s != null && c.s.DailyFundMain.DFDate == date ? c.s.DFSubId : 0),
                           AccountId = c.p.AccountId,
                           DailyFundMain = c.s != null ? c.s.DailyFundMain : null,
                           BankAccount = c.p,
                           BankOpeningBalance = c.s != null ? c.s.BankOpeningBalance : 0,
                           BookOpeningBalance = c.s != null ? c.s.BookOpeningBalance : 0,
                           DebitAmount = c.s != null ? c.s.DebitAmount : 0,
                           CreditAmount = c.s != null ? c.s.CreditAmount : 0,
                           InterestRate = c.s != null ? c.s.InterestRate : 0,
                           RowNo = Convert.ToInt16(c.s != null ? c.s.RowNo : 0),
                           BankClosingBalance = c.t != null ? c.t.BankClosingBalance : 0,
                           BookClosingBalance = c.s != null ? c.s.BookClosingBalance : 0
                       }).Distinct();

            subs.ToList().ForEach(c => c.BankAccount.CompanyName = dbProd.Companies.Find(c.BankAccount.CompanyId).ShortName);

            return PartialView("RowTemplate", subs);
        }
    }
}
