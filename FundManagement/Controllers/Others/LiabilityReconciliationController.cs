﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Others
{
    public class LiabilityReconciliationController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /LiabilityReconciliation/
        public ActionResult Index()
        {
            var reconciliationmains = db.ReconciliationMains.Include(r => r.Bank).Include(r => r.Branch);
            return View(reconciliationmains.ToList());
        }

        // GET: /LiabilityReconciliation/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReconciliationMain reconciliationMain = db.ReconciliationMains.Find(id);
            if (reconciliationMain == null)
            {
                return HttpNotFound();
            }
            return View(reconciliationMain);
        }

        // GET: /LiabilityReconciliation/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "ShortName");
            ViewBag.Bank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.Branch = new SelectList(db.BankBranchs.Where(c => c.BranchId < 0), "BranchId", "BranchName");

            return View();
        }

        // POST: /LiabilityReconciliation/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(ReconciliationMain rconmain, ICollection<ReconciliationSub> subs)
        {
            try
            {
                //if (db.CashFlowMains.Where(l => l.CFMonth == cashFlow.CFMonth && l.CFYear == cashFlow.CFYear).Count() == 0)
                //{
                if (rconmain.ReconciliationId == 0)
                {
                    var rmain = new ReconciliationMain(rconmain);
                    if (subs != null)
                    {
                        //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                        foreach (var sub in subs)
                        {
                            //cfdet.BranchCode = "BR" + (maxcid++).ToString("000");
                            rmain.ReconciliationSubs.Add(new ReconciliationSub(sub));
                        }
                    }

                    db.ReconciliationMains.Add(rmain);
                }
                else
                {
                    var rmain = db.ReconciliationMains.Find(rconmain.ReconciliationId);
                    rmain = (ReconciliationMain)rconmain.ConvertNotNull(rmain);
                    db.Entry(rmain).State = EntityState.Modified;

                    if (subs != null)
                    {
                        //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                        foreach (var sub in subs)
                        {
                            if (sub.ReconciliationSubId == 0)
                            {
                                //branch.BranchCode = "BR" + (maxcid++).ToString("000");
                                db.ReconciliationSubs.Add(new ReconciliationSub(sub));
                            }
                            else
                            {
                                var osub = db.ReconciliationSubs.Find(sub.ReconciliationSubId);
                                osub = (ReconciliationSub)sub.ConvertNotNull(osub);
                                db.Entry(osub).State = EntityState.Modified; ;
                            }
                        }

                        var prsub = db.ReconciliationSubs.Where(c => c.ReconciliationSubId == rconmain.ReconciliationId);
                        for (var rIndex = 0; rIndex < prsub.Count(); rIndex++)
                        {
                            var sub = subs.ToList()[rIndex];
                            if (db.Entry(sub).State == EntityState.Unchanged)
                                db.Entry(sub).State = EntityState.Deleted;
                        }
                    }
                }

                db.SaveChanges();

                return Json(new { status = "Success", cfmonth = rconmain.ReconciliationDate.ToString("MMMM, yyyy") }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //    return Json(new { status = "Error", message = "Duplicate bank name." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /LiabilityReconciliation/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReconciliationMain reconciliationMain = db.ReconciliationMains.Find(id);
            if (reconciliationMain == null)
            {
                return HttpNotFound();
            }
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", reconciliationMain.BankId);
            ViewBag.BranchId = new SelectList(db.BankBranchs, "BranchId", "BranchCode", reconciliationMain.BranchId);
            return View(reconciliationMain);
        }

        // POST: /LiabilityReconciliation/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult Edit([Bind(Include="ReconciliationId,ReconciliationYear,ReconciliationMonth,ReconciliationDate,CompanyId,BankId,BranchId,Remarks,CreatedBy,CreatedTime,RevisedBy,RevisedTime,IPAddress,PCName")] ReconciliationMain reconciliationMain)
        {
            if (ModelState.IsValid)
            {
                db.Entry(reconciliationMain).State = EntityState.Modified;
                db.SaveChanges();
                return RedirectToAction("Index");
            }
            ViewBag.BankId = new SelectList(db.Banks, "BankId", "BankName", reconciliationMain.BankId);
            ViewBag.BranchId = new SelectList(db.BankBranchs, "BranchId", "BranchCode", reconciliationMain.BranchId);
            return View(reconciliationMain);
        }

        // GET: /LiabilityReconciliation/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ReconciliationMain reconciliationMain = db.ReconciliationMains.Find(id);
            if (reconciliationMain == null)
            {
                return HttpNotFound();
            }
            return View(reconciliationMain);
        }

        // POST: /LiabilityReconciliation/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            ReconciliationMain reconciliationMain = db.ReconciliationMains.Find(id);
            db.ReconciliationMains.Remove(reconciliationMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult DetailsTemplate(ReconciliationSub sub)
        {
            ViewBag.Type = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE")), "TypeId", "TypeName", sub.LoanTypeId);

            return PartialView(sub);
        }

        // GET: /Bank/RowTemplate
        [HttpPost]
        public ActionResult RowTemplate(ICollection<ReconciliationSub> subs)
        {
            subs.ToList().ForEach(c => c.LoanType = db.CustomTypes.Find(c.LoanTypeId));

            return PartialView(subs);
        }

        [HttpPost]
        public ActionResult ReconciliationDetails(DateTime date, short comid, short bid, short brid)
        {
            //var test = (from p in db.ReconciliationMains.AsEnumerable()
            //            join q in db.ReconciliationSubs.AsEnumerable() on p.ReconciliationId equals q.ReconciliationId
            //            select new { p.ReconciliationDate, p.ReconciliationId}
            //            ).FirstOrDefault();//.Select(c => new { amt = c.ReconciliationDate }).FirstOrDefault();
            //var s = test.ReconciliationDate;
            //short rowNo = 1;

            var items = db.Database.SqlQuery<ReconciliationView>("Exec prcrptLoanPosition " + comid + ", " + bid + ", " + brid + ", " + 
                "'', 0, '" + date.ToString("dd-MMM-yyyy") + "', 'RCON'").ToList();
            var rconsubs = (db.ReconciliationMains.Where(c => c.ReconciliationDate == date && c.CompanyId == comid && c.BankId == bid && c.BranchId == brid).FirstOrDefault() ?? new ReconciliationMain()).ReconciliationSubs;

            var subs = (from p in items.AsEnumerable()
                        join q in rconsubs.AsEnumerable() on p.LoanTypeId equals q.LoanTypeId into r
                        from s in r.DefaultIfEmpty()
                        join t in db.CustomTypes.AsEnumerable() on p.LoanTypeId equals t.TypeId
                        select new { p, s, t }
                       ).Select(c => new ReconciliationSub()
                       {
                           ReconciliationId = Convert.ToInt16(rconsubs.Count > 0 ? rconsubs.First().ReconciliationId : 0),
                           ReconciliationSubId = Convert.ToInt16(c.s != null ? c.s.ReconciliationSubId : 0),
                           LoanTypeId = c.p.LoanTypeId,
                           LoanType = c.t,
                           ReconciliationMain = c.s != null ? c.s.ReconciliationMain : null, 
                           SoftwareBalance = c.p.SoftwareBalance,
                           BookBalance = c.s != null ? c.s.BookBalance : 0,
                           BankBalance = c.s != null ? c.s.BankBalance : 0,
                           SoftwareBookDifference = c.p.SoftwareBalance - (c.s != null ? c.s.BookBalance : 0),
                           SoftwareBookDifferenceRemarks = c.s != null ? c.s.SoftwareBookDifferenceRemarks : "",
                           SoftwareBankDifference = c.p.SoftwareBalance - (c.s != null ? c.s.BankBalance : 0),
                           SoftwareBankDifferenceRemarks = c.s != null ? c.s.SoftwareBankDifferenceRemarks : "",
                           BankBookDifference = c.s != null ? c.s.BankBalance - c.s.BookBalance : 0,
                           BankBookDifferenceRemarks = c.s != null ? c.s.BankBookDifferenceRemarks : "",
                           RowNo = Convert.ToInt16(c.s != null ? c.s.RowNo : 0),
                       }).Distinct();

            //subs.ToList().ForEach(c => c.RowNo = 1);

            return PartialView("RowTemplate", subs);
        }
    }
}
