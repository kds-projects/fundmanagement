﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Others
{
    public class AdjustmentController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Adjustment/
        public ActionResult Index()
        {
            return View(db.AdjustmentMains.ToList());
        }

        // GET: /Adjustment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjustmentMain AdjustmentMain = db.AdjustmentMains.Find(id);
            if (AdjustmentMain == null)
            {
                return HttpNotFound();
            }
            return View(AdjustmentMain);
        }

        // GET: /Adjustment/Create
        [CAuthorize]
        public ActionResult Create()
        {
            AdjustmentMain paymain = new AdjustmentMain();
            paymain.AdjustmentNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('Adjustment', '" + DateTime.Today.Year + "', '" + DateTime.Today.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();
            ViewBag.AdjustmentType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");

            return View(paymain);
        }

        // POST: /Adjustment/Create
        [HttpPost, CAuthorize]
        public ActionResult Save(AdjustmentMain AdjustmentMain, ICollection<AdjustmentSub> Sub)
        {
            try
            {
                //AdjustmentMain paymain = new AdjustmentMain(AdjustmentMain);
                AdjustmentMain.AdjustmentNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('Adjustment', '" + AdjustmentMain.AdjustmentDate.Year + "', '" + AdjustmentMain.AdjustmentDate.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();
                AdjustmentMain.CurrencyId = 23;

                if (Sub != null)
                {
                    foreach (var sub in Sub)
                    {
                        if (sub.AdjustmentFor.Equals("LOAN"))
                        {
                            var loan = db.LoanMains.Find(sub.RefId);
                            sub.LCFileId = loan.LCFileId;
                            sub.LCAcceptanceId = loan.LCAcceptanceId;
                            sub.LoanId = loan.LoanId;
                            sub.CurrencyId = loan.CurrencyId;
                            sub.BDTRate = 1;
                            sub.USDRate = 0;
                        }
                        else if(sub.AdjustmentFor.Equals("LC"))
                        {
                            var lc = db.LCMains.Find(sub.RefId);
                            sub.LCFileId = lc.LCFileId;
                            sub.LCAcceptanceId = 0;
                            sub.LoanId = 0; sub.CurrencyId = lc.CurrencyId;
                            sub.BDTRate = lc.BDTRate; sub.USDRate = lc.USDRate;
                        }
                        else //if (sub.AdjustmentFor.Equals("LC"))
                        {
                            var acp = db.LCAcceptances.Find(sub.RefId);
                            sub.LCFileId = acp.LCFileId;
                            sub.LCAcceptanceId = acp.LCAcceptanceId;
                            sub.LoanId = 0; sub.CurrencyId = acp.CurrencyId;
                            sub.BDTRate = acp.BDTRate; sub.USDRate = acp.USDRate;
                        }
                        //sub.Remarks = "-";

                        AdjustmentMain.AdjustmentSubs.Add(sub);
                    }
                }

                db.AdjustmentMains.Add(AdjustmentMain);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", adjno = AdjustmentMain.AdjustmentNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Adjustment/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjustmentMain AdjustmentMain = db.AdjustmentMains.Find(id);
            if (AdjustmentMain == null)
            {
                return HttpNotFound();
            }
            return View(AdjustmentMain);
        }

        // GET: /Adjustment/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            AdjustmentMain adjustmentMain = db.AdjustmentMains.Find(id);
            if (adjustmentMain == null)
            {
                return HttpNotFound();
            }
            return View(adjustmentMain);
        }

        // POST: /Adjustment/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            AdjustmentMain adjustmentMain = db.AdjustmentMains.Find(id);
            db.AdjustmentMains.Remove(adjustmentMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: /Adjustment/PayTemplate
        public ActionResult PayTemplate(AdjustmentSub Sub)
        {
            ViewBag.PayForList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("ADJUSTFOR")), "TypeName", "TypeName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(l => l.LoanId == 0), "LoanId", "LoanNo");
            ViewBag.ScheduleList = new SelectList(db.PaymentSchedules.Where(l => l.ScheduleId == 0).Select(c => new { InstNo = c.InstallmentNo }).Distinct(), "InstNo", "InstNo");

            return PartialView(Sub);
        }

        // GET: /Adjustment/RowTemplate
        public ActionResult RowTemplate(AdjustmentSub Sub)
        {
            return PartialView(Sub);
        }

        // GET: /Adjustment/ReferenceList
        [HttpPost]
        public ActionResult ReferenceList(string type = "LOAN")
        {
            if (type.Equals("LC"))
            {
                var lclist = (from p in db.LCMains.AsEnumerable()
                                join u in db.AdjustmentSubs.GroupBy(c => c.LCFileId).Select(c => new { LCFileId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LCFileId equals u.LCFileId into v
                                from w in v.DefaultIfEmpty()
                                where p.LCAmount - (w == null ? 0 : w.AdjustAmount) > 0
                                select new { p.LCFileId, p.LCNo }
                               ).Distinct();

                ViewBag.RefList = new SelectList(lclist, "LCFileId", "LCNo");
            }
            else if (type.Equals("LOAN"))
            {
                var loanlist = (from p in db.LoanMains.Include(c => c.LCMain).AsEnumerable()
                                join q in db.PaymentSchedules.AsEnumerable() on p.LoanId equals q.LoanId
                                join r in db.PaymentSubs.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals r.ScheduleId into s
                                from t in s.DefaultIfEmpty()
                                join u in db.AdjustmentSubs.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals u.ScheduleId into v
                                from w in v.DefaultIfEmpty()
                                //join u in db.LoanInterests.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals u.ScheduleId into v
                                //from w in v.DefaultIfEmpty()
                                //where q.DueAmount - ((t == null ? 0 : t.PaidAmount) + (w == null ? 0 : w.AdjustAmount)) <> 0
                                select new { p.LoanId, LoanNo = p.LoanNo + " >> " + p.LCMain.LCNo }
                               ).Distinct();

                ViewBag.RefList = new SelectList(loanlist, "LoanId", "LoanNo");
            }
            else
            {
                var acplist = (from p in db.LCAcceptances.Include(c => c.LCMain).AsEnumerable()
                               join q in db.FundDetails.GroupBy(c => c.LCAcceptanceId).Select(c => new { 
                                   LCAcceptanceId = c.Key, 
                                   Balance = c.Sum(l => (l.RefTypeId == 4 && l.Amount != 0 && l.AmountInActualCurrency != 0 ? Math.Abs(l.AmountInActualCurrency / l.Amount) : 1) *
                                        ((l.RefTypeId == 3 && l.LoanId != 0 ? 0 : l.Principal + l.Interest) + l.Libor + l.HandlingCharge + l.ExciseDuty + l.BankCharge + l.SettlementFee)
                                   )}).AsEnumerable() on p.LCAcceptanceId equals q.LCAcceptanceId
                               //join r in db.PaymentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LCAcceptanceId equals r.LCAcceptanceId into s
                               //from t in s.DefaultIfEmpty()
                               //join u in db.LoanMains.AsEnumerable() on p.LCAcceptanceId equals u.LCAcceptanceId into v
                               //from w in v.DefaultIfEmpty()
                               //join x in db.AdjustmentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LCAcceptanceId equals x.LCAcceptanceId into y
                               //from z in y.DefaultIfEmpty()
                               where (q.Balance * p.BDTRate) > 100
                             
                                    //w == null && p.AmountInBDT + ((p.LiborAmount + p.HandlingCharge) * p.BDTRate) -
                                    //((t == null ? 0 : t.PaidAmount) + (z == null ? 0 : z.AdjustAmount)) > 0
                               select new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo 
                                }
                              ).Distinct();

                ViewBag.RefList = new SelectList(acplist, "LCAcceptanceId", "AcceptanceNo");
            }

            return PartialView();
        }

        // GET: /Payment/RowTemplate
        [HttpPost]
        public ActionResult ReferenceDetails(string type = "Loan", int RefId = 0)
        {
            if (RefId == 0)
                return Content("");

            LCMain lcmain = new LCMain(); 
            
            if (type.Equals("LC"))
            {
                lcmain = db.LCMains.Find(RefId);
            }
            else if (type.Equals("LOAN"))
            {
                lcmain = db.LoanMains.Find(RefId).LCMain;
            }
            else
            {
                lcmain = db.LCAcceptances.Find(RefId).LCMain;
            }

            return PartialView(lcmain);
        }

        // GET: /Adjustment/ScheduleList
        [HttpPost]
        public ActionResult ScheduleList(string type = "Loan", int RefId = 0)
        {
            var slist = (new[] { new { InstallmentNo = "-", ScheduleId = 0 } }).ToList();


            if (RefId == 0)
            {
                slist.Clear();
            }
            else if (type.Equals("LOAN"))
            {
                var schlist = (from p in db.LoanMains.AsEnumerable()
                               join q in db.PaymentSchedules.AsEnumerable() on p.LoanId equals q.LoanId
                               join r in db.PaymentSubs.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals r.ScheduleId into s
                               from t in s.DefaultIfEmpty()
                               join u in db.AdjustmentSubs.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals u.ScheduleId into v
                               from w in v.DefaultIfEmpty()
                               where p.LoanId == RefId && Math.Round(q.DueAmount + (w == null ? 0 : w.AdjustAmount) - ((t == null ? 0 : t.PaidAmount)), 3) > 0
                               select q
                              ).Distinct();
                //var sql = ((System.Data.Entity.Core.Objects.ObjectQuery)schlist).ToTraceString();
                slist.AddRange(schlist.Select(c => new { InstallmentNo = c.InstallmentNo.ToString(), c.ScheduleId }).ToList());
            }

            ViewBag.ScheduleList = new SelectList(slist, "ScheduleId", "InstallmentNo");

            return PartialView();
        }

        // GET: /Adjustment/ScheduleDetails
        [HttpPost]
        public ActionResult ScheduleDetails(string type = "Loan", int RefId = 0, int SchId = 0)
        {
            if (type.Equals("LOAN") && SchId > 0)
            {
                var payschedule = db.PaymentSchedules.Find(SchId);

                var interest = db.LoanInterests.Where(l => l.LCAcceptanceId == payschedule.LCAcceptanceId && l.LoanId == payschedule.LoanId).Select(c => c.Interest + c.InterestOD).DefaultIfEmpty(0).Sum();

                //var ajdustment = db.LoanInterests.Where(l => l.LCAcceptanceId == payschedule.LCAcceptanceId && l.LoanId == payschedule.LoanId).Select(c => c.Interest + c.InterestOD).DefaultIfEmpty(0).Sum();

                var adjustment = db.AdjustmentSubs.Where(c => c.ScheduleId == SchId && c.LoanId == RefId).GroupBy(c => c.ScheduleId)
                    .Select(l => new
                    {
                        ScheduleId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                var paidamount = db.PaymentSubs.Where(c => c.ScheduleId == SchId && c.LoanId == RefId).GroupBy(c => c.ScheduleId)
                    .Select(l => new
                    {
                        ScheduleId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                if (payschedule.LCFileId == 0)
                {
                    return Json(new
                    {
                        principal = payschedule.PrincipalAmount + (adjustment != null ? adjustment.Principal : 0) - (paidamount != null ? paidamount.Principal : 0),
                        interest = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + (adjustment != null ? adjustment.Interest : 0) + payschedule.InterestAmount - (paidamount != null ? paidamount.Interest : 0),
                        libor = "0.00",
                        handling = "0.00",
                        exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                        bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                        settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                        total = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + payschedule.DueAmount + (adjustment != null ? adjustment.Total : 0) - (paidamount != null ? paidamount.Total : 0)
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        principal = payschedule.LoanMain.AcceptedAmountBDT + (adjustment != null ? adjustment.Principal : 0) - (paidamount != null ? paidamount.Principal : 0),
                        interest = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + (adjustment != null ? adjustment.Interest : 0) + payschedule.InterestAmount - (paidamount != null ? paidamount.Interest : 0),
                        libor = payschedule.LoanMain.LiborAmountBDT + (adjustment != null ? adjustment.Libor : 0) - (paidamount != null ? paidamount.Libor : 0),
                        handling = payschedule.LoanMain.HandlingChargeBDT + (adjustment != null ? adjustment.HandlingCharge : 0) - (paidamount != null ? paidamount.HandlingCharge : 0),
                        exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                        bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                        settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                        total = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + payschedule.DueAmount + (adjustment != null ? adjustment.Total : 0) - (paidamount != null ? paidamount.Total : 0)
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (type.Equals("LC") && RefId > 0)
            {
                var payschedule = db.LCMains.Find(RefId);

                var adjustamount = db.AdjustmentSubs.Where(c => c.LCFileId == RefId && c.LCAcceptanceId == 0).GroupBy(c => c.LCFileId)
                    .Select(l => new
                    {
                        LCAcceptanceId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                return Json(new
                {
                    principal = payschedule.LCAmount - (adjustamount != null ? adjustamount.Principal : 0),
                    interest = "0.00",
                    libor = "0.00",
                    handling = "0.00",
                    total = payschedule.LCAmount + (adjustamount != null ? adjustamount.Principal : 0)
                }, JsonRequestBehavior.AllowGet);
            }
            else if (type.Equals("ACCEPTANCE") && RefId > 0)
            {
                var payschedule = db.LCAcceptances.Find(RefId);

                var paidamount = db.PaymentSubs.Where(c => c.LCAcceptanceId == RefId && c.LoanId == 0).GroupBy(c => c.LCAcceptanceId)
                    .Select(l => new
                    {
                        LCAcceptanceId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                var adjustment = db.AdjustmentSubs.Where(c => c.LCAcceptanceId == RefId && c.LoanId == 0).GroupBy(c => c.LCAcceptanceId)
                    .Select(l => new
                    {
                        LCAcceptanceId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                return Json(new
                {
                    principal = Math.Round((payschedule.AcceptanceAmount + (adjustment != null ? adjustment.Principal : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.Principal : 0),
                    interest = (adjustment != null ? adjustment.Interest : 0) - (paidamount != null ? paidamount.Interest : 0),
                    libor = Math.Round((payschedule.LiborAmount + (adjustment != null ? adjustment.Libor : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.Libor : 0),
                    handling = Math.Round((payschedule.HandlingCharge + (adjustment != null ? adjustment.HandlingCharge : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.HandlingCharge : 0),
                    exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                    bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                    settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                    total = Math.Round((payschedule.AcceptanceAmount + payschedule.LiborAmount + payschedule.HandlingCharge + (adjustment != null ? adjustment.Total : 0)) * payschedule.BDTRate, 2) -
                        (paidamount != null ? paidamount.Total : 0)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { principal = "0.00", interest = "0.00", libor = "0.00", handling = "0.00", total = "0.00" }, JsonRequestBehavior.AllowGet);
        }
    }
}
