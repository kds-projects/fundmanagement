﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Others
{
    public class CashFlowController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /CashFlow/
        public ActionResult Index()
        {
            var CashFlowMain = db.CashFlowMains;
            return View(CashFlowMain.ToList());
        }

        // GET: /CashFlow/Details/5
        public ActionResult Details(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowMain CashFlowMain = db.CashFlowMains.Find(id);
            if (CashFlowMain == null)
            {
                return HttpNotFound();
            }
            return View(CashFlowMain);
        }

        // GET: /CashFlow/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.CaptionId = new SelectList(db.CaptionDetails, "CaptionId", "CaptionCode");
            ViewBag.GroupList = new SelectList(dbProd.GroupOfCompanies, "GroupId", "GroupName");

            return View();
        }

        // POST: /CashFlow/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(CashFlowMain cashFlow, ICollection<CashFlowDetails> cfdetails)
        {
            try
            {
                //if (db.CashFlowMains.Where(l => l.CFMonth == cashFlow.CFMonth && l.CFYear == cashFlow.CFYear).Count() == 0)
                //{
                    if (cashFlow.CFId == 0)
                    {
                        var cfmain = new CashFlowMain(cashFlow);
                        if (cfdetails != null)
                        {
                            //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                            foreach (var cfdet in cfdetails)
                            {
                                //cfdet.BranchCode = "BR" + (maxcid++).ToString("000");
                                cfmain.CashFlowDetails.Add(new CashFlowDetails(cfdet));
                            }
                        }

                        db.CashFlowMains.Add(cfmain);
                    }
                    else
                    {
                        var cfmain = db.CashFlowMains.Find(cashFlow.CFId);
                        cfmain = (CashFlowMain)cashFlow.ConvertNotNull(cfmain);
                        db.Entry(cfmain).State = EntityState.Modified;

                        if (cfdetails != null)
                        {
                            //var maxcid = Convert.ToInt32(db.BankBranchs.Select(c => c.BranchCode.Substring(2)).DefaultIfEmpty("000").Max()) + 1;
                            foreach (var cfdet in cfdetails)
                            {
                                if (cfdet.CFSubId == 0)
                                {
                                    //branch.BranchCode = "BR" + (maxcid++).ToString("000");
                                    db.CashFlowDetails.Add(new CashFlowDetails(cfdet));
                                }
                                else
                                {
                                    var ocfdet = db.CashFlowDetails.Find(cfdet.CFSubId);
                                    ocfdet = (CashFlowDetails)cfdet.ConvertNotNull(ocfdet);
                                    db.Entry(ocfdet).State = EntityState.Modified; ;
                                }
                            }

                            var pcfdet = db.CashFlowDetails.Where(c => c.CFId == cashFlow.CFId);
                            for (var rIndex = 0; rIndex < pcfdet.Count(); rIndex++)
                            {
                                if (cfdetails.ToList().Count > rIndex)
                                {
                                    var cfdet = cfdetails.ToList()[rIndex];
                                    if (db.Entry(cfdet).State == EntityState.Unchanged)
                                        db.Entry(cfdet).State = EntityState.Deleted;
                                }
                            }
                        }
                    }

                    db.SaveChanges();

                    return Json(new { status = "Success", cfmonth = cashFlow.CFDate.ToString("MMMM, yyyy") }, JsonRequestBehavior.AllowGet);
                //}
                //else
                //    return Json(new { status = "Error", message = "Duplicate bank name." }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /CashFlow/Edit/5
        public ActionResult Edit(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowMain cashFlowMain = db.CashFlowMains.Find(id);
            if (cashFlowMain == null)
            {
                return HttpNotFound();
            }
            ViewBag.CaptionId = new SelectList(db.CaptionDetails, "CaptionId", "CaptionCode");
            return View(cashFlowMain);
        }

        // GET: /CashFlow/Delete/5
        public ActionResult Delete(short? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            CashFlowMain cashFlowMain = db.CashFlowMains.Find(id);
            if (cashFlowMain == null)
            {
                return HttpNotFound();
            }
            return View(cashFlowMain);
        }

        // POST: /CashFlow/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(short id)
        {
            CashFlowMain cashFlowMain = db.CashFlowMains.Find(id);
            db.CashFlowMains.Remove(cashFlowMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        [HttpPost]
        public ActionResult CashTemplate(CashFlowDetails cfdet)
        {
            if (cfdet != null && cfdet.CaptionId > 0)
            {
                var caption = db.CaptionDetails.Find(cfdet.CaptionId);
                ViewBag.TypeId = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == 0), "CaptionId", "CaptionName", caption.ParentCaption.ParentCaption.CaptionId);
                ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == caption.ParentCaption.ParentCaption.CaptionId), "CaptionId", "CaptionName", caption.ParentCaption.CaptionId);
                ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == caption.ParentCaption.CaptionId), "CaptionId", "CaptionName", caption.CaptionId);
            }
            else
            {
                ViewBag.TypeId = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == 0), "CaptionId", "CaptionName");
                ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.CaptionId == 0), "CaptionId", "CaptionName");
                ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.CaptionId == 0), "CaptionId", "CaptionName");
            }

            return PartialView(cfdet);
        }

        // GET: /Bank/RowTemplate
        [HttpPost]
        public ActionResult RowTemplate(ICollection<CashFlowDetails> cfdet)
        {
            //cfdet.AccountHead = db.CaptionDetails.Find(cfdet.CaptionId);
            cfdet.ToList().ForEach(c => c.AccountHead = db.CaptionDetails.Find(c.CaptionId));

            return PartialView(cfdet);
        }

        [HttpPost]
        public ActionResult AccountList(short id, string type)
        {
            if (type == "cat")
                ViewBag.AccountHead = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == id), "CaptionId", "CaptionName");
            else
                ViewBag.AccountList = new SelectList(db.CaptionDetails.Where(c => c.CaptionPId == id), "CaptionId", "CaptionName");

            ViewBag.type = type;
            return PartialView();
        }

        [HttpPost]
        public ActionResult CFDetails(DateTime date, short groupid)
        {
            var exceptlist = new List<string>(){"020301", "020302", "020303", "020304"};
            var subs = (from p in db.CaptionDetails.AsEnumerable()
                        join q in db.CashFlowDetails.Include(c => c.CashFlowMain).Where(c => c.CashFlowMain.CFDate == date && c.CashFlowMain.GroupId == groupid).AsEnumerable() on p.CaptionId equals q.CaptionId into r
                        from s in r.DefaultIfEmpty()
                        where p.CaptionPCode.Length == 4 && !exceptlist.Contains(p.CaptionCode)
                        select new { p, s }
                       ).Select(c => new CashFlowDetails()
                       {
                           CFId = Convert.ToInt16(c.s != null ? c.s.CFId : 0),
                           CFSubId = Convert.ToInt16(c.s != null ? c.s.CFSubId : 0),
                           AccountHead = c.p,
                           CaptionId = c.p.CaptionId, 
                           CashFlowMain = c.s != null ? c.s.CashFlowMain : null,
                           BudgetAmount = c.s != null ? c.s.BudgetAmount : 0,
                           ActualAmount = c.s != null ? c.s.ActualAmount : 0,
                           RowNo = Convert.ToInt16(c.s != null ? c.s.RowNo : 0),
                       }).Distinct();

            //subs.ToList().ForEach(c => c.RowNo = 1);

            return PartialView("RowTemplate", subs);
        }
    }
}
