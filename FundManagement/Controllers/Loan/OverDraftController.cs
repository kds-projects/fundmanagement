﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Loan
{
    public class OverDraftController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /OverDraft/
        public ActionResult Index()
        {
            var odmains = db.ODMains.Include(o => o.IssuerBank).Include(o => o.IssuerBranch);
            return View(odmains.ToList());
        }

        // GET: /OverDraft/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ODMain oDMain = db.ODMains.Find(id);
            if (oDMain == null)
            {
                return HttpNotFound();
            }
            return View(oDMain);
        }

        // GET: /OverDraft/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");

            return View();
        }

        // POST: /OverDraft/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult Save(ODMain odmain)
        {
            try
            {
                if (odmain.ODId == 0)
                {
                    odmain.LoanTypeId = 38;
                    db.ODMains.Add(new ODMain(odmain));
                }
                else
                {
                    var od = db.ODMains.Find(odmain.ODId);
                    od = (ODMain)odmain.ConvertNotNull(od);
                    db.Entry(od).State = EntityState.Modified;
                }

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", odno = odmain.ODNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /OverDraft/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            ODMain odmain = db.ODMains.Find(id);
            if (odmain == null)
            {
                return HttpNotFound();
            }

            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName", odmain.IssuerBankId);
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == odmain.IssuerBankId), "BranchId", "BranchName", odmain.IssuerBranchId);
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName", odmain.CompanyId);

            return View("Create", odmain);
        }

        // GET: /OverDraft/Edit/5
        [CAuthorize]
        public ActionResult Manage(int? id)
        {
            //if (id == null)
            //{
            //    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            //}

            //ODMain odmain = db.ODMains.Find(id);
            //if (odmain == null)
            //{
            //    return HttpNotFound();
            //}

            ViewBag.ODId = new SelectList(db.ODMains, "ODId", "ODNo");

            return View();
        }

        // POST: /OverDraft/Edit/5
        [HttpPost]
        public ActionResult BalanceDetails(int id)
        {
            DateTime fdate = DateTime.Today.AddYears(-1);
            DateTime tdate = DateTime.Today;
            if (id == 0 || fdate == null || tdate == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            var clbalance = db.ODDetails.Where(c => c.ODId == id && c.TransactionDate >= fdate && c.TransactionDate <= tdate).Distinct().OrderByDescending(c => c.TransactionDate).ToList();

            return PartialView(clbalance);
        }

        // POST: /OverDraft/Edit/5
        [HttpPost]
        public ActionResult ClosingBalance(int id, DateTime date)
        {
            if (id == 0 || date == null)
            {
                return Json(new { clbal = "0.00", ir = "0.00", pir = "0.00", remarks = "", oddetailsid = 0 }, JsonRequestBehavior.AllowGet);
            }

            var odbal = db.ODDetails.Where(c => c.ODId == id && c.TransactionDate == date).FirstOrDefault();

            if (odbal == null)
                return Json(new { clbal = "0.00", ir = "0.00", pir = "0.00", remarks = "", oddetailsid = 0 }, JsonRequestBehavior.AllowGet);
            else
                return Json(new { clbal = odbal.ClosingBalance.ToString("##,##0.00"), ir = odbal.InterestRate.ToString("##,##0.00"), pir = odbal.InterestRateOD.ToString("##,##0.00"), remarks = odbal.Remarks, oddetailsid = odbal.ODDetailsId }, JsonRequestBehavior.AllowGet);
        }

        // POST: /OverDraft/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [CAuthorize]
        public ActionResult UpdateClosingBalance(ODDetails details)
        {
            try
            {
                var old = db.ODDetails.Where(c => c.ODId == details.ODId && c.TransactionDate == details.TransactionDate).FirstOrDefault();
                if (old == null)
                {
                    db.ODDetails.Add(new ODDetails(details));
                }
                else
                {
                    details.ODDetailsId = old.ODDetailsId;
                    old = (ODDetails)details.ConvertNotNull(old);
                    db.Entry(old).State = EntityState.Modified;
                }

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", ttype = old == null ? "save" : "update" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /OverDraft/Delete/5
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ODMain oDMain = db.ODMains.Find(id);
            if (oDMain == null)
            {
                return HttpNotFound();
            }
            return View(oDMain);
        }

        // POST: /OverDraft/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public ActionResult DeleteConfirmed(int id)
        {
            ODMain oDMain = db.ODMains.Find(id);
            db.ODMains.Remove(oDMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
