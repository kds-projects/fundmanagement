﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers.Loan
{
    public class PaymentController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Payment/
        public ActionResult Index()
        {
            return View(db.PaymentMains.ToList());
        }

        // GET: /Payment/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentMain paymentMain = db.PaymentMains.Find(id);
            if (paymentMain == null)
            {
                return HttpNotFound();
            }
            return View(paymentMain);
        }

        // GET: /Payment/Create
        [CAuthorize]
        public ActionResult Create()
        {
            PaymentMain paymain = new PaymentMain();
            paymain.PaymentNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('Payment', '" + DateTime.Today.Year + "', '" + DateTime.Today.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();
            ViewBag.PaymentType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYTYPE")), "TypeId", "TypeName");

            return View(paymain);
        }

        // POST: /Payment/Create
        [HttpPost, CAuthorize]
        public ActionResult Save(PaymentMain paymentMain, ICollection<PaymentSub> paySub)
        {
            try
            {
                //PaymentMain paymain = new PaymentMain(paymentMain);
                paymentMain.PaymentNo = db.Database.SqlQuery(typeof(string), "Select dbo.fncGetMaxNo ('Payment', '" + paymentMain.PaymentDate.Year + "', '" + paymentMain.PaymentDate.ToString("dd-MMM-yyyy") + "') As MaxNo", "").Cast<String>().ToList().FirstOrDefault();

                if (paySub != null)
                {
                    foreach (var sub in paySub)
                    {
                        if(sub.PaymentFor.Equals("LOAN"))
                        {
                            var loan = db.LoanMains.Find(sub.RefId);
                            sub.LCFileId = loan.LCFileId;
                            sub.LCAcceptanceId = loan.LCAcceptanceId;
                            sub.LoanId = loan.LoanId;
                        }
                        else //if(sub.PaymentFor.Equals("LOAN"))
                        {
                            var acp = db.LCAcceptances.Find(sub.RefId);
                            sub.LCFileId = acp.LCFileId;
                            sub.LCAcceptanceId = acp.LCAcceptanceId;
                            sub.LoanId = 0;
                        }
                        //sub.Remarks = "-";

                        paymentMain.PaymentSubs.Add(sub);
                    }
                }

                db.PaymentMains.Add(paymentMain);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", payno = paymentMain.PaymentNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Payment/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentMain paymentMain = db.PaymentMains.Find(id);
            if (paymentMain == null)
            {
                return HttpNotFound();
            }
            return View(paymentMain);
        }

        // GET: /Payment/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            PaymentMain paymentMain = db.PaymentMains.Find(id);
            if (paymentMain == null)
            {
                return HttpNotFound();
            }
            return View(paymentMain);
        }

        // POST: /Payment/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            PaymentMain paymentMain = db.PaymentMains.Find(id);
            db.PaymentMains.Remove(paymentMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: /Payment/PayTemplate
        //[HttpGet, HttpPost]
        public ActionResult PayTemplate(PaymentSub paySub)
        {
            ViewBag.PayForList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYFOR")), "TypeName", "TypeName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(l => l.LoanId == 0), "LoanId", "LoanNo");
            ViewBag.ScheduleList = new SelectList(db.PaymentSchedules.Where(l => l.ScheduleId == 0).Select(c => new { InstNo = c.InstallmentNo }).Distinct(), "InstNo", "InstNo");

            return PartialView(paySub);
        }

        // GET: /Payment/RowTemplate
        //[HttpGet, HttpPost]
        public ActionResult RowTemplate(PaymentSub paySub)
        {
            return PartialView(paySub);
        }

        // GET: /Payment/RowTemplate
        [HttpPost]
        public ActionResult ReferenceList(string type = "LOAN")
        {
            if (type.Equals("LOAN"))
            {
                var loanlist = (from p in db.LoanMains.Include(l => l.LCMain).AsEnumerable()
                                join q in db.PaymentSchedules.GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, DueAmount = c.Sum(l => l.DueAmount) }).AsEnumerable() on p.LoanId equals q.LoanId
                                join r in db.PaymentSubs.GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LoanId equals r.LoanId into s
                                from t in s.DefaultIfEmpty()
                                join u in db.AdjustmentSubs.GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LoanId equals u.LoanId into v
                                from w in v.DefaultIfEmpty()
                                join x in db.LoanInterests.GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, InterstAmount = c.Sum(l => l.Interest + l.InterestOD) }).AsEnumerable() on p.LoanId equals x.LoanId into y
                                from z in y.DefaultIfEmpty()
                                where q.DueAmount - (t == null ? 0 : t.PaidAmount) + (w == null ? 0 : w.AdjustAmount) + (z == null ? 0 : z.InterstAmount) > 0
                                select new { p.LoanId, LoanNo = p.LoanNo + " >> " + p.LCMain.LCNo }
                               ).Distinct();

                ViewBag.RefList = new SelectList(loanlist, "LoanId", "LoanNo");
            }
            else
            {
                var acplist = (from p in db.LCAcceptances.Include(l => l.LCMain).AsEnumerable()
                               join r in db.PaymentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LCAcceptanceId equals r.LCAcceptanceId into s
                               from t in s.DefaultIfEmpty()
                               join u in db.LoanMains.AsEnumerable() on p.LCAcceptanceId equals u.LCAcceptanceId into v
                               from w in v.DefaultIfEmpty()
                               join x in db.AdjustmentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, AdjustAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LCAcceptanceId equals x.LCAcceptanceId into y
                               from z in y.DefaultIfEmpty()
                               where w == null && p.AmountInBDT + ((p.LiborAmount + p.HandlingCharge) * p.BDTRate) -
                                    (t == null ? 0 : t.PaidAmount) + (z == null ? 0 : z.AdjustAmount) > 0
                               select new { p.LCAcceptanceId, AcceptanceNo = p.AcceptanceNo + " >> " + p.LCMain.LCNo }
                              ).Distinct();

                ViewBag.RefList = new SelectList(acplist, "LCAcceptanceId", "AcceptanceNo");
            }

            return PartialView();
        }
        
        // GET: /Payment/RowTemplate
        [HttpPost]
        public ActionResult ReferenceDetails(string type = "Loan", int RefId = 0)
        {
            if (RefId == 0)
                return Content("");

            LCMain lcmain = new LCMain();
            if (type.Equals("LOAN"))
            {
                var loan = db.LoanMains.Find(RefId);
                lcmain = db.LoanMains.Find(RefId).LCMain;
                if (loan.LCFileId == 0)
                {
                    lcmain.LCNo = loan.LoanNo;
                    lcmain.LCDate = loan.LoanDate;
                    lcmain.BeneficiaryId = loan.BeneficiaryId;
                    lcmain.IssuerBank = db.Banks.Find(loan.IssuerBankId);
                    lcmain.IssuerBranch = db.BankBranchs.Find(loan.IssuerBranchId);
                    lcmain.Currency = loan.Currency;
                }
                else
                {
                    lcmain.Currency = loan.Currency;
                }

            }
            else if (!type.Equals("LOAN"))
            {
                lcmain = db.LCAcceptances.Find(RefId).LCMain;
            }

            return PartialView(lcmain);
        }

        // GET: /Payment/RowTemplate
        [HttpPost]
        public ActionResult ScheduleList(string type = "Loan", int RefId = 0)
        {
            var slist = (new[] { new { InstallmentNo = "-", ScheduleId = 0 } }).ToList();


            if (RefId == 0)
            {
                slist.Clear();
            }
            else if (type.Equals("LOAN"))
            {
                var schlist = (from p in db.LoanMains.AsEnumerable()
                               join q in db.PaymentSchedules.AsEnumerable() on p.LoanId equals q.LoanId
                               join r in db.PaymentSubs.GroupBy(c => c.ScheduleId).Select(c => new { ScheduleId = c.Key, PaidAmount = c.Sum(l => l.Amount) }).AsEnumerable() on q.ScheduleId equals r.ScheduleId into s
                               from t in s.DefaultIfEmpty()
                               where p.LoanId == RefId && q.DueAmount - (t == null ? 0 : t.PaidAmount) > 0
                               select q
                              ).Distinct();

                if(schlist != null)
                slist.AddRange(schlist.Select(c => new { InstallmentNo = c.InstallmentNo.ToString() + " - DT: " + c.DueDate.ToString("dd-MMM-yyyy"), c.ScheduleId }).ToList());
            }
            
            ViewBag.ScheduleList = new SelectList(slist, "ScheduleId", "InstallmentNo");

            return PartialView();
        }

        // GET: /Payment/RowTemplate
        [HttpPost]
        public ActionResult ScheduleDetails(string type = "Loan", int RefId = 0, int SchId = 0)
        {
            if (type.Equals("LOAN") && SchId > 0)
            {
                var payschedule = db.PaymentSchedules.Find(SchId);

                var interest = db.LoanInterests.Where(l => l.LCAcceptanceId == payschedule.LCAcceptanceId && l.LoanId == payschedule.LoanId).Select(c => c.Interest + c.InterestOD).DefaultIfEmpty(0).Sum();

                //var ajdustment = db.LoanInterests.Where(l => l.LCAcceptanceId == payschedule.LCAcceptanceId && l.LoanId == payschedule.LoanId).Select(c => c.Interest + c.InterestOD).DefaultIfEmpty(0).Sum();

                var adjustment = db.AdjustmentSubs.Where(c => c.ScheduleId == SchId && c.LoanId == RefId).GroupBy(c => c.ScheduleId)
                    .Select(l => new
                    {
                        ScheduleId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                var paidamount = db.PaymentSubs.Where(c => c.ScheduleId == SchId && c.LoanId == RefId).GroupBy(c => c.ScheduleId)
                    .Select(l => new
                    {
                        ScheduleId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                if(payschedule.LCFileId == 0)
                {
                    return Json(new
                    {
                        principal = payschedule.PrincipalAmount + (adjustment != null ? adjustment.Principal : 0) - (paidamount != null ? paidamount.Principal : 0),
                        interest = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + (adjustment != null ? adjustment.Interest : 0) + payschedule.InterestAmount - (paidamount != null ? paidamount.Interest : 0),
                        libor = "0.00",
                        handling = "0.00",
                        exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                        bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                        settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                        total = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + payschedule.DueAmount + (adjustment != null ? adjustment.Total : 0) - (paidamount != null ? paidamount.Total : 0)
                    }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new
                    {
                        principal = payschedule.LoanMain.AcceptedAmountBDT + (adjustment != null ? adjustment.Principal : 0) - (paidamount != null ? paidamount.Principal : 0),
                        interest = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + (adjustment != null ? adjustment.Interest : 0) + payschedule.InterestAmount - (paidamount != null ? paidamount.Interest : 0),
                        libor = payschedule.LoanMain.LiborAmountBDT + (adjustment != null ? adjustment.Libor : 0) - (paidamount != null ? paidamount.Libor : 0),
                        handling = payschedule.LoanMain.HandlingChargeBDT + (adjustment != null ? adjustment.HandlingCharge : 0) - (paidamount != null ? paidamount.HandlingCharge : 0),
                        exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                        bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                        settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                        total = (payschedule.LoanMain.ScheduleTypeId == 63 ? 0 : interest) + payschedule.DueAmount + (adjustment != null ? adjustment.Total : 0) - (paidamount != null ? paidamount.Total : 0)
                    }, JsonRequestBehavior.AllowGet);
                }
            }
            else if (!type.Equals("LOAN") && RefId > 0)
            {
                var payschedule = db.LCAcceptances.Find(RefId);

                var adjustment = db.AdjustmentSubs.Where(c => c.LCAcceptanceId == RefId).GroupBy(c => c.LCAcceptanceId)
                    .Select(l => new
                    {
                        LCAcceptanceId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                var paidamount = db.PaymentSubs.Where(c => c.LCAcceptanceId == RefId && c.LoanId == 0).GroupBy(c => c.LCAcceptanceId)
                    .Select(l => new
                    {
                        LCAcceptanceId = l.Key,
                        Principal = l.Sum(c => c.Principal),
                        Interest = l.Sum(c => c.Interest),
                        Libor = l.Sum(c => c.Libor),
                        HandlingCharge = l.Sum(c => c.HandlingCharge),
                        ExciseDuty = l.Sum(c => c.ExciseDuty),
                        BankCharge = l.Sum(c => c.BankCharge),
                        SettlementFee = l.Sum(c => c.SettlementFee),
                        Total = l.Sum(c => c.Principal + c.Interest + c.Libor + c.HandlingCharge + c.ExciseDuty + c.BankCharge + c.SettlementFee)
                    }).FirstOrDefault();

                return Json(new
                {
                    principal = Math.Round((payschedule.AcceptanceAmount + (adjustment != null ? adjustment.Principal : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.Principal : 0),
                    interest = (adjustment != null ? adjustment.Interest : 0) - (paidamount != null ? paidamount.Interest : 0),
                    libor = Math.Round((payschedule.LiborAmount + (adjustment != null ? adjustment.Libor : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.Libor : 0),
                    handling = Math.Round((payschedule.HandlingCharge + (adjustment != null ? adjustment.HandlingCharge : 0)) * payschedule.BDTRate, 2) - (paidamount != null ? paidamount.HandlingCharge : 0),
                    exciseduty = (adjustment != null ? adjustment.ExciseDuty : 0) - (paidamount != null ? paidamount.ExciseDuty : 0),
                    bankcharge = (adjustment != null ? adjustment.BankCharge : 0) - (paidamount != null ? paidamount.BankCharge : 0),
                    settlefee = (adjustment != null ? adjustment.SettlementFee : 0) - (paidamount != null ? paidamount.SettlementFee : 0),
                    total = Math.Round((payschedule.AcceptanceAmount + payschedule.LiborAmount + payschedule.HandlingCharge + (adjustment != null ? adjustment.Total : 0)) * payschedule.BDTRate, 2) -
                        (paidamount != null ? paidamount.Total : 0)
                }, JsonRequestBehavior.AllowGet);
            }

            return Json(new { principal = "0.00", interest = "0.00", libor = "0.00", handling = "0.00", exciseduty = "0.00", bankcharge = "0.00", settlefee = "0.00", total = "0.00" }, JsonRequestBehavior.AllowGet);
        }
    }
}
