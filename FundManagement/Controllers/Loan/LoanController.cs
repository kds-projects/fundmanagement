﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Models;

namespace FundManagement.Controllers
{
    public class LoanController : ContextList
    {
        //private FundContext db = new FundContext();

        // GET: /Loan/
        public ActionResult Index()
        {
            return View(db.LoanMains.ToList());
        }

        // GET: /Loan/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LoanMain loanMain = db.LoanMains.Find(id);
            if (loanMain == null)
            {
                return HttpNotFound();
            }
            return View(loanMain);
        }

        // GET: /Loan/Create
        [CAuthorize]
        public ActionResult Create()
        {
            ViewBag.Currency = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CURCODE")), "TypeId", "TypeName", 23);
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.LoanTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && c.GroupName.Equals("FUNDED") && !c.TypeName.Equals("OD")), "TypeId", "ShortName");
            ViewBag.PTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("INTERESTBASIS")), "TypeId", "TypeName");
            ViewBag.AcceptanceList = new SelectList(db.LCAcceptances.Where(c => c.LCAcceptanceId == 0), "LCAcceptanceId", "AcceptanceNo");
            ViewBag.PayScheduleType = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYSCHTYPE")), "TypeId", "TypeName");
            ViewBag.PayPeriod = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYPERIOD")), "TypeId", "TypeName");

            var lclist = (new[] { new { LCFileId = 0, LCNo = "N/A" } }).ToList();

            var invalidlist = (db.LoanMains.Select(c => c.LCAcceptanceId).ToList())
                .Union(db.PaymentSubs.Where(c => c.Principal > 0).Select(c => c.LCAcceptanceId))
                .Union(db.AdjustmentSubs.Where(c => c.Principal > 0).Select(c => c.LCAcceptanceId))
                .Distinct().ToList();

            lclist.AddRange((from p in db.LCAcceptances.AsEnumerable()
                             join q in db.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                             where !invalidlist.Contains(p.LCAcceptanceId)
                             select new { q.LCFileId, q.LCNo }
                            ).Distinct().ToList());

            ViewBag.LCFile = new SelectList(lclist, "LCFileId", "LCNo");

            return View();
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult Save(LoanMain loanMain, ICollection<PaymentSchedule> paymentSchedules)
        {
            try
            {
                //LoanMain loanmain = new LoanMain(loanMain);

                if (paymentSchedules != null)
                    foreach (var sub in paymentSchedules)
                    {
                        //PaymentSchedule payschedule = new PaymentSchedule(sub);
                        sub.LCFileId = loanMain.LCFileId;
                        sub.LCAcceptanceId = loanMain.LCAcceptanceId;
                        loanMain.PaymentSchedules.Add(sub);
                    }

                var _interest_rate_deatils = new InterestRateDetails()
                {
                    LCFileId = loanMain.LCFileId,
                    LCAcceptanceId = loanMain.LCAcceptanceId,
                    FromDate = loanMain.InterestStartDate,
                    ToDate = DateTime.Parse("01-Jan-1900"),
                    InstallmentAmount = loanMain.InstallmentAmount,
                    InterestRate = loanMain.InterestRate,
                    ODInterestRate = loanMain.InterestRateOD,
                    RowNo = 0
                };

                loanMain.InterestRateDetails.Add(_interest_rate_deatils);

                db.LoanMains.Add(loanMain);
                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success", loanno = loanMain.LoanNo }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // GET: /Loan/Edit/5
        [CAuthorize]
        public ActionResult Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LoanMain loanMain = db.LoanMains.Find(id);
            if (loanMain == null)
            {
                return HttpNotFound();
            }
            return View(loanMain);
        }

        // GET: /Loan/Transfer
        [CAuthorize]
        public ActionResult Transfer()
        {
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(c => c.LoanId == 0), "LoanId", "LoanNo");
            ViewBag.Currency = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("CURCODE") && c.Activity == ActivityType.Active).OrderBy(c => c.RankId), "TypeId", "TypeName", 23);
            ViewBag.PTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("INTERESTBASIS") && c.Activity == ActivityType.Active).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.SPTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYPERIOD") && c.Activity == ActivityType.Active).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.PSTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("PAYSCHTYPE") && c.Activity == ActivityType.Active).OrderBy(c => c.RankId), "TypeId", "TypeName");
            ViewBag.LoanTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && c.GroupName.Equals("FUNDED") && !c.TypeName.Equals("OD") && !c.TypeName.Equals("LDBP") && c.Activity == ActivityType.Active).OrderBy(c => c.RankId), "TypeId", "TypeName");

            return View();
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult SaveTransfer(DateTime TransferDate, ICollection<int> tloanlist, LoanMain loanMain, ICollection<PaymentSchedule> paymentSchedules)
        {
            using (var transaction = db.Database.BeginTransaction())
            {
                try
                {
                    if (paymentSchedules != null)
                    {
                        foreach (var sub in paymentSchedules)
                        {
                            //PaymentSchedule payschedule = new PaymentSchedule(sub);
                            sub.LCFileId = loanMain.LCFileId;
                            sub.LCAcceptanceId = loanMain.LCAcceptanceId;
                            loanMain.PaymentSchedules.Add(sub);
                        }
                    }

                    //loanMain.Remarks = "-";
                    db.LoanMains.Add(loanMain);
                    db.SaveChanges();

                    if (tloanlist != null)
                    {
                        foreach (var id in tloanlist)
                        {
                            LoanMain loanmain = db.LoanMains.Find(id);
                            loanmain.SettlementDate = TransferDate.AddDays(-1);
                            loanmain.SettlementBy = clsMain.getCurrentUser();
                            loanmain.IsSettled = 1;
                            loanmain.SettlementEntryDate = clsMain.getCurrentTime();

                            loanmain.TransferDate = TransferDate;
                            loanmain.TransferBy = clsMain.getCurrentUser();
                            loanmain.IsTransferred = 1;
                            loanmain.TransferEntryDate = clsMain.getCurrentTime();

                            loanmain.TransferredLoanId = loanMain.LoanId;

                            db.Entry(loanmain).State = EntityState.Modified;
                        }

                        db.SaveChanges();
                    }

                    transaction.Commit();

                    return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
                }
                catch (Exception ex)
                {
                    transaction.Rollback();

                    return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        // GET: /Loan/Settlement
        [CAuthorize]
        public ActionResult Settlement()
        {
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(c => c.LoanId == 0), "LoanId", "LoanNo");

            return View();
        }

        [HttpPost]
        [CAuthorize]
        public ActionResult SaveSettlement(DateTime SettlementDate, ICollection<int> loanlist)
        {
            try
            {
                if (loanlist != null)
                {
                    foreach (var id in loanlist)
                    {
                        LoanMain loanmain = db.LoanMains.Find(id);
                        loanmain.SettlementDate = SettlementDate;
                        loanmain.SettlementBy = clsMain.getCurrentUser();
                        loanmain.IsSettled = 1;
                        loanmain.SettlementEntryDate = clsMain.getCurrentTime();

                        db.Entry(loanmain).State = EntityState.Modified;
                    }
                }

                db.SaveChanges();

                //return View(pimain);
                return Json(new { status = "Success" }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
        
        // GET: /Loan/Settlement
        [HttpGet]
        [CAuthorize]
        public ActionResult Reschedule()
        {
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(c => c.LoanId == 0), "LoanId", "LoanNo");

            return View();
        }

        // GET: /Loan/Delete/5
        [CAuthorize]
        public ActionResult Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            LoanMain loanMain = db.LoanMains.Find(id);
            if (loanMain == null)
            {
                return HttpNotFound();
            }
            return View(loanMain);
        }

        // GET: /Loan/InterestGenerate
        [CAuthorize]
        public ActionResult InterestGenerate()
        {
            ViewBag.IssuerBank = new SelectList(db.Banks, "BankId", "BankName");
            ViewBag.IssuerBranch = new SelectList(db.BankBranchs.Where(c => c.BranchId == 0), "BranchId", "BranchName");
            ViewBag.Company = new SelectList(dbProd.Companies, "CompanyId", "CompanyName");
            ViewBag.LoanTypeList = new SelectList(db.CustomTypes.Where(c => c.Flag.Equals("LIABILITYTYPE") && c.GroupName.Equals("FUNDED") && !c.TypeName.Equals("OD")), "TypeId", "ShortName");
            ViewBag.LoanList = new SelectList(db.LoanMains.Where(c => c.LoanId == 0), "LoanId", "LoanNo");
            
            return View();
        }

        // GET: /Loan/InterestGenerate
        [CAuthorize] [HttpPost]
        public ActionResult InterestProcess(ReportView param)
        {
            try
            {
                db.Database.ExecuteSqlCommand("Exec dbo.prcInterestProcess {0}, {1}, {2}, {3}, {4}, {5}", param.CompanyId, param.BankId, param.BranchId, param.LoanTypeId, param.LoanId, param.AsOnDate);

                return Json(new { status = "Success", mname = param.AsOnDate.ToString("MMM, yyyy") }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                //ViewBag.errormessage = ex.Message;

                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: /Loan/Delete/5
        [HttpPost, ActionName("Delete")]
        [CAuthorize]
        public ActionResult DeleteConfirmed(int id)
        {
            LoanMain loanMain = db.LoanMains.Find(id);
            db.LoanMains.Remove(loanMain);
            db.SaveChanges();
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        // GET: LC
        public ActionResult AcceptanceList(int id)
        {
            if (id == 0)
            {
                var acplist = (new[] { new { LCAcceptanceId = 0, AcceptanceNo = "N/A" } }).ToList();

                ViewBag.AcceptanceList = new SelectList(acplist, "LCAcceptanceId", "AcceptanceNo");
            }
            else
            {
                var invalidlist = (db.LoanMains.Select(c => c.LCAcceptanceId).ToList())
                    .Union(db.PaymentSubs.Where(c => c.Principal > 0).Select(c => c.LCAcceptanceId))
                    .Union(db.AdjustmentSubs.Where(c => c.Principal > 0).Select(c => c.LCAcceptanceId))
                    .Distinct().ToList();

                var acplist = (from p in db.LCAcceptances.AsEnumerable()
                               join q in db.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                               where !invalidlist.Contains(p.LCAcceptanceId) && p.LCFileId == id
                               select new { p.LCAcceptanceId, p.AcceptanceNo }
                              ).Distinct().ToList();

                ViewBag.AcceptanceList = new SelectList(acplist, "LCAcceptanceId", "AcceptanceNo");
            }

            return PartialView();
        }

        // GET: LC
        public ActionResult AcceptanceDetails(int id)
        {
            try
            {
                var acp = (from p in db.LCAcceptances.AsEnumerable()
                           join q in db.LCMains.AsEnumerable() on p.LCFileId equals q.LCFileId
                           join rr in db.Banks.AsEnumerable() on q.IssuerBankId equals rr.BankId into rq
                           from r in rq.DefaultIfEmpty()
                           join u in db.PaymentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, Libor = c.Sum(l => l.Libor), HandlingCharge = c.Sum(l => l.HandlingCharge) }).AsEnumerable() on p.LCAcceptanceId equals u.LCAcceptanceId into s
                           from t in s.DefaultIfEmpty()
                           join w in db.AdjustmentSubs.GroupBy(c => c.LCAcceptanceId).Select(c => new { LCAcceptanceId = c.Key, Libor = c.Sum(l => l.Libor), HandlingCharge = c.Sum(l => l.HandlingCharge) }).AsEnumerable() on p.LCAcceptanceId equals w.LCAcceptanceId into x
                           from v in x.DefaultIfEmpty()
                           where p.LCAcceptanceId == id
                           select new
                           {
                               q.BeneficiaryId,
                               q.IssuerBankId,
                               q.IssuerBranchId,
                               InterestRate = r.LatestInterestRate,
                               PenaltyRate = r.LatestODInterestRate,
                               p.AcceptanceAmount, p.CurrencyId, p.BDTRate, p.USDRate,
                               Libor = (t != null ? 0 : p.LiborAmount + (v == null ? 0 : v.Libor)),
                               HandlingCharge = (t != null ? 0 : p.HandlingCharge + (v == null ? 0 : v.HandlingCharge)),
                               Amount = p.AcceptanceAmount +
                                      (t != null ? 0 : p.LiborAmount + (v == null ? 0 : v.Libor)) +
                                      (t != null ? 0 : p.HandlingCharge + (v == null ? 0 : v.HandlingCharge))
                           }
                          ).Distinct().FirstOrDefault();

                return Json(new { status = "Success", acp = acp }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }

        [HttpPost]
        //[CAuthorize]
        public ActionResult LoanSchedule(short type, DateTime startdate, decimal loanamount, short instno, decimal instamount, double interestrate, short payperiodid = 101)
        {
            List<PaymentSchedule> paymentSchedule = new List<PaymentSchedule>();

            if (type == 63)
            {
                var payperiod = Convert.ToInt16(db.CustomTypes.Find(payperiodid).ShortName);
                var balance = loanamount; // for example
                var periods = instno; // 30 years
                var monthlyRate = (interestrate / 100.00) / (12 / payperiod);  // 0.065= APR of 6.5% as decimal
                var monthlyPayment = Convert.ToDecimal((monthlyRate / (1 - (Math.Pow((1 + monthlyRate), -(instno)))))) * balance;

                for (short i = 1; i <= instno; i++)
                {
                    PaymentSchedule schedule = new PaymentSchedule();
                    schedule.InstallmentNo = i;
                    schedule.DueDate = startdate.AddMonths((i - 1) * payperiod);
                    schedule.OpeningBalance = balance;
                    schedule.InterestAmount = balance * Convert.ToDecimal(monthlyRate);
                    schedule.InterestRate = Convert.ToDecimal(interestrate);
                    schedule.PrincipalAmount = monthlyPayment - schedule.InterestAmount;
                    schedule.DueAmount = monthlyPayment;
                    balance -= schedule.PrincipalAmount; // probably should be -= principalForMonth see comments below
                    schedule.ClosingBalance = balance;

                    paymentSchedule.Add(schedule);
                }

                return PartialView("LoanSchedule_AMOR", paymentSchedule);
            }
            //else if (type.Equals("1011"))
            //{
            //    decimal tamount = 0;
            //    for (short i = 1; i <= 4; i++)
            //    {
            //        PaymentSchedule schedule = new PaymentSchedule();
            //        schedule.InstallmentNo = i;
            //        schedule.DueDate = startdate.AddDays((i - 1) * 90);

            //        if (i < 4)
            //        {
            //            schedule.DueAmount = loanamount * Convert.ToDecimal((i == 1 ? 10 : i == 2 ? 11 : i == 3 ? 12 : 0) / 100.00);
            //            tamount += schedule.DueAmount;
            //        }
            //        else
            //            schedule.DueAmount = loanamount - tamount;

            //        paymentSchedule.Add(schedule);
            //    }

            //    return PartialView(paymentSchedule);
            //}
            else if (type == 62)
            {
                PaymentSchedule schedule = new PaymentSchedule();
                schedule.InstallmentNo = 1;
                schedule.DueDate = startdate;
                schedule.DueAmount = loanamount;

                paymentSchedule.Add(schedule);

                return PartialView(paymentSchedule);
            }
            else
            {
                for (short i = 1; i <= instno; i++)
                {
                    PaymentSchedule schedule = new PaymentSchedule();
                    schedule.InstallmentNo = i;
                    schedule.DueDate = startdate.AddMonths(i-1);
                    schedule.PrincipalAmount = instamount;
                    schedule.DueAmount = instamount;

                    paymentSchedule.Add(schedule);
                }

                return PartialView("LoanSchedule_CUST", paymentSchedule);
            }
        }

        [HttpPost]
        //[CAuthorize]
        public ActionResult LoanReschedule(int LoanId, DateTime effectdate, double interestrate)
        {
            var loan = db.LoanMains.Find(LoanId);
            var paymentSchedule = loan.PaymentSchedules;

            var payperiod = Convert.ToInt16(db.CustomTypes.Find(loan.PaymentPeriodTypeId).ShortName);
            if (payperiod == 0 && loan.LoanTypeId != 62)
                payperiod = 1;
            var instno = paymentSchedule.Where(c => c.DueDate >= effectdate).Count(); 
            var balance = paymentSchedule.Where(c => c.DueDate < effectdate).OrderByDescending(c => c.DueDate).First().ClosingBalance; // for example
            //var periods = paymentSchedule.Where(c => c.DueDate >= effectdate).Count(); // 30 years
            var monthlyRate = (interestrate / 100.00) / (12 / payperiod);  // 0.065= APR of 6.5% as decimal
            var monthlyPayment = Convert.ToDecimal((monthlyRate / (1 - (Math.Pow((1 + monthlyRate), -(instno)))))) * balance;

            //for (short i = instno; i <= periods; i++)
            foreach(var schedule in paymentSchedule.Where(c => c.DueDate >= effectdate).OrderBy(c => c.DueDate))
            {
                //PaymentSchedule schedule = new PaymentSchedule();
                //schedule.InstallmentNo = i;
                //schedule.DueDate = effectdate.AddMonths((i - 1) * payperiod);
                schedule.OpeningBalance = balance;
                schedule.InterestAmount = balance * Convert.ToDecimal(monthlyRate);
                schedule.InterestRate = Convert.ToDecimal(interestrate);
                schedule.PrincipalAmount = monthlyPayment - schedule.InterestAmount;
                schedule.DueAmount = monthlyPayment;
                balance -= schedule.PrincipalAmount; // probably should be -= principalForMonth see comments below
                schedule.ClosingBalance = balance;

                //paymentSchedule.Add(schedule);
            }

            return PartialView("LoanSchedule_AMOR", paymentSchedule);
        }

        [HttpPost]
        public ActionResult DetailsTemplate(PaymentSchedule schedule)
        {
            return PartialView(schedule);
        }

        // GET: /Loan/RowTemplate
        [HttpPost]
        public ActionResult RowTemplate(PaymentSchedule schedule)
        {
            return PartialView(schedule);
        }


        // GET: LoanList
        public ActionResult LoanList(int comid, int bankid, int branchid, DateTime asondate, bool is_reschedule = false)
        {
            if (comid == 0 || bankid == 0 || branchid == 0)
            {
                var loanlist = (new[] { new { LoanId = 0, LoanNo = "N/A" } }).ToList();

                ViewBag.LoanList = new SelectList(loanlist, "LoanId", "LoanNo");
            }
            else if (is_reschedule == false)
            {
                decimal chkBalance = 1;
                var loans = db.LoanMains.Include(c => c.LCMain).Where(c => c.BeneficiaryId == comid && c.IssuerBankId == bankid && c.IssuerBranchId == branchid && (c.IsSettled == 0 && c.IsTransferred == 0)).Select(c => new { c.LoanId, LoanNo = c.LoanNo + " >> " + c.LCMain.LCNo }).ToList();
                var loanlist = (from p in loans.AsEnumerable()
                                join q in db.FundDetails.Where(c => c.LoanId > 0 && c.ExpireDate <= asondate).GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, CLAmount = c.Sum(l => l.Amount) }).AsEnumerable() on p.LoanId equals q.LoanId
                                join s in db.PaymentSchedules.Include(c => c.LoanMain).Where(c => c.DueDate <= asondate && c.LoanMain.ScheduleTypeId == 63).GroupBy(c => c.LoanId).Select(c => new { LoanId = c.Key, CLAmount = c.OrderByDescending(l => l.DueDate).Select(l => l.ClosingBalance).DefaultIfEmpty(0).FirstOrDefault() }).AsEnumerable() on p.LoanId equals s.LoanId into t
                                from r in t.DefaultIfEmpty()
                                select new { p.LoanId, p.LoanNo, CLBalance = r == null ? q.CLAmount : r.CLAmount }
                               ).Distinct().ToList();//.Where(c => c.CLBalance >= chkBalance)

                ViewBag.LoanList = new SelectList(loanlist, "LoanId", "LoanNo");
            }
            else
            {
                var loans = db.LoanMains.Include(c => c.LCMain).Where(c => c.BeneficiaryId == comid && c.IssuerBankId == bankid && c.IssuerBranchId == branchid && (c.IsSettled == 0 && c.IsTransferred == 0)).Select(c => new { c.LoanId, LoanNo = c.LoanNo + " >> " + c.LCMain.LCNo }).ToList();
                var loanlist = (from p in loans.AsEnumerable()
                                join s in db.PaymentSchedules.Include(c => c.LoanMain).Where(c => c.DueDate >= asondate && c.LoanMain.ScheduleTypeId >= 63).AsEnumerable() on p.LoanId equals s.LoanId
                                select new { p.LoanId, p.LoanNo }
                               ).Distinct().ToList();//.Where(c => c.CLBalance >= chkBalance)

                ViewBag.LoanList = new SelectList(loanlist, "LoanId", "LoanNo");
            }

            return PartialView();
        }

        // GET: LoanDetails
        [HttpPost]
        public ActionResult LoanDetails(int id, DateTime asondate, bool is_reschedule = false)
        {
            try
            {
                if (!is_reschedule)
                {
                    var loan = db.LoanMains.Find(id);
                    decimal outbl = 0;

                    if (loan.ScheduleTypeId != 63)
                    {
                        outbl = loan.LoanAmountBDT + db.FundDetails.Where(c => c.LoanId == id && c.ExpireDate <= asondate && c.RefTypeId != 3).Select(c => c.Amount).DefaultIfEmpty(0).Sum();
                    }
                    else
                    {
                        outbl = db.PaymentSchedules.Where(c => c.LoanId == id && c.DueDate <= asondate).OrderByDescending(c => c.DueDate).First().ClosingBalance;
                    }

                    return Json(new { status = "Success", outbl = outbl.ToString("##,##0.00") }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    var paymentSchedule = db.PaymentSchedules.Where(c => c.LoanId == id);

                    return PartialView("LoanSchedule_AMOR", paymentSchedule);
                }
            }
            catch (Exception ex)
            {
                return Json(new { status = "Error", message = (ex.InnerException == null ? ex.Message : ex.InnerException.InnerException.Message) }, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
