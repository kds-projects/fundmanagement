﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(FundManagement.Startup))]
namespace FundManagement
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
