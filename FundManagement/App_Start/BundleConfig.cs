﻿using System.Web;
using System.Web.Optimization;

namespace FundManagement
{
    public class BundleConfig
    {
        // For more information on bundling, visit http://go.microsoft.com/fwlink/?LinkId=301862
        public static void RegisterBundles(BundleCollection bundles)
        {
            bundles.Add(new ScriptBundle("~/bundles/jquery").Include(
                        "~/Scripts/jquery-{version}.js"));

            bundles.Add(new ScriptBundle("~/bundles/jqueryval").Include(
                        "~/Scripts/jquery.validate*"));

            // Use the development version of Modernizr to develop with and learn from. Then, when you're
            // ready for production, use the build tool at http://modernizr.com to pick only the tests you need.
            bundles.Add(new ScriptBundle("~/bundles/modernizr").Include(
                        "~/Scripts/modernizr-*"));

            bundles.Add(new ScriptBundle("~/bundles/bootstrap").Include(
                      "~/Scripts/bootstrap.js",
                      "~/Scripts/respond.js"));

            bundles.Add(new StyleBundle("~/vendor/css").Include(
                      //"~/Content/app-assets/css/fonts.css",
                      "~/Content/app-assets/css/vendors.css",
                      "~/Content/app-assets/vendors/css/forms/icheck/icheck.css",
                      "~/Content/app-assets/vendors/css/forms/icheck/custom.css",
                      "~/Content/app-assets/vendors/css/charts/morris.css",
                      "~/Content/app-assets/vendors/css/extensions/unslider.css",
                      "~/Content/app-assets/vendors/css/weather-icons/climacons.min.css",
                      "~/Content/app-assets/vendors/css/forms/selects/select2.min.css",
                      "~/Content/app-assets/css/app.css",
                      //
                      "~/Content/app-assets/vendors/css/ui/jquery-ui.min.css",
                      "~/app-assets/css/plugins/ui/jqueryui.css",
                      //"~/Content/app-assets/css/pages/users.css",
                      //"~/Content/app-assets/css/plugins/calendars/clndr.css",
                      //"~/Content/assets/css/style.css"
                      //
                      "~/Content/app-assets/css/core/menu/menu-types/horizontal-top-icon-menu.css",
                      "~/Content/app-assets/css/core/colors/palette-climacon.css",
                      "~/Content/app-assets/css/pages/users.css",
                      "~/Content/assets/css/style.css",
                      "~/Content/app-assets/css/style.css"));

            bundles.Add(new ScriptBundle("~/datatable/js").Include(
                      "~/Content/app-assets/vendors/js/tables/datatable/datatables.min.js"//,
                      //"~/Content/app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js",
                      //"~/Content/app-assets/js/scripts/tables/datatables/datatable-styling.js"
                      ));

            bundles.Add(new StyleBundle("~/datatable/css").Include(
                      "~/Content/app-assets/vendors/css/tables/datatable/datatables.min.css"));
        }
    }
}
