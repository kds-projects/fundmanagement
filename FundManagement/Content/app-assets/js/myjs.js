﻿
$(document).ajaxStart(function () {
    ajaxindicatorstart('loading data.. please wait..');
}).ajaxStop(function () {
    ajaxindicatorstop();
});

function ajaxindicatorstart(text) {
    if (jQuery('body').find('#resultLoading').attr('id') != 'resultLoading') {
        jQuery('body').append('<div id="resultLoading" style="display:none"><div><img src="/fund/content/app-assets/images/spinner.gif"><div style="color: black;font-weight: bold;">working.. please wait..</div></div><div class="bg"></div></div>');
    }

    jQuery('#resultLoading').css({
        'width': '100%',
        'height': '100%',
        'position': 'fixed',
        'z-index': '10000000',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto'
    });

    jQuery('#resultLoading .bg').css({
        'background': '#000000',
        'opacity': '0.1',
        'width': '0%',
        'height': '0%',
        'position': 'absolute',
        'top': '0'
    });

    jQuery('#resultLoading>div:first').css({
        'width': '100%',
        'height': '30%',
        'text-align': 'center',
        'position': 'fixed',
        'top': '0',
        'left': '0',
        'right': '0',
        'bottom': '0',
        'margin': 'auto',
        'font-size': '16px',
        'z-index': '10',
        'color': '#ffffff'

    });

    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeIn(300);
    jQuery('body').css('cursor', 'wait');
}

function ajaxindicatorstop() {
    jQuery('#resultLoading .bg').height('100%');
    jQuery('#resultLoading').fadeOut(300);
    jQuery('body').css('cursor', 'default');
}

//$('.only-number').each(function () {
//    $(this).val((new Number($(this).val().replace(/\,/g, ''))).toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//});

//$('.only-currency').each(function () {
//    $(this).val((new Number($(this).val().replace(/\,/g, ''))).toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//});

//$('input.only-number').on('keypress', function (e) {
//    return (e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
//});

//$('input.only-currency').on('keypress', function (e) {
//    return (e.which != 45 && e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
//});

//$('input.only-number').on('blur', function () {
//    if (isNaN(eval($(this).val().replace(',', '')))) {
//        $(this).val(0);
//    }
//    else {
//        var num = new Number($(this).val().replace(/\,/g, ''));
//        $(this).val(num.toFixed(0).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//    }
//});

//$('input.only-currency').on('blur', function () {
//    if (isNaN(eval($(this).val().replace(/\,/g, '')))) {
//        $(this).val("0.00");
//    }
//    else {
//        var num = new Number($(this).val().replace(/\,/g, ''));
//        $(this).val(num.toFixed(2).replace(/\B(?=(\d{3})+(?!\d))/g, ","));
//    }
//});

//function keypress(e) {
//    return (e.which != 46 && e.which != 8 && e.which != 0 && (e.which < 48 || e.which > 57)) ? false : true;
//};

//$('.rdate').daterangepicker({
//    singleDatePicker: true,
//    showDropdowns: true,
//    locale: {
//        format: 'DD-MMM-YYYY'
//    }
//});

function getJsonDate(JsonDate) {
    return new Date(parseInt(JsonDate.substr(6)));
}

function getFormatedJsonDate(JsonDate) {
    return getFormatedDate(new Date(parseInt(JsonDate.substr(6))));
}

function getFormatedDate(date) {
    var m_names = new Array("Jan", "Feb", "Mar",
    "Apr", "May", "Jun", "Jul", "Aug", "Sep",
    "Oct", "Nov", "Dec");

    var d = date;
    var curr_date = ("0" + (d.getDate())).slice(-2);
    var curr_month = d.getMonth();
    var curr_year = d.getFullYear();

    return curr_date + "-" + m_names[curr_month] + "-" + curr_year;
}

function convertStrToDate(strDate)
{
    var pad = strDate.toLowerCase().split('-');
    var sad = new Date(pad[1] + " " + pad[0] + ", " + pad[2]);

    var newDate = new Date(Date.UTC(sad.getFullYear(), sad.getMonth(), sad.getDate(), sad.getHours(), sad.getMinutes(), sad.getSeconds(), sad.getMilliseconds()));

    return newDate;
}

var myjsbrid = '', myjsbr_status = true;
function getBranchList(id, container, ctlname, descname) {
    container = container || "brlist";
    ctlname = ctlname || "IssuerBranchId";
    descname = descname || "Issuer Branch";
    id = id || 0;
    $.ajax({
        type: "POST",
        datatype: "json",
        contentType: "application/json; charset=utf-8",
        url: '/fund/BankBranch/BranchList',
        data: JSON.stringify({ id: id, ctlname: ctlname }),
        success: function (data) {
            $('#' + container).html(data);

            $('#' + ctlname).attr('data-val-required', $('#' + ctlname).attr('data-val-required').replace('Issuer Branch', descname));

            var attr = $('#' + ctlname).attr('data-val-number');
            if (typeof attr !== typeof undefined && attr !== false) {
                $('#' + ctlname).attr('data-val-number', $('#' + ctlname).attr('data-val-number').replace('Issuer Branch', descname));
            }

            $('#' + container + ' select.select').select2
                ({
                placeholder: "Select or Type . . .",
                //dropdownAutoWidth: true
            });

            $('#' + ctlname).val(myjsbrid).trigger('change');
            if (!myjsbr_status)
                $('#' + ctlname).select2("enable", myjsbr_status);
            myjsbrid = ''; myjsbr_status = true;
        },
        error: function (jqXHR, textStatus, errorThrown) {
            alert(errorThrown);
        }
    });
};