namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.BankAccount",
                c => new
                    {
                        AccountId = c.Short(nullable: false, identity: true),
                        BranchId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        AccountCode = c.String(nullable: false, maxLength: 25, unicode: false),
                        SerialNo = c.Short(nullable: false),
                        AccountNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        AccountType = c.Short(nullable: false),
                        CategoryId = c.Short(nullable: false),
                        FinancialLedgerCode = c.String(nullable: false, maxLength: 25, unicode: false),
                        FactoryId = c.Short(nullable: false),
                        OppeningDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        CurrencyId = c.Short(nullable: false),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.CustomType", t => t.CategoryId, cascadeDelete: false)
                .ForeignKey("dbo.CustomType", t => t.CurrencyId, cascadeDelete: false)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: false)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: false)
                .Index(t => t.BranchId)
                .Index(t => t.BankId)
                .Index(t => t.CategoryId)
                .Index(t => t.CurrencyId);
            
            CreateTable(
                "dbo.Bank",
                c => new
                    {
                        BankId = c.Short(nullable: false, identity: true),
                        BankName = c.String(nullable: false, maxLength: 150, unicode: false),
                        ShortName = c.String(nullable: false, maxLength: 50, unicode: false),
                        BankType = c.Short(nullable: false),
                        LatestInterestRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        LatestODInterestRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        EarlySettlementRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        LCCommissionRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        AcceptanceCommissionRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.BankId);
            
            CreateTable(
                "dbo.BankBranch",
                c => new
                    {
                        BranchId = c.Short(nullable: false, identity: true),
                        BankId = c.Short(nullable: false),
                        BranchCode = c.String(nullable: false, maxLength: 5, unicode: false),
                        BranchName = c.String(nullable: false, maxLength: 150, unicode: false),
                        ShortName = c.String(nullable: false, maxLength: 50, unicode: false),
                        Address = c.String(nullable: false),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.BranchId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: true)
                .Index(t => t.BankId);
            
            CreateTable(
                "dbo.SanctionDetails",
                c => new
                    {
                        SanctionId = c.Int(nullable: false, identity: true),
                        BranchId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        SanctionType = c.Short(nullable: false),
                        SanctionYear = c.Short(nullable: false),
                        SanctionLimit = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        AccountNo = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.SanctionId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: false)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.CustomType", t => t.SanctionType, cascadeDelete: false)
                .Index(t => t.BranchId)
                .Index(t => t.BankId)
                .Index(t => t.SanctionType);
            
            CreateTable(
                "dbo.CustomType",
                c => new
                    {
                        TypeId = c.Short(nullable: false, identity: true),
                        TypeName = c.String(nullable: false),
                        ShortName = c.String(nullable: false, maxLength: 50, unicode: false),
                        Flag = c.String(nullable: false),
                        Activity = c.Int(nullable: false),
                        RankId = c.Short(nullable: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.TypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.BankAccount", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.BankAccount", "BankId", "dbo.Bank");
            DropForeignKey("dbo.SanctionDetails", "SanctionType", "dbo.CustomType");
            DropForeignKey("dbo.BankAccount", "CurrencyId", "dbo.CustomType");
            DropForeignKey("dbo.BankAccount", "CategoryId", "dbo.CustomType");
            DropForeignKey("dbo.SanctionDetails", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.SanctionDetails", "BankId", "dbo.Bank");
            DropForeignKey("dbo.BankBranch", "BankId", "dbo.Bank");
            DropIndex("dbo.SanctionDetails", new[] { "SanctionType" });
            DropIndex("dbo.SanctionDetails", new[] { "BankId" });
            DropIndex("dbo.SanctionDetails", new[] { "BranchId" });
            DropIndex("dbo.BankBranch", new[] { "BankId" });
            DropIndex("dbo.BankAccount", new[] { "CurrencyId" });
            DropIndex("dbo.BankAccount", new[] { "CategoryId" });
            DropIndex("dbo.BankAccount", new[] { "BankId" });
            DropIndex("dbo.BankAccount", new[] { "BranchId" });
            DropTable("dbo.CustomType");
            DropTable("dbo.SanctionDetails");
            DropTable("dbo.BankBranch");
            DropTable("dbo.Bank");
            DropTable("dbo.BankAccount");
        }
    }
}
