namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCaptionDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CaptionDetails",
                c => new
                    {
                        CaptionId = c.Short(nullable: false, identity: true),
                        CaptionCode = c.String(nullable: false, maxLength: 5, unicode: false, defaultValue: ""),
                        CaptionName = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: ""),
                        ShortName = c.String(nullable: false, maxLength: 150, unicode: false, defaultValue: ""),
                        Title = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: ""),
                        Flag = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: ""),
                        CaptionPId = c.Short(nullable: false, defaultValue: 0),
                        CaptionPCode = c.String(nullable: false, maxLength: 5, unicode: false, defaultValue: ""),
                        Activity = c.Int(nullable: false, defaultValue: 1),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.CaptionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CaptionDetails");
        }
    }
}
