namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCashFlowDetails_Amount : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.CashFlowDetails", "Amount", "BudgetAmount");
            AddColumn("dbo.CashFlowDetails", "ActualAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
        }
        
        public override void Down()
        {
            RenameColumn("dbo.CashFlowDetails", "BudgetAmount", "Amount");
            DropColumn("dbo.CashFlowDetails", "ActualAmount");
        }
    }
}
