namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSignatoriesToPayorder : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PayorderInfo", "Signatories", c => c.String(nullable: false, maxLength: 150, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PayorderInfo", "Signatories");
        }
    }
}
