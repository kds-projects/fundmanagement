namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class CreateIndex : DbMigration
    {
        public override void Up()
        {
            DropIndex("dbo.BankBranch", new[] { "BankId" });
            CreateIndex("dbo.Bank", "BankName", unique: true);
            CreateIndex("dbo.Bank", "ShortName", unique: true);
            CreateIndex("dbo.BankBranch", new[] { "BankId", "BranchName" }, unique: true, name: "IX_BranchName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.BankBranch", "IX_BranchName");
            DropIndex("dbo.Bank", new[] { "ShortName" });
            DropIndex("dbo.Bank", new[] { "BankName" });
            CreateIndex("dbo.BankBranch", "BankId");
        }
    }
}
