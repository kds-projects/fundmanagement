namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ChequeIssueSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeIssueSub",
                c => new
                    {
                        InstructionSubId = c.Int(nullable: false, identity: true),
                        InstructionId = c.Int(nullable: false),
                        LPBillForwardId = c.Int(nullable: false, defaultValue: 0),
                        LPBillForwardDetailsId = c.Int(nullable: false, defaultValue: 0),
                        BillAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                    })
                .PrimaryKey(t => t.InstructionSubId)
                .ForeignKey("dbo.ChequeIssueInstruction", t => t.InstructionId, cascadeDelete: true)
                .Index(t => t.InstructionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueSub", "InstructionId", "dbo.ChequeIssueInstruction");
            DropIndex("dbo.ChequeIssueSub", new[] { "InstructionId" });
            DropTable("dbo.ChequeIssueSub");
        }
    }
}
