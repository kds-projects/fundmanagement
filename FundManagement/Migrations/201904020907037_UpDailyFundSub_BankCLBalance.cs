namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpDailyFundSub_BankCLBalance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyFundSub", "BankClosingBalance", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyFundSub", "BankClosingBalance");
        }
    }
}
