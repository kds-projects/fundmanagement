namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCSSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CSSub",
                c => new
                    {
                        CSSubId = c.Int(nullable: false, identity: true),
                        CSId = c.Int(nullable: false),
                        ConvertRate1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        UPASS1 = c.Short(nullable: false, defaultValue: 0),
                        PostUPASS1 = c.Short(nullable: false, defaultValue: 0),
                        Libor1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        Handling1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        InterestRate1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        LCCommissionRate1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        AcceptanceCommissionRate1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        OtherChargesPerLC1 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        ConvertRate2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        UPASS2 = c.Short(nullable: false, defaultValue: 0),
                        PostUPASS2 = c.Short(nullable: false, defaultValue: 0),
                        Libor2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        Handling2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        InterestRate2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        LCCommissionRate2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        AcceptanceCommissionRate2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        OtherChargesPerLC2 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        ConvertRate3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        UPASS3 = c.Short(nullable: false, defaultValue: 0),
                        PostUPASS3 = c.Short(nullable: false, defaultValue: 0),
                        Libor3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        Handling3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        InterestRate3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        LCCommissionRate3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        AcceptanceCommissionRate3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                        OtherChargesPerLC3 = c.Decimal(nullable: false, precision: 24, scale: 2, defaultValue: 0),
                    })
                .PrimaryKey(t => t.CSSubId)
                .ForeignKey("dbo.CSMain", t => t.CSId, cascadeDelete: true)
                .Index(t => t.CSId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CSSub", "CSId", "dbo.CSMain");
            DropIndex("dbo.CSSub", new[] { "CSId" });
            DropTable("dbo.CSSub");
        }
    }
}
