namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_PayPeriod : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "PaymentPeriodTypeId", c => c.Short(nullable: false, defaultValue: 101));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "PaymentPeriodTypeId");
        }
    }
}
