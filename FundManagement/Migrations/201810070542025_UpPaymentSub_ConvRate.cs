namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPaymentSub_ConvRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentSub", "BDTRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "USDRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.PaymentSub ADD [AmountInBDT] AS [Amount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.PaymentSub ADD [AmountInUSD] AS [Amount] * [USDRate] Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentSub", "AmountInBDT");
            DropColumn("dbo.PaymentSub", "AmountInUSD");
            DropColumn("dbo.PaymentSub", "USDRate");
            DropColumn("dbo.PaymentSub", "BDTRate");
        }
    }
}
