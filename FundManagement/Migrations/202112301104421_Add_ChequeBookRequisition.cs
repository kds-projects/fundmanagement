namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ChequeBookRequisition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeBookRequisition",
                c => new
                    {
                        RequisitionId = c.Int(nullable: false, identity: true),
                        AccountId = c.Short(nullable: false),
                        BranchId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        RequisitionNo = c.String(nullable: false, maxLength: 20, unicode: false),
                        RequisitionDate = c.DateTime(nullable: false),
                        PaymentModeId = c.Short(nullable: false),
                        BookCount = c.Short(nullable: false),
                        TotalCheque = c.Short(nullable: false),
                        ApprovalType = c.Short(nullable: false, defaultValue: 0),
                        ApprovedBy = c.Short(nullable: false, defaultValue: 0),
                        ApprovedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        Status = c.Short(nullable: false),
                        Activity = c.Int(nullable: false, defaultValue: 1),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.RequisitionId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: true)
                .ForeignKey("dbo.BankAccount", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: true)
                .ForeignKey("dbo.CustomType", t => t.PaymentModeId, cascadeDelete: true)
                .Index(t => t.AccountId)
                .Index(t => t.BranchId)
                .Index(t => t.BankId)
                .Index(t => t.PaymentModeId);


            Sql("ALTER TABLE dbo.ChequeBookRequisition ADD [Month] AS Month([RequisitionDate]) Persisted");
            Sql("ALTER TABLE dbo.ChequeBookRequisition ADD [Year] AS Year([RequisitionDate]) Persisted");
            Sql("ALTER TABLE dbo.ChequeBookRequisition ADD [SerialNo] AS (CONVERT([int],substring([RequisitionNo],(3),(4)),(0))) Persisted");

            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeBookRequisition", "PaymentModeId", "dbo.CustomType");
            DropForeignKey("dbo.ChequeBookRequisition", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.ChequeBookRequisition", "AccountId", "dbo.BankAccount");
            DropForeignKey("dbo.ChequeBookRequisition", "BankId", "dbo.Bank");
            DropIndex("dbo.ChequeBookRequisition", new[] { "PaymentModeId" });
            DropIndex("dbo.ChequeBookRequisition", new[] { "BankId" });
            DropIndex("dbo.ChequeBookRequisition", new[] { "BranchId" });
            DropIndex("dbo.ChequeBookRequisition", new[] { "AccountId" });
            DropTable("dbo.ChequeBookRequisition");
        }
    }
}
