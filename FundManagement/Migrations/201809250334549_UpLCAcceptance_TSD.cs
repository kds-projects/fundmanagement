namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCAcceptance_TSD : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LCAcceptance", "TenureStartDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getDate()"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LCAcceptance", "TenureStartDate");
        }
    }
}
