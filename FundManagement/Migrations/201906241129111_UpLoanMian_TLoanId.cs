namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMian_TLoanId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "TransferredLoanId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "TransferredLoanId");
        }
    }
}
