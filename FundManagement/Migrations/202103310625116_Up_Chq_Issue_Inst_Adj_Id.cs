namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Chq_Issue_Inst_Adj_Id : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueSub", "AdjustmentId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueSub", "AdjustmentId");
        }
    }
}
