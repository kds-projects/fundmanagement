namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_SchType : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.LoanMain", "LoanType", "LoanTypeId");
            AddColumn("dbo.LoanMain", "ScheduleTypeId", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            RenameColumn("dbo.LoanMain", "LoanTypeId", "LoanType");
            DropColumn("dbo.LoanMain", "ScheduleTypeId");
        }
    }
}
