namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Bank_LCMId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bank", "LCMBankId", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bank", "LCMBankId");
        }
    }
}
