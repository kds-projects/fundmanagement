namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLocalAgent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LocalAgent",
                c => new
                    {
                        AgentId = c.Short(nullable: false, identity: true),
                        CountryId = c.Short(nullable: false),
                        AgentName = c.String(nullable: false, maxLength: 150, unicode: false),
                        AgentAddress = c.String(nullable: false, maxLength: 250, unicode: false),
                        PhoneNo = c.String(nullable: false, maxLength: 100, unicode: false),
                        Fax = c.String(nullable: false, maxLength: 100, unicode: false, defaultValue: "-"),
                        eMailAddress = c.String(nullable: false, maxLength: 100, unicode: false, defaultValue: ""),
                        WebAddress = c.String(nullable: false, maxLength: 100, unicode: false, defaultValue: ""),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.AgentId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LocalAgent");
        }
    }
}
