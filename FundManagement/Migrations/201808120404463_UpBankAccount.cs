namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpBankAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccount", "CompanyId", c => c.Short(nullable: false));
            DropColumn("dbo.BankAccount", "FactoryId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.BankAccount", "FactoryId", c => c.Short(nullable: false));
            DropColumn("dbo.BankAccount", "CompanyId");
        }
    }
}
