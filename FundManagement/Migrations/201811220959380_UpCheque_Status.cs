namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCheque_Status : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeInfo", "Status", c => c.Short(nullable: false, defaultValue: 1));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeInfo", "Status");
        }
    }
}
