namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddWhrafrent : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Wharfrent",
                c => new
                    {
                        WharfrentId = c.Int(nullable: false, identity: true),
                        LcFileId = c.Int(nullable: false),
                        LotNo = c.String(nullable: false),
                        BillDate = c.DateTime(nullable: false),
                        BrethingDate = c.DateTime(nullable: false),
                        CLDate = c.DateTime(nullable: false),
                        DeliveryDate = c.DateTime(nullable: false),
                        WhrafRentFromDate = c.DateTime(nullable: false),
                        WhrafRentToDate = c.DateTime(nullable: false),
                        WhrafRentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ContainerDetentionCharge = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Remarks = c.String(),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RevisedBy = c.Short(nullable: false),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.WharfrentId)
                .ForeignKey("dbo.LCMain", t => t.LcFileId, cascadeDelete: true)
                .Index(t => t.LcFileId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Wharfrent", "LcFileId", "dbo.LCMain");
            DropIndex("dbo.Wharfrent", new[] { "LcFileId" });
            DropTable("dbo.Wharfrent");
        }
    }
}
