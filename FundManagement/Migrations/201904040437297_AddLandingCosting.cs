namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLandingCosting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LandedCosting",
                c => new
                    {
                        LandedCostingId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        PLEId = c.Int(nullable: false),
                        LCOpeningCommission = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        LCOpeningCommissionVAT = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        SwiftCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        StampAndStationery = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        PostageAndCourier = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        AmendmentCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        CreditReportCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        ShippingAgentCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        PortCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        ShippingGuaranteeCommission = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        CNFCost = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        InsurancePremium = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        AcceptanceCommission = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        Duty = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        AIT = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        LIBOR = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        HandlingCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        CostValue = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        FreightCharges = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        Margin = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        CurrencyId = c.Short(nullable: false, defaultValue: 23),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.LandedCostingId)
                .ForeignKey("dbo.CustomType", t => t.CurrencyId, cascadeDelete: false)
                .ForeignKey("dbo.LCAcceptance", t => t.LCAcceptanceId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: false)
                .Index(t => t.LCFileId)
                .Index(t => t.LCAcceptanceId)
                .Index(t => t.CurrencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LandedCosting", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LandedCosting", "LCAcceptanceId", "dbo.LCAcceptance");
            DropForeignKey("dbo.LandedCosting", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.LandedCosting", new[] { "CurrencyId" });
            DropIndex("dbo.LandedCosting", new[] { "LCAcceptanceId" });
            DropIndex("dbo.LandedCosting", new[] { "LCFileId" });
            DropTable("dbo.LandedCosting");
        }
    }
}
