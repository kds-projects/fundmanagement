// <auto-generated />
namespace FundManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class AddTDSVDS : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(AddTDSVDS));
        
        string IMigrationMetadata.Id
        {
            get { return "202306130404145_AddTDSVDS"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
