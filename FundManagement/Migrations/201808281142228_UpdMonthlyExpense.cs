namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdMonthlyExpense : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.MonthlyExpense", "UtilityRemarks", c => c.String(nullable: false, maxLength: 250, unicode: false));
            DropColumn("dbo.MonthlyExpense", "UtilitiyRemarks");
        }
        
        public override void Down()
        {
            AddColumn("dbo.MonthlyExpense", "UtilitiyRemarks", c => c.String(nullable: false, maxLength: 250, unicode: false));
            DropColumn("dbo.MonthlyExpense", "UtilityRemarks");
        }
    }
}
