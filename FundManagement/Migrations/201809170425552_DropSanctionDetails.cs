namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropSanctionDetails : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.SanctionDetails", "BankId", "dbo.Bank");
            DropForeignKey("dbo.SanctionDetails", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.SanctionDetails", "SanctionType", "dbo.CustomType");
            DropIndex("dbo.SanctionDetails", new[] { "BranchId" });
            DropIndex("dbo.SanctionDetails", new[] { "BankId" });
            DropIndex("dbo.SanctionDetails", new[] { "SanctionType" });
            DropTable("dbo.SanctionDetails");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.SanctionDetails",
                c => new
                    {
                        SanctionId = c.Int(nullable: false, identity: true),
                        BranchId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        SanctionType = c.Short(nullable: false),
                        SanctionYear = c.Short(nullable: false),
                        SanctionLimit = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RevisedBy = c.Short(nullable: false),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.SanctionId);
            
            CreateIndex("dbo.SanctionDetails", "SanctionType");
            CreateIndex("dbo.SanctionDetails", "BankId");
            CreateIndex("dbo.SanctionDetails", "BranchId");
            AddForeignKey("dbo.SanctionDetails", "SanctionType", "dbo.CustomType", "TypeId", cascadeDelete: true);
            AddForeignKey("dbo.SanctionDetails", "BranchId", "dbo.BankBranch", "BranchId", cascadeDelete: true);
            AddForeignKey("dbo.SanctionDetails", "BankId", "dbo.Bank", "BankId", cascadeDelete: true);
        }
    }
}
