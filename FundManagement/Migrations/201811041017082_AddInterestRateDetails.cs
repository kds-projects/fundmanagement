namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddInterestRateDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.InterestRateDetails",
                c => new
                    {
                        InterestRateId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false, defaultValue: 0),
                        LCAcceptanceId = c.Int(nullable: false, defaultValue: 0),
                        LoanId = c.Int(nullable: false, defaultValue: 0),
                        FromDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        ToDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        InterestRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        ODInterestRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        InstallmentAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        RowNo = c.Short(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.InterestRateId)
                .ForeignKey("dbo.LCAcceptance", t => t.LCAcceptanceId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: false)
                .ForeignKey("dbo.LoanMain", t => t.LoanId, cascadeDelete: false)
                .Index(t => t.LCFileId)
                .Index(t => t.LCAcceptanceId)
                .Index(t => t.LoanId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InterestRateDetails", "LoanId", "dbo.LoanMain");
            DropForeignKey("dbo.InterestRateDetails", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.InterestRateDetails", "LCAcceptanceId", "dbo.LCAcceptance");
            DropIndex("dbo.InterestRateDetails", new[] { "LoanId" });
            DropIndex("dbo.InterestRateDetails", new[] { "LCAcceptanceId" });
            DropIndex("dbo.InterestRateDetails", new[] { "LCFileId" });
            DropTable("dbo.InterestRateDetails");
        }
    }
}
