namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChequeReceive : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeReceive",
                c => new
                    {
                        ChequeReceiveId = c.Int(nullable: false, identity: true),
                        ChequeId = c.Int(nullable: false),
                        ChequeNo = c.String(),
                        IssueDate = c.DateTime(nullable: false),
                        ChequeDate = c.DateTime(nullable: false),
                        ReceiveDate = c.DateTime(nullable: false),
                        IssueTo = c.String(),
                        ChequeAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Purpose = c.String(),
                        Remarks = c.String(),
                        AccountId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        BranchId = c.Short(nullable: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RevisedBy = c.Short(nullable: false),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.ChequeReceiveId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: true)
                .ForeignKey("dbo.BankAccount", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: true)
                .ForeignKey("dbo.ChequeInfo", t => t.ChequeId, cascadeDelete: true)
                .Index(t => t.ChequeId)
                .Index(t => t.AccountId)
                .Index(t => t.BankId)
                .Index(t => t.BranchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeReceive", "ChequeId", "dbo.ChequeInfo");
            DropForeignKey("dbo.ChequeReceive", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.ChequeReceive", "AccountId", "dbo.BankAccount");
            DropForeignKey("dbo.ChequeReceive", "BankId", "dbo.Bank");
            DropIndex("dbo.ChequeReceive", new[] { "BranchId" });
            DropIndex("dbo.ChequeReceive", new[] { "BankId" });
            DropIndex("dbo.ChequeReceive", new[] { "AccountId" });
            DropIndex("dbo.ChequeReceive", new[] { "ChequeId" });
            DropTable("dbo.ChequeReceive");
        }
    }
}
