namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoanMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoanMain",
                c => new
                    {
                        LoanId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        LaonNo = c.String(nullable: false, maxLength: 25, unicode: false),
                        LoanDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        BeneficiaryId = c.Short(nullable: false),
                        IssuerBankId = c.Short(nullable: false),
                        IssuerBranchId = c.Short(nullable: false),
                        LoanType = c.Short(nullable: false),
                        InterestRate = c.Decimal(nullable: false, precision: 5, scale: 2, storeType: "numeric"),
                        InterestRateOD = c.Decimal(nullable: false, precision: 5, scale: 2, storeType: "numeric"),
                        InterestStartDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        InterestEndDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        InterestProcessType = c.Short(nullable: false),
                        LaonAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        DownPayment = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        LeaseDeposit = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        InstallmentNo = c.Short(nullable: false, defaultValue: 1),
                        InstallmentAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        InstallmentStartDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        ExpireDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.LoanId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.LoanMain");
        }
    }
}
