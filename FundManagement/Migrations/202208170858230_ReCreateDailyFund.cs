namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ReCreateDailyFund : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyFundMain",
                c => new
                {
                    DFId = c.Int(nullable: false, identity: true),
                    DFDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                })
                .PrimaryKey(t => t.DFId);


            Sql("ALTER TABLE dbo.DailyFundMain ADD [DFYear] AS Month([DFDate]) Persisted");
            Sql("ALTER TABLE dbo.DailyFundMain ADD [DFMonth] AS Year([DFDate]) Persisted");

            AddColumn("dbo.DailyFundMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.DailyFundMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.DailyFundMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.DailyFundMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.DailyFundMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.DailyFundMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.DailyFundMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));

            CreateTable(
                "dbo.DailyFundSub",
                c => new
                    {
                        DFSubId = c.Int(nullable: false, identity: true),
                        DFId = c.Int(nullable: false),
                        AccountId = c.Short(nullable: false),
                        BankOpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BankClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BookOpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BookClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        DebitAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        CreditAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        InterestRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.DFSubId)
                .ForeignKey("dbo.BankAccount", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.DailyFundMain", t => t.DFId, cascadeDelete: true)
                .Index(t => t.DFId)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyFundSub", "DFId", "dbo.DailyFundMain");
            DropForeignKey("dbo.DailyFundSub", "AccountId", "dbo.BankAccount");
            DropIndex("dbo.DailyFundSub", new[] { "AccountId" });
            DropIndex("dbo.DailyFundSub", new[] { "DFId" });
            DropTable("dbo.DailyFundSub");
            DropTable("dbo.DailyFundMain");
        }
    }
}
