namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpBankBranch : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankBranch", "SwiftCode", c => c.String(nullable: false, maxLength: 15, unicode: false));
            AddColumn("dbo.BankBranch", "ContactPerson", c => c.String(nullable: false, maxLength: 75, unicode: false));
            AddColumn("dbo.BankBranch", "ContactNumber", c => c.String(nullable: false, maxLength: 30, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankBranch", "ContactNumber");
            DropColumn("dbo.BankBranch", "ContactPerson");
            DropColumn("dbo.BankBranch", "SwiftCode");
        }
    }
}
