namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAccountAgainstLoanType : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountAgainstLoanType",
                c => new
                    {
                        AccountId = c.Short(nullable: false, identity: true),
                        GroupId = c.Short(nullable: false),
                        AccountsCompanyId = c.String(nullable: false, maxLength: 50, unicode: false),
                        BankId = c.Short(nullable: false),
                        BranchId = c.Short(nullable: false),
                        LoanTypeId = c.Short(nullable: false),
                        LedgerCode = c.String(maxLength: 50, unicode: false),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: false)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.CustomType", t => t.LoanTypeId, cascadeDelete: false)
                .Index(t => t.BankId)
                .Index(t => t.BranchId)
                .Index(t => t.LoanTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AccountAgainstLoanType", "LoanTypeId", "dbo.CustomType");
            DropForeignKey("dbo.AccountAgainstLoanType", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.AccountAgainstLoanType", "BankId", "dbo.Bank");
            DropIndex("dbo.AccountAgainstLoanType", new[] { "LoanTypeId" });
            DropIndex("dbo.AccountAgainstLoanType", new[] { "BranchId" });
            DropIndex("dbo.AccountAgainstLoanType", new[] { "BankId" });
            DropTable("dbo.AccountAgainstLoanType");
        }
    }
}
