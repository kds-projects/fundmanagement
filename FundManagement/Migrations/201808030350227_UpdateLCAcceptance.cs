namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLCAcceptance : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LCAcceptance", "AcceptanceTenure", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LCAcceptance", "LoanTenure", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LCAcceptance", "LoanTenure");
            DropColumn("dbo.LCAcceptance", "AcceptanceTenure");
        }
    }
}
