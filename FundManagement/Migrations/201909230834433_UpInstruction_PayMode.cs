namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpInstruction_PayMode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "ChequeModeId", c => c.Short(nullable: false, defaultValue: 83));
            AddColumn("dbo.ChequeIssueInstruction", "PaymentModeId", c => c.Short(nullable: false, defaultValue: 8));
            CreateIndex("dbo.ChequeIssueInstruction", "ChequeModeId");
            CreateIndex("dbo.ChequeIssueInstruction", "PaymentModeId");
            AddForeignKey("dbo.ChequeIssueInstruction", "ChequeModeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            AddForeignKey("dbo.ChequeIssueInstruction", "PaymentModeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "PaymentModeId", "dbo.CustomType");
            DropForeignKey("dbo.ChequeIssueInstruction", "ChequeModeId", "dbo.CustomType");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "PaymentModeId" });
            DropIndex("dbo.ChequeIssueInstruction", new[] { "ChequeModeId" });
            DropColumn("dbo.ChequeIssueInstruction", "PaymentModeId");
            DropColumn("dbo.ChequeIssueInstruction", "ChequeModeId");
        }
    }
}
