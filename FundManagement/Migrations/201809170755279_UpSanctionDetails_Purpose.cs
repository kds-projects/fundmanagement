namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpSanctionDetails_Purpose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SanctionDetails", "Purpose", c => c.String(nullable: false, maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.SanctionDetails", "Purpose");
        }
    }
}
