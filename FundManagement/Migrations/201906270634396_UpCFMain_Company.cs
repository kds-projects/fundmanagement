namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCFMain_Company : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CashFlowMain", "GroupId", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CashFlowMain", "GroupId");
        }
    }
}
