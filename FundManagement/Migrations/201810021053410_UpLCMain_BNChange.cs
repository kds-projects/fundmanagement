namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCMain_BNChange : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.LCMain", "NegotiatingBankId", "NegotiatingBank");
            RenameColumn("dbo.LCMain", "BeneficiaryBankId", "BeneficiaryBank");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.LCMain", "BeneficiaryBank", "BeneficiaryBankId");
            RenameColumn("dbo.LCMain", "NegotiatingBank", "NegotiatingBankId");
        }
    }
}
