namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Chq_Issue_Inst_Service_Bill : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueSub", "ServiceBillForwardId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueSub", "ServiceBillForwardId");
        }
    }
}
