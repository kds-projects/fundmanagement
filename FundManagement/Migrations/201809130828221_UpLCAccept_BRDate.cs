namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCAccept_BRDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LCAcceptance", "BankReceiveDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LCAcceptance", "BankReceiveDate");
        }
    }
}
