namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqIssueSub_Adj : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueSub", "Adjustment", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.ChequeIssueSub", "AdjustmentDate", c => c.DateTime(storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueSub", "AdjustmentDate");
            DropColumn("dbo.ChequeIssueSub", "Adjustment");
        }
    }
}
