namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_LaonAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "LoanAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            DropColumn("dbo.LoanMain", "LaonAmount");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LoanMain", "LaonAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            DropColumn("dbo.LoanMain", "LoanAmount");
        }
    }
}
