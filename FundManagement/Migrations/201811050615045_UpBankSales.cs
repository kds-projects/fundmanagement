namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpBankSales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Bank", "SalesBankId", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Bank", "SalesBankId");
        }
    }
}
