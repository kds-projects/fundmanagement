namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_ConvRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "BDTRate", c => c.Decimal(nullable: false, precision: 8, scale: 3, storeType: "numeric", defaultValue: 1));
            AddColumn("dbo.LoanMain", "USDRate", c => c.Decimal(nullable: false, precision: 8, scale: 3, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.LoanMain ADD [AmountInBDT] AS [LoanAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [AmountInUSD] AS [LoanAmount] * [USDRate] Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "AmountInBDT");
            DropColumn("dbo.LoanMain", "AmountInUSD");
            DropColumn("dbo.LoanMain", "USDRate");
            DropColumn("dbo.LoanMain", "BDTRate");
        }
    }
}
