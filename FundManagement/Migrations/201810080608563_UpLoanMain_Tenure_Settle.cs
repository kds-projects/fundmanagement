namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_Tenure_Settle : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "TenureStartDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LoanMain", "LoanTenure", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanMain", "SettlementDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LoanMain", "SettlementBy", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "SettlementBy");
            DropColumn("dbo.LoanMain", "SettlementDate");
            DropColumn("dbo.LoanMain", "LoanTenure");
            DropColumn("dbo.LoanMain", "TenureStartDate");
        }
    }
}
