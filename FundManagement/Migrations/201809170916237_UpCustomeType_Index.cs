namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCustomeType_Index : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomType", "TypeName", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.CustomType", "Flag", c => c.String(nullable: false, maxLength: 50, unicode: false));
            CreateIndex("dbo.CustomType", new[] { "TypeName", "Flag" }, unique: true, name: "IX_TypeName");
        }
        
        public override void Down()
        {
            DropIndex("dbo.CustomType", "IX_TypeName");
            AlterColumn("dbo.CustomType", "Flag", c => c.String(nullable: false));
            AlterColumn("dbo.CustomType", "TypeName", c => c.String(nullable: false));
        }
    }
}
