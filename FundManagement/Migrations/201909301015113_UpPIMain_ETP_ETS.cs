namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPIMain_ETP_ETS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PIMain", "ETS", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.PIMain", "ETP", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PIMain", "ETP");
            DropColumn("dbo.PIMain", "ETS");
        }
    }
}
