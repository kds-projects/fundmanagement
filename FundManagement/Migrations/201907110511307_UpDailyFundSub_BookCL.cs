namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpDailyFundSub_BookCL : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.DailyFundSub", "BookClosingBalance", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.DailyFundSub", "BookClosingBalance");
        }
    }
}
