namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReturnDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wharfrent", "ContainerReturnDate", c => c.DateTime(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wharfrent", "ContainerReturnDate");
        }
    }
}
