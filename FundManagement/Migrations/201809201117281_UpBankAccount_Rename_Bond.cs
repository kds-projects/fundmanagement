namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpBankAccount_Rename_Bond : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.BankAccount", "AccountType", "AccountTypeId");
            RenameColumn("dbo.BankAccount", "FinancialLedgerCode", "LedgerCode");
            RenameColumn("dbo.BankAccount", "OppeningDate", "OpeningDate");
            AddColumn("dbo.BankAccount", "IsBondAccount", c => c.Short(nullable: false));
            CreateIndex("dbo.BankAccount", "AccountTypeId");
            AddForeignKey("dbo.BankAccount", "AccountTypeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            RenameColumn("dbo.BankAccount", "AccountTypeId", "AccountType");
            RenameColumn("dbo.BankAccount", "LedgerCode", "FinancialLedgerCode");
            RenameColumn("dbo.BankAccount", "OpeningDate", "OppeningDate");
            DropForeignKey("dbo.BankAccount", "AccountTypeId", "dbo.CustomType");
            DropIndex("dbo.BankAccount", new[] { "AccountTypeId" });
            DropColumn("dbo.BankAccount", "IsBondAccount");
        }
    }
}
