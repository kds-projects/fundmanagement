namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCSSub_Bank : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CSSub", "BankId1", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.CSSub", "BankId2", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.CSSub", "BankId3", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CSSub", "BankId3");
            DropColumn("dbo.CSSub", "BankId2");
            DropColumn("dbo.CSSub", "BankId1");
        }
    }
}
