namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpDAPosition_DAPId : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.DailyAccountPosition");
            AlterColumn("dbo.DailyAccountPosition", "DAPId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.DailyAccountPosition", "DAPId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.DailyAccountPosition");
            AlterColumn("dbo.DailyAccountPosition", "DAPId", c => c.Short(nullable: false, identity: true));
            AddPrimaryKey("dbo.DailyAccountPosition", "DAPId");
        }
    }
}
