namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentMain",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        PaymentNo = c.String(nullable: false, maxLength: 20, unicode: false),
                        PaymentDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.PaymentId)
                .Index(t => t.PaymentDate);


            Sql("ALTER TABLE dbo.PaymentMain ADD [PaymentSDate] AS (CONVERT([smalldatetime],CONVERT([date],[PaymentDate],(0)),(0))) Persisted");
            Sql("ALTER TABLE dbo.PaymentMain ADD [Month] AS Month([PaymentDate]) Persisted");
            Sql("ALTER TABLE dbo.PaymentMain ADD [Year] AS Year([PaymentDate]) Persisted");
            Sql("ALTER TABLE dbo.PaymentMain ADD [SerialNo] AS (CONVERT([int],substring([PaymentNo],(3),(4)),(0))) Persisted");

            AddColumn("dbo.PaymentMain", "PaymentTypeId", c => c.Short(nullable: false, defaultValue: 21));
            AddColumn("dbo.PaymentMain", "DocumentNo", c => c.String(maxLength: 75, defaultValue: "-", unicode: false));
            AddColumn("dbo.PaymentMain", "DocumentDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.PaymentMain", "PaymentAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentMain", "CurrencyId", c => c.Short(nullable: false, defaultValue: 21));
            AddColumn("dbo.PaymentMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.PaymentMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.PaymentMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.PaymentMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.PaymentMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.PaymentMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.PaymentMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
        }
        
        public override void Down()
        {
            DropIndex("dbo.PaymentMain", new[] { "PaymentDate" });
            DropTable("dbo.PaymentMain");
        }
    }
}
