namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class DropDailyFund : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.DailyFundSub", "AccountId", "dbo.BankAccount");
            DropForeignKey("dbo.DailyFundSub", "DFId", "dbo.DailyFundMain");
            DropIndex("dbo.DailyFundSub", new[] { "DFId" });
            DropIndex("dbo.DailyFundSub", new[] { "AccountId" });
            DropTable("dbo.DailyFundMain");
            DropTable("dbo.DailyFundSub");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.DailyFundSub",
                c => new
                    {
                        DFSubId = c.Short(nullable: false, identity: true),
                        DFId = c.Short(nullable: false),
                        AccountId = c.Short(nullable: false),
                        BankOpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BankClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BookOpeningBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        BookClosingBalance = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        DebitAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        CreditAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        InterestRate = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        Remarks = c.String(nullable: false, maxLength: 350, unicode: false),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.DFSubId);
            
            CreateTable(
                "dbo.DailyFundMain",
                c => new
                    {
                        DFId = c.Short(nullable: false, identity: true),
                        DFYear = c.Int(nullable: false),
                        DFMonth = c.Int(nullable: false),
                        DFDate = c.DateTime(nullable: false),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RevisedBy = c.Short(nullable: false),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.DFId);
            
            CreateIndex("dbo.DailyFundSub", "AccountId");
            CreateIndex("dbo.DailyFundSub", "DFId");
            AddForeignKey("dbo.DailyFundSub", "DFId", "dbo.DailyFundMain", "DFId", cascadeDelete: true);
            AddForeignKey("dbo.DailyFundSub", "AccountId", "dbo.BankAccount", "AccountId", cascadeDelete: true);
        }
    }
}
