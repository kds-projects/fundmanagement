namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ChequeIssuePipeline : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeIssuePipeline",
                c => new
                    {
                        InstructionPipelineId = c.Int(nullable: false, identity: true),
                        InstructionSubId = c.Int(nullable: false),
                        InstructionId = c.Int(nullable: false),
                        LPBillForwardId = c.Int(nullable: false),
                        LPBillForwardDetailsId = c.Int(nullable: false),
                        Tax = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric", defaultValue: 0),
                        VAT = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric", defaultValue: 0),
                    })
                .PrimaryKey(t => t.InstructionPipelineId)
                .ForeignKey("dbo.ChequeIssueInstruction", t => t.InstructionId, cascadeDelete: false)
                .ForeignKey("dbo.ChequeIssueSub", t => t.InstructionSubId, cascadeDelete: true)
                .Index(t => t.InstructionSubId)
                .Index(t => t.InstructionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssuePipeline", "InstructionSubId", "dbo.ChequeIssueSub");
            DropForeignKey("dbo.ChequeIssuePipeline", "InstructionId", "dbo.ChequeIssueInstruction");
            DropIndex("dbo.ChequeIssuePipeline", new[] { "InstructionId" });
            DropIndex("dbo.ChequeIssuePipeline", new[] { "InstructionSubId" });
            DropTable("dbo.ChequeIssuePipeline");
        }
    }
}
