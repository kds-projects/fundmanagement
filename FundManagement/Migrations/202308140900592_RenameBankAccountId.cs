namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RenameBankAccountId : DbMigration
    {
        public override void Up()
        {
            RenameColumn(table: "dbo.PayorderInfo", name: "BankAccountId", newName: "AccountId");
            RenameIndex(table: "dbo.PayorderInfo", name: "IX_BankAccountId", newName: "IX_AccountId");
        }
        
        public override void Down()
        {
            RenameIndex(table: "dbo.PayorderInfo", name: "IX_AccountId", newName: "IX_BankAccountId");
            RenameColumn(table: "dbo.PayorderInfo", name: "AccountId", newName: "BankAccountId");
        }
    }
}
