namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_BankAccount_Remarks : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankAccount", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankAccount", "Remarks");
        }
    }
}
