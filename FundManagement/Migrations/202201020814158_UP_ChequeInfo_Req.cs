namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UP_ChequeInfo_Req : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeInfo", "RequisitionId", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeInfo", "RequisitionId");
        }
    }
}
