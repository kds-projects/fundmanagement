namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddImportBudget : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ImportBudget",
                c => new
                    {
                        BudgetId = c.Short(nullable: false, identity: true),
                        BudgetYear = c.Short(nullable: false),
                        BudgetMonth = c.Short(nullable: false),
                        FactoryId = c.Short(nullable: false, defaultValue: 0),
                        TypeId = c.Short(nullable: false, defaultValue: 0),
                        BudgetAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        BudgetQuntity = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        QuantityUnitId = c.Short(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.BudgetId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ImportBudget");
        }
    }
}
