namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChequeIssueInstruction : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeIssueInstruction",
                c => new
                    {
                        InstructionId = c.Int(nullable: false, identity: true),
                        ChequeId = c.Int(nullable: false),
                        InstructionDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        ChequeDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IssuedAccountId = c.Short(nullable: false),
                        ChequeAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        Purpose = c.String(nullable: false, maxLength: 500, unicode: false),
                        UrgencyTypeId = c.Short(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.InstructionId)
                .ForeignKey("dbo.ChequeInfo", t => t.ChequeId, cascadeDelete: true)
                .ForeignKey("dbo.BankAccount", t => t.IssuedAccountId, cascadeDelete: false)
                .ForeignKey("dbo.CustomType", t => t.UrgencyTypeId, cascadeDelete: false)
                .Index(t => t.ChequeId)
                .Index(t => t.InstructionDate)
                .Index(t => t.ChequeDate)
                .Index(t => t.IssuedAccountId)
                .Index(t => t.UrgencyTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "UrgencyTypeId", "dbo.CustomType");
            DropForeignKey("dbo.ChequeIssueInstruction", "IssuedAccountId", "dbo.BankAccount");
            DropForeignKey("dbo.ChequeIssueInstruction", "ChequeId", "dbo.ChequeInfo");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "UrgencyTypeId" });
            DropIndex("dbo.ChequeIssueInstruction", new[] { "IssuedAccountId" });
            DropIndex("dbo.ChequeIssueInstruction", new[] { "ChequeDate" });
            DropIndex("dbo.ChequeIssueInstruction", new[] { "InstructionDate" });
            DropIndex("dbo.ChequeIssueInstruction", new[] { "ChequeId" });
            DropTable("dbo.ChequeIssueInstruction");
        }
    }
}
