namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdjustmentMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjustmentMain",
                c => new
                    {
                        AdjustmentId = c.Int(nullable: false, identity: true),
                        AdjustmentNo = c.String(nullable: false, maxLength: 20, unicode: false),
                        AdjustmentDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.AdjustmentId)
                .Index(t => t.AdjustmentDate);

            Sql("ALTER TABLE dbo.AdjustmentMain ADD [AdjustmentSDate] AS (CONVERT([smalldatetime],CONVERT([date],[AdjustmentDate],(0)),(0))) Persisted");
            Sql("ALTER TABLE dbo.AdjustmentMain ADD [Month] AS Month([AdjustmentDate]) Persisted");
            Sql("ALTER TABLE dbo.AdjustmentMain ADD [Year] AS Year([AdjustmentDate]) Persisted");
            Sql("ALTER TABLE dbo.AdjustmentMain ADD [SerialNo] AS (CONVERT([int],substring([AdjustmentNo],(3),(4)),(0))) Persisted");

            AddColumn("dbo.AdjustmentMain", "AdjustmentAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.AdjustmentMain", "CurrencyId", c => c.Short(nullable: false, defaultValue: 21));
            AddColumn("dbo.AdjustmentMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.AdjustmentMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.AdjustmentMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.AdjustmentMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.AdjustmentMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.AdjustmentMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.AdjustmentMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            CreateIndex("dbo.AdjustmentMain", "CurrencyId");
            AddForeignKey("dbo.AdjustmentMain", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdjustmentMain", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.AdjustmentMain", new[] { "CurrencyId" });
            DropIndex("dbo.AdjustmentMain", new[] { "AdjustmentDate" });
            DropTable("dbo.AdjustmentMain");
        }
    }
}
