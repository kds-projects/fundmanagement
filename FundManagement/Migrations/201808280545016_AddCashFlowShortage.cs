namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCashFlowShortage : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashFlowShortage",
                c => new
                    {
                        CFSId = c.Short(nullable: false, identity: true),
                        CFSYear = c.Short(nullable: false),
                        CFSMonth = c.Short(nullable: false),
                        DiscountingBDT = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        DiscountingUSD = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        LTL = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Revolving = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        STL_DL = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SisterConcern = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SisterConcernRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        ProjectLoan = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        ProjectRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        Others = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        OtherRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.CFSId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CashFlowShortage");
        }
    }
}
