namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqPayment_InstructionId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequePayment", "InstructionId", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequePayment", "InstructionId");
        }
    }
}
