namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPaymentSub_ActualCurrency : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentSub", "AmountInActualCurrency", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "ActualCurrencyRate", c => c.Decimal(nullable: false, precision: 10, scale: 7, storeType: "numeric", defaultValue: 1));
            AddColumn("dbo.PaymentSub", "ActualCurrencyId", c => c.Short(nullable: false, defaultValue: 23));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentSub", "ActualCurrencyId");
            DropColumn("dbo.PaymentSub", "ActualCurrencyRate");
            DropColumn("dbo.PaymentSub", "AmountInActualCurrency");
        }
    }
}
