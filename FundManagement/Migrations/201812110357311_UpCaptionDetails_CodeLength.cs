namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCaptionDetails_CodeLength : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CaptionDetails", "CaptionCode", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.CaptionDetails", "CaptionPCode", c => c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CaptionDetails", "CaptionPCode", c => c.String(nullable: false, maxLength: 5, unicode: false));
            AlterColumn("dbo.CaptionDetails", "CaptionCode", c => c.String(nullable: false, maxLength: 5, unicode: false));
        }
    }
}
