namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Chq_Issue_Inst_Adj_Amt : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "AdjustAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.ChequeIssueInstruction ADD [TotalAmount] AS ([ChequeAmount] + [AdjustAmount]) Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "TotalAmount");
            DropColumn("dbo.ChequeIssueInstruction", "AdjustAmount");
        }
    }
}
