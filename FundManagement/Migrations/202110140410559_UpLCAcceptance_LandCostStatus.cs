namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCAcceptance_LandCostStatus : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LCAcceptance", "LandedCostingStatus", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LCAcceptance", "LandedCostingStatus");
        }
    }
}
