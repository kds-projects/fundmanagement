namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddParentInstruction : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueTDSVDS", "ParentInstructionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueTDSVDS", "ParentInstructionId");
        }
    }
}
