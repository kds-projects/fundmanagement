namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_FK_LC_ACP : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.LoanMain", "LCFileId");
            CreateIndex("dbo.LoanMain", "LCAcceptanceId");
            AddForeignKey("dbo.LoanMain", "LCAcceptanceId", "dbo.LCAcceptance", "LCAcceptanceId", cascadeDelete: false);
            AddForeignKey("dbo.LoanMain", "LCFileId", "dbo.LCMain", "LCFileId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanMain", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LoanMain", "LCAcceptanceId", "dbo.LCAcceptance");
            DropIndex("dbo.LoanMain", new[] { "LCAcceptanceId" });
            DropIndex("dbo.LoanMain", new[] { "LCFileId" });
        }
    }
}
