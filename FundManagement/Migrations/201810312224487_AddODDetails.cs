namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;

    public partial class AddODDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ODDetails",
                c => new
                {
                    ODDetailsId = c.Int(nullable: false, identity: true),
                    ODId = c.Int(nullable: false),
                    TransactionDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                    ClosingBalance = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                    InterestRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0),
                    InterestRateOD = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0),
                    Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"),
                    CreatedBy = c.Short(nullable: false, defaultValue: 0),
                    CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                    RevisedBy = c.Short(nullable: false, defaultValue: 0),
                    RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                    IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                })
                .PrimaryKey(t => t.ODDetailsId)
                .ForeignKey("dbo.ODMain", t => t.ODId, cascadeDelete: true)
                .Index(t => t.ODId);

            Sql("ALTER TABLE dbo.ODDetails ADD [Interest] AS Round(([ClosingBalance] * [InterestRate])/100, 2) Persisted");
            Sql("ALTER TABLE dbo.ODDetails ADD [ODInterest] AS Round(([ClosingBalance] * [InterestRateOD])/100, 2) Persisted");

        }

        public override void Down()
        {
            DropForeignKey("dbo.ODDetails", "ODId", "dbo.ODMain");
            DropIndex("dbo.ODDetails", new[] { "ODId" });
            DropTable("dbo.ODDetails");
        }
    }
}
