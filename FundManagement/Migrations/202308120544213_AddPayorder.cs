namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPayorder : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PayorderDetails",
                c => new
                    {
                        PayorderDetailsId = c.Int(nullable: false, identity: true),
                        PayOrderId = c.Int(nullable: false),
                        IssuedAccountId = c.Short(nullable: false),
                        IssuedAccountName = c.String(nullable: false),
                        PayorderAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PayorderDetailsId)
                .ForeignKey("dbo.PayorderInfo", t => t.PayOrderId, cascadeDelete: true)
                .Index(t => t.PayOrderId);
            
            CreateTable(
                "dbo.PayorderInfo",
                c => new
                    {
                        PayorderId = c.Int(nullable: false, identity: true),
                        ReferenceNo = c.String(nullable: false),
                        BankAccountId = c.Short(nullable: false),
                        PayorderDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        RevisedBy = c.Short(nullable: false),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false),
                    })
                .PrimaryKey(t => t.PayorderId)
                .ForeignKey("dbo.BankAccount", t => t.BankAccountId, cascadeDelete: true)
                .Index(t => t.BankAccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PayorderDetails", "PayOrderId", "dbo.PayorderInfo");
            DropForeignKey("dbo.PayorderInfo", "BankAccountId", "dbo.BankAccount");
            DropIndex("dbo.PayorderInfo", new[] { "BankAccountId" });
            DropIndex("dbo.PayorderDetails", new[] { "PayOrderId" });
            DropTable("dbo.PayorderInfo");
            DropTable("dbo.PayorderDetails");
        }
    }
}
