namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqPayment_ChqMode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequePayment", "PaymentNo", c => c.String(nullable: false, maxLength: 15, unicode: false));
            AddColumn("dbo.ChequePayment", "IssuedAccountId", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequePayment", "ChequeModeId", c => c.Short(nullable: false, defaultValue: 83));
            CreateIndex("dbo.ChequePayment", "PaymentNo", unique: true, name: "IX_ChequePaymentNo");
            CreateIndex("dbo.ChequePayment", "ChequeModeId");
            AddForeignKey("dbo.ChequePayment", "ChequeModeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequePayment", "ChequeModeId", "dbo.CustomType");
            DropIndex("dbo.ChequePayment", new[] { "ChequeModeId" });
            DropIndex("dbo.ChequePayment", "IX_ChequePaymentNo");
            DropColumn("dbo.ChequePayment", "ChequeModeId");
            DropColumn("dbo.ChequePayment", "IssuedAccountId");
            DropColumn("dbo.ChequePayment", "PaymentNo");
        }
    }
}
