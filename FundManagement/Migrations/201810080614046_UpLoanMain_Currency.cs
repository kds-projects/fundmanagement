namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_Currency : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "CurrencyId", c => c.Short(nullable: false, defaultValue: 23));
            CreateIndex("dbo.LoanMain", "CurrencyId");
            AddForeignKey("dbo.LoanMain", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanMain", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.LoanMain", new[] { "CurrencyId" });
            DropColumn("dbo.LoanMain", "CurrencyId");
        }
    }
}
