namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFilePath : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wharfrent", "FilePath", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.Wharfrent", "FilePath");
        }
    }
}
