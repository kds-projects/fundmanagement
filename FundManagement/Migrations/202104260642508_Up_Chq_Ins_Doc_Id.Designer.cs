// <auto-generated />
namespace FundManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class Up_Chq_Ins_Doc_Id : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(Up_Chq_Ins_Doc_Id));
        
        string IMigrationMetadata.Id
        {
            get { return "202104260642508_Up_Chq_Ins_Doc_Id"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
