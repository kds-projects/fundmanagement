namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UP_ChequeBookRequisitionSub : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ChequeBookRequisition", "Activity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChequeBookRequisition", "Activity", c => c.Int(nullable: false));
        }
    }
}
