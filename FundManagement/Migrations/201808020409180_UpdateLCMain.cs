namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLCMain : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LCMain", "LCFileNo", c => c.String(nullable: false, maxLength: 15, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LCMain", "LCFileNo", c => c.String(nullable: false, maxLength: 10, unicode: false));
        }
    }
}
