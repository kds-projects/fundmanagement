namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReceivableDocument : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReceivableDocument",
                c => new
                    {
                        DocumentId = c.Short(nullable: false, identity: true),
                        DocumentNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        DocumentDate = c.DateTime(nullable: false),
                        ReceiveDate = c.DateTime(nullable: false),
                        DocumentTypeId = c.Short(nullable: false),
                        PayableId = c.Short(nullable: false),
                        PayableAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Activity = c.Int(nullable: false, defaultValue: 1),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.DocumentId)
                .ForeignKey("dbo.CustomType", t => t.DocumentTypeId, cascadeDelete: true)
                .Index(t => t.DocumentTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReceivableDocument", "DocumentTypeId", "dbo.CustomType");
            DropIndex("dbo.ReceivableDocument", new[] { "DocumentTypeId" });
            DropTable("dbo.ReceivableDocument");
        }
    }
}
