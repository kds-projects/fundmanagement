namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChequeInstruction_ChqNo : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "ChequeId", "dbo.ChequeInfo");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "ChequeId" });
            AddColumn("dbo.ChequeIssueInstruction", "InstructionNo", c => c.String(nullable: false, maxLength: 15, unicode: false, defaultValue: ""));
            CreateIndex("dbo.ChequeIssueInstruction", "InstructionNo", unique: true);
            DropColumn("dbo.ChequeIssueInstruction", "ChequeId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChequeIssueInstruction", "ChequeId", c => c.Int(nullable: false));
            DropIndex("dbo.ChequeIssueInstruction", new[] { "InstructionNo" });
            DropColumn("dbo.ChequeIssueInstruction", "InstructionNo");
            CreateIndex("dbo.ChequeIssueInstruction", "ChequeId");
            AddForeignKey("dbo.ChequeIssueInstruction", "ChequeId", "dbo.ChequeInfo", "ChequeId", cascadeDelete: false);
        }
    }
}
