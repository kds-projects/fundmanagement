namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChequeInfo_Cancel : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeInfo", "CancelDate", c => c.DateTime(nullable: false, defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.ChequeInfo", "CancelReason", c => c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"));
            AddColumn("dbo.ChequeInfo", "CancelEntryDate", c => c.DateTime(nullable: false, defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.ChequeInfo", "CancelledBy", c => c.Short(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeInfo", "CancelledBy");
            DropColumn("dbo.ChequeInfo", "CancelEntryDate");
            DropColumn("dbo.ChequeInfo", "CancelReason");
            DropColumn("dbo.ChequeInfo", "CancelDate");
        }
    }
}
