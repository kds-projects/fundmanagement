namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UPChequeInstruction_BillConfirm : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "OIBillConfirmId", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequeIssueInstruction", "OIBillConfirmFavourSubId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "OIBillConfirmFavourSubId");
            DropColumn("dbo.ChequeIssueInstruction", "OIBillConfirmId");
        }
    }
}
