namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCashFlowMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashFlowMain",
                c => new
                    {
                        CFId = c.Short(nullable: false, identity: true),
                        CFDate = c.DateTime(nullable: false, defaultValueSql: "getdate()"),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: ""),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.CFId);

            Sql("ALTER TABLE dbo.CashFlowMain ADD [CFMonth] AS Month([CFDate]) Persisted");
            Sql("ALTER TABLE dbo.CashFlowMain ADD [CFYear] AS Year([CFDate]) Persisted");
        }
        
        public override void Down()
        {
            DropTable("dbo.CashFlowMain");
        }
    }
}
