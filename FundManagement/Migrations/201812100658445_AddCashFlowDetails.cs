namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCashFlowDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CashFlowDetails",
                c => new
                    {
                        CFSubId = c.Short(nullable: false, identity: true),
                        CFId = c.Short(nullable: false),
                        CaptionId = c.Short(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.CFSubId)
                .ForeignKey("dbo.CaptionDetails", t => t.CaptionId, cascadeDelete: false)
                .ForeignKey("dbo.CashFlowMain", t => t.CFId, cascadeDelete: true)
                .Index(t => t.CFId)
                .Index(t => t.CaptionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CashFlowDetails", "CFId", "dbo.CashFlowMain");
            DropForeignKey("dbo.CashFlowDetails", "CaptionId", "dbo.CaptionDetails");
            DropIndex("dbo.CashFlowDetails", new[] { "CaptionId" });
            DropIndex("dbo.CashFlowDetails", new[] { "CFId" });
            DropTable("dbo.CashFlowDetails");
        }
    }
}
