namespace FundManagement.Migrations
{
    using FundManagement.Models;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<FundManagement.Models.FundContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(FundManagement.Models.FundContext context)
        {
            //  This method will be called after migrating to the latest version.

            //  You can use the DbSet<T>.AddOrUpdate() helper extension method 
            //  to avoid creating duplicate seed data.
            //
            //context.Banks.AddOrUpdate(
            //  p => p.BankId,
            //  new Bank()
            //  {
            //      BankId = 1,
            //      BankName = "N/A",
            //      ShortName = "N/A",
            //      BankType = 0,
            //      LatestInterestRate = 0,
            //      LatestODInterestRate = 0,
            //      AcceptanceCommissionRate = 0,
            //      LCCommissionRate = 0,
            //      EarlySettlementRate = 0,
            //      Activity = ActivityType.Active,
            //      CreatedBy = 0,
            //      CreatedTime = DateTime.UtcNow,
            //      RevisedBy = 0,
            //      RevisedTime = DateTime.Parse("01-Jan-1900")
            //  }
            //);
            ////
            //context.BankBranchs.AddOrUpdate(
            //  p => p.BranchId,
            //  new BankBranch()
            //  {
            //      BranchId = 1,
            //      BankId = 1,
            //      BranchCode = "BR001",
            //      BranchName = "N/A",
            //      ShortName = "N/A",
            //      Address = "-",
            //      Activity = ActivityType.Active,
            //      CreatedBy = 0,
            //      CreatedTime = DateTime.UtcNow,
            //      RevisedBy = 0,
            //      RevisedTime = DateTime.Parse("01-Jan-1900")
            //  }
            //);
            //
            //context.BankBranchs.AddOrUpdate(
            //  p => p.BranchId,
            //  new BankBranch()
            //  {
            //      BranchId = 1,
            //      BankId = 1,
            //      BranchCode = "BR001",
            //      BranchName = "N/A",
            //      ShortName = "N/A",
            //      Address = "-",
            //      Activity = ActivityType.Active,
            //      CreatedBy = 0,
            //      CreatedTime = DateTime.UtcNow,
            //      RevisedBy = 0,
            //      RevisedTime = DateTime.Parse("01-Jan-1900")
            //  }
            //);
        }
    }
}
