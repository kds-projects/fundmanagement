namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpFundDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FundDetails", "ExpireDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getDate()"));
            AddColumn("dbo.FundDetails", "ReferenceId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FundDetails", "ReferenceId");
            DropColumn("dbo.FundDetails", "ExpireDate");
        }
    }
}
