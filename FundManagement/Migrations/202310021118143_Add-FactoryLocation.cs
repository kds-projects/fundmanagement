namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFactoryLocation : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FactoryLoacation",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        FactoryId = c.Int(nullable: false),
                        LocationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FactoryLoacation");
        }
    }
}
