namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLandingCost : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LandedCosting", "OnlineCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "DonationFees", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "VAT", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "CNFAIT", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "ScanningCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "Wharfrent", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "LabourCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "ExcessGoods", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "CustomJettyExpenses", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "ServiceCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "TruckFare", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "BerthOperatorCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "RCCCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "HystarCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LandedCosting", "MiscellaneousExpenses", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            DropColumn("dbo.LandedCosting", "CNFCost");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LandedCosting", "CNFCost", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            DropColumn("dbo.LandedCosting", "MiscellaneousExpenses");
            DropColumn("dbo.LandedCosting", "HystarCharge");
            DropColumn("dbo.LandedCosting", "RCCCharge");
            DropColumn("dbo.LandedCosting", "BerthOperatorCharge");
            DropColumn("dbo.LandedCosting", "TruckFare");
            DropColumn("dbo.LandedCosting", "ServiceCharge");
            DropColumn("dbo.LandedCosting", "CustomJettyExpenses");
            DropColumn("dbo.LandedCosting", "ExcessGoods");
            DropColumn("dbo.LandedCosting", "LabourCharge");
            DropColumn("dbo.LandedCosting", "Wharfrent");
            DropColumn("dbo.LandedCosting", "ScanningCharge");
            DropColumn("dbo.LandedCosting", "CNFAIT");
            DropColumn("dbo.LandedCosting", "VAT");
            DropColumn("dbo.LandedCosting", "DonationFees");
            DropColumn("dbo.LandedCosting", "OnlineCharge");
        }
    }
}
