namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddFundDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.FundDetails",
                c => new
                    {
                        TransactionId = c.Int(nullable: false, identity: true),
                        TransactionDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        LCFileId = c.Int(nullable: false, defaultValue: 0),
                        LCAcceptanceId = c.Int(nullable: false, defaultValue: 0),
                        LoanId = c.Int(nullable: false, defaultValue: 0),
                        ScheduleId = c.Int(nullable: false, defaultValue: 0),
                        PaymentId = c.Int(nullable: false, defaultValue: 0),
                        PaymentSubId = c.Int(nullable: false, defaultValue: 0),
                        AdjustmentId = c.Int(nullable: false, defaultValue: 0),
                        AdjustmentSubId = c.Int(nullable: false, defaultValue: 0),
                        RefTypeId = c.Short(nullable: false),
                        RefNo = c.String(nullable: false, defaultValue: ""),
                        Principal = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        Interest = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        Libor = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        HandlingCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        ExciseDuty = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        BankCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        SettlementFee = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        Amount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0),
                        CurrencyId = c.Short(nullable: false),
                        IsConvertedToPrincipal = c.Short(nullable: false, defaultValue: 0),
                        ConvertDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValue: DateTime.Parse("1900-01-01")),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: ""),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.TransactionId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.FundDetails");
        }
    }
}
