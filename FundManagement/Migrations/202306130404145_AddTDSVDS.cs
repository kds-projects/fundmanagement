namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTDSVDS : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeIssueTDSVDS",
                c => new
                    {
                        InstructionTDSVDSId = c.Int(nullable: false, identity: true),
                        InstructionId = c.Int(nullable: false),
                        InsTructionSubId = c.Int(nullable: false),
                        BillAmount = c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"),
                        PaymentAmount = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BillNo = c.Int(nullable: false),
                        BillId = c.String(nullable: false),
                        Supp_id = c.Int(nullable: false),
                        BillType = c.String(nullable: false),
                        PaymentFor = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.InstructionTDSVDSId)
                .ForeignKey("dbo.ChequeIssueInstruction", t => t.InstructionId, cascadeDelete: false)
                .ForeignKey("dbo.ChequeIssueSub", t => t.InsTructionSubId, cascadeDelete: false)
                .Index(t => t.InstructionId)
                .Index(t => t.InsTructionSubId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueTDSVDS", "InsTructionSubId", "dbo.ChequeIssueSub");
            DropForeignKey("dbo.ChequeIssueTDSVDS", "InstructionId", "dbo.ChequeIssueInstruction");
            DropIndex("dbo.ChequeIssueTDSVDS", new[] { "InsTructionSubId" });
            DropIndex("dbo.ChequeIssueTDSVDS", new[] { "InstructionId" });
            DropTable("dbo.ChequeIssueTDSVDS");
        }
    }
}
