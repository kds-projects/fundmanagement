namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoanInterestTemp : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoanInterest_Temp",
                c => new
                {
                    InterestId = c.Int(nullable: false, identity: true),
                    LCFileId = c.Int(nullable: false),
                    LCAcceptanceId = c.Int(nullable: false),
                    LoanId = c.Int(nullable: false),
                    InterestDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                })
                .PrimaryKey(t => t.InterestId)
                .ForeignKey("dbo.LCAcceptance", t => t.LCAcceptanceId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: false)
                .ForeignKey("dbo.LoanMain", t => t.LoanId, cascadeDelete: false)
                .Index(t => t.LCFileId)
                .Index(t => t.LCAcceptanceId)
                .Index(t => t.LoanId);

            Sql("ALTER TABLE dbo.LoanInterest_Temp ADD [Month] AS Month([InterestDate]) Persisted");
            Sql("ALTER TABLE dbo.LoanInterest_Temp ADD [Year] AS Year([InterestDate]) Persisted");

            AddColumn("dbo.LoanInterest_Temp", "Principal", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "InterestRate", c => c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "Interest", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "PrincipalOD", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "InterestRateOD", c => c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "InterestOD", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "CurrencyId", c => c.Short(nullable: false, defaultValue: 21));
            AddColumn("dbo.LoanInterest_Temp", "IsConvertedToPrincipal", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "ConvertDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));

            Sql("ALTER TABLE dbo.LoanInterest_Temp ADD [Status] AS case when [Interest] > 0 And [InterestOD] > 0 then 'BOTH' when [Interest] > 0 then 'GENERAL' when [InterestOD] > 0 then 'PENALTY' else 'NONE' end Persisted");

            AddColumn("dbo.LoanInterest_Temp", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.LoanInterest_Temp", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.LoanInterest_Temp", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest_Temp", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LoanInterest_Temp", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.LoanInterest_Temp", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));

            CreateIndex("dbo.LoanInterest_Temp", "CurrencyId");
            AddForeignKey("dbo.LoanInterest_Temp", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanInterest_Temp", "LoanId", "dbo.LoanMain");
            DropForeignKey("dbo.LoanInterest_Temp", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LoanInterest_Temp", "LCAcceptanceId", "dbo.LCAcceptance");
            DropForeignKey("dbo.LoanInterest_Temp", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.LoanInterest_Temp", new[] { "CurrencyId" });
            DropIndex("dbo.LoanInterest_Temp", new[] { "LoanId" });
            DropIndex("dbo.LoanInterest_Temp", new[] { "LCAcceptanceId" });
            DropIndex("dbo.LoanInterest_Temp", new[] { "LCFileId" });
            DropTable("dbo.LoanInterest_Temp");
        }
    }
}
