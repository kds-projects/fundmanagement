namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddODMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ODMain",
                c => new
                    {
                        ODId = c.Int(nullable: false, identity: true),
                        ODNo = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: ""),
                        CompanyId = c.Short(nullable: false, defaultValue: 0),
                        IssuerBankId = c.Short(nullable: false, defaultValue: 0),
                        IssuerBranchId = c.Short(nullable: false, defaultValue: 0),
                        OpeningDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: ""),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.ODId)
                .ForeignKey("dbo.Bank", t => t.IssuerBankId, cascadeDelete: true)
                .ForeignKey("dbo.BankBranch", t => t.IssuerBranchId, cascadeDelete: true)
                .Index(t => t.IssuerBankId)
                .Index(t => t.IssuerBranchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ODMain", "IssuerBranchId", "dbo.BankBranch");
            DropForeignKey("dbo.ODMain", "IssuerBankId", "dbo.Bank");
            DropIndex("dbo.ODMain", new[] { "IssuerBranchId" });
            DropIndex("dbo.ODMain", new[] { "IssuerBankId" });
            DropTable("dbo.ODMain");
        }
    }
}
