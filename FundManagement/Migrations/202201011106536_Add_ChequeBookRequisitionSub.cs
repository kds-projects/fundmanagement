namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Add_ChequeBookRequisitionSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeBookRequisitionSub",
                c => new
                    {
                        RequisitionSubId = c.Int(nullable: false, identity: true),
                        RequisitionId = c.Int(nullable: false),
                        SerialRange = c.String(),
                        TotalCheque = c.Short(nullable: false),
                        StockAvailable = c.Short(nullable: false),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.RequisitionSubId)
                .ForeignKey("dbo.ChequeBookRequisition", t => t.RequisitionId, cascadeDelete: true)
                .Index(t => t.RequisitionId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeBookRequisitionSub", "RequisitionId", "dbo.ChequeBookRequisition");
            DropIndex("dbo.ChequeBookRequisitionSub", new[] { "RequisitionId" });
            DropTable("dbo.ChequeBookRequisitionSub");
        }
    }
}
