namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanInterest_InterestId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FundDetails", "InterestId", c => c.Int(nullable: false, defaultValue: 0));
            //AlterColumn("dbo.PaymentSchedule", "Status", c => c.String(nullable: false));
        }
        
        public override void Down()
        {
            //AlterColumn("dbo.PaymentSchedule", "Status", c => c.String(nullable: false));
            DropColumn("dbo.FundDetails", "InterestId");
        }
    }
}
