namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain : DbMigration
    {
        public override void Up()
        {
            RenameColumn("dbo.LoanMain", "LaonNo", "LoanNo");
        }
        
        public override void Down()
        {
            RenameColumn("dbo.LoanMain", "LoanNo", "LaonNo");
        }
    }
}
