namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Rec_Doc_Doc_Name : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ReceivableDocument", "DocumentName", c => c.String(nullable: false, maxLength: 150, unicode: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ReceivableDocument", "DocumentName");
        }
    }
}
