namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCheque_PayMode : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeInfo", "PaymentModeId", c => c.Short(nullable: false, defaultValue: 8));
            CreateIndex("dbo.ChequeInfo", "PaymentModeId");
            AddForeignKey("dbo.ChequeInfo", "PaymentModeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeInfo", "PaymentModeId", "dbo.CustomType");
            DropIndex("dbo.ChequeInfo", new[] { "PaymentModeId" });
            DropColumn("dbo.ChequeInfo", "PaymentModeId");
        }
    }
}
