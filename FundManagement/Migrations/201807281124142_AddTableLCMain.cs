namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableLCMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LCMain",
                c => new
                {
                    LCFileId = c.Int(nullable: false, identity: true),
                    LCFileNo = c.String(nullable: false, maxLength: 10, unicode: false),
                    LCNo = c.String(nullable: false, maxLength: 25, unicode: false),
                    LCDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                })
                .PrimaryKey(t => t.LCFileId);

            Sql("ALTER TABLE dbo.LCMain ADD [Month] AS Month([LCDate]) Persisted");
            Sql("ALTER TABLE dbo.LCMain ADD [Year] AS Year([LCDate]) Persisted");
            Sql("ALTER TABLE dbo.LCMain ADD [SerialNo] AS (CONVERT([int],substring([LCFileNo],(3),(4)),(0))) Persisted");

            AddColumn("dbo.LCMain", "BeneficiaryId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "SupplierId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "LocalAgentId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "OriginCountryId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "POLId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "IssuerBankId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "IssuerBranchId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "NegotiatingBankId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "BeneficiaryBankId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "LCType", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AddColumn("dbo.LCMain", "ShipmentDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "ExpireDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "SupplierTenure", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "STenureTypeId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "BankTenure", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "BTenureTypeId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "TenureStartDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "CoverNoteNo", c => c.String(nullable: false, maxLength: 75, unicode: false));
            AddColumn("dbo.LCMain", "CoverNoteDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "MarineNoteNo", c => c.String(nullable: false, maxLength: 75, unicode: false));
            AddColumn("dbo.LCMain", "MarineNoteDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "LCAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LCMain", "CurrencyId", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "BDTRate", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LCMain", "USDRate", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.LCMain ADD [AmountInBDT] AS [LCAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LCMain ADD [AmountInUSD] AS [LCAmount] * [USDRate] Persisted");
            //AddColumn("dbo.LCMain", "AmountInUSD", c => c.Decimal(precision: 24, scale: 2, storeType: "numeric"));
            //AddColumn("dbo.LCMain", "AmountInBDT", c => c.Decimal(precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LCMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.LCMain", "CreatedBy", c => c.Short(nullable: false));
            AddColumn("dbo.LCMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.LCMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LCMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.LCMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            CreateIndex("dbo.LCMain", "IssuerBankId");
            CreateIndex("dbo.LCMain", "IssuerBranchId");
            CreateIndex("dbo.LCMain", "NegotiatingBankId");
            CreateIndex("dbo.LCMain", "BeneficiaryBankId");
            CreateIndex("dbo.LCMain", "STenureTypeId");
            CreateIndex("dbo.LCMain", "BTenureTypeId");
            CreateIndex("dbo.LCMain", "CurrencyId");
            AddForeignKey("dbo.LCMain", "BTenureTypeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "IssuerBankId", "dbo.Bank", "BankId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "IssuerBranchId", "dbo.BankBranch", "BranchId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "NegotiatingBankId", "dbo.Bank", "BankId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "STenureTypeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "BeneficiaryBankId", "dbo.Bank", "BankId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LCMain", "BeneficiaryBankId", "dbo.Bank");
            DropForeignKey("dbo.LCMain", "NegotiatingBankId", "dbo.Bank");
            DropForeignKey("dbo.LCMain", "IssuerBankId", "dbo.Bank");
            DropForeignKey("dbo.LCMain", "IssuerBranchId", "dbo.BankBranch");
            DropForeignKey("dbo.LCMain", "STenureTypeId", "dbo.CustomType");
            DropForeignKey("dbo.LCMain", "CurrencyId", "dbo.CustomType");
            DropForeignKey("dbo.LCMain", "BTenureTypeId", "dbo.CustomType");
            DropIndex("dbo.LCMain", new[] { "CurrencyId" });
            DropIndex("dbo.LCMain", new[] { "BTenureTypeId" });
            DropIndex("dbo.LCMain", new[] { "STenureTypeId" });
            DropIndex("dbo.LCMain", new[] { "BeneficiaryBankId" });
            DropIndex("dbo.LCMain", new[] { "NegotiatingBankId" });
            DropIndex("dbo.LCMain", new[] { "IssuerBranchId" });
            DropIndex("dbo.LCMain", new[] { "IssuerBankId" });
            DropTable("dbo.LCMain");
        }
    }
}
