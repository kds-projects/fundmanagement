namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChequePayment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequePayment",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        ChequeId = c.Int(nullable: false),
                        IssueDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        ChequeDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        IssueTo = c.String(nullable: false, maxLength: 150, unicode: false),
                        ChequeAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Purpose = c.String(nullable: false, maxLength: 500, unicode: false),
                        Signatories = c.String(nullable: false, maxLength: 150, unicode: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.ChequeInfo", t => t.ChequeId, cascadeDelete: true)
                .Index(t => t.ChequeId)
                .Index(t => t.IssueDate)
                .Index(t => t.ChequeDate);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequePayment", "ChequeId", "dbo.ChequeInfo");
            DropIndex("dbo.ChequePayment", new[] { "ChequeDate" });
            DropIndex("dbo.ChequePayment", new[] { "IssueDate" });
            DropIndex("dbo.ChequePayment", new[] { "ChequeId" });
            DropTable("dbo.ChequePayment");
        }
    }
}
