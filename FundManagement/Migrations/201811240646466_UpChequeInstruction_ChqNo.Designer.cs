// <auto-generated />
namespace FundManagement.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.0-30225")]
    public sealed partial class UpChequeInstruction_ChqNo : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(UpChequeInstruction_ChqNo));
        
        string IMigrationMetadata.Id
        {
            get { return "201811240646466_UpChequeInstruction_ChqNo"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
