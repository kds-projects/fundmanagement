namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLoanInterest : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LoanInterest",
                c => new
                    {
                        InterestId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        LoanId = c.Int(nullable: false),
                        InterestDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                    })
                .PrimaryKey(t => t.InterestId)
                .ForeignKey("dbo.LCAcceptance", t => t.LCAcceptanceId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: false)
                .ForeignKey("dbo.LoanMain", t => t.LoanId, cascadeDelete: false)
                .Index(t => t.LCFileId)
                .Index(t => t.LCAcceptanceId)
                .Index(t => t.LoanId);

            Sql("ALTER TABLE dbo.LoanInterest ADD [Month] AS Month([InterestDate]) Persisted");
            Sql("ALTER TABLE dbo.LoanInterest ADD [Year] AS Year([InterestDate]) Persisted");

            AddColumn("dbo.LoanInterest", "Principal", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "InterestRate", c => c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "Interest", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "PrincipalOD", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "InterestRateOD", c => c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "InterestOD", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.LoanInterest", "CurrencyId", c => c.Short(nullable: false, defaultValue: 21));

            Sql("ALTER TABLE dbo.LoanInterest ADD [Status] AS case when [Interest] > 0 And [InterestOD] > 0 then 'BOTH' when [Interest] > 0 then 'GENERAL' when [InterestOD] > 0 then 'PENALTY' else 'NONE' end Persisted");

            AddColumn("dbo.LoanInterest", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.LoanInterest", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.LoanInterest", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LoanInterest", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.LoanInterest", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));

            CreateIndex("dbo.LoanInterest", "CurrencyId");
            AddForeignKey("dbo.LoanInterest", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanInterest", "LoanId", "dbo.LoanMain");
            DropForeignKey("dbo.LoanInterest", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LoanInterest", "LCAcceptanceId", "dbo.LCAcceptance");
            DropForeignKey("dbo.LoanInterest", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.LoanInterest", new[] { "CurrencyId" });
            DropIndex("dbo.LoanInterest", new[] { "LoanId" });
            DropIndex("dbo.LoanInterest", new[] { "LCAcceptanceId" });
            DropIndex("dbo.LoanInterest", new[] { "LCFileId" });
            DropTable("dbo.LoanInterest");
        }
    }
}
