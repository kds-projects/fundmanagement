namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPIMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PIMain",
                c => new
                {
                    PIFileId = c.Int(nullable: false, identity: true),
                    PIFileNo = c.String(nullable: false, maxLength: 15, unicode: false),
                    PINo = c.String(nullable: false, maxLength: 75, unicode: false),
                    PIDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                })
                .PrimaryKey(t => t.PIFileId);

            Sql("ALTER TABLE dbo.PIMain ADD [Month] AS Month([PIDate]) Persisted");
            Sql("ALTER TABLE dbo.PIMain ADD [Year] AS Year([PIDate]) Persisted");
            Sql("ALTER TABLE dbo.PIMain ADD [SerialNo] AS (CONVERT([int],substring([PIFileNo],(3),(4)),(0))) Persisted");

            AddColumn("dbo.PIMain", "BeneficiaryId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "SupplierId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "LocalAgentId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "OriginCountryId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "POLId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "ETD", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "ETA", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "RequisitionDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "BookingConfirmDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "PIReceiveDateFromSourcing", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "ShipmentDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "ExpireDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "LeadTime", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "Tenure", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "PaymentTerms", c => c.String(nullable: false, maxLength: 250, unicode: false));
            AddColumn("dbo.PIMain", "PIAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.PIMain", "CurrencyId", c => c.Short(nullable: false));
            AddColumn("dbo.PIMain", "BDTRate", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.PIMain", "USDRate", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));

            Sql("ALTER TABLE dbo.PIMain ADD [AmountInBDT] AS [PIAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.PIMain ADD [AmountInUSD] AS [PIAmount] * [USDRate] Persisted");

            AddColumn("dbo.PIMain", "ApprovalRef", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.PIMain", "IsForwardToBank", c => c.Byte(nullable: false, defaultValue: Convert.ToByte("0")));
            AddColumn("dbo.PIMain", "ForwardToBankDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "ForwardBankId", c => c.Short(nullable: false, defaultValue: Convert.ToByte("0")));
            AddColumn("dbo.PIMain", "ForwardBranchId", c => c.Short(nullable: false, defaultValue: Convert.ToByte("0")));
            AddColumn("dbo.PIMain", "ForwardUserId", c => c.Short(nullable: false, defaultValue: Convert.ToByte("0")));
            AddColumn("dbo.PIMain", "IsLCOpen", c => c.Byte(nullable: false, defaultValue: Convert.ToByte("0")));
            AddColumn("dbo.PIMain", "LCOpenDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.PIMain", "DelayReason", c => c.String(nullable: false, maxLength: 250, unicode: false));
            AddColumn("dbo.PIMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));

            Sql("ALTER TABLE dbo.PIMain ADD [Status] AS case when [IsLCOpen]=1 then 'LC OPEN' when [IsForwardToBank]=1 then 'FORWARDED TO BANK' else 'IN HAND' end Persisted");

            AddColumn("dbo.PIMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.PIMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.PIMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.PIMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.PIMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.PIMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));

        }
        
        public override void Down()
        {
            DropTable("dbo.PIMain");
        }
    }
}
