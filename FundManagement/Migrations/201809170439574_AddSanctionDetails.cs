namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSanctionDetails : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SanctionDetails",
                c => new
                    {
                        SanctionDetailsId = c.Int(nullable: false, identity: true),
                        SanctionId = c.Int(nullable: false),
                        SanctionTypeId = c.Short(nullable: false),
                        SanctionLimit = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        InterestRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        ODInterestRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        EarlySettlementRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        LCCommissionRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        AcceptanceCommissionRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        ChargeRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        OtherChargeRate = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        Margin = c.Decimal(nullable: false, precision: 10, scale: 2, storeType: "numeric"),
                        Status = c.Short(nullable: false, defaultValue: 1),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.SanctionDetailsId)
                .ForeignKey("dbo.SanctionMain", t => t.SanctionId, cascadeDelete: true)
                .ForeignKey("dbo.CustomType", t => t.SanctionTypeId, cascadeDelete: true)
                .Index(t => t.SanctionId)
                .Index(t => t.SanctionTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SanctionDetails", "SanctionTypeId", "dbo.CustomType");
            DropForeignKey("dbo.SanctionDetails", "SanctionId", "dbo.SanctionMain");
            DropIndex("dbo.SanctionDetails", new[] { "SanctionTypeId" });
            DropIndex("dbo.SanctionDetails", new[] { "SanctionId" });
            DropTable("dbo.SanctionDetails");
        }
    }
}
