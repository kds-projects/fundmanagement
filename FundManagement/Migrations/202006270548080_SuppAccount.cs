namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class SuppAccount : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SupplierAccount",
                c => new
                    {
                        SAId = c.Int(nullable: false, identity: true),
                        SupplierId = c.String(nullable: false, maxLength: 10, unicode: false),
                        AccountName = c.String(nullable: false, maxLength: 150, unicode: false),
                    })
                .PrimaryKey(t => t.SAId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.SupplierAccount");
        }
    }
}
