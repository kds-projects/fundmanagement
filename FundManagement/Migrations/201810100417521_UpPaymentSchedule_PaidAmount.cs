namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPaymentSchedule_PaidAmount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentSchedule", "PaidAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));

            Sql("ALTER TABLE dbo.PaymentSchedule ADD [Status] AS case when [PaidAmount]=0 then 'DUE' when [PaidAmount]<[DueAmount] then 'PARTIAL PAID' when [PaidAmount]=[DueAmount] then 'FULL PAID' else 'EXCESS PAID' end Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentSchedule", "Status");
            DropColumn("dbo.PaymentSchedule", "PaidAmount");
        }
    }
}
