namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpFundDetails_ConvRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FundDetails", "BDTRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.FundDetails", "USDRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.FundDetails ADD [AmountInBDT] AS [Amount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.FundDetails ADD [AmountInUSD] AS [Amount] * [USDRate] Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.FundDetails", "AmountInBDT");
            DropColumn("dbo.FundDetails", "AmountInUSD");
            DropColumn("dbo.FundDetails", "USDRate");
            DropColumn("dbo.FundDetails", "BDTRate");
        }
    }
}
