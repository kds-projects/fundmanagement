namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpFundDetailsRefDate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FundDetails", "ReferenceDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FundDetails", "ReferenceDate");
        }
    }
}
