namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentSub",
                c => new
                    {
                        PaymentSubId = c.Int(nullable: false, identity: true),
                        PaymentId = c.Int(nullable: false),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        LoanId = c.Int(nullable: false),
                        ScheduleId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CurrencyId = c.Short(nullable: false, defaultValue: 21),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false),
                    })
                .PrimaryKey(t => t.PaymentSubId)
                .ForeignKey("dbo.PaymentMain", t => t.PaymentId, cascadeDelete: true)
                .Index(t => t.PaymentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PaymentSub", "PaymentId", "dbo.PaymentMain");
            DropIndex("dbo.PaymentSub", new[] { "PaymentId" });
            DropTable("dbo.PaymentSub");
        }
    }
}
