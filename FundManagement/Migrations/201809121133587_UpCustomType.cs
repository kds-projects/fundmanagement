namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCustomType : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomType", "GroupName", c => c.String(nullable: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomType", "GroupName");
        }
    }
}
