namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpBankBranchSales : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.BankBranch", "SalesBranchId", c => c.Short(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.BankBranch", "SalesBranchId");
        }
    }
}
