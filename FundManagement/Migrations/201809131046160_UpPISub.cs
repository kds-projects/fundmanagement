namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPISub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PISub", "PIQuantity", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.PISub", "IsCapex", c => c.Byte(nullable: false));
            AddColumn("dbo.PISub", "RowNo", c => c.Short(nullable: false));
            DropColumn("dbo.PISub", "BookingQuantity");
        }
        
        public override void Down()
        {
            AddColumn("dbo.PISub", "BookingQuantity", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            DropColumn("dbo.PISub", "RowNo");
            DropColumn("dbo.PISub", "IsCapex");
            DropColumn("dbo.PISub", "PIQuantity");
        }
    }
}
