namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqPay_InstId : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.ChequePayment", "InstructionId", c => c.Int(nullable: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.ChequePayment", "InstructionId", c => c.Short(nullable: false));
        }
    }
}
