namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpInstruction_AppStatus : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "IssuedAccountId", "dbo.BankAccount");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "IssuedAccountId" });
            AddColumn("dbo.ChequeIssueInstruction", "ApprovedType", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequeIssueInstruction", "ApprovedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequeIssueInstruction", "ApprovedTime", c => c.DateTime(nullable: false, defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.ChequeIssueInstruction", "DisapprovedReason", c => c.String(nullable: false, maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "DisapprovedReason");
            DropColumn("dbo.ChequeIssueInstruction", "ApprovedTime");
            DropColumn("dbo.ChequeIssueInstruction", "ApprovedBy");
            DropColumn("dbo.ChequeIssueInstruction", "ApprovedType");
            CreateIndex("dbo.ChequeIssueInstruction", "IssuedAccountId");
            AddForeignKey("dbo.ChequeIssueInstruction", "IssuedAccountId", "dbo.BankAccount", "AccountId", cascadeDelete: true);
        }
    }
}
