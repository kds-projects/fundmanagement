namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReconciliationSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReconciliationSub",
                c => new
                    {
                        ReconciliationSubId = c.Short(nullable: false, identity: true),
                        ReconciliationId = c.Short(nullable: false),
                        LoanTypeId = c.Short(nullable: false),
                        SoftwareBalance = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        BookBalance = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        BankBalance = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SoftwareBookDifference = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SoftwareBookDifferenceRemarks = c.String(nullable: false, maxLength: 350, unicode: false),
                        SoftwareBankDifference = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SoftwareBankDifferenceRemarks = c.String(nullable: false, maxLength: 350, unicode: false),
                        BankBookDifference = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        BankBookDifferenceRemarks = c.String(nullable: false, maxLength: 350, unicode: false),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.ReconciliationSubId)
                .ForeignKey("dbo.CustomType", t => t.LoanTypeId, cascadeDelete: false)
                .ForeignKey("dbo.ReconciliationMain", t => t.ReconciliationId, cascadeDelete: true)
                .Index(t => t.ReconciliationId)
                .Index(t => t.LoanTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReconciliationSub", "ReconciliationId", "dbo.ReconciliationMain");
            DropForeignKey("dbo.ReconciliationSub", "LoanTypeId", "dbo.CustomType");
            DropIndex("dbo.ReconciliationSub", new[] { "LoanTypeId" });
            DropIndex("dbo.ReconciliationSub", new[] { "ReconciliationId" });
            DropTable("dbo.ReconciliationSub");
        }
    }
}
