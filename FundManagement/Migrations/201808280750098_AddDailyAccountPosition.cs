namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDailyAccountPosition : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyAccountPosition",
                c => new
                    {
                        DAPId = c.Short(nullable: false, identity: true),
                        DailyDate = c.DateTime(nullable: false),
                        CurrentAccount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        DLAccount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        ODAccount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        ReceivedAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.DAPId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DailyAccountPosition");
        }
    }
}
