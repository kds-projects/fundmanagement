namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLCAcceptance1 : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.LCAcceptance", "BLDate");
            DropColumn("dbo.LCAcceptance", "BLNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LCAcceptance", "BLNo", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AddColumn("dbo.LCAcceptance", "BLDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
        }
    }
}
