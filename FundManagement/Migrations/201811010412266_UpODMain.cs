namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpODMain : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ODMain", "LoanTypeId", c => c.Short(nullable: false, defaultValue: 38));
            CreateIndex("dbo.ODMain", "LoanTypeId");
            AddForeignKey("dbo.ODMain", "LoanTypeId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ODMain", "LoanTypeId", "dbo.CustomType");
            DropIndex("dbo.ODMain", new[] { "LoanTypeId" });
            DropColumn("dbo.ODMain", "LoanTypeId");
        }
    }
}
