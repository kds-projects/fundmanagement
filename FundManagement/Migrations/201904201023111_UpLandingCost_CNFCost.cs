namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLandingCost_CNFCost : DbMigration
    {
        public override void Up()
        {
            Sql("ALTER TABLE dbo.LandedCosting ADD [CNFCost] AS [OnlineCharge] + [DonationFees] + [VAT] + [CNFAIT] + [ScanningCharge] + [PortCharges] + " +
                   "[Wharfrent] + [LabourCharge] + [ExcessGoods] + [CustomJettyExpenses] + [ServiceCharge] + [TruckFare] + [ShippingAgentCharges] + " +
                   "[BerthOperatorCharge] + [RCCCharge] + [HystarCharge] + [MiscellaneousExpenses] Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.LandedCosting", "CNFCost");
        }
    }
}
