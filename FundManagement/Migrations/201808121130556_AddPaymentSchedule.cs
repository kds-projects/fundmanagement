namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPaymentSchedule : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PaymentSchedule",
                c => new
                    {
                        ScheduleId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        LoanId = c.Int(nullable: false),
                        InstallmentNo = c.Short(nullable: false, defaultValue: 0),
                        DueDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        OpeningBalance = c.Decimal(nullable: false, defaultValue: 0, precision: 24, scale: 2, storeType: "numeric"),
                        DueAmount = c.Decimal(nullable: false, defaultValue: 0, precision: 24, scale: 2, storeType: "numeric"),
                        PrincipalAmount = c.Decimal(nullable: false, defaultValue: 0, precision: 24, scale: 2, storeType: "numeric"),
                        InterestAmount = c.Decimal(nullable: false, defaultValue: 0, precision: 24, scale: 2, storeType: "numeric"),
                        InterestRate = c.Decimal(nullable: false, defaultValue: 0, precision: 5, scale: 2, storeType: "numeric"),
                        ClosingBalance = c.Decimal(nullable: false, defaultValue: 0, precision: 24, scale: 2, storeType: "numeric"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.ScheduleId)
                .ForeignKey("dbo.LoanMain", t => t.LoanId, cascadeDelete: true)
                .Index(t => t.LoanId)
                .Index(t => t.DueDate);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PaymentSchedule", "LoanId", "dbo.LoanMain");
            DropIndex("dbo.PaymentSchedule", new[] { "DueDate" });
            DropIndex("dbo.PaymentSchedule", new[] { "LoanId" });
            DropTable("dbo.PaymentSchedule");
        }
    }
}
