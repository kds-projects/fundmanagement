namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCustomType_Cat_SubCat : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.CustomType", "Category", c => c.String(maxLength: 150, unicode: false, defaultValue: ""));
            AddColumn("dbo.CustomType", "SubCategory", c => c.String(maxLength: 150, unicode: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            DropColumn("dbo.CustomType", "SubCategory");
            DropColumn("dbo.CustomType", "Category");
        }
    }
}
