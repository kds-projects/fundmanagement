namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddMonthlyExpense : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.MonthlyExpense",
                c => new
                    {
                        ExpenseId = c.Short(nullable: false, identity: true),
                        ExpenseYear = c.Short(nullable: false),
                        ExpenseMonth = c.Short(nullable: false),
                        CashDisbursementCTG = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CashDisbursementDHK = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CreditorsPayment = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Salaries = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Royalties = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        PrfessionalFees = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Utilities = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        UtilitiyRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        ProjectPayment = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        ProjectRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        AssetsAcquisiation = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        InsurancePremium = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Tax = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        TaxRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        Miscellaneous = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        MiscellaneousRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        DirectorsPayment = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        DirectorRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        Devidend = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Complaince = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Others = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        OtherRemarks = c.String(nullable: false, maxLength: 250, unicode: false, defaultValue: "-"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.ExpenseId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.MonthlyExpense");
        }
    }
}
