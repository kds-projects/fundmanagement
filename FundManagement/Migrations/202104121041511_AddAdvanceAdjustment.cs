namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdvanceAdjustment : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdvanceAdjustment",
                c => new
                    {
                        AdjustmentId = c.Int(nullable: false, identity: true),
                        AdjustmentNo = c.String(nullable: false, maxLength: 15, unicode: false),
                        AdjustmentDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        InstructionId = c.Int(nullable: false),
                        AdjustedInstructionId = c.Int(nullable: false),
                        InstructionSubId = c.Int(nullable: false, defaultValue: 0),
                        AdjustedInstructionSubId = c.Int(nullable: false, defaultValue: 0),
                        AdjustAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Purpose = c.String(nullable: false, maxLength: 500, unicode: false, defaultValue: "-"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.AdjustmentId)
                .ForeignKey("dbo.ChequeIssueInstruction", t => t.AdjustedInstructionId, cascadeDelete: true)
                .ForeignKey("dbo.ChequeIssueInstruction", t => t.InstructionId, cascadeDelete: false)
                .Index(t => t.AdjustmentNo, unique: true)
                .Index(t => t.AdjustmentDate)
                .Index(t => t.InstructionId)
                .Index(t => t.AdjustedInstructionId);

            Sql("ALTER TABLE dbo.AdvanceAdjustment ADD [AdjustmentSDate] AS (CONVERT([smalldatetime],CONVERT([date],[AdjustmentDate],(0)),(0))) Persisted");
            Sql("ALTER TABLE dbo.AdvanceAdjustment ADD [Month] AS Month([AdjustmentDate]) Persisted");
            Sql("ALTER TABLE dbo.AdvanceAdjustment ADD [Year] AS Year([AdjustmentDate]) Persisted");
            Sql("ALTER TABLE dbo.AdvanceAdjustment ADD [SerialNo] AS (CONVERT([int],substring([AdjustmentNo],(4),(4)),(0))) Persisted");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdvanceAdjustment", "InstructionId", "dbo.ChequeIssueInstruction");
            DropForeignKey("dbo.AdvanceAdjustment", "AdjustedInstructionId", "dbo.ChequeIssueInstruction");
            DropIndex("dbo.AdvanceAdjustment", new[] { "AdjustedInstructionId" });
            DropIndex("dbo.AdvanceAdjustment", new[] { "InstructionId" });
            DropIndex("dbo.AdvanceAdjustment", new[] { "AdjustmentDate" });
            DropIndex("dbo.AdvanceAdjustment", new[] { "AdjustmentNo" });
            DropTable("dbo.AdvanceAdjustment");
        }
    }
}
