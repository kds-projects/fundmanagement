namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqIssueInst_InstTypeId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "InstructionTypeId", c => c.Short(nullable: false, defaultValue: 209));
            CreateIndex("dbo.ChequeIssueInstruction", "InstructionTypeId");
            AddForeignKey("dbo.ChequeIssueInstruction", "InstructionTypeId", "dbo.CustomType", "TypeId", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "InstructionTypeId", "dbo.CustomType");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "InstructionTypeId" });
            DropColumn("dbo.ChequeIssueInstruction", "InstructionTypeId");
        }
    }
}
