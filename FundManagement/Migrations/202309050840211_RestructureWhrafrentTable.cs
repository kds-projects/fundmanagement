namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class RestructureWhrafrentTable : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Wharfrent", "PortLandingDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Wharfrent", "DetentionFromDate", c => c.DateTime(nullable: false));
            AddColumn("dbo.Wharfrent", "DetentionToDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Wharfrent", "CLDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Wharfrent", "CLDate", c => c.DateTime(nullable: false));
            DropColumn("dbo.Wharfrent", "DetentionToDate");
            DropColumn("dbo.Wharfrent", "DetentionFromDate");
            DropColumn("dbo.Wharfrent", "PortLandingDate");
        }
    }
}
