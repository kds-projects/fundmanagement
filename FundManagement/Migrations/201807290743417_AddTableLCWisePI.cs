namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableLCWisePI : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LCWisePI",
                c => new
                    {
                        LCPIId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        LCSubId = c.Int(nullable: false),
                        PIFileId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.LCPIId)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: false)
                .ForeignKey("dbo.LCSub", t => t.LCSubId, cascadeDelete: true)
                .Index(t => t.LCFileId)
                .Index(t => t.LCSubId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LCWisePI", "LCSubId", "dbo.LCSub");
            DropForeignKey("dbo.LCWisePI", "LCFileId", "dbo.LCMain");
            DropIndex("dbo.LCWisePI", new[] { "LCSubId" });
            DropIndex("dbo.LCWisePI", new[] { "LCFileId" });
            DropTable("dbo.LCWisePI");
        }
    }
}
