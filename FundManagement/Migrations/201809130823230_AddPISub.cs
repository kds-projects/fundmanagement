namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPISub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.PISub",
                c => new
                {
                    PIFileSubId = c.Int(nullable: false, identity: true),
                    PIFileId = c.Int(nullable: false),
                    PIFileRelationId = c.Int(nullable: false),
                    RelationId = c.Int(nullable: false),
                    PApprovalSubId = c.Int(nullable: false),
                    QApprovalSubId = c.Int(nullable: false),
                    PRSubId = c.Int(nullable: false),
                    POSubId = c.Int(nullable: false),
                    FactoryId = c.Short(nullable: false),
                    ProductId = c.Short(nullable: false),
                    CurrentStock = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    StockInTransit = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    TotalStockArrangement = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    PDRequirement = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    TotalStockForDay = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    QuantityUnitId = c.Short(nullable: false),
                    BookingQuantity = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    UnitPrice = c.Decimal(nullable: false, precision: 24, scale: 6, storeType: "numeric"),
                    Amount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    CurrencyId = c.Short(nullable: false),
                    BDTRate = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                    USDRate = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                })
                .PrimaryKey(t => t.PIFileSubId)
                .ForeignKey("dbo.PIMain", t => t.PIFileId, cascadeDelete: true)
                .Index(t => t.PIFileId);

            Sql("ALTER TABLE dbo.PISub ADD [AmountInBDT] AS [Amount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.PISub ADD [AmountInUSD] AS [Amount] * [USDRate] Persisted");
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PISub", "PIFileId", "dbo.PIMain");
            DropIndex("dbo.PISub", new[] { "PIFileId" });
            DropTable("dbo.PISub");
        }
    }
}
