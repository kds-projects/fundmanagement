namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpdateLoan : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "Remarks");
        }
    }
}
