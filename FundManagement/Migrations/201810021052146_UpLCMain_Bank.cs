namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCMain_Bank : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.LCMain", "NegotiatingBankId", "dbo.Bank");
            DropForeignKey("dbo.LCMain", "BeneficiaryBankId", "dbo.Bank");
            DropIndex("dbo.LCMain", new[] { "NegotiatingBankId" });
            DropIndex("dbo.LCMain", new[] { "BeneficiaryBankId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.LCMain", "BeneficiaryBankId");
            CreateIndex("dbo.LCMain", "NegotiatingBankId");
            AddForeignKey("dbo.LCMain", "BeneficiaryBankId", "dbo.Bank", "BankId", cascadeDelete: false);
            AddForeignKey("dbo.LCMain", "NegotiatingBankId", "dbo.Bank", "BankId", cascadeDelete: false);
        }
    }
}
