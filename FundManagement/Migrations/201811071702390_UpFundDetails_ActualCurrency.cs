namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpFundDetails_ActualCurrency : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.FundDetails", "AmountInActualCurrency", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.FundDetails", "ActualCurrencyRate", c => c.Decimal(nullable: false, precision: 10, scale: 7, storeType: "numeric", defaultValue: 1));
            AddColumn("dbo.FundDetails", "ActualCurrencyId", c => c.Short(nullable: false, defaultValue: 23));
        }
        
        public override void Down()
        {
            DropColumn("dbo.FundDetails", "ActualCurrencyId");
            DropColumn("dbo.FundDetails", "ActualCurrencyRate");
            DropColumn("dbo.FundDetails", "AmountInActualCurrency");
        }
    }
}
