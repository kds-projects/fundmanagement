namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLCMain_LCNo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LCMain", "LCNo", c => c.String(nullable: false, maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LCMain", "LCNo", c => c.String(nullable: false, maxLength: 25, unicode: false));
        }
    }
}
