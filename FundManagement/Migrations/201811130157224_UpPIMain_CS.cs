namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPIMain_CS : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PIMain", "IsCSDone", c => c.Byte(nullable: false, defaultValue: 0));
            AddColumn("dbo.PIMain", "CSDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PIMain", "CSDate");
            DropColumn("dbo.PIMain", "IsCSDone");
        }
    }
}
