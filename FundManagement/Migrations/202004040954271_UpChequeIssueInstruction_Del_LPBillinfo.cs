namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChequeIssueInstruction_Del_LPBillinfo : DbMigration
    {
        public override void Up()
        {
            DropColumn("dbo.ChequeIssueInstruction", "LPBillForwardId");
            DropColumn("dbo.ChequeIssueInstruction", "LPBillForwardDetailsId");
        }
        
        public override void Down()
        {
            AddColumn("dbo.ChequeIssueInstruction", "LPBillForwardDetailsId", c => c.Int(nullable: false));
            AddColumn("dbo.ChequeIssueInstruction", "LPBillForwardId", c => c.Int(nullable: false));
        }
    }
}
