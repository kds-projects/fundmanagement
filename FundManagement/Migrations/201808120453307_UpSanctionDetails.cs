namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpSanctionDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.SanctionDetails", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.SanctionDetails", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getDate()"));
            AddColumn("dbo.SanctionDetails", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.SanctionDetails", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.SanctionDetails", "IPAddress", c => c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"));
            AddColumn("dbo.SanctionDetails", "PCName", c => c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"));
            DropColumn("dbo.SanctionDetails", "AccountNo");
        }
        
        public override void Down()
        {
            AddColumn("dbo.SanctionDetails", "AccountNo", c => c.String(nullable: false, maxLength: 50, unicode: false));
            DropColumn("dbo.SanctionDetails", "PCName");
            DropColumn("dbo.SanctionDetails", "IPAddress");
            DropColumn("dbo.SanctionDetails", "RevisedTime");
            DropColumn("dbo.SanctionDetails", "RevisedBy");
            DropColumn("dbo.SanctionDetails", "CreatedTime");
            DropColumn("dbo.SanctionDetails", "CreatedBy");
        }
    }
}
