namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqInstruction_BankAccount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "BankAccountId", c => c.Short(nullable: false));
            CreateIndex("dbo.ChequeIssueInstruction", "BankAccountId");
            AddForeignKey("dbo.ChequeIssueInstruction", "BankAccountId", "dbo.BankAccount", "AccountId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeIssueInstruction", "BankAccountId", "dbo.BankAccount");
            DropIndex("dbo.ChequeIssueInstruction", new[] { "BankAccountId" });
            DropColumn("dbo.ChequeIssueInstruction", "BankAccountId");
        }
    }
}
