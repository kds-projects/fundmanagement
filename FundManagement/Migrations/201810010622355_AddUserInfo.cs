namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddUserInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.UserInfo",
                c => new
                    {
                        UserId = c.Short(nullable: false, identity: true),
                        EmployeeId = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 25, unicode: false),
                        UserPWD = c.String(nullable: false, maxLength: 25, unicode: false),
                        UserTitle = c.String(nullable: false, maxLength: 75, unicode: false),
                        DepartmentId = c.Short(nullable: false, defaultValue: 0),
                        DepartmentName = c.String(nullable: false, maxLength: 125, unicode: false, defaultValue: "-"),
                        ShortName = c.String(nullable: false, maxLength: 25, unicode: false),
                        eMail = c.String(nullable: false, maxLength: 75, unicode: false, defaultValue: "-"),
                        LocationId = c.Short(nullable: false, defaultValue: 0),
                        FactoryId = c.String(nullable: false, defaultValue: ""),
                        LoginKey = c.String(nullable: false, maxLength: 25, unicode: false, defaultValue: ""),
                        KeyStartDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        KeyEndDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        UserTypeId = c.Short(nullable: false, defaultValue: 0),
                        UserLevelId = c.Short(nullable: false, defaultValue: 0),
                        IsLogged = c.Short(nullable: false, defaultValue: 0),
                        IsActive = c.Short(nullable: false, defaultValue: 0),
                        IsAdmin = c.Short(nullable: false, defaultValue: 0),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.UserId)
                .Index(t => t.UserName, unique: true)
                .Index(t => t.ShortName);
            
        }
        
        public override void Down()
        {
            DropIndex("dbo.UserInfo", new[] { "ShortName" });
            DropIndex("dbo.UserInfo", new[] { "UserName" });
            DropTable("dbo.UserInfo");
        }
    }
}
