namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMain_LoanNo : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.LoanMain", "LoanNo", c => c.String(nullable: false, maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.LoanMain", "LoanNo", c => c.String(nullable: false, maxLength: 25, unicode: false));
        }
    }
}
