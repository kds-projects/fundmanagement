namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPurpose : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PayorderDetails", "Purpose", c => c.String());
        }
        
        public override void Down()
        {
            DropColumn("dbo.PayorderDetails", "Purpose");
        }
    }
}
