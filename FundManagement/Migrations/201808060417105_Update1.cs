namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LCMain", "InsuranceValue", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            AddColumn("dbo.LCMain", "InsurancePremium", c => c.Decimal(nullable: false, precision: 18, scale: 2));
            DropColumn("dbo.LCMain", "MarineNoteNo");
            DropColumn("dbo.LCMain", "MarineNoteDate");
        }
        
        public override void Down()
        {
            AddColumn("dbo.LCMain", "MarineNoteDate", c => c.DateTime(nullable: false, storeType: "smalldatetime"));
            AddColumn("dbo.LCMain", "MarineNoteNo", c => c.String(nullable: false, maxLength: 75, unicode: false));
            DropColumn("dbo.LCMain", "InsurancePremium");
            DropColumn("dbo.LCMain", "InsuranceValue");
        }
    }
}
