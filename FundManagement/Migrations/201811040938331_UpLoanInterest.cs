namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanInterest : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanInterest", "IsConvertedToPrincipal", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanInterest", "ConvertDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanInterest", "ConvertDate");
            DropColumn("dbo.LoanInterest", "IsConvertedToPrincipal");
        }
    }
}
