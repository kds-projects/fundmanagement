namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddChequeInfo : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ChequeInfo",
                c => new
                    {
                        ChequeId = c.Int(nullable: false, identity: true),
                        AccountId = c.Short(nullable: false),
                        BranchId = c.Short(nullable: false),
                        BankId = c.Short(nullable: false),
                        ChequeNo = c.String(nullable: false, maxLength: 25, unicode: false),
                        Prefix = c.String(nullable: false, maxLength: 10, unicode: false),
                        CurrencyId = c.Short(nullable: false, defaultValue: 23),
                        SerialRange = c.String(nullable: false, maxLength: 50, unicode: false),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.ChequeId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: false)
                .ForeignKey("dbo.BankAccount", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: false)
                .ForeignKey("dbo.CustomType", t => t.CurrencyId, cascadeDelete: false)
                .Index(t => t.AccountId)
                .Index(t => t.BranchId)
                .Index(t => t.BankId)
                .Index(t => t.CurrencyId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ChequeInfo", "CurrencyId", "dbo.CustomType");
            DropForeignKey("dbo.ChequeInfo", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.ChequeInfo", "AccountId", "dbo.BankAccount");
            DropForeignKey("dbo.ChequeInfo", "BankId", "dbo.Bank");
            DropIndex("dbo.ChequeInfo", new[] { "CurrencyId" });
            DropIndex("dbo.ChequeInfo", new[] { "BankId" });
            DropIndex("dbo.ChequeInfo", new[] { "BranchId" });
            DropIndex("dbo.ChequeInfo", new[] { "AccountId" });
            DropTable("dbo.ChequeInfo");
        }
    }
}
