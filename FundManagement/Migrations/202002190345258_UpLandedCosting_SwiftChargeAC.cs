namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLandedCosting_SwiftChargeAC : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LandedCosting", "SwiftChargesAcceptanceCommission", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric", defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LandedCosting", "SwiftChargesAcceptanceCommission");
        }
    }
}
