namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPIMain_FK : DbMigration
    {
        public override void Up()
        {
            CreateIndex("dbo.PIMain", "CurrencyId");
            AddForeignKey("dbo.PIMain", "CurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.PIMain", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.PIMain", new[] { "CurrencyId" });
        }
    }
}
