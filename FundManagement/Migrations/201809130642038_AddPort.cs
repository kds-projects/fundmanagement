namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddPort : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Port",
                c => new
                    {
                        PortId = c.Short(nullable: false, identity: true),
                        CountryId = c.Short(nullable: false),
                        ShortName = c.String(nullable: false, maxLength: 50, unicode: false),
                        PortName = c.String(nullable: false, maxLength: 150, unicode: false),
                        Activity = c.Int(nullable: false),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.PortId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.Port");
        }
    }
}
