namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDailyFundMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyFundMain",
                c => new
                    {
                        DFId = c.Short(nullable: false, identity: true),
                        DFDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.DFId);


            Sql("ALTER TABLE dbo.DailyFundMain ADD [DFYear] AS Month([DFDate]) Persisted");
            Sql("ALTER TABLE dbo.DailyFundMain ADD [DFMonth] AS Year([DFDate]) Persisted");

            AddColumn("dbo.DailyFundMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.DailyFundMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.DailyFundMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.DailyFundMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.DailyFundMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.DailyFundMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.DailyFundMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            
        }
        
        public override void Down()
        {
            DropTable("dbo.DailyFundMain");
        }
    }
}
