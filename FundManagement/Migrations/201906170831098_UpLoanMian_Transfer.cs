namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoanMian_Transfer : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "IsSettled", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanMain", "IsEarlySettled", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanMain", "SettlementEntryDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValue: DateTime.Parse("01-Jan-1900")));
            AddColumn("dbo.LoanMain", "IsTransferred", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanMain", "TransferDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValue: DateTime.Parse("01-Jan-1900")));
            AddColumn("dbo.LoanMain", "TransferBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LoanMain", "TransferEntryDate", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValue: DateTime.Parse("01-Jan-1900")));
            AddColumn("dbo.LoanMain", "BusinessTypeId", c => c.Short(nullable: false, defaultValue: 92));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LoanMain", "BusinessTypeId");
            DropColumn("dbo.LoanMain", "TransferEntryDate");
            DropColumn("dbo.LoanMain", "TransferBy");
            DropColumn("dbo.LoanMain", "TransferDate");
            DropColumn("dbo.LoanMain", "IsTransferred");
            DropColumn("dbo.LoanMain", "SettlementEntryDate");
            DropColumn("dbo.LoanMain", "IsEarlySettled");
            DropColumn("dbo.LoanMain", "IsSettled");
        }
    }
}
