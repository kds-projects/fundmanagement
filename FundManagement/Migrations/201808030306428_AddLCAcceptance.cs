namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddLCAcceptance : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LCAcceptance",
                c => new
                    {
                        LCAcceptanceId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        PLEId = c.Int(nullable: false),
                        DocumentRetirementDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        HandoverToCNFDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        HandoverToSourcingDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        BLDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        BLNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        AcceptanceDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        ExpireDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        AcceptanceNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        AcceptanceAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CurrencyId = c.Short(nullable: false),
                        BDTRate = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        USDRate = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        LiborAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        LiborInterestRate = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        LiborInterestRateOD = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        LiborInterestStartDate = c.Decimal(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        HandlingCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        HCInterestRate = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        HCInterestRateOD = c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric"),
                        HCInterestStartDate = c.Decimal(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)")
                    })
                .PrimaryKey(t => t.LCAcceptanceId)
                .ForeignKey("dbo.CustomType", t => t.CurrencyId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: true)
                .Index(t => t.LCFileId)
                .Index(t => t.CurrencyId);

            Sql("ALTER TABLE dbo.LCAcceptance ADD [AmountInBDT] AS [AcceptanceAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LCAcceptance ADD [AmountInUSD] AS [AcceptanceAmount] * [USDRate] Persisted");
            AddColumn("dbo.LCAcceptance", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));

            AddColumn("dbo.LCAcceptance", "CreatedBy", c => c.Short(nullable: false));
            AddColumn("dbo.LCAcceptance", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.LCAcceptance", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.LCAcceptance", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.LCAcceptance", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.LCAcceptance", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LCAcceptance", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LCAcceptance", "CurrencyId", "dbo.CustomType");
            DropIndex("dbo.LCAcceptance", new[] { "CurrencyId" });
            DropIndex("dbo.LCAcceptance", new[] { "LCFileId" });
            DropTable("dbo.LCAcceptance");
        }
    }
}
