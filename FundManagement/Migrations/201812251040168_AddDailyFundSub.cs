namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDailyFundSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.DailyFundSub",
                c => new
                    {
                        DFSubId = c.Short(nullable: false, identity: true),
                        DFId = c.Short(nullable: false),
                        AccountId = c.Short(nullable: false),
                        DebitAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CreditAmount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Remarks = c.String(nullable: false, maxLength: 350, unicode: false),
                        RowNo = c.Short(nullable: false),
                    })
                .PrimaryKey(t => t.DFSubId)
                .ForeignKey("dbo.BankAccount", t => t.AccountId, cascadeDelete: false)
                .ForeignKey("dbo.DailyFundMain", t => t.DFId, cascadeDelete: true)
                .Index(t => t.DFId)
                .Index(t => t.AccountId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.DailyFundSub", "DFId", "dbo.DailyFundMain");
            DropForeignKey("dbo.DailyFundSub", "AccountId", "dbo.BankAccount");
            DropIndex("dbo.DailyFundSub", new[] { "AccountId" });
            DropIndex("dbo.DailyFundSub", new[] { "DFId" });
            DropTable("dbo.DailyFundSub");
        }
    }
}
