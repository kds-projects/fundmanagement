namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpLoandMain_Amount : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LoanMain", "AcceptedAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LoanMain", "LiborAmount", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LoanMain", "HandlingCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LoanMain", "FCurrencyId", c => c.Short(nullable: false, defaultValue: 21));
            AddColumn("dbo.LoanMain", "LoanAmountBDT", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            CreateIndex("dbo.LoanMain", "FCurrencyId");
            AddForeignKey("dbo.LoanMain", "FCurrencyId", "dbo.CustomType", "TypeId", cascadeDelete: false);
            DropColumn("dbo.LoanMain", "AmountInUSD");
            DropColumn("dbo.LoanMain", "AmountInBDT");
            DropColumn("dbo.LoanMain", "LoanAmount");
            
            Sql("ALTER TABLE dbo.LoanMain ADD [LoanAmount] AS [AcceptedAmount] + [LiborAmount] + [HandlingCharge] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [AcceptedAmountBDT] AS [AcceptedAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [LiborAmountBDT] AS [LiborAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [HandlingChargeBDT] AS [HandlingCharge] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [LoanAmountUSD] AS Round(Case When [AcceptedAmount] + [LiborAmount] + [HandlingCharge] = 0 Then [LoanAmountBDT] Else ([AcceptedAmount] + [LiborAmount] + [HandlingCharge]) End * [USDRate], 2) Persisted");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LoanMain", "FCurrencyId", "dbo.CustomType");
            DropIndex("dbo.LoanMain", new[] { "FCurrencyId" });
            AlterColumn("dbo.LoanMain", "LoanAmount", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric", defaultValue: 0));
            DropColumn("dbo.LoanMain", "LoanAmountUSD");
            DropColumn("dbo.LoanMain", "LoanAmountBDT");
            DropColumn("dbo.LoanMain", "HandlingChargeBDT");
            DropColumn("dbo.LoanMain", "LiborAmountBDT");
            DropColumn("dbo.LoanMain", "AcceptedAmountBDT");
            DropColumn("dbo.LoanMain", "FCurrencyId");
            DropColumn("dbo.LoanMain", "HandlingCharge");
            DropColumn("dbo.LoanMain", "LiborAmount");
            DropColumn("dbo.LoanMain", "AcceptedAmount");
            DropColumn("dbo.LoanMain", "AmountInBDT");
            DropColumn("dbo.LoanMain", "AmountInUSD");

            Sql("ALTER TABLE dbo.LoanMain ADD [AmountInBDT] AS [LoanAmount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.LoanMain ADD [AmountInUSD] AS [LoanAmount] * [USDRate] Persisted");
        }
    }
}
