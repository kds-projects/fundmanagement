namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Chq_Issue_Inst_Ref_Pay_Type : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "PaymentTypeId", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequeIssueInstruction", "ReferenceNo", c => c.String(nullable: false, defaultValue: "-"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "ReferenceNo");
            DropColumn("dbo.ChequeIssueInstruction", "PaymentTypeId");
        }
    }
}
