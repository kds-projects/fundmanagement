namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddReconciliationMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ReconciliationMain",
                c => new
                    {
                        ReconciliationId = c.Short(nullable: false, identity: true),
                        ReconciliationDate = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.ReconciliationId);


            Sql("ALTER TABLE dbo.ReconciliationMain ADD [ReconciliationYear] AS Month([ReconciliationDate]) Persisted");
            Sql("ALTER TABLE dbo.ReconciliationMain ADD [ReconciliationMonth] AS Year([ReconciliationDate]) Persisted");

            AddColumn("dbo.ReconciliationMain", "CompanyId", c => c.Short(nullable: false));
            AddColumn("dbo.ReconciliationMain", "BankId", c => c.Short(nullable: false));
            AddColumn("dbo.ReconciliationMain", "BranchId", c => c.Short(nullable: false));
            
            AddColumn("dbo.ReconciliationMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.ReconciliationMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ReconciliationMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.ReconciliationMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.ReconciliationMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.ReconciliationMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.ReconciliationMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));

            AddForeignKey("dbo.ReconciliationMain", "BankId", "dbo.Bank", "BankId", cascadeDelete: false);
            AddForeignKey("dbo.ReconciliationMain", "BranchId", "dbo.BankBranch", "BranchId", cascadeDelete: false);

            CreateIndex("dbo.ReconciliationMain", "CompanyId");
            CreateIndex("dbo.ReconciliationMain", "BankId");
            CreateIndex("dbo.ReconciliationMain", "BranchId");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ReconciliationMain", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.ReconciliationMain", "BankId", "dbo.Bank");
            DropIndex("dbo.ReconciliationMain", new[] { "BranchId" });
            DropIndex("dbo.ReconciliationMain", new[] { "BankId" });
            DropTable("dbo.ReconciliationMain");
        }
    }
}
