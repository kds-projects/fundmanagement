namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpInstruction_VoucherNo : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "VoucherNo", c => c.String(nullable: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "VoucherNo");
        }
    }
}
