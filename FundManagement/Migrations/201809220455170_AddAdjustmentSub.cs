namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddAdjustmentSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AdjustmentSub",
                c => new
                    {
                        AdjustmentSubId = c.Int(nullable: false, identity: true),
                        AdjustmentId = c.Int(nullable: false),
                        LCFileId = c.Int(nullable: false),
                        LCAcceptanceId = c.Int(nullable: false),
                        LoanId = c.Int(nullable: false),
                        ScheduleId = c.Int(nullable: false),
                        Amount = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Principal = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Interest = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Libor = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        HandlingCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        ExciseDuty = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        BankCharge = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        SettlementFee = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CurrencyId = c.Short(nullable: false),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false),
                    })
                .PrimaryKey(t => t.AdjustmentSubId)
                .ForeignKey("dbo.AdjustmentMain", t => t.AdjustmentId, cascadeDelete: true)
                .Index(t => t.AdjustmentId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AdjustmentSub", "AdjustmentId", "dbo.AdjustmentMain");
            DropIndex("dbo.AdjustmentSub", new[] { "AdjustmentId" });
            DropTable("dbo.AdjustmentSub");
        }
    }
}
