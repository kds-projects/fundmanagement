namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpPaymentSub : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.PaymentSub", "Principal", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "Interest", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "Libor", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "HandlingCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "ExciseDuty", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "BankCharge", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.PaymentSub", "SettlementFee", c => c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric", defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.PaymentSub", "SettlementFee");
            DropColumn("dbo.PaymentSub", "BankCharge");
            DropColumn("dbo.PaymentSub", "ExciseDuty");
            DropColumn("dbo.PaymentSub", "HandlingCharge");
            DropColumn("dbo.PaymentSub", "Libor");
            DropColumn("dbo.PaymentSub", "Interest");
            DropColumn("dbo.PaymentSub", "Principal");
        }
    }
}
