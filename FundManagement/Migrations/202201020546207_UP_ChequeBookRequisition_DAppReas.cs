namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UP_ChequeBookRequisition_DAppReas : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeBookRequisition", "DisapprovedReason", c => c.String(nullable: false, maxLength: 500, unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeBookRequisition", "DisapprovedReason");
        }
    }
}
