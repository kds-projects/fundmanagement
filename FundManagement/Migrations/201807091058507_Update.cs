namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.BankAccount", "IPAddress", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.BankAccount", "PCName", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Bank", "IPAddress", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.Bank", "PCName", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.BankBranch", "IPAddress", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.BankBranch", "PCName", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.CustomType", "IPAddress", c => c.String(maxLength: 50, unicode: false));
            AlterColumn("dbo.CustomType", "PCName", c => c.String(maxLength: 50, unicode: false));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomType", "PCName", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.CustomType", "IPAddress", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.BankBranch", "PCName", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.BankBranch", "IPAddress", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.Bank", "PCName", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.Bank", "IPAddress", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.BankAccount", "PCName", c => c.String(nullable: false, maxLength: 50, unicode: false));
            AlterColumn("dbo.BankAccount", "IPAddress", c => c.String(nullable: false, maxLength: 50, unicode: false));
        }
    }
}
