namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddTableLCSub : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.LCSub",
                c => new
                    {
                        LCSubId = c.Int(nullable: false, identity: true),
                        LCFileId = c.Int(nullable: false),
                        AmendmentNo = c.String(nullable: false, maxLength: 20, unicode: false),
                        AmendmentDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        AmendmentTypeId = c.Short(nullable: false),
                        Remarks = c.String(nullable: false, maxLength: 500, unicode: false),
                        CreatedBy = c.Short(nullable: false),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(maxLength: 50, defaultValue: "-", unicode: false),
                        PCName = c.String(maxLength: 50, defaultValue: "-", unicode: false),
                    })
                .PrimaryKey(t => t.LCSubId)
                .ForeignKey("dbo.CustomType", t => t.AmendmentTypeId, cascadeDelete: false)
                .ForeignKey("dbo.LCMain", t => t.LCFileId, cascadeDelete: true)
                .Index(t => t.LCFileId)
                .Index(t => t.AmendmentTypeId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.LCSub", "LCFileId", "dbo.LCMain");
            DropForeignKey("dbo.LCSub", "AmendmentTypeId", "dbo.CustomType");
            DropIndex("dbo.LCSub", new[] { "AmendmentTypeId" });
            DropIndex("dbo.LCSub", new[] { "LCFileId" });
            DropTable("dbo.LCSub");
        }
    }
}
