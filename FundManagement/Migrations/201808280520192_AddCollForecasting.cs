namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCollForecasting : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CollectionForecasting",
                c => new
                    {
                        ForecastId = c.Short(nullable: false, identity: true),
                        ForecastYear = c.Short(nullable: false),
                        ForecastMonth = c.Short(nullable: false),
                        CollectionProjectedBDT = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CollectionProjectedUSD = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        WastageSales = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        Others = c.Decimal(nullable: false, precision: 24, scale: 2, storeType: "numeric"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.ForecastId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.CollectionForecasting");
        }
    }
}
