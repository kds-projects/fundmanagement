namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpCustomType_GroupName : DbMigration
    {
        public override void Up()
        {
            AlterColumn("dbo.CustomType", "GroupName", c => c.String(maxLength: 50, unicode: false, defaultValue: ""));
        }
        
        public override void Down()
        {
            AlterColumn("dbo.CustomType", "GroupName", c => c.String(nullable: false));
        }
    }
}
