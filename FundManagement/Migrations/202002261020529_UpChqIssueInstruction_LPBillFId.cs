namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqIssueInstruction_LPBillFId : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueInstruction", "LPBillForwardId", c => c.Int(nullable: false, defaultValue: 0));
            AddColumn("dbo.ChequeIssueInstruction", "LPBillForwardDetailsId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueInstruction", "LPBillForwardDetailsId");
            DropColumn("dbo.ChequeIssueInstruction", "LPBillForwardId");
        }
    }
}
