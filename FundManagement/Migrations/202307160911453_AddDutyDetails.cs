namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddDutyDetails : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.LandedCosting", "CusTomDuty", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "RegularDuty", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "SupplementaryDuty", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "ValueAddedTax", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "AdvanceTax", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "Depocharges", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
            AddColumn("dbo.LandedCosting", "ContainerRepairCharges", c => c.Decimal(nullable: false, precision: 18, scale: 2, storeType: "numeric"));
        }
        
        public override void Down()
        {
            DropColumn("dbo.LandedCosting", "ContainerRepairCharges");
            DropColumn("dbo.LandedCosting", "Depocharges");
            DropColumn("dbo.LandedCosting", "AdvanceTax");
            DropColumn("dbo.LandedCosting", "ValueAddedTax");
            DropColumn("dbo.LandedCosting", "SupplementaryDuty");
            DropColumn("dbo.LandedCosting", "RegularDuty");
            DropColumn("dbo.LandedCosting", "CusTomDuty");
        }
    }
}
