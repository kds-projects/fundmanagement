namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpChqIssueSub_WID : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.ChequeIssueSub", "WorkOrderId", c => c.Int(nullable: false, defaultValue: 0));
        }
        
        public override void Down()
        {
            DropColumn("dbo.ChequeIssueSub", "WorkOrderId");
        }
    }
}
