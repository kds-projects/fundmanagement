namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddCSMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.CSMain",
                c => new
                {
                    CSId = c.Int(nullable: false, identity: true),
                    PIFileId = c.Int(nullable: false),
                    CSNo = c.String(nullable: false, maxLength: 15, unicode: false),
                    CSDate = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                })
                .PrimaryKey(t => t.CSId)
                .ForeignKey("dbo.PIMain", t => t.PIFileId, cascadeDelete: true)
                .Index(t => t.PIFileId);

            Sql("ALTER TABLE dbo.CSMain ADD [Month] AS Month([CSDate]) Persisted");
            Sql("ALTER TABLE dbo.CSMain ADD [Year] AS Year([CSDate]) Persisted");
            Sql("ALTER TABLE dbo.CSMain ADD [SerialNo] AS (CONVERT([int],substring([CSNo],(7),(4)),(0))) Persisted");

            AddColumn("dbo.CSMain", "Remarks", c => c.String(nullable: false, maxLength: 500, unicode: false));
            AddColumn("dbo.CSMain", "CreatedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.CSMain", "CreatedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"));
            AddColumn("dbo.CSMain", "RevisedBy", c => c.Short(nullable: false, defaultValue: 0));
            AddColumn("dbo.CSMain", "RevisedTime", c => c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"));
            AddColumn("dbo.CSMain", "IPAddress", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));
            AddColumn("dbo.CSMain", "PCName", c => c.String(maxLength: 50, defaultValue: "-", unicode: false));            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.CSMain", "PIFileId", "dbo.PIMain");
            DropIndex("dbo.CSMain", new[] { "PIFileId" });
            DropTable("dbo.CSMain");
        }
    }
}
