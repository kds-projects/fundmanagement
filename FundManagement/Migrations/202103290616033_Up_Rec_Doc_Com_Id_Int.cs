namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Up_Rec_Doc_Com_Id_Int : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ReceivableDocument");
            AddColumn("dbo.ReceivableDocument", "CompanyId", c => c.Short(nullable: false, defaultValue: 0));
            AlterColumn("dbo.ReceivableDocument", "DocumentId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ReceivableDocument", "DocumentId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ReceivableDocument");
            AlterColumn("dbo.ReceivableDocument", "DocumentId", c => c.Short(nullable: false, identity: true));
            DropColumn("dbo.ReceivableDocument", "CompanyId");
            AddPrimaryKey("dbo.ReceivableDocument", "DocumentId");
        }
    }
}
