namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class AddSanctionMain : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.SanctionMain",
                c => new
                    {
                        SanctionId = c.Int(nullable: false, identity: true),
                        BankId = c.Short(nullable: false),
                        BranchId = c.Short(nullable: false),
                        SanctionNo = c.String(nullable: false, maxLength: 50, unicode: false),
                        SanctionYear = c.Short(nullable: false),
                        ExpireDate = c.DateTime(nullable: false, storeType: "smalldatetime"),
                        CreatedBy = c.Short(nullable: false, defaultValue: 0),
                        CreatedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "getdate()"),
                        RevisedBy = c.Short(nullable: false, defaultValue: 0),
                        RevisedTime = c.DateTime(nullable: false, storeType: "smalldatetime", defaultValueSql: "Cast('01-Jan-1900' As smalldatetime)"),
                        IPAddress = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                        PCName = c.String(nullable: false, maxLength: 50, unicode: false, defaultValue: "-"),
                    })
                .PrimaryKey(t => t.SanctionId)
                .ForeignKey("dbo.Bank", t => t.BankId, cascadeDelete: true)
                .ForeignKey("dbo.BankBranch", t => t.BranchId, cascadeDelete: true)
                .Index(t => t.BankId)
                .Index(t => t.BranchId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.SanctionMain", "BranchId", "dbo.BankBranch");
            DropForeignKey("dbo.SanctionMain", "BankId", "dbo.Bank");
            DropIndex("dbo.SanctionMain", new[] { "BranchId" });
            DropIndex("dbo.SanctionMain", new[] { "BankId" });
            DropTable("dbo.SanctionMain");
        }
    }
}
