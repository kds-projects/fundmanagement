namespace FundManagement.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class UpAdjustmentSub_ConvRate : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.AdjustmentSub", "BDTRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            AddColumn("dbo.AdjustmentSub", "USDRate", c => c.Decimal(nullable: false, precision: 8, scale: 2, storeType: "numeric", defaultValue: 0));
            Sql("ALTER TABLE dbo.AdjustmentSub ADD [AmountInBDT] AS [Amount] * [BDTRate] Persisted");
            Sql("ALTER TABLE dbo.AdjustmentSub ADD [AmountInUSD] AS [Amount] * [USDRate] Persisted");
        }
        
        public override void Down()
        {
            DropColumn("dbo.AdjustmentSub", "AmountInBDT");
            DropColumn("dbo.AdjustmentSub", "AmountInUSD");
            DropColumn("dbo.AdjustmentSub", "USDRate");
            DropColumn("dbo.AdjustmentSub", "BDTRate");
        }
    }
}
