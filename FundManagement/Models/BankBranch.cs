﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class BankBranch: EntityBase, IFIMSContext
    {
        public BankBranch()
        {
            BankAccounts = new List<BankAccount>();
            SanctionMains = new List<SanctionMain>();
        }

        public BankBranch(IFIMSContext viewModel)
        {
            BankAccounts = new List<BankAccount>();
            SanctionMains = new List<SanctionMain>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("BankBranch", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short BranchId { get; set; }
        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        [Index("IX_BranchName", 1, IsUnique = true)]
        public short BankId { get; set; }
        [Required, Display(Name = "Code"), MaxLength(5), Column(TypeName = "VARCHAR")]
        public string BranchCode { get; set; }
        [NotMapped, Display(Name = "Bank")]
        public string BankName { get; set; }
        [Required, Display(Name = "Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        [Index("IX_BranchName", 2, IsUnique = true)]
        public string BranchName { get; set; }
        [Required, Display(Name = "Short Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string ShortName { get; set; }
        [Required, Display(Name = "Swift Code"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string SwiftCode { get; set; }
        [Required, Display(Name = "Contact Person"), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string ContactPerson { get; set; }
        [Required, Display(Name = "Contact Number"), MaxLength(30), Column(TypeName = "VARCHAR")]
        public string ContactNumber { get; set; }
        [Required, Display(Name = "Address"), DefaultValue("-"), DataType(DataType.MultilineText)]
        public String Address { get; set; }

        [Display(Name = "Sales Branch Id")]
        public short SalesBranchId { get; set; }

        [Required, Display(Name = "Activity"), DefaultValue(ActivityType.Active)]
        public ActivityType? Activity { get; set; }
        [NotMapped, Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        public virtual ICollection<SanctionMain> SanctionMains { get; set; }
        public virtual ICollection<LCMain> LCMains { get; set; }
    }
}