﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using FundManagement.Controllers;
using System.Text.RegularExpressions;
using System.Data.Entity.Infrastructure;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel;
using System.Transactions;

namespace FundManagement.Models
{
    //[CAuthorize]
    public static class clsMain
    {
        public static bool IsInValidDate(DateTime checkDate)
        {
            FundContext db = new FundContext();
            List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            //if (UserList.Contains(Int16.Parse(HttpContext.Current.Session["UserId"].ToString())) || checkDate >= db.CustomParameters.FirstOrDefault().LockDate)
            //    return false;
            //else
                return true;
        }

        public static bool IsPermitted(string Controller, string Action)
        {
            FundContext db = new FundContext();
            List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            //if (UserList.Contains(Int16.Parse(HttpContext.Current.Session["UserId"].ToString())) || checkDate >= db.CustomParameters.FirstOrDefault().LockDate)
            //    return false;
            //else
            return true;
        }

        public static bool IsActionEnabled(string Controller, string Action)
        {
            //FundContext db = new FundContext();
            //List<Int16> UserList = new List<Int16> { 2, 7, 12 }; //noman, luna, ilias

            //var m = (from p in db.User_Permission.AsEnumerable()
            //         join q in db.User_Role_Details.AsEnumerable() on p.RoleId equals q.RoleId into r
            //         from s in r.DefaultIfEmpty()
            //         //join t in db.MenuWiseActions.AsEnumerable() on (s == null ? p.ActionId : s.ActionId) equals t.ActionId
            //         join u in db.User_Controller.AsEnumerable() on (s == null ? p.ControllerId : s.ControllerId) equals u.ControllerId
            //         join v in db.User_Action.AsEnumerable() on (s == null ? p.ActionId : s.ActionId) equals v.ActionId
            //         where p.UserId == clsMain.getCurrentUser() && u.ControllerName == Controller && v.ActionName == Action
            //         select p).Distinct().ToList();


            //if (m == null || m.Count() == 0)
            //    return false;
            //else
                return true;
        }

        public static string getEmail(string strmail)
        {
            int o;
            var vmails = strmail.Split(new string[] { "," }, StringSplitOptions.None).Where(l => int.TryParse(l, out o)).Select(c => short.Parse(c)).ToList();
            var ivmails = strmail.Split(new string[] { "," }, StringSplitOptions.None).Where(l => !int.TryParse(l, out o)).Select(c => c.Replace("@UserId", getCurrentUserName())).ToList();
            var mails = getEntity().UserInfos.Where(l => vmails.Contains(l.UserId)).Select(c => c.eMail).Union(ivmails).ToList().Aggregate((i, j) => i + "; " + j);

            return mails;
        }

        public static IQueryable<FundManagement.Models.DBFirst.Factory> getAccessFactory()
        {
            var facid = getEntity().UserInfos.Find(getCurrentUser()).FactoryId.Split(new string[] { "," }, StringSplitOptions.None).Select(c => int.Parse(c)).ToList();
            var factory = (new FundManagement.Models.DBFirst.ProductContext()).Factories.Where(l => facid.Contains(l.FactoryId));

            return factory;
        }

        public static FundContext getEntity()
        {
            using (var scope = new TransactionScope(TransactionScopeOption.Suppress))
            {
                return new FundContext();
            }
        }

        public static DateTime getCurrentTime()
        {
            return getEntity().Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault(); 
        }

        public static short getCurrentUser()
        {
            return Int16.Parse(HttpContext.Current.Session["UserId"] != null ? HttpContext.Current.Session["UserId"].ToString() : "0");
        }

        public static string getCurrentUserName()
        {
            return HttpContext.Current.Session["UserName"].ToString();
        }

        public static string GetUser_IP()
        {
            HttpContext httpContext = HttpContext.Current;
            string VisitorsIPAddr = string.Empty;
            if (httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] != null)
            {
                VisitorsIPAddr = httpContext.Request.ServerVariables["HTTP_X_FORWARDED_FOR"].ToString();
            }
            else if (httpContext.Request.ServerVariables["REMOTE_ADDR"] != null)
            {
                VisitorsIPAddr = httpContext.Request.ServerVariables["REMOTE_ADDR"].ToString();
            }
            else if (httpContext.Request.UserHostAddress.Length != 0)
            {
                VisitorsIPAddr = httpContext.Request.UserHostAddress;
            }
            //IPHostEntry ipEntry = System.Net.Dns.GetHostEntry("Win8N");
            //IPAddress[] addr = ipEntry.AddressList;
            return VisitorsIPAddr;
        }

        public static string GetUser_PCName()
        {
            string CName = "";
            try
            {
                CName = System.Net.Dns.GetHostEntry(HttpContext.Current.Request.ServerVariables["REMOTE_ADDR"]).HostName;
            }
            catch (System.Net.Sockets.SocketException ex)
            {
                CName = "INTERNET";
            }
            catch (Exception ex)
            {
                CName = "=N/A=";
            }

            return CName;
        }

        public static string getExternalIp()
        {
            try
            {
                string externalIP;
                externalIP = (new WebClient()).DownloadString("http://checkip.dyndns.org/");
                externalIP = (new Regex(@"\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}"))
                             .Matches(externalIP)[0].ToString();
                return externalIP;
            }
            catch { return null; }
        }

        public static string getCurrentEmail()
        {
            return (new FundContext()).UserInfos.Find(getCurrentUser()).eMail;
        }

        public static UserView getCurrentUserInfo()
        {
            return (UserView)(new FundContext()).UserInfos.Find(getCurrentUser()).Convert(new UserView());
        }

        //public static void prcLoginReport(ref ReportDocument reportDocument, bool IsLoginRequired)
        //{
        //    clsServerConfig ServerConfig = clsMain.GetReportLogon();

        //    if (IsLoginRequired)
        //    {
        //        ConnectionInfo rptConnection = new ConnectionInfo();
        //        rptConnection.ServerName = ServerConfig.Server;
        //        rptConnection.DatabaseName = ServerConfig.Database;
        //        rptConnection.UserID = ServerConfig.UserId;
        //        rptConnection.Password = ServerConfig.Password;

        //        foreach (ReportDocument subRpt in reportDocument.Subreports)
        //        {
        //            foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in subRpt.Database.Tables)
        //            {
        //                TableLogOnInfo logInfo = crTable.LogOnInfo;
        //                logInfo.ConnectionInfo = rptConnection;
        //                crTable.ApplyLogOnInfo(logInfo);
        //            }
        //        }

        //        foreach (CrystalDecisions.CrystalReports.Engine.Table crTable in reportDocument.Database.Tables)
        //        {
        //            TableLogOnInfo loi = crTable.LogOnInfo;
        //            loi.ConnectionInfo = rptConnection;
        //            crTable.ApplyLogOnInfo(loi);
        //        }
        //    }
        //}

        internal static clsServerConfig GetReportLogon()
        {
            clsServerConfig ServerConfig = new clsServerConfig();
            ServerConfig.Server = "203.202.249.115,14333";
            ServerConfig.UserId = "sa";
            ServerConfig.Password = "#GPH-S/shark@115sdk";
            ServerConfig.Database = "GPH";

            return ServerConfig;
        }

        public static object getDefaultValue(string TableName, string PropertyName, Type type)
        {
            using (FundContext db = new FundContext())
            {
                object obj = null;
                var viewDefaultLists = db.Database.SqlQuery<clsDefaultValue>("Select * From viewDefaultList").ToList();
                string DefaultValue = viewDefaultLists.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                        {
                            var dateValue = db.Database.SqlQuery(typeof(DateTime), "Select " + DefaultValue.Substring(1, DefaultValue.Length - 2), "").Cast<DateTime>().ToList().FirstOrDefault();
                            obj = dateValue;//DateTime.Parse();
                        }
                    }
                    else if (type == typeof(ActivityType))
                    {
                        if (Convert.ToInt32(DefaultValue.Replace("((", "").Replace("))", "")) == 1)
                        {
                            obj = ActivityType.Active;
                        }
                        else
                        {
                            obj = ActivityType.Inactive;
                        }
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(ActivityType))
                    {
                        obj = ActivityType.Inactive;
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }

                return obj;
            }
        }

        public static object getPCDefaultValue(string TableName, string PropertyName, Type type)
        {
            object obj = null;
            //using (ProductCubeEntities db = new ProductCubeEntities())
            //{
            //    string DefaultValue = db.viewDefaultLists.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
            //    if (DefaultValue != null)
            //    {
            //        if (type == typeof(int))
            //        {
            //            obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(short))
            //        {
            //            obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(byte))
            //        {
            //            obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(double))
            //        {
            //            obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(decimal))
            //        {
            //            obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(DateTime))
            //        {
            //            if (DefaultValue.ToLower().Equals("(getdate())"))
            //                obj = (new LCManagementEntities()).CustomParameters.FirstOrDefault().CurrentDate;
            //            else if (DefaultValue.ToLower().Equals("('')"))
            //                obj = DateTime.Parse("01-Jan-1900");
            //            else
            //                obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
            //        }
            //        else if (type == typeof(string))
            //        {
            //            obj = DefaultValue.Replace("('", "").Replace("')", "");
            //        }
            //    }
            //    else
            //    {
            //        if (type == typeof(int))
            //        {
            //            obj = int.Parse("0");
            //        }
            //        else if (type == typeof(short))
            //        {
            //            obj = Int16.Parse("0");
            //        }
            //        else if (type == typeof(byte))
            //        {
            //            obj = byte.Parse("0");
            //        }
            //        else if (type == typeof(double))
            //        {
            //            obj = double.Parse("0");
            //        }
            //        else if (type == typeof(decimal))
            //        {
            //            obj = decimal.Parse("0");
            //        }
            //        else if (type == typeof(DateTime))
            //        {
            //            obj = DateTime.Parse("01-Jan-1900");
            //        }
            //        else if (type == typeof(string))
            //        {
            //            obj = "";
            //        }
            //    }

            //}

            return obj;
        }

        public static object getDefaultValueLC(string TableName, string PropertyName, Type type)
        {
            object obj = null;

            //using (LCManagementEntities db = new LCManagementEntities())
            //{
            //    string DefaultValue = db.viewDefaultList_LC.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
            //    if (DefaultValue != null)
            //    {
            //        if (type == typeof(int))
            //        {
            //            obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(short))
            //        {
            //            obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(byte))
            //        {
            //            obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(double))
            //        {
            //            obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(decimal))
            //        {
            //            obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
            //        }
            //        else if (type == typeof(DateTime))
            //        {
            //            if (DefaultValue.ToLower().Equals("(getdate())"))
            //                obj = db.CustomParameters.FirstOrDefault().CurrentDate;
            //            else if (DefaultValue.ToLower().Equals("('')"))
            //                obj = DateTime.Parse("01-Jan-1900");
            //            else
            //                obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
            //        }
            //        else if (type == typeof(string))
            //        {
            //            obj = DefaultValue.Replace("('", "").Replace("')", "");
            //        }
            //    }
            //    else
            //    {
            //        if (type == typeof(int))
            //        {
            //            obj = int.Parse("0");
            //        }
            //        else if (type == typeof(short))
            //        {
            //            obj = Int16.Parse("0");
            //        }
            //        else if (type == typeof(byte))
            //        {
            //            obj = byte.Parse("0");
            //        }
            //        else if (type == typeof(double))
            //        {
            //            obj = double.Parse("0");
            //        }
            //        else if (type == typeof(decimal))
            //        {
            //            obj = decimal.Parse("0");
            //        }
            //        else if (type == typeof(DateTime))
            //        {
            //            obj = DateTime.Parse("01-Jan-1900");
            //        }
            //        else if (type == typeof(string))
            //        {
            //            obj = "";
            //        }
            //    }
            //}

            return obj;
        }

        public static object getDefaultValueEMC(string TableName, string PropertyName, Type type)
        {
            object obj = null;
            using (Models.DBFirst.EMailEntities db = new Models.DBFirst.EMailEntities())
            {
                string DefaultValue = db.viewDefaultList_EMC.Where(l => l.Table == TableName && l.Column == PropertyName).Select(l => l.DefaultValue).FirstOrDefault();
                if (DefaultValue != null)
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse(DefaultValue.Replace("((", "").Replace("))", ""));
                    }
                    else if (type == typeof(DateTime))
                    {
                        if (DefaultValue.ToLower().Equals("(getdate())"))
                            obj = clsMain.getCurrentTime();
                        else if (DefaultValue.ToLower().Equals("('')"))
                            obj = DateTime.Parse("01-Jan-1900");
                        else
                            obj = DateTime.Parse(DefaultValue.Replace("('", "").Replace("')", ""));
                    }
                    else if (type == typeof(string))
                    {
                        obj = DefaultValue.Replace("('", "").Replace("')", "");
                    }
                }
                else
                {
                    if (type == typeof(int))
                    {
                        obj = int.Parse("0");
                    }
                    else if (type == typeof(short))
                    {
                        obj = Int16.Parse("0");
                    }
                    else if (type == typeof(byte))
                    {
                        obj = byte.Parse("0");
                    }
                    else if (type == typeof(double))
                    {
                        obj = double.Parse("0");
                    }
                    else if (type == typeof(decimal))
                    {
                        obj = decimal.Parse("0");
                    }
                    else if (type == typeof(DateTime))
                    {
                        obj = DateTime.Parse("01-Jan-1900");
                    }
                    else if (type == typeof(string))
                    {
                        obj = "";
                    }
                }
            }

            return obj;
        }

        public static object getDescription(string TableName, string PropertyName)
        {
            //using (FundContext db = new FundContext())
            //{
            //    return db.viewDecriptionLists.Where(l => l.TableName == TableName && l.ColumnName == PropertyName).Select(l => "[Diplay(name=\"" + "" + "\")]").FirstOrDefault();
            //}

            return "";
        }
    }

    public class PagedData<T> where T : class
    {
        public IEnumerable<T> Data { get; set; }
        public int NumberOfPages { get; set; }
        public int CurrentPage { get; set; }
    }


    public enum ActivityType
    {
        Active = 1, Inactive = 0
    }

    public enum BondType
    {
        Yes = 1, No = 0
    }

    public class EntityBase
    {
        public EntityBase()
        {
            CreatedTime = DateTime.Parse("01-Jan-1900");
            RevisedTime = DateTime.Parse("01-Jan-1900");
            IPAddress = "-";
            PCName = "-";
        }

        [Required]
        public short CreatedBy { get; set; }
        [DatabaseGenerated(DatabaseGeneratedOption.Computed), Column(TypeName = "smalldatetime")]
        public DateTime CreatedTime { get; set; }
        [Required, DefaultValue(0)]
        public short RevisedBy { get; set; }
        [Column(TypeName = "smalldatetime")]
        public DateTime RevisedTime { get; set; }
        [Required, Display(Name = "IP Address"), MaxLength(50), Column(TypeName = "VARCHAR"), DefaultValue("-")]
        public string IPAddress { get; set; }
        [Required, Display(Name = "PC Name"), MaxLength(50), Column(TypeName = "VARCHAR"), DefaultValue("-")]
        public string PCName { get; set; }

        public void OnBeforeInsert(DbEntityEntry entity)
        {
            CreatedBy = clsMain.getCurrentUser();
            //entity.Property("CreatedBy").CurrentValue = short.Parse("0");
            //entity.Property("CreatedTime").CurrentValue = DateTime.Parse("01-Jan-2019");
            CreatedTime = clsMain.getCurrentTime();
            IPAddress = clsMain.GetUser_IP();
            PCName = clsMain.GetUser_PCName();
        }

        public void OnBeforeUpdate(DbEntityEntry entity)
        {
            entity.Property("CreatedBy").IsModified = false;
            entity.Property("CreatedTime").IsModified = false;
            entity.Property("PCName").IsModified = false;
            entity.Property("IPAddress").IsModified = false;
            
            if (!entity.CurrentValues.PropertyNames.Any(c => c.Equals("_editStatus")) || Convert.ToInt32(entity.Property("_editStatus").CurrentValue ?? 0) != 2)
            {
                entity.Property("RevisedBy").CurrentValue = clsMain.getCurrentUser();
                entity.Property("RevisedTime").CurrentValue = clsMain.getCurrentTime();
            }
        }
    }

    public interface IControllerHooks
    {
        void OnBeforeInsert(DbEntityEntry entity);
        void OnBeforeUpdate(DbEntityEntry entity);
    }

    public interface IFIMSContext
    {

    }

    public class PersistPropertyOnEdit : Attribute
    {
        public readonly bool PersistPostbackDataFlag;
        public PersistPropertyOnEdit(bool persistPostbackDataFlag)
        {
            this.PersistPostbackDataFlag = persistPostbackDataFlag;
        }
    }

    public class clsServerConfig
    {
        public string Server { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string Database { get; set; }
    }
}