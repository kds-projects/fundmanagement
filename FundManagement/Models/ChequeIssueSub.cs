﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeIssueSub : IFIMSContext
    {
        public ChequeIssueSub()
        {
            AdjustmentDate = DateTime.Parse("01-Jan-1900");
            ChequeIssuePipelines = new List<ChequeIssuePipeline>();
        }

        public ChequeIssueSub(IFIMSContext viewModel)
        {
            AdjustmentDate = DateTime.Parse("01-Jan-1900");
            ChequeIssuePipelines = new List<ChequeIssuePipeline>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeIssueSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int InstructionSubId { get; set; }

        [Required, Display(Name = "ChequeIssueInstruction")]
        [ForeignKey("ChequeIssueInstruction")]
        public int InstructionId { get; set; }

        [Required, Display(Name = "Bill Forward Id")]
        public int LPBillForwardId { get; set; }

        [Required, Display(Name = "Service Bill Forward Id")]
        public int ServiceBillForwardId { get; set; }

        [Required, Display(Name = "Bill Forward Id")]
        public int LPBillForwardDetailsId { get; set; }

        [Required, Display(Name = "Work Order Id")]
        public int WorkOrderId { get; set; }

        [Required, Display(Name = "Service Work Order Id")]
        public int ServiceWorkOrderId { get; set; }

        [Required, Display(Name = "Adjustment Id")]
        public int AdjustmentId { get; set; }
    
        [Required, Display(Name = "Bill Amount"), Column(TypeName = "numeric")]
        public Decimal BillAmount { get; set; }

        [Required, NotMapped, Display(Name = "Tax"), Column(TypeName = "numeric")]
        public Decimal Tax { get; set; }

        [Required, NotMapped, Display(Name = "VAT"), Column(TypeName = "numeric")]
        public Decimal VAT { get; set; }

        [Required, Display(Name = "Adjustment"), Column(TypeName = "numeric")]
        public Decimal Adjustment { get; set; }

        [Display(Name = "Adjustment"), Column(TypeName = "smalldatetime")]
        public Nullable<DateTime> AdjustmentDate { get; set; }

        public virtual ChequeIssueInstruction ChequeIssueInstruction { get; set; }
        public virtual ICollection<ChequeIssuePipeline> ChequeIssuePipelines { get; set; }
    }
}