﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class LCMain: EntityBase
    {
        public LCMain()
        {
            LCSubs = new List<LCSub>();
            LCWisePIs = new List<LCWisePI>();
        }

        public LCMain(IFIMSContext viewModel)
        {
            LCSubs = new List<LCSub>();
            LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LCMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LCFileId { get; set; }

        [Required, Display(Name = "File #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string LCFileNo { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        //[Display(Name = "Payment Date")]
        //public Nullable<System.DateTime> PaymentSDate { get; set; }

        //[Required, Display(Name = "Payment Date"), Column(TypeName = "smalldatetime")]
        //public System.DateTime PaymentDate { get; set; }

        [Required, Display(Name = "LC No"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string LCNo { get; set; }

        [Required, Display(Name = "LC Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime LCDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return LCDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return LCDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return short.Parse(LCFileNo.Substring(2, 4)); } protected set { } }

        [Required, Display(Name = "Applicant")]
        public short BeneficiaryId { get; set; }

        [Required, Display(Name = "Supplier")]
        public short SupplierId { get; set; }

        [Required, Display(Name = "Agent")]
        public short LocalAgentId { get; set; }

        [Required, Display(Name = "Country of Origin")]
        public short OriginCountryId { get; set; }

        [Required, Display(Name = "Port of Loading")]
        public short POLId { get; set; }

        [Required, Display(Name = "Issuer Bank")]
        [ForeignKey("IssuerBank")]
        public short IssuerBankId { get; set; }

        [Required, Display(Name = "Issuer Branch")]
        [ForeignKey("IssuerBranch")]
        public short IssuerBranchId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Negotiation Bank"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string NegotiatingBank { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Supplier Bank"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string BeneficiaryBank { get; set; }

        [Required, Display(Name = "LC Type"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string LCType { get; set; }

        [Required, Display(Name = "Shipment Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime ShipmentDate { get; set; }

        [Required, Display(Name = "Expire Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime ExpireDate { get; set; }

        [Required, Display(Name = "Supplier Tenure")]
        public short SupplierTenure { get; set; }

        [Required, Display(Name = "Sup Ten Type")]
        [ForeignKey("STenureType")]
        public short STenureTypeId { get; set; }

        [Required, Display(Name = "Bank Tenure")]
        public short BankTenure { get; set; }

        [Required, Display(Name = "Bank Ten Type")]
        [ForeignKey("BTenureType")]
        public short BTenureTypeId { get; set; }

        [Required, Display(Name = "Tenure Start Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime TenureStartDate { get; set; }

        [Required, Display(Name = "Ins. CN No"), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string CoverNoteNo { get; set; }

        [Required, Display(Name = "Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime CoverNoteDate { get; set; }

        [Required, Display(Name = "Ins. Value")]
        public decimal InsuranceValue { get; set; }

        [Required, Display(Name = "Ins. Premium")]
        public decimal InsurancePremium { get; set; }

        [Required, Display(Name = "LC Amount"), Column(TypeName = "numeric")]
        public decimal LCAmount { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return LCAmount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return LCAmount * BDTRate; } protected set { } }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Display(Name = "PI List"), NotMapped]
        public string[] PIList { get; set; }

        [NotMapped]
        public DBFirst.Company Benificiary
        {
            get
            {
                return (new DBFirst.ProductContext()).Companies.Find(BeneficiaryId);
            }
        }

        public virtual Bank IssuerBank { get; set; }
        public virtual BankBranch IssuerBranch { get; set; }
        public virtual CustomType STenureType { get; set; }
        public virtual CustomType BTenureType { get; set; }
        public virtual CustomType Currency { get; set; }
        [JsonIgnore]
        public virtual ICollection<LCSub> LCSubs { get; set; }
        public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
        public virtual ICollection<LCAcceptance> LCAcceptances { get; set; }
    }
}