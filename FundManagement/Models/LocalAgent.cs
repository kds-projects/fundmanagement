﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class LocalAgent : EntityBase, IControllerHooks, IFIMSContext
    {
        public LocalAgent()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();
        }

        public LocalAgent(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LocalAgent", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short AgentId { get; set; }

        [Required, Display(Name = "Country")]
        public short CountryId { get; set; }

        [NotMapped, Display(Name = "Code"), MaxLength(5), Column(TypeName = "VARCHAR")]
        public string AgentCode { get; set; }

        [Required, Display(Name = "Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string AgentName { get; set; }

        [Required, Display(Name = "Address"), MaxLength(250), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string AgentAddress { get; set; }

        [Required, Display(Name = "Phone"), MaxLength(100), Column(TypeName = "VARCHAR")]
        public string PhoneNo { get; set; }

        [Required, Display(Name = "Fax"), MaxLength(100), Column(TypeName = "VARCHAR")]
        public string Fax { get; set; }

        [Required, Display(Name = "eMail"), MaxLength(100), Column(TypeName = "VARCHAR")]
        public string eMailAddress { get; set; }

        [Required, Display(Name = "Web"), MaxLength(100), Column(TypeName = "VARCHAR")]
        public string WebAddress { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
    }
}