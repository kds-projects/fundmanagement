﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class LCAcceptance : EntityBase, IFIMSContext, IControllerHooks
    {
        public LCAcceptance()
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public LCAcceptance(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LCAcceptance", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Required, Display(Name = "LC #")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LCAcceptanceId { get; set; }

        [Required, Display(Name = "Lot #")]
        public int PLEId { get; set; }

        [Required, Display(Name = "Doc Ret Date"), Column(TypeName = "smalldatetime")]
        public DateTime DocumentRetirementDate { get; set; }

        [Required, Display(Name = "Bank Recv Date"), Column(TypeName = "smalldatetime")]
        public DateTime BankReceiveDate { get; set; }

        [Required, Display(Name = "Handover To C&F"), Column(TypeName = "smalldatetime")]
        public DateTime HandoverToCNFDate { get; set; }

        [Required, Display(Name = "Handover To Sourcing"), Column(TypeName = "smalldatetime")]
        public DateTime HandoverToSourcingDate { get; set; }

        //[Required, Display(Name = "BL Date"), Column(TypeName = "smalldatetime")]
        //public DateTime BLDate { get; set; }

        //[Required, Display(Name = "BL #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        //public string BLNo { get; set; }

        [Required, Display(Name = "Acceptance #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string AcceptanceNo { get; set; }

        [Required, Display(Name = "Acceptance Date"), Column(TypeName = "smalldatetime")]
        public DateTime AcceptanceDate { get; set; }

        [Required, Display(Name = "Tenure Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime TenureStartDate { get; set; }

        [Required, Display(Name = "Acceptance Tenure")]
        public short AcceptanceTenure { get; set; }

        [Required, Display(Name = "Loan Tenure")]
        public short LoanTenure { get; set; }

        [Required, Display(Name = "Maturity Date"), Column(TypeName = "smalldatetime")]
        public DateTime ExpireDate { get; set; }

        [Required, Display(Name = "Accepted Amount"), Column(TypeName = "numeric")]
        public decimal AcceptanceAmount { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("AcceptanceCurrency")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "Libor Amount"), Column(TypeName = "numeric")]
        public decimal LiborAmount { get; set; }

        [Required, Display(Name = "Libor IR"), Column(TypeName = "numeric")]
        public decimal LiborInterestRate { get; set; }

        [Required, Display(Name = "Libor OD IR"), Column(TypeName = "numeric")]
        public decimal LiborInterestRateOD { get; set; }

        [Required, Display(Name = "Libor IR Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime LiborInterestStartDate { get; set; }

        [Required, Display(Name = "Handling Charge"), Column(TypeName = "numeric")]
        public decimal HandlingCharge { get; set; }

        [Required, Display(Name = "HC IR"), Column(TypeName = "numeric")]
        public decimal HCInterestRate { get; set; }

        [Required, Display(Name = "HC OD IR"), Column(TypeName = "numeric")]
        public decimal HCInterestRateOD { get; set; }

        [Required, Display(Name = "HC IR Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime HCInterestStartDate { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return AcceptanceAmount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return AcceptanceAmount * BDTRate; } protected set { } }

        [Required, Display(Name = "Landed Costing Status")]
        public short LandedCostingStatus { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual LCMain LCMain { get; set; }
        public virtual CustomType AcceptanceCurrency { get; set; }
    }
}