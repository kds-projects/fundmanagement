﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CSSub: IFIMSContext
    {
        public CSSub()
        {
            //CSMain = new CSMain();
        }

        public CSSub(IFIMSContext viewModel)
        {
            //CSMain = new CSMain();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CSSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "CS #")]
        [ForeignKey("CSMain")]
        public int CSId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int CSSubId { get; set; }

        [Required, Display(Name = "Bank")]
        public short BankId1 { get; set; }

        [Required, Display(Name = "BDT Rate")]
        public decimal ConvertRate1 { get; set; }

        [Required, Display(Name = "UPAS (Days)")]
        public short UPASS1 { get; set; }

        [Required, Display(Name = "Post-UPAS (Days)")]
        public short PostUPASS1 { get; set; }

        [Required, Display(Name = "LIBOR")]
        public decimal Libor1 { get; set; }

        [Required, Display(Name = "Handling")]
        public decimal Handling1 { get; set; }

        [Required, Display(Name = "Interest Rate")]
        public decimal InterestRate1 { get; set; }

        [Required, Display(Name = "LC Com Rate")]
        public decimal LCCommissionRate1 { get; set; }

        [Required, Display(Name = "ACP Com Rate")]
        public decimal AcceptanceCommissionRate1 { get; set; }

        [Required, Display(Name = "Other Charges")]
        public decimal OtherChargesPerLC1 { get; set; }

        //[Required, Display(Name = "Swift Charges (Accept Com)")]
        //public decimal SwiftChargesAcceptanceCommission1 { get; set; }

        [Required, Display(Name = "Bank")]
        public short BankId2 { get; set; }

        [Required, Display(Name = "BDT Rate")]
        public decimal ConvertRate2 { get; set; }

        [Required, Display(Name = "UPAS (Days)")]
        public short UPASS2 { get; set; }

        [Required, Display(Name = "Post-UPAS (Days)")]
        public short PostUPASS2 { get; set; }

        [Required, Display(Name = "LIBOR")]
        public decimal Libor2 { get; set; }

        [Required, Display(Name = "Handling")]
        public decimal Handling2 { get; set; }

        [Required, Display(Name = "Interest Rate")]
        public decimal InterestRate2 { get; set; }

        [Required, Display(Name = "LC Com Rate")]
        public decimal LCCommissionRate2 { get; set; }

        [Required, Display(Name = "ACP Com Rate")]
        public decimal AcceptanceCommissionRate2 { get; set; }

        [Required, Display(Name = "Other Charges")]
        public decimal OtherChargesPerLC2 { get; set; }

        //[Required, Display(Name = "Swift Charges (Accept Com)")]
        //public decimal SwiftChargesAcceptanceCommission2 { get; set; }

        [Required, Display(Name = "Bank")]
        public short BankId3 { get; set; }

        [Required, Display(Name = "BDT Rate")]
        public decimal ConvertRate3 { get; set; }

        [Required, Display(Name = "UPAS (Days)")]
        public short UPASS3 { get; set; }

        [Required, Display(Name = "Post-UPAS (Days)")]
        public short PostUPASS3 { get; set; }

        [Required, Display(Name = "LIBOR")]
        public decimal Libor3 { get; set; }

        [Required, Display(Name = "Handling")]
        public decimal Handling3 { get; set; }

        [Required, Display(Name = "Interest Rate")]
        public decimal InterestRate3 { get; set; }

        [Required, Display(Name = "LC Com Rate")]
        public decimal LCCommissionRate3 { get; set; }

        [Required, Display(Name = "ACP Com Rate")]
        public decimal AcceptanceCommissionRate3 { get; set; }

        [Required, Display(Name = "Other Charges")]
        public decimal OtherChargesPerLC3 { get; set; }

        //[Required, Display(Name = "Swift Charges (Accept Com)")]
        //public decimal SwiftChargesAcceptanceCommission3 { get; set; }

        [NotMapped, Display(Name = "Status"), MaxLength(500), Column(TypeName = "VARCHAR")]
        public string _editstatus { get; set; }

        public virtual CSMain CSMain { get; set; }
    }
}