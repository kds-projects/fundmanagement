﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ODDetails : EntityBase, IControllerHooks, IFIMSContext
    {
        public ODDetails()
        {
            TransactionDate = DateTime.Today;
        }

        public ODDetails(IFIMSContext viewModel)
        {
            TransactionDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ODDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ODDetailsId { get; set; }

        [Display(Name = "OD #")]
        [ForeignKey("ODMain")]
        public int ODId { get; set; }

        [Required, Display(Name = "Closing Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime TransactionDate { get; set; }

        [Required, Display(Name = "Closing Balance"), Column(TypeName = "numeric")]
        public decimal ClosingBalance { get; set; }

        [Required, Display(Name = "Interest Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Penal Int Rate"), Column(TypeName = "numeric")]
        public decimal InterestRateOD { get; set; }

        [Display(Name = "Interest"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> Interest { get; set; }

        [Display(Name = "Penal Interest"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> ODInterest { get { return 0; } protected set { } }

        [Required(AllowEmptyStrings=true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [NotMapped]
        public string _editstatus { get; set; }

        public virtual ODMain ODMain { get; set; }
    }
}