﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class DailyFundSub : IFIMSContext
    {
        public DailyFundSub()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public DailyFundSub(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("DailyFundSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int DFSubId { get; set; }

        [Display(Name = "DailyFund #")]
        [ForeignKey("DailyFundMain")]
        public int DFId { get; set; }

        [Required, Display(Name = "Account #")]
        [ForeignKey("BankAccount")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Op Balance Bank"), Column(TypeName = "numeric")]
        public Decimal BankOpeningBalance { get; set; }

        [Required, Display(Name = "CL Balance Bank"), Column(TypeName = "numeric")]
        public Decimal BankClosingBalance { get; set; }

        [Required, Display(Name = "Op Balance Book"), Column(TypeName = "numeric")]
        public Decimal BookOpeningBalance { get; set; }

        [Required, Display(Name = "CL Balance Book"), Column(TypeName = "numeric")]
        public Decimal BookClosingBalance { get; set; }

        [Required, Display(Name = "Disposit"), Column(TypeName = "numeric")]
        public Decimal DebitAmount { get; set; }

        [Required, Display(Name = "Withdraw"), Column(TypeName = "numeric")]
        public Decimal CreditAmount { get; set; }

        [Required, Display(Name = "Int. Rate"), Column(TypeName = "numeric")]
        public Decimal InterestRate { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(350), Column(TypeName = "VARCHAR")]
        public string Remarks { get; set; }

        [Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual BankAccount BankAccount { get; set; }
        public virtual DailyFundMain DailyFundMain { get; set; }
    }
}