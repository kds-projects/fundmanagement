﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ImportBudget : EntityBase
    {
        public ImportBudget()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public ImportBudget(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ImportBudget", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short BudgetId { get; set; }
        [Required, Display(Name = "Year")]
        public short BudgetYear { get; set; }
        [Required, Display(Name = "Month")]
        public short BudgetMonth { get; set; }
        [Required, Display(Name = "Factory")]
        public short FactoryId { get; set; }
        [Required, Display(Name = "Type")]
        public short TypeId { get; set; }
        [Required, Display(Name = "Budget Amount"), Column(TypeName = "numeric")]
        public Decimal BudgetAmount { get; set; }
        [Required, Display(Name = "Budget Quantity"), Column(TypeName = "numeric")]
        public Decimal BudgetQuntity { get; set; }
        [Required, Display(Name = "Quantity Unit")]
        public short QuantityUnitId { get; set; }
    }
}