﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CollectionForecasting : EntityBase
    {
        public CollectionForecasting()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public CollectionForecasting(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CollectionForecasting", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short ForecastId { get; set; }
        [Required, Display(Name = "Year")]
        public short ForecastYear { get; set; }
        [Required, Display(Name = "Month")]
        public short ForecastMonth { get; set; }
        [NotMapped, Required, Display(Name = "Coll. in Hand (BDT)"), Column(TypeName = "numeric")]
        public Decimal CollectionInHandBDT { get; set; }
        [NotMapped, Required, Display(Name = "Coll. in Hand (USD)"), Column(TypeName = "numeric")]
        public Decimal CollectionInHandUSD { get; set; }
        [Required, Display(Name = "Coll. Projected (BDT)"), Column(TypeName = "numeric")]
        public Decimal CollectionProjectedBDT { get; set; }
        [Required, Display(Name = "Coll. Projected (USD)"), Column(TypeName = "numeric")]
        public Decimal CollectionProjectedUSD { get; set; }
        [NotMapped, Required, Display(Name = "Coll. Maturity"), Column(TypeName = "numeric")]
        public Decimal CollectionMaturity { get; set; }
        [Required, Display(Name = "Wastage Sales"), Column(TypeName = "numeric")]
        public Decimal WastageSales { get; set; }
        [Required, Display(Name = "Others"), Column(TypeName = "numeric")]
        public Decimal Others { get; set; }
    }
}