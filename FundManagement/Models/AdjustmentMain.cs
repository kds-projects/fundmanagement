﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class AdjustmentMain : EntityBase, IFIMSContext
    {
        public AdjustmentMain()
        {
            AdjustmentSubs = new List<AdjustmentSub>();
            AdjustmentDate = DateTime.Today;
        }

        public AdjustmentMain(IFIMSContext viewModel)
        {
            AdjustmentSubs = new List<AdjustmentSub>();
            AdjustmentDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("AdjustmentMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int AdjustmentId { get; set; }

        [Required, Display(Name = "File #"), MaxLength(20), Column(TypeName = "VARCHAR")]
        public string AdjustmentNo { get; set; }

        [Required, Display(Name = "Adjustment Date"), Column(TypeName = "smalldatetime")]
        [Index("IX_AdjustmentDate", 1)]
        public DateTime AdjustmentDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Adjustment Date")]
        public Nullable<System.DateTime> AdjustmentSDate { get { return AdjustmentDate.Date; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return AdjustmentDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return AdjustmentDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return short.Parse(AdjustmentNo.Substring(2, 4)); } protected set { } }

        [Required, Display(Name = "Adjustment Amount"), Column(TypeName = "numeric")]
        public decimal AdjustmentAmount { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("CurrencyType")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual CustomType CurrencyType { get; set; }
        public virtual ICollection<AdjustmentSub> AdjustmentSubs { get; set; }
    }
}