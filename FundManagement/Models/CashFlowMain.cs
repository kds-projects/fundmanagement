﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;
using FundManagement.Models.DBFirst;

namespace FundManagement.Models
{
    public partial class CashFlowMain : EntityBase, IFIMSContext, IControllerHooks
    {
        public CashFlowMain()
        {
            CashFlowDetails = new List<CashFlowDetails>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public CashFlowMain(IFIMSContext viewModel)
        {
            CashFlowDetails = new List<CashFlowDetails>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CashFlowMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short CFId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Year")]
        public int CFYear { get { return CFDate == null ? 0 : CFDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Month")]
        public int CFMonth { get { return CFDate == null ? 0 : CFDate.Month; } protected set { } }

        [NotMapped, Required, Display(Name = "Month")]
        public string CFMonthName { get { return CFDate == null ? "" : CFDate.ToString("MMMM"); } protected set { } }

        [Required, Display(Name = "Month")]
        public DateTime CFDate { get; set; }

        [Required, Display(Name = "Company")]
        public short GroupId { get; set; }

        [NotMapped]
        public GroupOfCompany Company
        {
            get
            {
                using (var ctx = new ProductContext())
                {
                    return GroupId == 0 ? null : ctx.GroupOfCompanies.Find(GroupId);
                }
            }
            protected set { }
        }

        [Required(AllowEmptyStrings=true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual ICollection<CashFlowDetails> CashFlowDetails { get; set; }
    }
}