﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class ComparativeAnalysis : EntityBase, IControllerHooks, IFIMSContext
    {
        public ComparativeAnalysis()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();
        }

        public ComparativeAnalysis(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ComparativeAnalysis", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int AnalysisId { get; set; }

        [Display(Name = "PI #")]
        [ForeignKey("PIMain")]
        public int PIFileId { get; set; }

        [Required, Display(Name = "Analysis #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string AnalysisNo { get; set; }

        [Required, Display(Name = "Analysis Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime AnalysisDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return AnalysisDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return AnalysisDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return short.Parse(AnalysisNo.Substring(2, 4)); } protected set { } }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "Bank Credit")]
        public short BankCreditPeriod { get; set; }

        [Required, Display(Name = "Libor Rate"), Column(TypeName = "numeric")]
        public decimal LiborRate { get; set; }

        [Required, Display(Name = "HC Rate"), Column(TypeName = "numeric")]
        public decimal HandlingChargeRate { get; set; }

        [Required, Display(Name = "Op. Com. Rate"), Column(TypeName = "numeric")]
        public decimal LCOpeningCommissionRate { get; set; }

        [Required, Display(Name = "Acp Com. Rate"), Column(TypeName = "numeric")]
        public decimal LCAcceptanceCommissionRate { get; set; }

        [Required, Display(Name = "Acp Com. Rate"), Column(TypeName = "numeric")]
        public decimal OtherChargeRate { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual PIMain PIMain { get; set; }
    }
}