﻿using FundManagement.Models.DBFirst;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class ReceivableDocument : EntityBase, IControllerHooks, IFIMSContext
    {
        public ReceivableDocument()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionMains = new List<SanctionMain>();
        }

        public ReceivableDocument(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionMains = new List<SanctionMain>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (pi.CanWrite)
                {
                    if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                    {
                        if (pi.PropertyType.Name.Equals("DateTime") && DateTime.Parse(viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel).ToString()) < DateTime.Parse("01-Jan-1900"))
                        {
                            object obj = clsMain.getDefaultValue("ReceivableDocument", pi.Name, pi.PropertyType);
                            if (obj != null)
                                pi.SetValue(this, obj);
                        }
                        else
                        {
                            pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                        }
                    }
                    else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                    {
                        object obj = clsMain.getDefaultValue("ReceivableDocument", pi.Name, pi.PropertyType);
                        if (obj != null)
                            pi.SetValue(this, obj);
                    }
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int DocumentId { get; set; }
        [Required, Display(Name = "Document No"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string DocumentNo { get; set; }
        [Required, Display(Name = "Document Title"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string DocumentName { get; set; }
        [Required, Display(Name = "Document Date"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime DocumentDate { get; set; }
        [Required, Display(Name = "Receive Date"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime ReceiveDate { get; set; }
        [Required, Display(Name = "Type"), DefaultValue(0)]
        [ForeignKey("DocumentType")]
        public short DocumentTypeId { get; set; }
        [Required, Display(Name = "Company")]
        public short CompanyId { get; set; }
        [Required, Display(Name = "Payable ID")]
        public short PayableId { get; set; }
        [NotMapped, Display(Name = "Payable Name")]
        public string PayableName { get {
            var payable = (new FundContext()).Database.SqlQuery<BillForwardView>("Exec PO_NEW.dbo.prcSupplierCS").Where(l => l.Supp_ID.Equals(PayableId.ToString())).ToList().FirstOrDefault();
            return payable.Supp_Name;
        } }
        [Required, Display(Name = "Payable Amount"), Column(TypeName = "numeric"), DisplayFormat(DataFormatString = "{0:N2}")]
        public decimal PayableAmount { get; set; }
        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual CustomType DocumentType { get; set; }

        //public virtual ICollection<BankBranch> BankBranchs { get; set; }
        //public virtual ICollection<BankAccount> BankAccounts { get; set; }
        //public virtual ICollection<SanctionMain> SanctionMains { get; set; }

        //[InverseProperty("IssuerBank")]
        //public virtual ICollection<LCMain> LCIssuerBank { get; set; }
        //[InverseProperty("NegotiationBank")]
        //public virtual ICollection<LCMain> LCNegotiationBank { get; set; }
        //[InverseProperty("SupplierBank")]
        //public virtual ICollection<LCMain> LCSupplierBank { get; set; }
    }
}