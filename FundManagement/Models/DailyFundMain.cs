﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class DailyFundMain : EntityBase, IFIMSContext, IControllerHooks
    {
        public DailyFundMain()
        {
            DailyFundSubs = new List<DailyFundSub>();
        }

        public DailyFundMain(IFIMSContext viewModel)
        {
            DailyFundSubs = new List<DailyFundSub>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("DailyFundMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int DFId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Year")]
        public int DFYear { get { return DFDate == null ? 0 : DFDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Month")]
        public int DFMonth { get { return DFDate == null ? 0 : DFDate.Month; } protected set { } }

        [NotMapped, Required, Display(Name = "Month")]
        public string DFMonthName { get { return DFDate == null ? "" : DFDate.ToString("MMMM"); } protected set { } }

        [Required, Display(Name = "Date")]
        public DateTime DFDate { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual ICollection<DailyFundSub> DailyFundSubs { get; set; }
    }
}