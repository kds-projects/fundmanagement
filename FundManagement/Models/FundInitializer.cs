﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class FundInitializer : System.Data.Entity.DropCreateDatabaseIfModelChanges<FundContext>
    {
        protected override void Seed(FundContext context)
        {
            var banks = new List<Bank>
            {
                new Bank{BankName="=N/A=",ShortName="=N/A=",BankType=1}
            };

            banks.ForEach(s => context.Banks.Add(s));
            context.SaveChanges();
        }
    }
}