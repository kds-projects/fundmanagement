﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class BankAccount : EntityBase, IControllerHooks, IFIMSContext
    {
        public BankAccount()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public BankAccount(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("BankAccount", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("BankBranch")]
        public short BranchId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Code"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string AccountCode { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public short SerialNo { get { return short.Parse((AccountCode ?? "000").Substring(2)); } private set { } }

        [NotMapped, Display(Name = "Bank")]
        public string BankName { get; set; }

        [NotMapped, Display(Name = "Branch")]
        public string BranchName { get; set; }

        [Required, Display(Name = "Account No"), DefaultValue("-"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string AccountNo { get; set; }

        [Required, Display(Name = "Type")]
        [ForeignKey("AccountType")]
        public short AccountTypeId { get; set; }

        [Required, Display(Name = "Op. Category")]
        [ForeignKey("CategoryType")]
        public short CategoryId { get; set; }
        [Required, Display(Name = "Ledger Code"), MaxLength(25), Column(TypeName = "VARCHAR")]
        //[ForeignKey("Ledger")]
        //Financial Ledger Code
        public string LedgerCode { get; set; }
        [NotMapped, Display(Name = "Ledger Name")]
        //Financial Ledger Name
        public string LedgerName { get; set; }
        [Required, Display(Name = "Company")]
        //[ForeignKey("Factory")]
        public short CompanyId { get; set; }
        [NotMapped, Display(Name = "Company")]
        public string CompanyName { get; set; }
        [NotMapped, Display(Name = "Category")]
        public string Category { get; set; }
        [Required, Display(Name = "Open Date"), Column(TypeName = "smalldatetime")]
        public DateTime OpeningDate { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("CurrencyType")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string Currency { get; set; }

        [Required, Display(Name = "Bond Account?")]
        public short IsBondAccount { get; set; }

        [NotMapped, Required, Display(Name = "Bonded?")]
        public BondType BondTypeId { get { return IsBondAccount == 1 ? BondType.Yes : BondType.No; } set { IsBondAccount = Convert.ToInt16(value == BondType.Yes ? 1 : 0); } }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required, Display(Name = "Activity"), DefaultValue(ActivityType.Active)]
        public ActivityType? Activity { get; set; }
        
        [NotMapped]
        public Models.DBFirst.Company CompanyAccount
        {
            get
            {
                var _db = new Models.DBFirst.ProductContext();
                return _db.Companies.Find(CompanyId);
            }
            set
            {

            }
        }

        //[NotMapped]
        //public short GroupId
        //{
        //    get
        //    {
        //        var _db = new Models.DBFirst.ProductContext();
        //        return _db.Companies.Find(CompanyId).GroupId;
        //    }
        //    set
        //    {

        //    }
        //}

        //[NotMapped]
        //public Models.DBFirst.GroupOfCompany GroupOfCompany
        //{
        //    get
        //    {
        //        var _db = new Models.DBFirst.ProductContext();
        //        var groupid = _db.Companies.Find(CompanyId).GroupId;
        //        return _db.GroupOfCompanies.Find(groupid);
        //    }
        //    set
        //    {

        //    }
        //}

        public virtual Bank Bank { get; set; }
        public virtual BankBranch BankBranch { get; set; }
        public virtual CustomType CurrencyType { get; set; }
        public virtual CustomType CategoryType { get; set; }
        public virtual CustomType AccountType { get; set; }
    }
}