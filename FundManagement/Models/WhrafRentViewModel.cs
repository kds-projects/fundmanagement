﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class WhrafRentViewModel
    {
        public int LcFileId { get; set; }
        public int WharfrentId { get; set; }
        public string LcNo { get; set; }
        public DateTime LcDate { get; set; }
        public string LotNo { get; set; }
        public DateTime BillDate { get; set; }
        public DateTime BreathingDate { get; set; }
        public DateTime PortLandingDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime FromDate { get; set; }
        public DateTime ToDate { get; set; }
        public decimal WhrafRentAmount { get; set; }
        public decimal DetentionCharge { get; set; }
        public string Remarks { get; set; }

        public DateTime DetentionFromDate { get; set; }
        public DateTime DetentionToDate { get; set; }

    }
}