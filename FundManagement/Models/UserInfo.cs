﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class UserInfo : EntityBase, IFIMSContext, IControllerHooks
    {
    	
        public UserInfo()
        {
            //this.User_Permission = new HashSet<User_Permission>();
        }

        public UserInfo(IFIMSContext viewModel)
        {
            //this.User_Permission = new HashSet<User_Permission>();
    
    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("UserInfo", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
    	public short UserId { get; set; }
        [Required, Display(Name = "Employee")]
        public int EmployeeId { get; set; }
        [Required, Display(Name = "User Name"), MaxLength(25), Column(TypeName = "VARCHAR")]
        [Index("IX_UserName", 1, IsUnique = true)]
    	public string UserName { get; set; }
        [Required, Display(Name = "User Name"), MaxLength(25), Column(TypeName = "VARCHAR")]
    	public string UserPWD { get; set; }
        [Required, Display(Name = "User Name"), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string UserTitle { get; set; }
        [Required, Display(Name = "Department")]
    	public short DepartmentId { get; set; }
        [Required, Display(Name = "Department"), MaxLength(125), Column(TypeName = "VARCHAR")]
        public string DepartmentName { get; set; }
        [Required, Display(Name = "Short Name"), MaxLength(25), Column(TypeName = "VARCHAR")]
        [Index("IX_ShortName", 1)]
        public string ShortName { get; set; }
        [Required, Display(Name = "eMail"), MaxLength(75), Column(TypeName = "VARCHAR")]
    	public string eMail { get; set; }
        public short LocationId { get; set; }
        [Required, Display(Name = "Login Key"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string LoginKey { get; set; }
        [Required, Display(Name = "Key Star tDate"), Column(TypeName = "smalldatetime")]
        public DateTime KeyStartDate { get; set; }
        [Required, Display(Name = "Key End Date"), Column(TypeName = "smalldatetime")]
    	public DateTime KeyEndDate { get; set; }
    	public short UserTypeId { get; set; }
    	public short UserLevelId { get; set; }
    	public short IsLogged { get; set; }
        public short IsActive { get; set; }
        public short IsAdmin { get; set; }
        [Required, Display(Name = "Factory"), MaxLength(125), Column(TypeName = "VARCHAR")]
    	public string FactoryId { get; set; }
    
        //public virtual ICollection<User_Permission> User_Permission { get; set; }
    }
}