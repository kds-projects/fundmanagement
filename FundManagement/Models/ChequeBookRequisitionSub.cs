﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeBookRequisitionSub : IFIMSContext
    {
        public ChequeBookRequisitionSub()
        {
        }

        public ChequeBookRequisitionSub(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeBookRequisitionSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [ForeignKey("ChequeBookRequisition")]
        [Display(Name = "ID")]
        public int RequisitionId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int RequisitionSubId { get; set; }

        [Display(Name = "Serial No")]
        public string SerialRange { get; set; }

        [Required, Display(Name = "Total Cheque")]
        public short TotalCheque { get; set; }

        [Required, Display(Name = "Stock Available")]
        public short StockAvailable { get; set; }

        [Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual ChequeBookRequisition ChequeBookRequisition { get; set; }
    }
}