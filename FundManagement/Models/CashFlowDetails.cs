﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CashFlowDetails : IFIMSContext
    {
        public CashFlowDetails()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public CashFlowDetails(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CashFlowDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short CFSubId { get; set; }

        [Display(Name = "Cash Flow #")]
        [ForeignKey("CashFlowMain")]
        public short CFId { get; set; }

        [Required, Display(Name = "Account")]
        [ForeignKey("AccountHead")]
        public short CaptionId { get; set; }

        [Required, Display(Name = "Budget Amount"), Column(TypeName = "numeric")]
        public Decimal BudgetAmount { get; set; }

        [Required, Display(Name = "Actual Amount"), Column(TypeName = "numeric")]
        public Decimal ActualAmount { get; set; }

        [Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual CaptionDetails AccountHead { get; set; }
        public virtual CashFlowMain CashFlowMain { get; set; }
    }
}