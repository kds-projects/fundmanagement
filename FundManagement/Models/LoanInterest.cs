﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class LoanInterest : EntityBase, IFIMSContext, IControllerHooks
    {
        public LoanInterest()
        {
            InterestDate = DateTime.Today;
        }

        public LoanInterest(IFIMSContext viewModel)
        {
            InterestDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LoanInterest", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int InterestId { get; set; }

        [Required, Display(Name = "LC #")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Acceptance #")]
        [ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Required, Display(Name = "Loan #")]
        [ForeignKey("LoanMain")]
        public int LoanId { get; set; }

        [Required, Display(Name = "Int. Date"), Column(TypeName = "smalldatetime")]
        public DateTime InterestDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return InterestDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return InterestDate.Year; } protected set { } }

        [Required, Display(Name = "Principal"), Column(TypeName = "numeric")]
        public decimal Principal { get; set; }

        [Required, Display(Name = "Interest Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Interest"), Column(TypeName = "numeric")]
        public decimal Interest { get; set; }

        [Required, Display(Name = "Principal Penalty"), Column(TypeName = "numeric")]
        public decimal PrincipalOD { get; set; }

        [Required, Display(Name = "Penalty Rate"), Column(TypeName = "numeric")]
        public decimal InterestRateOD { get; set; }

        [Required, Display(Name = "Penalty"), Column(TypeName = "numeric")]
        public decimal InterestOD { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("InterestCurrency")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "Convert?")]
        public short IsConvertedToPrincipal { get; set; }

        [Required, Display(Name = "Convert Date"), Column(TypeName = "smalldatetime")]
        public DateTime ConvertDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Status")]
        public string Status { get { return Interest > 0 && InterestOD > 0 ? "BOTH" : Interest > 0 ? "GENERAL" : "PENALTY"; } set { } }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual LCMain LCMain { get; set; }
        public virtual LCAcceptance LCAcceptance { get; set; }
        public virtual LoanMain LoanMain { get; set; }
        public virtual CustomType InterestCurrency { get; set; }
    }
}