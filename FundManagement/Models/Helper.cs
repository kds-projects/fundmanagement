﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using System.Web.WebPages;

namespace System.Web.Mvc.Html
{
    public static class LabelExtensions
    {
        //public static MvcHtmlString LabelFor2<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes)
        //{
        //    return LabelFor(html, expression, null, null, new RouteValueDictionary(htmlAttributes));
        //}

        //public static MvcHtmlString LabelFor1<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Func<object, HelperResult> startHtml)
        //{
        //    return LabelFor(html, expression, startHtml, null, new RouteValueDictionary("new {}"));
        //}

        ///// <summary>Creates a Label with custom Html before the label text.  Starting Html and a single Html attribute is provided.</summary>
        ///// <param name="startHtml">Html to preempt the label text.</param>
        ///// <param name="htmlAttributes">A single Html attribute to include.</param>
        ///// <returns>MVC Html for the Label</returns>
        //public static MvcHtmlString LabelFor1<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Func<object, HelperResult> startHtml, object htmlAttributes)
        //{
        //    return LabelFor(html, expression, startHtml, null, new RouteValueDictionary(htmlAttributes));
        //}

        ///// <summary>Creates a Label with custom Html before the label text.  Starting Html and a collection of Html attributes are provided.</summary>
        ///// <param name="startHtml">Html to preempt the label text.</param>
        ///// <param name="htmlAttributes">A collection of Html attributes to include.</param>
        ///// <returns>MVC Html for the Label</returns>
        //public static MvcHtmlString LabelFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, Func<object, HelperResult> startHtml, IDictionary<string, object> htmlAttributes)
        //{
        //    return LabelFor(html, expression, startHtml, null, htmlAttributes);
        //}

        ///// <summary>Creates a Label with custom Html before and after the label text.  Starting Html and ending Html are provided.</summary>
        ///// <param name="startHtml">Html to preempt the label text.</param>
        ///// <param name="endHtml">Html to follow the label text.</param>
        ///// <returns>MVC Html for the Label</returns>
        //public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Func<object, HelperResult> startHtml, Func<object, HelperResult> endHtml)
        //{
        //    return LabelFor(html, expression, startHtml, endHtml, new RouteValueDictionary("new {}"));
        //}

        ///// <summary>Creates a Label with custom Html before and after the label text.  Starting Html, ending Html, and a single Html attribute are provided.</summary>
        ///// <param name="startHtml">Html to preempt the label text.</param>
        ///// <param name="endHtml">Html to follow the label text.</param>
        ///// <param name="htmlAttributes">A single Html attribute to include.</param>
        ///// <returns>MVC Html for the Label</returns>
        //public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, Func<object, HelperResult> startHtml, Func<object, HelperResult> endHtml, object htmlAttributes)
        //{
        //    return LabelFor(html, expression, startHtml, endHtml, new RouteValueDictionary(htmlAttributes));
        //}

        ///// <summary>Creates a Label with custom Html before and after the label text.  Starting Html, ending Html, and a collection of Html attributes are provided.</summary>
        ///// <param name="startHtml">Html to preempt the label text.</param>
        ///// <param name="endHtml">Html to follow the label text.</param>
        ///// <param name="htmlAttributes">A collection of Html attributes to include.</param>
        ///// <returns>MVC Html for the Label</returns>
        //public static MvcHtmlString LabelFor<TModel, TProperty>(this HtmlHelper<TModel> html, Expression<Func<TModel, TProperty>> expression, Func<object, HelperResult> startHtml, Func<object, HelperResult> endHtml, IDictionary<string, object> htmlAttributes)
        //{
        //    ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, html.ViewData);
        //    string htmlFieldName = ExpressionHelper.GetExpressionText(expression);

        //    //Use the DisplayName or PropertyName for the metadata if available.  Otherwise default to the htmlFieldName provided by the user.
        //    string labelText = metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
        //    if (String.IsNullOrEmpty(labelText))
        //    {
        //        return MvcHtmlString.Empty;
        //    }
            
        //    var sb = new StringBuilder();
        //    sb.Append(labelText);
        //    if (metadata.IsRequired)
        //        sb.Append("*");

        //    //Create the new label.
        //    TagBuilder tag = new TagBuilder("label");

        //    //Add the specified Html attributes
        //    tag.MergeAttributes(htmlAttributes);

        //    //Specify what property the label is tied to.
        //    tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

        //    //Run through the various iterations of null starting or ending Html text.
        //    if (startHtml == null && endHtml == null) tag.InnerHtml = labelText;
        //    else if (startHtml != null && endHtml == null) tag.InnerHtml = string.Format("{0}{1}", startHtml(null).ToHtmlString(), labelText);
        //    else if (startHtml == null && endHtml != null) tag.InnerHtml = string.Format("{0}{1}", labelText, endHtml(null).ToHtmlString());
        //    else tag.InnerHtml = string.Format("{0}{1}{2}", startHtml(null).ToHtmlString(), labelText, endHtml(null).ToHtmlString());

        //    return MvcHtmlString.Create(tag.ToString());
        //}

        //public static MvcHtmlString Label(this HtmlHelper html, string expression, object htmlAttributes, bool generateId = false)
        //{
        //    return LabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, new RouteValueDictionary("new {}"));
        //}

        public static MvcHtmlString Label(this HtmlHelper html, string expression, object htmlAttributes, bool generateId = false)
        {
            return LabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, null, new RouteValueDictionary("new {}"), generateId);
        }

        public static MvcHtmlString Label(this HtmlHelper html, string expression, string labelText, object htmlAttributes, bool generateId = false)
        {
            return LabelHelper(html, ModelMetadata.FromStringExpression(expression, html.ViewData), expression, labelText, new RouteValueDictionary("new {}"), generateId);
        }

        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, object htmlAttributes, bool generateId = false)
        {
            return LabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), null, new RouteValueDictionary(htmlAttributes), generateId);
        }

        //[SuppressMessage("Microsoft.Design", "CA1006:DoNotNestGenericTypesInMemberSignatures", Justification = "This is an appropriate nesting of generic types")]
        public static MvcHtmlString LabelFor<TModel, TValue>(this HtmlHelper<TModel> html, Expression<Func<TModel, TValue>> expression, string labelText, object htmlAttributes, bool generateId = false)
        {
            return LabelHelper(html, ModelMetadata.FromLambdaExpression(expression, html.ViewData), ExpressionHelper.GetExpressionText(expression), labelText, new RouteValueDictionary(htmlAttributes), generateId);
        }

        internal static MvcHtmlString LabelHelper(HtmlHelper html, ModelMetadata metadata, string htmlFieldName, string labelText, IDictionary<string, object> htmlAttributes, bool generateId = false)
        {
            string LabelText = labelText ?? metadata.DisplayName ?? metadata.PropertyName ?? htmlFieldName.Split('.').Last();
            if (String.IsNullOrEmpty(LabelText))
            {
                return MvcHtmlString.Empty;
            }
            var sb = new StringBuilder();
            sb.Append(LabelText);
            if (metadata.IsRequired && generateId)
                sb.Append("<span class=\"required\">*</span>");

            var tag = new TagBuilder("label");
            //if (!string.IsNullOrWhiteSpace(id))
            //{
            //    tag.Attributes.Add("id1", id);
            //}
            //else if (generatedId)
            //{
            //    tag.Attributes.Add("id", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));
            //}

                tag.Attributes.Add("for", html.ViewContext.ViewData.TemplateInfo.GetFullHtmlFieldId(htmlFieldName));

                tag.MergeAttributes(htmlAttributes);
                tag.InnerHtml = sb.ToString();

            return MvcHtmlString.Create(tag.ToString(TagRenderMode.Normal));
        }
    }
}