﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class SupplierAccount : IFIMSContext
    {
        public SupplierAccount()
        {
            //OpeningDate = DateTime.Today;
        }

        public SupplierAccount(IFIMSContext viewModel)
        {
            //OpeningDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("SupplierAccount", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int SAId { get; set; }

        [Required, Display(Name = "Supplier #"), MaxLength(10), Column(TypeName = "VARCHAR")]
        public string SupplierId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Account Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string AccountName { get; set; }
    }
}