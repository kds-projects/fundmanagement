﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class LCSub : EntityBase, IFIMSContext, IControllerHooks
    {
        public LCSub()
        {
            LCWisePIs = new List<LCWisePI>();
        }

        public LCSub(IFIMSContext viewModel)
        {
            LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LCSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "LC")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LCSubId { get; set; }

        [Required, Display(Name = "Amendment #"), MaxLength(20), Column(TypeName = "VARCHAR")]
        public string AmendmentNo { get; set; }

        [Required, Display(Name = "Amendment Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime AmendmentDate { get; set; }

        [Required, Display(Name = "Amendment Type")]
        [ForeignKey("AmendmentType")]
        public short AmendmentTypeId { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR")]
        public string Remarks { get; set; }

        [NotMapped, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR")]
        public string _editstatus { get; set; }

        public virtual CustomType AmendmentType { get; set; }
        public virtual LCMain LCMain { get; set; }
        public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
    }
}