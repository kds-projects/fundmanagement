﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeIssueTDSVDS : IFIMSContext
    {
        public ChequeIssueTDSVDS()
        {
            //InstructionDate = clsMain.getCurrentTime();
            //ChequeDate = InstructionDate;
            //ApprovedTime = DateTime.Parse("01-Jan-1900");
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public ChequeIssueTDSVDS(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeIssuePipeline", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }

        [Key]
        [Display(Name = "ID")]
        public int InstructionTDSVDSId { get; set; }

        [Required, Display(Name = "Instruction No")]
        [ForeignKey("ChequeIssueInstruction")]
        public int InstructionId { get; set; }

        [Required, Display(Name = "Instruction Sub Id")]
        [ForeignKey("ChequeIssueSub")]
        public int InsTructionSubId { get; set; }

        [Required, Display(Name = "BillAmount"), Column(TypeName = "numeric")]
        public Decimal BillAmount { get; set; }

        [Required]
        public Decimal PaymentAmount { get; set; }


        [Required, Display(Name = "Ref No")]
        public int BillNo { get; set; }

        [Required, Display(Name = "Ref Type")]
        public string BillId { get; set; }

        [Required, Display(Name = "Supplier Id")]
        public int Supp_id { get; set; }
        
        [Required]
        public string BillType { get; set; }
        [Required]
        public string PaymentFor { get; set; }

        [Required]       
        public int ParentInstructionId { get; set; }
        public virtual ChequeIssueInstruction ChequeIssueInstruction { get; set; }
        public virtual ChequeIssueSub ChequeIssueSub { get; set; }

    }
}