﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeIssueInstruction : EntityBase, IControllerHooks, IFIMSContext
    {
        public ChequeIssueInstruction()
        {
            InstructionDate = clsMain.getCurrentTime();
            ChequeDate = InstructionDate;
            ApprovedTime = DateTime.Parse("01-Jan-1900");
            ChequeIssueSubs = new List<ChequeIssueSub>();
            //ChequePayments = new List<ChequePayment>();
            //AdvanceAdjustments = new List<AdvanceAdjustment>();
        }

        public ChequeIssueInstruction(IFIMSContext viewModel)
        {
            ChequeIssueSubs = new List<ChequeIssueSub>();
            //ChequePayments = new List<ChequePayment>();
            //AdvanceAdjustments = new List<AdvanceAdjustment>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeIssueInstruction", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int InstructionId { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Instruction #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        [Index("IX_InstructionNo", 1, IsUnique = true)]
        public string InstructionNo { get; set; }

        [Required, Display(Name = "Instruction Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        [Index("IX_InstructionDate", 1)]
        public DateTime InstructionDate { get; set; }

        [Required, Display(Name = "Instruction Type")]
        [ForeignKey("InstructionType")]
        public short InstructionTypeId { get; set; }

        [Required, Display(Name = "Payment Type")]
        public short PaymentTypeId { get; set; }

        [Required, Display(Name = "Bank Account")]
        [ForeignKey("PaymentFrom")]
        public short BankAccountId { get; set; }

        [Required, Display(Name = "Cheque Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        [Index("IX_ChequeDate", 1)]
        public DateTime ChequeDate { get; set; }

        [Required, Display(Name = "To (Infavor of)")]
        //[ForeignKey("IssuedAccount")]
        public short IssuedAccountId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "To (Infavor of)")]
        public string IssuedAccountName { get; set; }

        [Required(AllowEmptyStrings = true), DisplayFormat(ConvertEmptyStringToNull = false), Display(Name = "Voucher No")]
        //[NotMapped]
        public string VoucherNo { get; set; }

        [Required(AllowEmptyStrings = true), DisplayFormat(ConvertEmptyStringToNull = false), Display(Name = "Ref. No")]
        //[NotMapped]
        public string ReferenceNo { get; set; }

        [Required, Display(Name = "Cheque Amount"), Column(TypeName = "numeric")]
        public decimal ChequeAmount { get; set; }

        [Required, Display(Name = "Adjust Amount"), Column(TypeName = "numeric")]
        public decimal AdjustAmount { get; set; }

        [Display(Name = "Total Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public decimal TotalAmount { get { return ChequeAmount + AdjustAmount; } protected set { } }

        [Required, Display(Name = "Urgency")]
        [ForeignKey("UrgencyType")]
        public short UrgencyTypeId { get; set; }

        [Required, Display(Name = "Payment Type")]
        [ForeignKey("ChequeMode")]
        public short ChequeModeId { get; set; }

        [Required, Display(Name = "Payment Mode")]
        [ForeignKey("PaymentMode")]
        public short PaymentModeId { get; set; }

        [Required, Display(Name = "Approval Status")]
        public short ApprovedType { get; set; }

        [Required, Display(Name = "Approved By")]
        public short ApprovedBy { get; set; }

        [Required, Display(Name = "Approval Time")]
        public System.DateTime ApprovedTime { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Disapproved Reasons"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string DisapprovedReason { get; set; }

        [Required, Display(Name = "Status")]
        public short InstructionStatus { get; set; }

        [Required, Display(Name = "Advance For")]
        public short PaymentAgainstId { get; set; }

        [Required, Display(Name = "Confirm Id")]
        public int OIBillConfirmId { get; set; }

        [Required, Display(Name = "To (Infavor of)")]
        public int OIBillConfirmFavourSubId { get; set; }

        [Required, Display(Name = "Project/Aggrement")]
        public int DocumentId { get; set; }

        [Required, Display(Name = "Purpose Details"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Purpose { get; set; }

        [NotMapped]
        public BankAccount IssuedAccount { 
            get 
            { 
                var _db = new FundContext(); 
                return _db.BankAccount.Find(IssuedAccountId);  
            }
            set
            {

            }
        }

        [NotMapped]
        public ICollection<ChequePayment> ChequePayments
        {
            get
            {
                var _db = new FundContext();
                return _db.ChequePayments.Where(c => c.InstructionId == InstructionId && InstructionId > 0).ToList();
            }
            set
            {

            }
        }

        [NotMapped]
        public ICollection<AdvanceAdjustment> AdvanceAdjustments
        {
            get
            {
                var _db = new FundContext();
                return _db.AdvanceAdjustments.Where(c => c.InstructionId == InstructionId && InstructionId > 0).ToList();
            }
            set
            {

            }
        }

        public virtual CustomType UrgencyType { get; set; }
        public virtual BankAccount PaymentFrom { get; set; }
        public virtual CustomType ChequeMode { get; set; }
        public virtual CustomType PaymentMode { get; set; }
        public virtual CustomType InstructionType { get; set; }
        public virtual ICollection<ChequeIssueSub> ChequeIssueSubs { get; set; }
        //public virtual ICollection<ChequePayment> ChequePayments { get; set; }
        //public virtual ICollection<AdvanceAdjustment> AdvanceAdjustments { get; set; }
    }
}