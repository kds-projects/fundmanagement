﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class ChequeReceive : EntityBase, IFIMSContext, IControllerHooks
    {

        public ChequeReceive()
        { 
        
        }

        public ChequeReceive(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeReceive", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }

        [Key]
        public int ChequeReceiveId { get; set; }

        [ForeignKey("ChequeInfo")]
        public int ChequeId { get; set; }
        public string ChequeNo { get; set; }
        public DateTime IssueDate { get; set; }
        public DateTime ChequeDate { get; set; }
        public DateTime ReceiveDate { get; set; }
        public string IssueTo { get;set;}         
        public decimal ChequeAmount { get; set; }
        public string Purpose { get; set; }

        public string Remarks { get; set; }

        [ForeignKey("BankAccount")]
        public short AccountId { get; set; }

        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [ForeignKey("BankBranch")]
        public short BranchId { get; set; }
        public virtual ChequeInfo ChequeInfo { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual BankBranch BankBranch { get; set; }
        public virtual Bank Bank { get; set; }








    }
}