﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class PaymentMain : EntityBase
    {
        public PaymentMain()
        {
            PaymentSubs = new List<PaymentSub>();
            DocumentDate = DateTime.Today;
            PaymentDate = DateTime.Today;
        }

        public PaymentMain(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PaymentMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int PaymentId { get; set; }

        [Required, Display(Name = "File #"), MaxLength(20), Column(TypeName = "VARCHAR")]
        public string PaymentNo { get; set; }

        [Required, Display(Name = "Payment Date"), Column(TypeName = "smalldatetime")]
        [Index("IX_PaymentDate", 1)]
        public DateTime PaymentDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Payment Date")]
        public Nullable<System.DateTime> PaymentSDate { get { return PaymentDate.Date; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return PaymentDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return PaymentDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return short.Parse(PaymentNo.Substring(2, 4)); } protected set { } }

        [Required, Display(Name = "Payment Type")]
        public short PaymentTypeId { get; set; }

        [NotMapped, Required, Display(Name = "Payment Against")]
        public short PaymentAgainst { get; set; }

        [Required, Display(Name = "Document Ref."), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string DocumentNo { get; set; }

        [Required, Display(Name = "Document Date"), Column(TypeName = "smalldatetime")]
        public DateTime DocumentDate { get; set; }

        [Required, Display(Name = "Payment Amount"), Column(TypeName = "numeric")]
        public decimal PaymentAmount { get; set; }

        [Required, Display(Name = "Currency")]
        //[ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        //public virtual LoanMain LoanMain { get; set; }
        //public virtual BankBranch IssuerBranch { get; set; }
        //public virtual CustomType STenureType { get; set; }
        //public virtual CustomType BTenureType { get; set; }
        //public virtual CustomType Currency { get; set; }
        public virtual ICollection<PaymentSub> PaymentSubs { get; set; }
        //public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
        //public virtual ICollection<LCAcceptance> LCAcceptances { get; set; }
    }
}