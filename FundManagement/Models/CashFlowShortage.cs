﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CashFlowShortage : EntityBase
    {
        public CashFlowShortage()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public CashFlowShortage(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CashFlowShortage", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short CFSId { get; set; }
        [Required, Display(Name = "Year")]
        public short CFSYear { get; set; }
        [Required, Display(Name = "Month")]
        public short CFSMonth { get; set; }
        [NotMapped, Required, Display(Name = "Month")]
        public string CFSMonthName { get; set; }
        [Required, Display(Name = "Discounting (BDT)"), Column(TypeName = "numeric")]
        public Decimal DiscountingBDT { get; set; }
        [Required, Display(Name = "Discounting (USD)"), Column(TypeName = "numeric")]
        public Decimal DiscountingUSD { get; set; }
        [Required, Display(Name = "LTL"), Column(TypeName = "numeric")]
        public Decimal LTL { get; set; }
        [Required, Display(Name = "Revolving/OD"), Column(TypeName = "numeric")]
        public Decimal Revolving { get; set; }
        [Required, Display(Name = "STL & DL"), Column(TypeName = "numeric")]
        public Decimal STL_DL { get; set; }
        [Required, Display(Name = "Sister Concern"), Column(TypeName = "numeric")]
        public Decimal SisterConcern { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string SisterConcernRemarks { get; set; }
        [Required, Display(Name = "Project Loan"), Column(TypeName = "numeric")]
        public Decimal ProjectLoan { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string ProjectRemarks { get; set; }
        [Required, Display(Name = "Others"), Column(TypeName = "numeric")]
        public Decimal Others { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string OtherRemarks { get; set; }
    }
}