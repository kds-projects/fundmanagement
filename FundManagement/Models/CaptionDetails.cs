﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class CaptionDetails : EntityBase, IControllerHooks, IFIMSContext
    {
        public CaptionDetails()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();
        }

        public CaptionDetails(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CaptionDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Required, Display(Name = "ID")]
        public short CaptionId { get; set; }

        [NotMapped]
        public CaptionDetails ParentCaption
        {
            get
            {
                return (new FundContext()).CaptionDetails.Find(CaptionPId);
            }
        }

        [Required, Display(Name = "Code"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string CaptionCode { get; set; }

        [Required, Display(Name = "Name"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string CaptionName { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Short Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string ShortName { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Title"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string Title { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Flag"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string Flag { get; set; }

        [Required, Display(Name = "P ID")]
        public short CaptionPId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "P. Code"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string CaptionPCode { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
    }
}