﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class PISub : IFIMSContext
    {
        public PISub()
        {
            //LCWisePIs = new List<LCWisePI>();
        }

        public PISub(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PISub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "PI #")]
        [ForeignKey("PIMain")]
        public int PIFileId { get; set; }

        [Key]
        [Display(Name = "ID")]
        public int PIFileSubId { get; set; }

        [Required, Display(Name = "ID")]
        public int PIFileRelationId { get; set; }

        [Required, Display(Name = "ID")]
        public int RelationId { get; set; }

        [Required, Display(Name = "P. Approval #")]
        public int PApprovalSubId { get; set; }

        [Required, Display(Name = "Q. Approval #")]
        public int QApprovalSubId { get; set; }

        [Required, Display(Name = "Req. #")]
        public int PRSubId { get; set; }

        [Required, Display(Name = "PO #")]
        public int POSubId { get; set; }

        [Required, Display(Name = "Factory")]
        public short FactoryId { get; set; }

        [Required, Display(Name = "Product")]
        public short ProductId { get; set; }

        [Required, Display(Name = "Present Stock"), Column(TypeName = "numeric")]
        public decimal CurrentStock { get; set; }

        [Required, Display(Name = "Pipeline"), Column(TypeName = "numeric")]
        public decimal StockInTransit { get; set; }

        [Required, Display(Name = "Total Stock"), Column(TypeName = "numeric")]
        public decimal TotalStockArrangement { get; set; }

        [Required, Display(Name = "Per Day Reqirement"), Column(TypeName = "numeric")]
        public decimal PDRequirement { get; set; }

        [Required, Display(Name = "Stock Avaiable For Days"), Column(TypeName = "numeric")]
        public decimal TotalStockForDay { get; set; }

        [Required, Display(Name = "Quantity Unit")]
        public short QuantityUnitId { get; set; }

        [Required, Display(Name = "PI Quantity"), Column(TypeName = "numeric")]
        public decimal PIQuantity { get; set; }

        [Required, Display(Name = "Unit Price"), Column(TypeName = "numeric")]
        public decimal UnitPrice { get; set; }

        [Required, Display(Name = "Amount"), Column(TypeName = "numeric")]
        public decimal Amount { get; set; }

        [Required, Display(Name = "Currency")]
        //[ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return Amount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return Amount * BDTRate; } protected set { } }
        
        [Display(Name = "Capex?")]
        public byte IsCapex { get; set; }

        [Display(Name = "SL #")]
        public short RowNo { get; set; }

        [NotMapped, Display(Name = "PI Item?")]
        public short IsPItemId { get; set; }

        public virtual PIMain PIMain { get; set; }
    }
}