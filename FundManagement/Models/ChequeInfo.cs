﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeInfo : EntityBase, IControllerHooks, IFIMSContext
    {
        public ChequeInfo()
        {
            CancelDate = DateTime.Parse("01-Jan-1900");
            CancelEntryDate = DateTime.Parse("01-Jan-1900");
        }

        public ChequeInfo(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeInfo", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ChequeId { get; set; }

        [Display(Name = "Account #")]
        [ForeignKey("BankAccount")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("BankBranch")]
        public short BranchId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Requisition #"), DefaultValue(0)]
        //[ForeignKey("ChequeBookRequisition")]
        public short RequisitionId { get; set; }

        [Required, Display(Name = "Cheque #"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string ChequeNo { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Prefix"), MaxLength(10), Column(TypeName = "VARCHAR")]
        public string Prefix { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("CurrencyType")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "Payment Mode")]
        [ForeignKey("PaymentMode")]
        public short PaymentModeId { get; set; }

        [Required, Display(Name = "Status")]
        //1 = Unused, 2 = Used, 3 = Cancel, 4 = Return
        public short Status { get; set; }

        [Required, Display(Name = "Cancel Date")]
        public DateTime CancelDate { get; set; }

        [Required, Display(Name = "Cancel Reason"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string CancelReason { get; set; }

        [Required, Display(Name = "Cancel Entry Date")]
        public DateTime CancelEntryDate { get; set; }

        [Required, Display(Name = "Cancelled By")]
        public short CancelledBy { get; set; }

        [Required, Display(Name = "Range"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string SerialRange { get; set; }

        [Required, Display(Name = "Activity"), DefaultValue(ActivityType.Active)]
        public ActivityType? Activity { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual BankBranch BankBranch { get; set; }
        public virtual CustomType CurrencyType { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual ICollection<ChequePayment> ChequePayments { get; set; }
        public virtual CustomType PaymentMode { get; set; }
        //public virtual ChequeBookRequisition ChequeBookRequisition { get; set; }
    }
}