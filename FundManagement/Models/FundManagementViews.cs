﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using FundManagement.Controllers;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Reflection;
using FundManagement.Models.DBFirst;

namespace FundManagement.Models
{
    public static class clsConverter
    {
        static clsConverter()
        {

        }

        public static object Convert(this Object obj, object viewModel)
        {
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                PropertyInfo pii = viewModel.GetType().GetProperty(pi.Name);

                if (pii != null && (pi.PropertyType.IsAssignableFrom(pii.PropertyType) || pii.PropertyType.IsAssignableFrom(pi.PropertyType)))
                {
                    if (pii.CanWrite)
                    {
                        pii.SetValue(viewModel, pi.GetValue(obj));
                    }
                }
            }

            return viewModel;
        }

        public static object ConvertNotNull(this Object obj, object viewModel)
        {
            foreach (PropertyInfo pi in obj.GetType().GetProperties())
            {
                PropertyInfo pii = viewModel.GetType().GetProperty(pi.Name);

                if (pii != null && pi.GetValue(obj) != null && pi.PropertyType.IsAssignableFrom(pii.PropertyType))
                {
                    if (pii.CanWrite)
                    {
                        pii.SetValue(viewModel, pi.GetValue(obj));
                    }
                }
            }

            return viewModel;
        }
        //public static object UserAccess(this Object obj, object newobj)
        //{
        //    var facid = clsMain.getEntity().Sys_User_Name.Find(clsMain.getCurrentUser()).FactoryId.Split(new string[] { "," }, StringSplitOptions.None).Select(c => int.Parse(c)).ToList();
        //    var factory = ((DbSet)obj).AsQueryable

        //    return viewModel;
        //}
    }

    public partial class clsDefaultValue
    {
        public string Table { get; set; }
        public string Column { get; set; }
        public string DefaultValue { get; set; }
    }

    public partial class clsMonthList
    {
        public short MonthId { get; set; }
        public string MonthName { get; set; }
        public string ShortName { get; set; }
    }

    public partial class ReportView
    {
        public ReportView()
        {
            AsOnDate = DateTime.Today;
            FromDate = DateTime.Today.AddDays(1 - DateTime.Today.Day);
            EndDate = DateTime.Today;
        }

        [Display(Name = "Bank")]
        public short BankId { get; set; }
        [Display(Name = "Branch")]
        public short BranchId { get; set; }
        [Display(Name = "Company")]
        public short CompanyId { get; set; }
        [Display(Name = "Factory")]
        public short FactoryId { get; set; }
        [Required, Display(Name = "LC #")]
        public int LCFileId { get; set; }
        [Required, Display(Name = "Loan #")]
        public int LoanId { get; set; }
        [Required, Display(Name = "Laon Category")]
        public string LoanCategory { get; set; }
        [Required, Display(Name = "Payment Against")]
        public string PaymentAgainst { get; set; }
        [Required, Display(Name = "Adjust Against")]
        public string AdjustAgainst { get; set; }
        [Required, Display(Name = "Loan Type")]
        public int LoanTypeId { get; set; }
        [Required, Display(Name = "Account Type")]
        public int AccountTypeId { get; set; }
        [Display(Name = "From")]
        public DateTime FromDate { get; set; }
        [Display(Name = "To")]
        public DateTime EndDate { get; set; }
        [Display(Name = "As On")]
        public DateTime AsOnDate { get; set; }
        [Display(Name = "Summary")]
        public bool IsSummary { get; set; }

        public int BondId { get; set; }
    }

    public partial class BankAccountView : EntityBase, IFIMSContext, IControllerHooks
    {
        [Display(Name = "ID")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("BankBranch")]
        public short? BranchId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short? BankId { get; set; }

        [Required, Display(Name = "Code"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string AccountCode { get; set; }

        [Required, Display(Name = "Account No")]
        public string AccountNo { get; set; }

        [Required, Display(Name = "Type")]
        [ForeignKey("AccountType")]
        public short? AccountTypeId { get; set; }

        [Required, Display(Name = "Op. Category")]
        public short? CategoryId { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Ledger Code")]
        public string LedgerCode { get; set; }

        [Display(Name = "Ledger Name")]
        //Financial Ledger Name
        public string LedgerName { get; set; }

        [Required, Display(Name = "Company")]
        public short? CompanyId { get; set; }
        [NotMapped, Display(Name = "Company")]
        public string CompanyName { get; set; }
        [NotMapped, Display(Name = "Category")]
        public string Category { get; set; }
        [Required, Display(Name = "Open Date")]
        public DateTime? OpeningDate { get; set; }

        [Required, Display(Name = "Currency")]
        public short? CurrencyId { get; set; }

        [Display(Name = "Currency")]
        public string Currency { get; set; }

        [Required, Display(Name = "Bond Account?")]
        public short? IsBondAccount { get; set; }

        [Required, Display(Name = "Bonded?")]
        public BondType? BondTypeId { get { return IsBondAccount == 1 ? BondType.Yes : BondType.No; } set { IsBondAccount = Convert.ToInt16(value == BondType.Yes ? 1 : 0); } }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
    }

    public partial class BudgetEntryView: ContextList
    {
        public BudgetEntryView(string type = "amount", short year = 2020, short qunit = 13)
        {
            MonthList = dbProd.CustomTypes.Where(l => l.Flag.Equals("MONTHNAME")).Select(c => new clsMonthList() { MonthId = c.RankId, MonthName = c.TypeName, ShortName = c.ShortName }).ToList();
            FactoryList = dbProd.Factories.ToList();
            ImportBudgets = db.ImportBudgets.Where(c => c.BudgetYear == year).ToList();
            BudgetType = type;
            BudgetYear = year;
            QuantityUnitId = qunit;
        }

        public string BudgetType { get; set; }
        public short BudgetYear { get; set; }
        public short QuantityUnitId { get; set; }
        public List<clsMonthList> MonthList { get; set; }
        public List<Factory> FactoryList { get; set; }
        public List<ImportBudget> ImportBudgets { get; set; }
    }

    public partial class PIWiseItemDetails: ContextList
    {
        public PIWiseItemDetails()
        {
            
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int Id { get; set; }
        public short SLNo { get; set; }
        public string PRNo { get; set; }
        public string ItemName { get; set; }
        public string HSCode { get; set; }
        public DateTime FactoryETA { get; set; }
        public Decimal Quantity { get; set; }
        public string QuantityUnit { get; set; }
        public Decimal UnitPrice { get; set; }
        public Decimal Amount { get { return Quantity * UnitPrice; } protected set { } }
        public short CurrencyId { get; set; }
        public string Currency { get; set; }
    }

    public partial class LCMainView : IFIMSContext
    {
        public int LCFileId { get; set; }

        [Required, Display(Name = "File #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string LCFileNo { get; set; }

        //[DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        //[Display(Name = "Payment Date")]
        //public Nullable<System.DateTime> PaymentSDate { get; set; }

        //[Required, Display(Name = "Payment Date"), Column(TypeName = "smalldatetime")]
        //public System.DateTime PaymentDate { get; set; }

        [Required, Display(Name = "LC No"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string LCNo { get; set; }

        [Required, Display(Name = "LC Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime? LCDate { get; set; }

        [Required, Display(Name = "Applicant")]
        public short? BeneficiaryId { get; set; }

        [Display(Name = "Applicant")]
        public string Applicant { get; set; }

        [Required, Display(Name = "Supplier")]
        public short? SupplierId { get; set; }

        [Display(Name = "Supplier")]
        public string Supplier { get; set; }

        [Required, Display(Name = "Agent")]
        public short? LocalAgentId { get; set; }

        [Display(Name = "Local Agent")]
        public string LocalAgent { get; set; }

        [Required, Display(Name = "Country of Origin")]
        public short? OriginCountryId { get; set; }

        [Display(Name = "Origin")]
        public string Origin { get; set; }

        [Required, Display(Name = "Port of Loading")]
        public short? POLId { get; set; }

        [Display(Name = "Port of Loading")]
        public string PortName { get; set; }

        [Required, Display(Name = "Issuer Bank")]
        [ForeignKey("IssuerBank")]
        public short? IssuerBankId { get; set; }

        [Display(Name = "Issuer Bank")]
        public string IssuerBank { get; set; }

        [Required, Display(Name = "Issuer Branch")]
        [ForeignKey("IssuerBranch")]
        public short? IssuerBranchId { get; set; }

        [Display(Name = "Issuer Branch")]
        public string IssuerBranch { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Negotiating Bank")]
        public string NegotiatingBank { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Supplier Bank")]
        public string BeneficiaryBank { get; set; }

        [Required, Display(Name = "LC Type"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string LCType { get; set; }

        [Required, Display(Name = "Shipment Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime? ShipmentDate { get; set; }

        [Required, Display(Name = "Expire Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime? ExpireDate { get; set; }

        [Required, Display(Name = "Supplier Tenure")]
        public short? SupplierTenure { get; set; }

        [Required, Display(Name = "Sup Ten Type")]
        [ForeignKey("STenureType")]
        public short? STenureTypeId { get; set; }

        [Required, Display(Name = "Bank Tenure")]
        public short? BankTenure { get; set; }

        [Required, Display(Name = "Bank Ten Type")]
        [ForeignKey("BTenureType")]
        public short? BTenureTypeId { get; set; }

        [Required, Display(Name = "Tenure Start Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime? TenureStartDate { get; set; }

        [Required, Display(Name = "Ins. CN No"), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string CoverNoteNo { get; set; }

        [Required, Display(Name = "Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime? CoverNoteDate { get; set; }

        [Required, Display(Name = "Ins. Value")]
        public decimal? InsuranceValue { get; set; }

        [Required, Display(Name = "Ins. Premium")]
        public decimal? InsurancePremium { get; set; }

        [Required, Display(Name = "LC Amount"), Column(TypeName = "numeric")]
        public decimal? LCAmount { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("Currency")]
        public short? CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal? BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal? USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return LCAmount * USDRate ?? 0; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return LCAmount * BDTRate ?? 0; } protected set { } }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [Required, Display(Name = "PI List"), NotMapped]
        public string[] PIList { get; set; }

        //public ICollection<LCSub> LCSubs { get; set; }
        //public ICollection<LCWisePI> LCWisePIs { get; set; }
    }

    public partial class LCAcceptanceView : IFIMSContext
    {
        [Required, Display(Name = "LC #")]
        public int LCFileId { get; set; }

        [Display(Name = "ID")]
        public int LCAcceptanceId { get; set; }

        [Required, Display(Name = "Lot #")]
        public int PLEId { get; set; }

        [Required, Display(Name = "Bank Recv. Date"), Column(TypeName = "smalldatetime")]
        public DateTime? BankReceiveDate { get; set; }

        [Required, Display(Name = "Doc Ret Date"), Column(TypeName = "smalldatetime")]
        public DateTime? DocumentRetirementDate { get; set; }

        [Required, Display(Name = "Handover To C&F"), Column(TypeName = "smalldatetime")]
        public DateTime? HandoverToCNFDate { get; set; }

        [Required, Display(Name = "Handover To Sourcing"), Column(TypeName = "smalldatetime")]
        public DateTime? HandoverToSourcingDate { get; set; }

        [Display(Name = "BL #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string BLNo { get; set; }

        [Display(Name = "BL Date"), Column(TypeName = "smalldatetime")]
        public DateTime? BLDate { get; set; }

        [Display(Name = "B. Entry #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string BENo { get; set; }

        [Display(Name = "B. Entry Date"), Column(TypeName = "smalldatetime")]
        public DateTime? BEDate { get; set; }

        [Required, Display(Name = "Acceptance Date"), Column(TypeName = "smalldatetime")]
        public DateTime? AcceptanceDate { get; set; }

        [Required, Display(Name = "Tenure Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime? TenureStartDate { get; set; }

        [Required, Display(Name = "Acceptance Tenure")]
        public short AcceptanceTenure { get; set; }

        [Required, Display(Name = "Loan Tenure")]
        public short LoanTenure { get; set; }

        [Required, Display(Name = "Maturity Date"), Column(TypeName = "smalldatetime")]
        public DateTime? ExpireDate { get; set; }

        [Required, Display(Name = "Acceptance #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string AcceptanceNo { get; set; }

        [Required, Display(Name = "Accepted Amount"), Column(TypeName = "numeric")]
        public decimal? AcceptanceAmount { get; set; }

        [Required, Display(Name = "Currency")]
        public short? CurrencyId { get; set; }

        [Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "Libor Amount"), Column(TypeName = "numeric")]
        public decimal? LiborAmount { get; set; }

        [Required, Display(Name = "Libor IR"), Column(TypeName = "numeric")]
        public decimal? LiborInterestRate { get; set; }

        [Required, Display(Name = "Libor Penalty IR"), Column(TypeName = "numeric")]
        public decimal? LiborInterestRateOD { get; set; }

        [Required, Display(Name = "Libor IR Start"), Column(TypeName = "smalldatetime")]
        public DateTime? LiborInterestStartDate { get; set; }
        
        [Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal? LiborBDTRate { get; set; }

        [Display(Name = "Libor (BDT)"), Column(TypeName = "numeric")]
        public decimal? LiborBDT { get; set; }

        [Required, Display(Name = "Handling Charge"), Column(TypeName = "numeric")]
        public decimal? HandlingCharge { get; set; }

        [Required, Display(Name = "HC IR"), Column(TypeName = "numeric")]
        public decimal? HCInterestRate { get; set; }

        [Required, Display(Name = "HC Penalty IR"), Column(TypeName = "numeric")]
        public decimal? HCInterestRateOD { get; set; }

        [Required, Display(Name = "HC IR Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime? HCInterestStartDate { get; set; }

        [Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal? HCBDTRate { get; set; }

        [Display(Name = "HC (BDT)"), Column(TypeName = "numeric")]
        public decimal? HCBDT { get; set; }


        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal? BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal? USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return AcceptanceAmount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return AcceptanceAmount * BDTRate; } protected set { } }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
    }

    public partial class BudgetView : IFIMSContext
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public Nullable<short> BudgetId { get; set; }
        [Required, Display(Name = "Year")]
        public Nullable<short> BudgetYear { get; set; }
        [Required, Display(Name = "Month")]
        public Nullable<short> BudgetMonth { get; set; }
        [Required, Display(Name = "Factory")]
        public Nullable<short> FactoryId { get; set; }
        [Required, Display(Name = "Type")]
        public Nullable<short> TypeId { get; set; }
        [Required, Display(Name = "Budget Amount"), Column(TypeName = "numeric")]
        public Nullable<Decimal> BudgetAmount { get; set; }
        [Required, Display(Name = "Budget Quantity"), Column(TypeName = "numeric")]
        public Nullable<Decimal> BudgetQuntity { get; set; }
        [Required, Display(Name = "Quantity Unit")]
        public Nullable<short> QuantityUnitId { get; set; }
    }

    public partial class CustomTypeView: IFIMSContext
    {
        public short TypeId { get; set; }
        [Required, Display(Name = "Name")]
        public string TypeName { get; set; }
        [Required, Display(Name = "Short Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string ShortName { get; set; }
        [Required, Display(Name = "Flag")]
        public string Flag { get; set; }
        [Display(Name = "Group"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string GroupName { get; set; }
        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
        [Required, Display(Name = "SL #")]
        public short RankId { get; set; }
    }

    public enum UserStatus { Success = 1, LockedOut = 2, Failure = 0 };
    public partial class UserView
    {
        public UserView()
        {
        }

        public UserView(UserInfo user)
        {
            UserId = user.UserId;
            UserName = user.UserName;
            UserTitle = user.UserTitle;
            eMail = user.eMail;
            UserPWD = user.UserPWD;
            IsAdmin = user.UserTypeId;
        }

        public short UserId { get; set; }
        public string UserName { get; set; }
        public string UserTitle { get; set; }
        public string eMail { get; set; }
        public string UserPWD { get; set; }
        public string ConfirmPassword { get; set; }
        public string OldPassword { get; set; }
        public bool IsRemember { get; set; }
        public bool IsLock { get; set; }
        public short IsAdmin { get; set; }
    }

    public partial class ReconciliationView : IFIMSContext
    {
        public short CompanyId { get; set; }
        public string CompanyName { get; set; }
        public short BankId { get; set; }
        public string BankName { get; set; }
        public short BranchId { get; set; }
        public string BranchName { get; set; }
        public short LoanTypeId { get; set; }
        public string LoanType { get; set; }
        public decimal SoftwareBalance { get; set; }
    }

    public partial class PipelineView : IFIMSContext
    {
        public PipelineView()
        {
        }

        [Display(Name = "ID")]
        public int? PLEId { get; set; }
        [Display(Name = "Actual Recv. Date"), Column(TypeName = "smalldatetime")]
        public DateTime ActualReceiveDate { get; set; }
        [Display(Name = "MRR No")]
        public string MRRNo { get; set; }
        [Display(Name = "ID")]
        public int? PLESubId { get; set; }
        [Display(Name = "LC #"), Required]
        public int LCFileSubId { get; set; }
        [Display(Name = "PI #"), Required]
        public int PIFileSubId { get; set; }
        [Display(Name = "Category")]
        public short CategoryId { get; set; }
        [Display(Name = "Category")]
        public string CategoryName { get; set; }
        [Display(Name = "Item Name"), Required]
        public short ItemId { get; set; }
        [Display(Name = "Item Name")]
        public string ItemName { get; set; }
        [Display(Name = "Factory ETA")]
        public DateTime FactoryETA { get; set; }
        [Display(Name = "Recv. Quantity")]
        public decimal ActualReceivedQuantity { get; set; }
        [Display(Name = "Pipeline Quantity"), Required]
        public decimal StockInTransit { get; set; }
        [Display(Name = "Quantity Unit")]
        public short QuantityUnitId { get; set; }
        [Display(Name = "Quantity Unit")]
        public string QuantityUnit { get; set; }
        [Display(Name = "Unit Price")]
        public decimal UnitPrice { get; set; }
        [Display(Name = "Pipeline Value")]
        public decimal PipelineValue { get; set; }
        public short? RowNo { get; set; }
    }

    public partial class OIConfirmView : IFIMSContext
    {
        public int InstructionId { get; set; }
        public int oiId { get; set; }
        public string OIConfirmNo { get; set; }
        public int OIBILLFevConfirmSubId { get; set; }
        public int OIBILLFevSubId { get; set; }
        public string ChequeConfirmFavour { get; set; }
        public decimal ChequeConfirmAmount { get; set; }
        public string CustomerName { get; set; }
        public string RepresentativeName { get; set; }
    }

    public partial class BillForwardView : IFIMSContext
    {
        public int InstructionId { get; set; }
        public int WorkOrderId { get; set; }
        public int CPBPlanNo { get; set; }
        public int CPB_Det_ID { get; set; }
        public string CPB_Plan_ID { get; set; }
        public string BillNo { get; set; }
        public string SupplierId { get; set; }
        public string Supp_ID { get; set; }
        public short CreditPeriod { get; set; }
        public string Payment_Favour { get; set; }
        public string Supp_Name { get; set; }
        public DateTime ChequeDate { get; set; }
        public DateTime DeliveryDate { get; set; }
        public DateTime DueDate { get; set; }
        public Decimal CPB_AdjustAmount { get; set; }
    }

    public partial class StockPosition : IFIMSContext
    {
        public int RequisitionSubId { get; set; }
        public string SerialRange { get; set; }
        public int TotalStock { get; set; }
        public int StockAvailable { get; set; }
    }

    public partial class ChequeBookInfo : IFIMSContext
    {
        public int RequisitionId { get; set; }
        public string Prefix { get; set; }
        public string SerialRange { get; set; }
    }
}

namespace FundManagement.Models.DBFirst
{
    public partial class EMailCollection : IFIMSContext
    {
        public EMailCollection()
        {
            EntryDate = clsMain.getCurrentTime();
            LastProcessingDate = DateTime.Parse("01-Jan-1900");
            ProcessAt = EntryDate;
            SendingDate = LastProcessingDate;
        }

        public EMailCollection(IFIMSContext viewModel)
        {
            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValueEMC("EMailCollection", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }
    }
}
