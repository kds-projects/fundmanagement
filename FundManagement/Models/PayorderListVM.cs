﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{

          /*    Reference = gr.Key.ReferenceNo,
                                   PayorderDate = gr.Key.PayorderDate.ToString("dd-MM-yyyy"),
                                   BankAccount = db.BankAccount.Select(x => x.AccountId == gr.Key.BankAccountId).FirstOrDefault(),
                                   Amount = gr.Sum(x=>x.b.PayorderAmount)*/
    public class PayorderListVM
    {

        public string Reference { get; set; }
        public string PayorderDate { get; set; }
        public string BankAccount { get; set; }

        public decimal Amount { get; set; }

    }
}