﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class LandedCosting : EntityBase, IFIMSContext, IControllerHooks
    {
        public LandedCosting()
        {
            
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public LandedCosting(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LandedCosting", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LandedCostingId { get; set; }

        [Required, Display(Name = "LC #")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Required, Display(Name = "Acceptance #")]
        [ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Required, Display(Name = "Lot #")]
        public int PLEId { get; set; }

        [Required, Display(Name = "Opening Amount"), Column(TypeName = "numeric")]
        public decimal LCOpeningCommission { get; set; }

        [Required, Display(Name = "VAT on Opening Amount"), Column(TypeName = "numeric")]
        public decimal LCOpeningCommissionVAT { get; set; }

        [Required, Display(Name = "Swift Charges (Op Comm)"), Column(TypeName = "numeric")]
        public decimal SwiftCharges { get; set; }

        [Required, Display(Name = "Stamp & Stationery"), Column(TypeName = "numeric")]
        public decimal StampAndStationery { get; set; }

        [Required, Display(Name = "Postage & Courier"), Column(TypeName = "numeric")]
        public decimal PostageAndCourier { get; set; }

        [Required, Display(Name = "Amendment Charges"), Column(TypeName = "numeric")]
        public decimal AmendmentCharges { get; set; }

        [Required, Display(Name = "Credit Report Charges"), Column(TypeName = "numeric")]
        public decimal CreditReportCharges { get; set; }

        [Required, Display(Name = "Shipping Guarantee Commission"), Column(TypeName = "numeric")]
        public decimal ShippingGuaranteeCommission { get; set; }

        [Required, Display(Name = "Online Charge"), Column(TypeName = "numeric")]
        public decimal OnlineCharge { get; set; }

        [Required, Display(Name = "Donation Fees"), Column(TypeName = "numeric")]
        public decimal DonationFees { get; set; }

        [Required, Display(Name = "VAT"), Column(TypeName = "numeric")]
        public decimal VAT { get; set; }

        [Required, Display(Name = "CNF AIT"), Column(TypeName = "numeric")]
        public decimal CNFAIT { get; set; }

        [Required, Display(Name = "Scanning Charge"), Column(TypeName = "numeric")]
        public decimal ScanningCharge { get; set; }

        [Required, Display(Name = "Port Charges"), Column(TypeName = "numeric")]
        public decimal PortCharges { get; set; }

        [Required, Display(Name = "Wharf Rent"), Column(TypeName = "numeric")]
        public decimal Wharfrent { get; set; }

        [Required, Display(Name = "Labour Charge"), Column(TypeName = "numeric")]
        public decimal LabourCharge { get; set; }

        [Required, Display(Name = "ExcessGoods"), Column(TypeName = "numeric")]
        public decimal ExcessGoods { get; set; }

        [Required, Display(Name = "Custom & Jetty Expenses"), Column(TypeName = "numeric")]
        public decimal CustomJettyExpenses { get; set; }

        [Required, Display(Name = "Service Charge"), Column(TypeName = "numeric")]
        public decimal ServiceCharge { get; set; }

        [Required, Display(Name = "Truck Fare"), Column(TypeName = "numeric")]
        public decimal TruckFare { get; set; }

        [Required, Display(Name = "Shipping Agent Charges"), Column(TypeName = "numeric")]
        public decimal ShippingAgentCharges { get; set; }

        [Required, Display(Name = "Berth Operator"), Column(TypeName = "numeric")]
        public decimal BerthOperatorCharge { get; set; }

        [Required, Display(Name = "RCC Charge"), Column(TypeName = "numeric")]
        public decimal RCCCharge { get; set; }

        [Required, Display(Name = "Hystar Charge"), Column(TypeName = "numeric")]
        public decimal HystarCharge { get; set; }

        [Required, Display(Name = "Miscellaneous Expenses"), Column(TypeName = "numeric")]
        public decimal MiscellaneousExpenses { get; set; }

        [Required, Display(Name = "Depo Charges"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal Depocharges { get; set; }

        [Required, Display(Name = "Container Repair Charges"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal ContainerRepairCharges { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "C & F Cost")]
        public Decimal CNFCost
        {
            get
            {
                return (OnlineCharge + DonationFees + VAT + CNFAIT + ScanningCharge + PortCharges + Wharfrent +
                    LabourCharge + ExcessGoods + CustomJettyExpenses + ServiceCharge + TruckFare +
                    ShippingAgentCharges + BerthOperatorCharge + RCCCharge + HystarCharge + MiscellaneousExpenses+ Depocharges+ContainerRepairCharges);
            }
            protected set { }
        }

        [Required, Display(Name = "Insurance Premium"), Column(TypeName = "numeric")]
        public decimal InsurancePremium { get; set; }

        [Required, Display(Name = "Acceptance Commission"), Column(TypeName = "numeric")]
        public decimal AcceptanceCommission { get; set; }

        [Required, Display(Name = "Swift Charges (Accept Comm)"), Column(TypeName = "numeric")]
        public decimal SwiftChargesAcceptanceCommission { get; set; }

        [Required, Display(Name = "Duty"), Column(TypeName = "numeric")]
        public decimal Duty { get; set; }

        [Required, Display(Name = "AIT"), Column(TypeName = "numeric")]
        public decimal AIT { get; set; }

        [Required, Display(Name = "LIBOR"), Column(TypeName = "numeric")]
        public decimal LIBOR { get; set; }

        [Required, Display(Name = "Handling Charge"), Column(TypeName = "numeric")]
        public decimal HandlingCharge { get; set; }

        [Required, Display(Name = "Cost Value"), Column(TypeName = "numeric")]
        public decimal CostValue { get; set; }

        [Required, Display(Name = "Freight Charges"), Column(TypeName = "numeric")]
        public decimal FreightCharges { get; set; }

        [Required, Display(Name = "Margin"), Column(TypeName = "numeric")]
        public decimal Margin { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [DefaultValue(0)]
        [Required, Display(Name = "Custom Duty(CD)"), Column(TypeName = "numeric")]
        public decimal CusTomDuty { get; set; }

        [Required, Display(Name = "Regular Duty(RD)"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal RegularDuty { get; set; }

        [Required, Display(Name = "Supplementary Duty(SD)"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal SupplementaryDuty { get; set; }

        [Required, Display(Name = "Value Added Tax(VAT)"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal ValueAddedTax { get; set; }

        [Required, Display(Name = "Advance Tax(AT)"), Column(TypeName = "numeric")]
        [DefaultValue(0)]
        public decimal AdvanceTax { get; set; }

        public virtual LCMain LCMain { get; set; }
        public virtual LCAcceptance LCAcceptance { get; set; }
        public virtual CustomType Currency { get; set; }
    }
}