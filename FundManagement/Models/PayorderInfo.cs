﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class PayorderInfo:EntityBase,IFIMSContext,IControllerHooks
    {

        public PayorderInfo()
        {
            payorderDetails = new List<PayorderDetails>();
        }

        public PayorderInfo(IFIMSContext viewModel)
        {
           
            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PayorderInfo", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }

        [Key]
        public int PayorderId { get; set; }
        [Required]
        [Display(Name ="Reference No :")]
        public string ReferenceNo { get; set; }

        [ForeignKey("BankAccount")]
        [Required]
        [Display(Name ="Account No :")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Payorder Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime PayorderDate { get; set; }

        [Required, Display(Name = "Signatories"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string Signatories { get; set; }

        public virtual BankAccount BankAccount { get; set; }
        public virtual List<PayorderDetails> payorderDetails { get; set; }

    }
}