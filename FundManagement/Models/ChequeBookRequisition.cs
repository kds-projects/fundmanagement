﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeBookRequisition : EntityBase, IControllerHooks, IFIMSContext
    {
        public ChequeBookRequisition()
        {
            ApprovedTime = DateTime.Parse("01-Jan-1900");
            ChequeBookRequisitionSubs = new List<ChequeBookRequisitionSub>();
        }

        public ChequeBookRequisition(IFIMSContext viewModel)
        {
            ChequeBookRequisitionSubs = new List<ChequeBookRequisitionSub>();
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeBookRequisition", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int RequisitionId { get; set; }

        [Display(Name = "Account #")]
        [ForeignKey("BankAccount")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("BankBranch")]
        public short BranchId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Requisition #"), MaxLength(25), Column(TypeName = "VARCHAR")]
        public string RequisitionNo { get; set; }

        [Required, Display(Name = "Approved Date")]
        public DateTime RequisitionDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return RequisitionDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return RequisitionDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return RequisitionNo == null ? 0 : short.Parse(RequisitionNo.Substring(3, 4)); } protected set { } }

        [Required, Display(Name = "Payment Mode")]
        [ForeignKey("PaymentMode")]
        public short PaymentModeId { get; set; }

        [Required, Display(Name = "No of Book")]
        public short BookCount { get; set; }

        [Required, Display(Name = "Total Count")]
        public short TotalCheque { get; set; }

        [Required, Display(Name = "Approval Type")]
        public short ApprovalType { get; set; }

        [Required, Display(Name = "Approved By")]
        public short ApprovedBy { get; set; }

        [Required, Display(Name = "Approved Date")]
        public DateTime ApprovedTime { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Disapproved Reasons"), DisplayFormat(ConvertEmptyStringToNull = false), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string DisapprovedReason { get; set; }

        [Required, Display(Name = "Status")]
        //1 = Unused, 2 = Used, 3 = Cancel, 4 = Return
        public short Status { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual BankBranch BankBranch { get; set; }
        public virtual BankAccount BankAccount { get; set; }
        public virtual CustomType PaymentMode { get; set; }
        public virtual ICollection<ChequeBookRequisitionSub> ChequeBookRequisitionSubs { get; set; }
    }
}