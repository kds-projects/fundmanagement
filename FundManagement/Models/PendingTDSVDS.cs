﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class PendingTDSVDS
    {
        public string InstructionNo { get; set; }
        public int InstructionId { get; set; }
        public decimal BillAmount { get; set; }
        public int CPBPlanNo { get; set; }
        public string CPB_Plan_ID { get; set; }
        public int Supp_ID { get; set; }
        public string Supp_Name { get; set; }
        public decimal TaxOrVat { get; set; }

        public string Type { get; set; }
        public int InstructionSubId { get; set; }

        public string ChequeNo { get; set; }
        public DateTime ChequeDate { get; set; }







    }
}