﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public partial class LCWisePI
    {
        [Display(Name = "LC")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Amendment")]
        [ForeignKey("LCSub")]
        public int LCSubId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LCPIId { get; set; }

        [Required, Display(Name = "PI No")]
        public int PIFileId { get; set; }

        public virtual LCSub LCSub { get; set; }
        public virtual LCMain LCMain { get; set; }
    }
}