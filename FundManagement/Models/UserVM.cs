﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class UserVM
    {
        public string User_ID { get; set; }
        public string User_Name { get; set; }
    }
}