﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class LoanMain : EntityBase
    {
        public LoanMain()
        {
            ExpireDate = DateTime.Today;
            InterestEndDate = DateTime.Today;
            SettlementDate = DateTime.Parse("01-Jan-1900");
            InterestStartDate = DateTime.Parse("01-Jan-1900");
            InstallmentStartDate = DateTime.Parse("01-Jan-1900");
            TenureStartDate = DateTime.Parse("01-Jan-1900");
            InterestEndDate = DateTime.Parse("01-Jan-1900");
            SettlementEntryDate = DateTime.Parse("01-Jan-1900");
            TransferDate = DateTime.Parse("01-Jan-1900");
            TransferEntryDate = DateTime.Parse("01-Jan-1900");

            PaymentSchedules = new List<PaymentSchedule>();
            InterestRateDetails = new List<InterestRateDetails>();
        }

        public LoanMain(IFIMSContext viewModel)
        {
            PaymentSchedules = new List<PaymentSchedule>();
            InterestRateDetails = new List<InterestRateDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("LoanMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Required, Display(Name = "LC #")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Acceptance #")]
        [ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int LoanId { get; set; }

        [Required, Display(Name = "Loan No"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string LoanNo { get; set; }

        [Required, Display(Name = "Loan Date"), Column(TypeName = "smalldatetime")]
        public DateTime LoanDate { get; set; }

        [Required, Display(Name = "Applicant")]
        public short BeneficiaryId { get; set; }

        [Required, Display(Name = "Issuer Bank")]
        //[ForeignKey("IssuerBank")]
        public short IssuerBankId { get; set; }

        [Required, Display(Name = "Issuer Branch")]
        //[ForeignKey("IssuerBranch")]
        public short IssuerBranchId { get; set; }

        [Required, Display(Name = "Loan Type")]
        public short LoanTypeId { get; set; }

        [Required, Display(Name = "Pay Sch. Type")]
        public short ScheduleTypeId { get; set; }

        [Required, Display(Name = "Interest Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Penal Int Rate"), Column(TypeName = "numeric")]
        public decimal InterestRateOD { get; set; }

        [Required, Display(Name = "Interest Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime InterestStartDate { get; set; }

        [Required, Display(Name = "Interest End Date"), Column(TypeName = "smalldatetime")]
        public DateTime InterestEndDate { get; set; }

        [Required, Display(Name = "Interest Rest")]
        public short InterestProcessType { get; set; }

        [Required, Display(Name = "Accepted Amount"), Column(TypeName = "numeric")]
        public decimal AcceptedAmount { get; set; }

        [Required, Display(Name = "Libor Amount"), Column(TypeName = "numeric")]
        public decimal LiborAmount { get; set; }

        [Required, Display(Name = "Handling Charge"), Column(TypeName = "numeric")]
        public decimal HandlingCharge { get; set; }

        [Display(Name = "Loan Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> LoanAmount { get { return AcceptedAmount + LiborAmount + HandlingCharge; } protected set { } }

        [Required, Display(Name = "Currency")]
        [ForeignKey("FCurrency")]
        public short FCurrencyId { get; set; }

        [Required, Display(Name = "Accepted Amount (BDT)"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AcceptedAmountBDT { get { return LoanAmountBDT - ((LiborAmount + HandlingCharge) * BDTRate); } protected set { } }

        [Required, Display(Name = "Libor Amount (BDT)"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> LiborAmountBDT { get { return LiborAmount * BDTRate; } protected set { } }

        [Required, Display(Name = "Handling Charge (BDT)"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> HandlingChargeBDT { get { return HandlingCharge * BDTRate; } protected set { } }

        [Display(Name = "Loan Amount (BDT)"), Column(TypeName = "numeric")]
        public decimal LoanAmountBDT { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; } //{ return Math.Round(LoanAmountBDT / (AcceptedAmount + LiborAmount + HandlingCharge), 2); }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> LoanAmountUSD { get { return Math.Round((LoanAmount == 0 ? LoanAmountBDT : LoanAmount.Value) * USDRate, 2); } protected set { } }

        [Required, Display(Name = "Down Payment"), Column(TypeName = "numeric")]
        public decimal DownPayment { get; set; }

        [Required, Display(Name = "Lease Deposit"), Column(TypeName = "numeric")]
        public decimal LeaseDeposit { get; set; }

        [Required, Display(Name = "Pay Period")]
        public short PaymentPeriodTypeId { get; set; }

        [Required, Display(Name = "Installment No")]
        public short InstallmentNo { get; set; }

        [Required, Display(Name = "Installment Amount"), Column(TypeName = "numeric")]
        public decimal InstallmentAmount { get; set; }

        [Required, Display(Name = "Installment Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime InstallmentStartDate { get; set; }

        [Required, Display(Name = "Tenure Start Date"), Column(TypeName = "smalldatetime")]
        public DateTime TenureStartDate { get; set; }

        [Required, Display(Name = "Tenure")]
        public short LoanTenure { get; set; }

        [Required, Display(Name = "Expire Date"), Column(TypeName = "smalldatetime")]
        public DateTime ExpireDate { get; set; }

        [Required, Display(Name = "Settled?")]
        public short IsSettled { get; set; }

        [Required, Display(Name = "Early Settled?")]
        public short IsEarlySettled { get; set; }

        [Required, Display(Name = "Settlement Date"), Column(TypeName = "smalldatetime")]
        public DateTime SettlementDate { get; set; }

        [Required, Display(Name = "Settlement User")]
        public short SettlementBy { get; set; }

        [Required, Display(Name = "Settlement Entry Date"), Column(TypeName = "smalldatetime")]
        public DateTime SettlementEntryDate { get; set; }

        [Required, Display(Name = "Transferred?")]
        public short IsTransferred { get; set; }

        [Required, Display(Name = "Transfer Date"), Column(TypeName = "smalldatetime")]
        public DateTime TransferDate { get; set; }

        [Required, Display(Name = "Transfer User")]
        public short TransferBy { get; set; }

        [Required, Display(Name = "Transfer Entry Date"), Column(TypeName = "smalldatetime")]
        public DateTime TransferEntryDate { get; set; }

        [Required, Display(Name = "Transferred Loan #")]
        public int TransferredLoanId { get; set; }

        [Required, Display(Name = "Business Type")]
        public short BusinessTypeId { get; set; }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        //public virtual Bank IssuerBank { get; set; }
        //public virtual BankBranch IssuerBranch { get; set; }
        public virtual LCMain LCMain { get; set; }
        public virtual LCAcceptance LCAcceptance { get; set; }
        public virtual CustomType Currency { get; set; } 
        public virtual CustomType FCurrency { get; set; }
        //public virtual ICollection<LCSub> LCSubs { get; set; }
        //public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
        //public virtual ICollection<LCAcceptance> LCAcceptances { get; set; }
        public virtual ICollection<PaymentSchedule> PaymentSchedules { get; set; }
        public virtual ICollection<InterestRateDetails> InterestRateDetails { get; set; }
    }
}