﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class PaymentSub : IFIMSContext
    {
        public PaymentSub()
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public PaymentSub(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PaymentSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "Payment #")]
        [ForeignKey("PaymentMain")]
        public int PaymentId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int PaymentSubId { get; set; }

        [Required, Display(Name = "LC #")]
        //[ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Acceptance #")]
        //[ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Display(Name = "Loan #")]
        //[ForeignKey("LoanMain")]
        public int LoanId { get; set; }

        [NotMapped, Display(Name = "Ref. No")]
        public int RefId { get; set; }

        [NotMapped, Display(Name = "Ref. No")]
        public string RefNo { get; set; }

        [Required, Display(Name = "Installment No")]
        public int ScheduleId { get; set; }

        [NotMapped, Display(Name = "Inst. No")]
        public string InstallmentNo { get; set; }

        [NotMapped, Display(Name = "Pay For")]
        public string PaymentFor { get; set; }

        [Required, Display(Name = "Principal"), Column(TypeName = "numeric")]
        public decimal Principal { get; set; }

        [Required, Display(Name = "Interest"), Column(TypeName = "numeric")]
        public decimal Interest { get; set; }

        [Required, Display(Name = "Libor"), Column(TypeName = "numeric")]
        public decimal Libor { get; set; }

        [Required, Display(Name = "Handling"), Column(TypeName = "numeric")]
        public decimal HandlingCharge { get; set; }

        [Required, Display(Name = "Excise Duty"), Column(TypeName = "numeric")]
        public decimal ExciseDuty { get; set; }

        [Required, Display(Name = "Bank Charge"), Column(TypeName = "numeric")]
        public decimal BankCharge { get; set; }

        [Required, Display(Name = "Settle Fee"), Column(TypeName = "numeric")]
        public decimal SettlementFee { get; set; }

        [Required, Display(Name = "Payment Amount"), Column(TypeName = "numeric")]
        public decimal Amount { get; set; }

        [Required, Display(Name = "Currency")]
        //[ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [Required, Display(Name = "Actual Amount"), Column(TypeName = "numeric")]
        public decimal AmountInActualCurrency { get; set; }

        [Required, Display(Name = "Convert Rate"), Column(TypeName = "numeric")]
        public decimal ActualCurrencyRate { get; set; }

        [Required, Display(Name = "Currency")]
        //[ForeignKey("Currency")]
        public short ActualCurrencyId { get; set; }

        [NotMapped, Display(Name = "Actual Currency")]
        public string ActualCurrency { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return Amount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return Amount * BDTRate; } protected set { } }

        [Required, Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }
        
        [NotMapped, Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual PaymentMain PaymentMain { get; set; }
        //public virtual LoanMain LoanMain { get; set; }
        //public virtual BankBranch IssuerBranch { get; set; }
        //public virtual CustomType STenureType { get; set; }
        //public virtual CustomType BTenureType { get; set; }
        //public virtual CustomType Currency { get; set; }
        //public virtual ICollection<LCSub> LCSubs { get; set; }
        //public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
        //public virtual ICollection<LCAcceptance> LCAcceptances { get; set; }
    }
}