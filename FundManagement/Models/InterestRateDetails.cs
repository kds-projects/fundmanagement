﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class InterestRateDetails : EntityBase, IFIMSContext, IControllerHooks
    {
        public InterestRateDetails()
        {
            FromDate = DateTime.Today;
            ToDate = DateTime.Today;
        }

        public InterestRateDetails(IFIMSContext viewModel)
        {
            FromDate = DateTime.Today;
            ToDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("InterestRateDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int InterestRateId { get; set; }

        [Required, Display(Name = "LC #")]
        [ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Acceptance #")]
        [ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Required, Display(Name = "Loan #")]
        [ForeignKey("LoanMain")]
        public int LoanId { get; set; }

        [Required, Display(Name = "Start Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime FromDate { get; set; }

        [Required, Display(Name = "End Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime ToDate { get; set; }

        [Required, Display(Name = "Int Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Penal Int Rate"), Column(TypeName = "numeric")]
        public decimal ODInterestRate { get; set; }

        [Required, Display(Name = "Inst Amount"), Column(TypeName = "numeric")]
        public decimal InstallmentAmount { get; set; }

        [Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual LCMain LCMain { get; set; }
        public virtual LCAcceptance LCAcceptance { get; set; }
        public virtual LoanMain LoanMain { get; set; }
    }
}