﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class AdvanceAdjustment : EntityBase, IControllerHooks, IFIMSContext
    {
        public AdvanceAdjustment()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public AdvanceAdjustment(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("AdvanceAdjustment", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int AdjustmentId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Adjustment #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        [Index("IX_AdjustmentNo", 1, IsUnique = true)]
        public string AdjustmentNo { get; set; }

        [NotMapped, Display(Name = "Serial #")]
        public int SerialNo { get { return AdjustmentNo == null || AdjustmentNo.Length < 8 ? 0 : int.Parse(AdjustmentNo.Substring(3, 4)); } protected set { } }

        [Required, Display(Name = "Adjustment Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        [Index("IX_AdjustmentDate", 1)]
        public DateTime AdjustmentDate { get; set; }

        [Required, Display(Name = "Instruction #")]
        [ForeignKey("ChequeIssueInstruction")]
        public int InstructionId { get; set; }

        [Required, Display(Name = "Advance #")]
        [ForeignKey("AdjustedInstruction")]
        public int AdjustedInstructionId { get; set; }

        [Display(Name = "Bill #")]
        //[ForeignKey("ChequeIssueSub")]
        public int InstructionSubId { get; set; }

        [Required, Display(Name = "Advance #")]
        //[ForeignKey("AdjustedInstruction")]
        public int AdjustedInstructionSubId { get; set; }

        [Required, Display(Name = "Adjust Amount"), Column(TypeName = "numeric")]
        public decimal AdjustAmount { get; set; }

        [Required, Display(Name = "Purpose Details"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Purpose { get; set; }

        public virtual ChequeIssueInstruction ChequeIssueInstruction { get; set; }
        public virtual ChequeIssueInstruction AdjustedInstruction { get; set; }
        //public virtual ChequeIssueSub ChequeIssueSub { get; set; }
    }
}