namespace FundManagement.Models
{
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Core.Objects;
    using System.Data.Entity.Infrastructure;
    using System.Data.Entity.ModelConfiguration.Conventions;
    using System.Linq;

    public class FundContext : DbContext
    {
        // Your context has been configured to use a 'FundModel' connection string from your application's 
        // configuration file (App.config or Web.config). By default, this connection string targets the 
        // 'FundManagement.Models.FundModel' database on your LocalDb instance. 
        // 
        // If you wish to target a different database and/or database provider, modify the 'FundModel' 
        // connection string in the application configuration file.
        public FundContext()
            : base("FundContext")
        {
            //Database.SetInitializer(new FundInitializer());
            //((IObjectContextAdapter)this).ObjectContext.SavingChanges += new EventHandler(ObjectContext_SavingChanges);
            this.Database.CommandTimeout = 300;
        }

        public override int SaveChanges()
        {
            var changedEntities = ChangeTracker.Entries();

            foreach (var changedEntity in changedEntities)
            {
                if (changedEntity.Entity is IControllerHooks)
                {
                    var entity = (IControllerHooks)changedEntity.Entity;

                    switch (changedEntity.State)
                    {
                        case EntityState.Added:
                            entity.OnBeforeInsert(changedEntity);
                            break;

                        case EntityState.Modified:
                            entity.OnBeforeUpdate(changedEntity);
                            break;

                    }
                }
            }

            return base.SaveChanges();
        }

        public void ObjectContext_SavingChanges(object sender, EventArgs e)
        {
            ObjectContext context = sender as ObjectContext;
            if (context != null)
            {
                // You can use other EntityState constants here
                foreach (ObjectStateEntry entry in context.ObjectStateManager.GetObjectStateEntries(EntityState.Added | EntityState.Modified))
                {
                    // You can handle multiple Entity Types here. I use Student as an example for the sake of simplicity
                    if (entry.Entity.GetType().GetInterfaces().Contains(typeof(IControllerHooks)))
                    {
                        IControllerHooks IControllerHooks = entry.Entity as IControllerHooks;
                        // Do whatever you want with your entities.
                        // You can throw an exception here if you want to
                        // prevent SaveChanges().
                        //if (student.Teacher == null)
                        //    throw Exception("Something went wrong.");
                        if (entry.State == EntityState.Added)
                        {
                            IControllerHooks.OnBeforeInsert(this.Entry(entry.Entity));
                        }
                        else if (entry.State == EntityState.Modified)
                        {
                            //foreach(string pname in this.Entry(entry.Entity).CurrentValues.PropertyNames)
                            //{
                            //    System.Diagnostics.Debug.WriteLine(pname + ": " + this.Entry(entry.Entity).Property(pname).CurrentValue);
                            //    //System.Diagnostics.Debug.WriteLine("A");
                            //    if (this.Entry(entry.Entity).Property(pname).CurrentValue == null)
                            //    {
                            //        this.Entry(entry.Entity).Property(pname).CurrentValue = 0;
                            //    }
                            //}
                            //this.Entry(entry.Entity).Property("EntryUserId").IsModified = false;
                            //this.Entry(entry.Entity).Property("EntryDate").IsModified = false;
                            //this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                            //this.Entry(entry.Entity).Property("PCName").IsModified = false;

                            //if (entry.Entity.GetType().GetProperty("EntryUserId") != null)
                            //    this.Entry(entry.Entity).Property("EntryUserId").CurrentValue = HttpContext.Current.Session["UserId"];
                            //if (entry.Entity.GetType().GetProperty("EntryDate") != null)
                            //    this.Entry(entry.Entity).Property("EntryDate").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                            //if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                            //    this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                            //if (entry.Entity.GetType().GetProperty("PCName") != null)
                            //    this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();

                            IControllerHooks.OnBeforeUpdate(this.Entry(entry.Entity));
                        }
                    }
                    //else if (entry.Entity.GetType().GetInterfaces().Contains(typeof(IFIMSContext)))
                    //{
                    //    IFIMSContext FIMSContext = entry.Entity as IFIMSContext;
                    //    // Do whatever you want with your entities.
                    //    // You can throw an exception here if you want to
                    //    // prevent SaveChanges().
                    //    //if (student.Teacher == null)
                    //    //    throw Exception("Something went wrong.");
                    //    if (entry.State == EntityState.Added)
                    //    {
                    //        if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    //            this.Entry(entry.Entity).Property("CreatedBy").CurrentValue = clsMain.getCurrentUser();
                    //        if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                    //            this.Entry(entry.Entity).Property("CreatedTime").CurrentValue = clsMain.getCurrentTime();
                    //        if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //            this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                    //        if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //            this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();
                    //    }
                    //    else if (entry.State == EntityState.Modified)
                    //    {
                    //        //foreach(string pname in this.Entry(entry.Entity).CurrentValues.PropertyNames)
                    //        //{
                    //        //    System.Diagnostics.Debug.WriteLine(pname + ": " + this.Entry(entry.Entity).Property(pname).CurrentValue);
                    //        //    //System.Diagnostics.Debug.WriteLine("A");
                    //        //    if (this.Entry(entry.Entity).Property(pname).CurrentValue == null)
                    //        //    {
                    //        //        this.Entry(entry.Entity).Property(pname).CurrentValue = 0;
                    //        //    }
                    //        //}
                    //        //this.Entry(entry.Entity).Property("EntryUserId").IsModified = false;
                    //        //this.Entry(entry.Entity).Property("EntryDate").IsModified = false;
                    //        //this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                    //        //this.Entry(entry.Entity).Property("PCName").IsModified = false;

                    //        //if (entry.Entity.GetType().GetProperty("EntryUserId") != null)
                    //        //    this.Entry(entry.Entity).Property("EntryUserId").CurrentValue = HttpContext.Current.Session["UserId"];
                    //        //if (entry.Entity.GetType().GetProperty("EntryDate") != null)
                    //        //    this.Entry(entry.Entity).Property("EntryDate").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                    //        //if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //        //    this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                    //        //if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //        //    this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();

                    //        //ControllerHooks.OnEdit(this.Entry(entry.Entity));
                    //    }
                    //}
                    //else
                    //{
                    //    if (entry.State == EntityState.Added)
                    //    {
                    //        if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    //            this.Entry(entry.Entity).Property("CreatedBy").CurrentValue = HttpContext.Current.Session["UserId"];
                    //        if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                    //            this.Entry(entry.Entity).Property("CreatedTime").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                    //        if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //            this.Entry(entry.Entity).Property("IPAddress").CurrentValue = clsMain.GetUser_IP();
                    //        if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //            this.Entry(entry.Entity).Property("PCName").CurrentValue = clsMain.GetUser_PCName();
                    //    }
                    //    else if (entry.State == EntityState.Modified)
                    //    {

                    //        if (entry.Entity.GetType().GetProperty("CreatedBy") != null)
                    //            this.Entry(entry.Entity).Property("CreatedBy").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("CreatedTime") != null)
                    //            this.Entry(entry.Entity).Property("CreatedTime").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("IPAddress") != null)
                    //            this.Entry(entry.Entity).Property("IPAddress").IsModified = false;
                    //        if (entry.Entity.GetType().GetProperty("PCName") != null)
                    //            this.Entry(entry.Entity).Property("PCName").IsModified = false;

                    //        if (entry.Entity.GetType().GetProperty("UpdatedBy") != null)
                    //            this.Entry(entry.Entity).Property("UpdatedBy").CurrentValue = HttpContext.Current.Session["UserId"];
                    //        if (entry.Entity.GetType().GetProperty("UpdateTime") != null)
                    //            this.Entry(entry.Entity).Property("UpdateTime").CurrentValue = this.Database.SqlQuery<System.DateTime>("Select GetDate() as CurDate").DefaultIfEmpty(DateTime.Parse("01-Jan-1900")).FirstOrDefault();
                    //    }
                    //}
                }
            }
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();

            //modelBuilder.Entity<BankAccount>()
            //.Property(p => p.Activity)
            //.HasColumnType("smallint");
        }

        // Add a DbSet for each entity type that you want to include in your model. For more information 
        // on configuring and using a Code First model, see http://go.microsoft.com/fwlink/?LinkId=390109.

        // public virtual DbSet<MyEntity> MyEntities { get; set; }
        public virtual DbSet<Bank> Banks { get; set; }
        public virtual DbSet<BankBranch> BankBranchs { get; set; }
        public virtual DbSet<CustomType> CustomTypes { get; set; }
        public virtual DbSet<BankAccount> BankAccount { get; set; }
        public virtual DbSet<SanctionMain> SanctionMains { get; set; }
        public virtual DbSet<SanctionDetails> SanctionDetails { get; set; }
        public virtual DbSet<LCMain> LCMains { get; set; }
        public virtual DbSet<LCSub> LCSubs { get; set; }
        public virtual DbSet<LCWisePI> LCWisePIs { get; set; }
        public virtual DbSet<LCAcceptance> LCAcceptances { get; set; }
        public virtual DbSet<LoanMain> LoanMains { get; set; }
        public virtual DbSet<PaymentSchedule> PaymentSchedules { get; set; }
        public virtual DbSet<ODMain> ODMains { get; set; }
        public virtual DbSet<ODDetails> ODDetails { get; set; }
        public virtual DbSet<CollectionForecasting> CollectionForecastings { get; set; }
        public virtual DbSet<MonthlyExpense> MonthlyExpenses { get; set; }
        public virtual DbSet<CashFlowShortage> CashFlowShortages { get; set; }
        public virtual DbSet<CashFlowMain> CashFlowMains { get; set; }
        public virtual DbSet<CashFlowDetails> CashFlowDetails { get; set; }
        public virtual DbSet<ImportBudget> ImportBudgets { get; set; }
        public virtual DbSet<DailyAccountPosition> DailyAccountPositions { get; set; }
        public virtual DbSet<PaymentMain> PaymentMains { get; set; }
        public virtual DbSet<PaymentSub> PaymentSubs { get; set; }
        public virtual DbSet<ChequeInfo> ChequeInfos { get; set; }
        public virtual DbSet<ChequePayment> ChequePayments { get; set; }
        public virtual DbSet<ChequeIssueInstruction> ChequeIssueInstructions { get; set; }
        public virtual DbSet<ChequeIssueSub> ChequeIssueSubs { get; set; }
        public virtual DbSet<LocalAgent> LocalAgents { get; set; }
        public virtual DbSet<Port> Ports { get; set; }
        public virtual DbSet<PIMain> PIMains { get; set; }
        public virtual DbSet<PISub> PISubs { get; set; }
        public virtual DbSet<CSMain> CSMains { get; set; }
        public virtual DbSet<CSSub> CSSubs { get; set; }
        public virtual DbSet<AdjustmentMain> AdjustmentMains { get; set; }
        public virtual DbSet<AdjustmentSub> AdjustmentSubs { get; set; }
        public virtual DbSet<LoanInterest> LoanInterests { get; set; }
        public virtual DbSet<LoanInterest_Temp> LoanInterest_Temps { get; set; }
        public virtual DbSet<InterestRateDetails> InterestRateDetails { get; set; }
        public virtual DbSet<FundDetails> FundDetails { get; set; }
        public virtual DbSet<UserInfo> UserInfos { get; set; }
        public virtual DbSet<CaptionDetails> CaptionDetails { get; set; }
        public virtual DbSet<ReconciliationMain> ReconciliationMains { get; set; }
        public virtual DbSet<ReconciliationSub> ReconciliationSubs { get; set; }
        public virtual DbSet<DailyFundMain> DailyFundMains { get; set; }
        public virtual DbSet<DailyFundSub> DailyFundSubs { get; set; }
        public virtual DbSet<LandedCosting> LandedCostings { get; set; }
        public virtual DbSet<AccountAgainstLoanType> AccountAgainstLoanTypes { get; set; }
        public virtual DbSet<SupplierAccount> SupplierAccounts { get; set; }
        public virtual DbSet<ReceivableDocument> ReceivableDocuments { get; set; }
        public virtual DbSet<AdvanceAdjustment> AdvanceAdjustments { get; set; }
        public virtual DbSet<ChequeBookRequisition> ChequeBookRequisitions { get; set; }
        public virtual DbSet<ChequeBookRequisitionSub> ChequeBookRequisitionSubs { get; set; }

        public virtual DbSet<ChequeIssueTDSVDS> ChequeIssueTDSVDS { get; set; }
        public virtual DbSet<ChequeReceive> ChequeReceives { get; set; }
        public virtual DbSet<PayorderInfo> PayorderInfos { get; set; }
        public virtual DbSet<PayorderDetails> PayorderDetails { get; set; }
        public virtual DbSet<Wharfrent> Wharfrents { get; set; }
        public virtual DbSet<FactoryLoacation> FactoryLoacations { get; set; }
    }

    //public class MyEntity
    //{
    //    public int Id { get; set; }
    //    public string Name { get; set; }
    //}
}