﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class DailyAccountPosition : EntityBase
    {
        public DailyAccountPosition()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public DailyAccountPosition(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("DailyAccountPosition", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int DAPId { get; set; }
        [Required, Display(Name = "Date")]
        public DateTime DailyDate { get; set; }
        [Required, Display(Name = "All Current A/C"), Column(TypeName = "numeric")]
        public Decimal CurrentAccount { get; set; }
        [Required, Display(Name = "All DL Account"), Column(TypeName = "numeric")]
        public Decimal DLAccount { get; set; }
        [Required, Display(Name = "All OD Account"), Column(TypeName = "numeric")]
        public Decimal ODAccount { get; set; }
        [Required, Display(Name = "Daily Rec. Amount"), Column(TypeName = "numeric")]
        public Decimal ReceivedAmount { get; set; }
    }
}