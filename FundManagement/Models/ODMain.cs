﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ODMain : EntityBase, IControllerHooks, IFIMSContext
    {
        public ODMain()
        {
            OpeningDate = DateTime.Today;
        }

        public ODMain(IFIMSContext viewModel)
        {
            OpeningDate = DateTime.Today;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (pi.CanWrite && viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (pi.CanWrite && viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ODMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ODId { get; set; }

        [Required, Display(Name = "Over Draft #"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string ODNo { get; set; }

        [Required, Display(Name = "Company")]
        public short CompanyId { get; set; }

        [Required, Display(Name = "Issuer Bank")]
        [ForeignKey("IssuerBank")]
        public short IssuerBankId { get; set; }

        [Required, Display(Name = "Issuer Branch")]
        [ForeignKey("IssuerBranch")]
        public short IssuerBranchId { get; set; }

        [Required, Display(Name = "Type")]
        [ForeignKey("CustomType")]
        public short LoanTypeId { get; set; }

        [Required, Display(Name = "Opening Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime OpeningDate { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [NotMapped]
        public DBFirst.Company Company
        {
            get
            {
                return (new DBFirst.ProductContext()).Companies.Find(CompanyId);
            }
        }

        [NotMapped]
        public string _editstatus { get; set; }

        public virtual CustomType CustomType { get; set; }
        public virtual Bank IssuerBank { get; set; }
        public virtual BankBranch IssuerBranch { get; set; }
        public virtual ICollection<ODDetails> ODDetails { get; set; }
    }
}