﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class SanctionDetails : EntityBase, IControllerHooks, IFIMSContext
    {
        public SanctionDetails()
        {
            //SanctionType = new List<CustomType>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public SanctionDetails(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("SanctionDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int SanctionDetailsId { get; set; }

        [Display(Name = "Ref No")]
        [ForeignKey("SanctionMain")]
        public int SanctionId { get; set; }

        [Required, Display(Name = "Facility Type")]
        [ForeignKey("SanctionType")]
        public short SanctionTypeId { get; set; }

        [NotMapped, Required, Display(Name = "Facility Type")]
        public string SanctionTypeName { get; set; }

        [Required, Display(Name = "Facility Limit"), Column(TypeName = "numeric")]
        public decimal SanctionLimit { get; set; }

        [Required, Display(Name = "Interest Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Penalty Rate"), Column(TypeName = "numeric")]
        public decimal ODInterestRate { get; set; }

        [Required, Display(Name = "Early Settle Rate"), Column(TypeName = "numeric")]
        public decimal EarlySettlementRate { get; set; }

        [Required, Display(Name = "LC Com Rate"), Column(TypeName = "numeric")]
        public decimal LCCommissionRate { get; set; }

        [Required, Display(Name = "Accept Com Rate"), Column(TypeName = "numeric")]
        public decimal AcceptanceCommissionRate { get; set; }

        [Required, Display(Name = "Charge Rate"), Column(TypeName = "numeric")]
        public decimal ChargeRate { get; set; }

        [Required, Display(Name = "Other Charge Rate"), Column(TypeName = "numeric")]
        public decimal OtherChargeRate { get; set; }

        [Required, Display(Name = "Margin"), Column(TypeName = "numeric")]
        public decimal Margin { get; set; }

        [Required, Display(Name = "Purpose"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Purpose { get; set; }

        [Required, Display(Name = "Status")]
        public short Status { get; set; }

        [NotMapped, Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual CustomType SanctionType { get; set; }
        public virtual SanctionMain SanctionMain { get; set; }
    }
}