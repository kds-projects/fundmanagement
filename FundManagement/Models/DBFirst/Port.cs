//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FundManagement.Models.DBFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class Port
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Port()
        {
            this.PIMains = new HashSet<PIMain>();
        }
    
        public short PortId { get; set; }
        public short CountryId { get; set; }
        public string PortCode { get; set; }
        public string PortName { get; set; }
        public byte IsActive { get; set; }
        public short EntryUserId { get; set; }
        public System.DateTime EntryDate { get; set; }
        public short RevisionUserId { get; set; }
        public System.DateTime RevisionDate { get; set; }
        public string IPAddress { get; set; }
        public string PCName { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<PIMain> PIMains { get; set; }
    }
}
