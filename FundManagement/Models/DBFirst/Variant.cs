//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FundManagement.Models.DBFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class Variant
    {
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2214:DoNotCallOverridableMethodsInConstructors")]
        public Variant()
        {
            this.ItemVariants = new HashSet<ItemVariant>();
            this.Variant_Value = new HashSet<Variant_Value>();
        }
    
        public short VariantId { get; set; }
        public string VariantName { get; set; }
        public string DisplayName { get; set; }
        public string Prefix { get; set; }
        public string Suffix { get; set; }
        public short PVariantId { get; set; }
        public short VariantTextType { get; set; }
        public short VariantInputType { get; set; }
        public byte IsActive { get; set; }
        public short RankId { get; set; }
        public short CreatedBy { get; set; }
        public System.DateTime CreatedTime { get; set; }
        public short UpdatedBy { get; set; }
        public System.DateTime UpdateTime { get; set; }
        public string PCName { get; set; }
        public string IPAddress { get; set; }
    
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<ItemVariant> ItemVariants { get; set; }
        [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2227:CollectionPropertiesShouldBeReadOnly")]
        public virtual ICollection<Variant_Value> Variant_Value { get; set; }
    }
}
