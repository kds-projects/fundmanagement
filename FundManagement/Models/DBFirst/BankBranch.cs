//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FundManagement.Models.DBFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class BankBranch
    {
        public short BranchId { get; set; }
        public short BankId { get; set; }
        public string BranchCode { get; set; }
        public string BranchName { get; set; }
        public string ShortName { get; set; }
        public string BranchAddress { get; set; }
        public string PhoneNo { get; set; }
        public string Fax { get; set; }
        public byte IsActive { get; set; }
        public short EntryUserId { get; set; }
        public System.DateTime EntryDate { get; set; }
        public short RevisionUserId { get; set; }
        public System.DateTime RevisionDate { get; set; }
        public string IPAddress { get; set; }
        public string PCName { get; set; }
    
        public virtual Bank Bank { get; set; }
    }
}
