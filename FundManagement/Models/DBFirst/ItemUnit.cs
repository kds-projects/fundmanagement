//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated from a template.
//
//     Manual changes to this file may cause unexpected behavior in your application.
//     Manual changes to this file will be overwritten if the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace FundManagement.Models.DBFirst
{
    using System;
    using System.Collections.Generic;
    
    public partial class ItemUnit
    {
        public int ItemUnitId { get; set; }
        public short ItemId { get; set; }
        public short UnitId { get; set; }
        public decimal KGProportion { get; set; }
    
        public virtual ItemInfo ItemInfo { get; set; }
        public virtual Variant_Value Variant_Value { get; set; }
    }
}
