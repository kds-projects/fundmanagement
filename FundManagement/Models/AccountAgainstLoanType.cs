﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class AccountAgainstLoanType : EntityBase, IControllerHooks, IFIMSContext
    {
        public AccountAgainstLoanType()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();
        }

        public AccountAgainstLoanType(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("AccountAgainstLoanType", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short AccountId { get; set; }

        [Required, Display(Name = "Group")]
        public short GroupId { get; set; }

        [Required, Display(Name = "Acounts Company"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string AccountsCompanyId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("Branch")]
        public short BranchId { get; set; }

        [Required, Display(Name = "Loan Type")]
        [ForeignKey("LoanType")]
        public short LoanTypeId { get; set; }

        [Display(Name = "Code"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string LedgerCode { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }

        public virtual CustomType LoanType { get; set; }
        public virtual Bank Bank { get; set; }
        public virtual BankBranch Branch { get; set; }
    }
}