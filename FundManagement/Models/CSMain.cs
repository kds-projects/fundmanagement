﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CSMain : EntityBase, IFIMSContext, IControllerHooks
    {
        
        public CSMain()
        {
            //PIMain = new PIMain();
            CSSubs = new List<CSSub>();
            CSDate = DateTime.Now;
        }

        public CSMain(IFIMSContext viewModel)
        {
            //PIMain = new PIMain();
            CSSubs = new List<CSSub>();
            CSDate = DateTime.Now;

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CSMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Display(Name = "PI")]
        [ForeignKey("PIMain")]
        public int PIFileId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int CSId { get; set; }

        [Required, Display(Name = "CS No"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string CSNo { get; set; }

        [Required, Display(Name = "CS Date"), Column(TypeName = "smalldatetime")]
        public System.DateTime CSDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return CSDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return CSDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get; set; } //{ return short.Parse(CSNo.Substring(5, 4)); } protected set { } }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual PIMain PIMain { get; set; }
        public virtual ICollection<CSSub> CSSubs { get; set; }
    }
}