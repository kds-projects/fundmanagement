﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequePayment : EntityBase, IControllerHooks, IFIMSContext
    {
        public ChequePayment()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public ChequePayment(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequePayment", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int PaymentId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Payment #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        [Index("IX_ChequePaymentNo", 1, IsUnique = true)]
        public string PaymentNo { get; set; }

        [NotMapped, Display(Name = "Serial #")]
        public int SerialNo { get { return PaymentNo == null || PaymentNo.Length < 7 ? 0 : int.Parse(PaymentNo.Substring(3, 4)); } protected set { } }

        [Required, Display(Name = "Instruction #")]
        public int InstructionId { get; set; }

        [Display(Name = "Cheque #")]
        [ForeignKey("ChequeInfo")]
        public int ChequeId { get; set; }

        [Required, Display(Name = "Issue Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        [Index("IX_IssueDate", 1)]
        public DateTime IssueDate { get; set; }

        [Required, Display(Name = "Cheque Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        [Index("IX_ChequeDate", 1)]
        public DateTime ChequeDate { get; set; }

        [Required, Display(Name = "To Bank Account")]
        public short IssuedAccountId { get; set; }

        [NotMapped, Display(Name = "Bank Account")]
        public string IssuedAccountNo { get; set; }

        [Required, Display(Name = "To (Infavor of)"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string IssueTo { get; set; }

        [Required, Display(Name = "Cheque Amount"), Column(TypeName = "numeric")]
        public decimal ChequeAmount { get; set; }

        [Required, Display(Name = "Payment Type")]
        [ForeignKey("ChequeMode")]
        public short ChequeModeId { get; set; }

        [Required, Display(Name = "Purpose Details"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Purpose { get; set; }

        [Required, Display(Name = "Signatories"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string Signatories { get; set; }

        [NotMapped]
        public ChequeIssueInstruction IssueInstruction
        {
            get
            {
                var _db = new FundContext();
                return _db.ChequeIssueInstructions.Find(InstructionId);
            }
            set
            {

            }
        }

        public virtual ChequeInfo ChequeInfo { get; set; }
        public virtual CustomType ChequeMode { get; set; }
    }
}