﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class PayorderDetails : IFIMSContext
    {
        public PayorderDetails()
        {

        }

        public PayorderDetails(IFIMSContext viewModel)
        {

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PayorderDetails", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }

        [Key]
        public int PayorderDetailsId { get; set; }

        [ForeignKey("PayorderInfo")]
        public int PayOrderId { get; set; }

        [Required]
        public short IssuedAccountId { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "To (Infavor of)")]
        public string IssuedAccountName { get; set; }

        public decimal PayorderAmount { get; set; }

        public string Purpose { get; set; }

        public virtual PayorderInfo PayorderInfo {get;set;}



    }
}