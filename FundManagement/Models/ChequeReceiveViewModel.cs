﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class ChequeReceiveViewModel:IFIMSContext
    {
       
        public int ChequeReceiveId { get; set; }


        [Required]
        public int ChequeId { get; set; }

        [Required(ErrorMessage ="Please Enter ChequeNo")]
        [Display(Name ="Cheque No")]
        public string ChequeNo { get; set; }

        [Required(ErrorMessage ="Please Enter IssueDate")]
        [Display(Name = "Issue Date")]
        public DateTime IssueDate { get; set; }

        [Required(ErrorMessage = "Please Enter Cheque Date")]
        [Display(Name = "Cheque Date")]
        public DateTime ChequeDate { get; set; }

        [Required(ErrorMessage = "Please Enter Issue To")]
        [Display(Name = "Issue To")]
        public string IssueTo { get; set; }

        [Required(ErrorMessage = "Please Enter Cheque Amount")]
        [Display(Name = "Cheque Amount")]
        public decimal ChequeAmount { get; set; }

        [Required(ErrorMessage = "Please Enter Purpose")]
        [Display(Name = "Purpose")]
        public string Purpose { get; set; }
        public short AccountId { get; set; }
        public short BankId { get; set; }
        public short BranchId { get; set; }

        [Required(ErrorMessage ="Enter Bank Name")]
        [Display(Name ="Bank")]
        public string BankName { get; set; }

        [Required(ErrorMessage = "Enter Bank Name")]
        [Display(Name = "Account No")]
        public string AccountNo { get; set; }

        [Required(ErrorMessage = "Enter Bank Name")]
        [Display(Name = "Branch")]
        public string BankBranchName { get; set; }

        [Required]
        [Display(Name ="Receive Date")]
        public DateTime ReceiveDate { get; set; }

        [Display(Name = "Remarks")]
        public string Remarks { get; set; }

        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }

        
    }
}