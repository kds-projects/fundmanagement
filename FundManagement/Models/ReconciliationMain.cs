﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ReconciliationMain : EntityBase, IFIMSContext, IControllerHooks
    {
        public ReconciliationMain()
        {
            ReconciliationSubs = new List<ReconciliationSub>();
        }

        public ReconciliationMain(IFIMSContext viewModel)
        {
            ReconciliationSubs = new List<ReconciliationSub>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ReconciliationMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short ReconciliationId { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Year")]
        public int ReconciliationYear { get { return ReconciliationDate == null ? 0 : ReconciliationDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Month")]
        public int ReconciliationMonth { get { return ReconciliationDate == null ? 0 : ReconciliationDate.Month; } protected set { } }

        [NotMapped, Required, Display(Name = "Month")]
        public string ReconciliationMonthName { get { return ReconciliationDate == null ? "" : ReconciliationDate.ToString("MMMM"); } protected set { } }

        [Required, Display(Name = "Date")]
        public DateTime ReconciliationDate { get; set; }

        [Required, Display(Name = "Company")]
        public short CompanyId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("Branch")]
        public short BranchId { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual BankBranch Branch { get; set; }

        public virtual ICollection<ReconciliationSub> ReconciliationSubs { get; set; }
    }
}