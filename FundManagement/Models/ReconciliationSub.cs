﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ReconciliationSub : IFIMSContext
    {
        public ReconciliationSub()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public ReconciliationSub(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ReconciliationSub", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short ReconciliationSubId { get; set; }

        [Display(Name = "Reconciliation #")]
        [ForeignKey("ReconciliationMain")]
        public short ReconciliationId { get; set; }

        [Required, Display(Name = "Loan Type")]
        [ForeignKey("LoanType")]
        public short LoanTypeId { get; set; }

        [Required, Display(Name = "SWR Balance"), Column(TypeName = "numeric")]
        public Decimal SoftwareBalance { get; set; }

        [Required, Display(Name = "Book Balance"), Column(TypeName = "numeric")]
        public Decimal BookBalance { get; set; }

        [Required, Display(Name = "Bank Balance"), Column(TypeName = "numeric")]
        public Decimal BankBalance { get; set; }

        [Required, Display(Name = "SWR & Book Diff"), Column(TypeName = "numeric")]
        public Decimal SoftwareBookDifference { get; set; }

        [Required(AllowEmptyStrings=true), Display(Name = "SWR & Book Diff Remarks"), MaxLength(350), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string SoftwareBookDifferenceRemarks { get; set; }

        [Required, Display(Name = "SWR & Bank Diff"), Column(TypeName = "numeric")]
        public Decimal SoftwareBankDifference { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "SWR & Bank Diff Remarks"), MaxLength(350), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string SoftwareBankDifferenceRemarks { get; set; }

        [Required, Display(Name = "Bank & Book Diff"), Column(TypeName = "numeric")]
        public Decimal BankBookDifference { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Bank & Book Diff Remarks"), MaxLength(350), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string BankBookDifferenceRemarks { get; set; }

        [Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual CustomType LoanType { get; set; }
        public virtual ReconciliationMain ReconciliationMain { get; set; }
    }
}