﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class FactoryLoacation
    {
        public int Id { get; set; }
        public int FactoryId { get; set; }
        public int LocationId { get; set; }
    }
}