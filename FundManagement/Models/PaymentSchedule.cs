﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class PaymentSchedule : EntityBase
    {
        public PaymentSchedule()
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public PaymentSchedule(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PaymentSchedule", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Required, Display(Name = "LC #")]
        //[ForeignKey("LCMain")]
        public int LCFileId { get; set; }

        [Display(Name = "Acceptance #")]
        //[ForeignKey("LCAcceptance")]
        public int LCAcceptanceId { get; set; }

        [Display(Name = "Loan #")]
        [ForeignKey("LoanMain")]
        public int LoanId { get; set; }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int ScheduleId { get; set; }

        [Required, Display(Name = "Installment No")]
        public short InstallmentNo { get; set; }

        //[NotMapped, Display(Name = "Installment No")]
        //public string InstallmentSLNo { get { return InstallmentSLNo ?? InstallmentNo.ToString(); } set { } }

        [Required, Display(Name = "Due Date"), Column(TypeName = "smalldatetime")]
        [Index("IX_DueDate", 1)]
        public DateTime DueDate { get; set; }

        [Required, Display(Name = "Opening"), Column(TypeName = "numeric")]
        public decimal OpeningBalance { get; set; }

        [Required, Display(Name = "Due Amount"), Column(TypeName = "numeric")]
        public decimal DueAmount { get; set; }

        [Required, Display(Name = "Principal Amount"), Column(TypeName = "numeric")]
        public decimal PrincipalAmount { get; set; }

        [Required, Display(Name = "Interest Amount"), Column(TypeName = "numeric")]
        public decimal InterestAmount { get; set; }

        [Required, Display(Name = "Interest Rate"), Column(TypeName = "numeric")]
        public decimal InterestRate { get; set; }

        [Required, Display(Name = "Closing"), Column(TypeName = "numeric")]
        public decimal ClosingBalance { get; set; }

        [Required, Display(Name = "Paid Amount"), Column(TypeName = "numeric")]
        public decimal PaidAmount { get; set; }

        [Required, Display(Name = "Status"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public string Status { get { return PaidAmount == 0 ? "DUE" : PaidAmount < DueAmount ? "PARTIAL PAID" : PaidAmount == DueAmount ? "FULL PAID" : "EXCESS PAID"; } protected set { } }

        [NotMapped, Required, Display(Name = "SL #")]
        public short RowNo { get; set; }

        public virtual LoanMain LoanMain { get; set; }
        //public virtual BankBranch IssuerBranch { get; set; }
        //public virtual CustomType STenureType { get; set; }
        //public virtual CustomType BTenureType { get; set; }
        //public virtual CustomType Currency { get; set; }
        //public virtual ICollection<LCSub> LCSubs { get; set; }
        //public virtual ICollection<LCWisePI> LCWisePIs { get; set; }
        //public virtual ICollection<LCAcceptance> LCAcceptances { get; set; }
    }
}