﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class MonthlyExpense : EntityBase
    {
        public MonthlyExpense()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public MonthlyExpense(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("MonthlyExpense", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short ExpenseId { get; set; }
        [Required, Display(Name = "Year")]
        public short ExpenseYear { get; set; }
        [Required, Display(Name = "Month")]
        public short ExpenseMonth { get; set; }
        [NotMapped, Required, Display(Name = "Month")]
        public string ExpenseMonthName { get; set; }
        [Required, Display(Name = "Cash Disbursement (CTG)"), Column(TypeName = "numeric")]
        public Decimal CashDisbursementCTG { get; set; }
        [Required, Display(Name = "Cash Disbursement (DHK)"), Column(TypeName = "numeric")]
        public Decimal CashDisbursementDHK { get; set; }
        [Required, Display(Name = "Creditors/Suppliers Payment"), Column(TypeName = "numeric")]
        public Decimal CreditorsPayment { get; set; }
        [Required, Display(Name = "Salaries & Wages"), Column(TypeName = "numeric")]
        public Decimal Salaries { get; set; }
        [Required, Display(Name = "Royalties"), Column(TypeName = "numeric")]
        public Decimal Royalties { get; set; }
        [Required, Display(Name = "Prfessional Fees"), Column(TypeName = "numeric")]
        public Decimal PrfessionalFees { get; set; }
        [Required, Display(Name = "Utilities"), Column(TypeName = "numeric")]
        public Decimal Utilities { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string UtilityRemarks { get; set; }
        [Required, Display(Name = "Projects Payment"), Column(TypeName = "numeric")]
        public Decimal ProjectPayment { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string ProjectRemarks { get; set; }
        [Required, Display(Name = "Assets Acquisiations"), Column(TypeName = "numeric")]
        public Decimal AssetsAcquisiation { get; set; }
        [Required, Display(Name = "Insurance Premium"), Column(TypeName = "numeric")]
        public Decimal InsurancePremium { get; set; }
        [Required, Display(Name = "Tax Payment"), Column(TypeName = "numeric")]
        public Decimal Tax { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string TaxRemarks { get; set; }
        [Required, Display(Name = "Miscellaneous"), Column(TypeName = "numeric")]
        public Decimal Miscellaneous { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string MiscellaneousRemarks { get; set; }
        [Required, Display(Name = "Directors Payment"), Column(TypeName = "numeric")]
        public Decimal DirectorsPayment { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string DirectorRemarks { get; set; }
        [Required, Display(Name = "Devidend"), Column(TypeName = "numeric")]
        public Decimal Devidend { get; set; }
        [Required, Display(Name = "Complaince"), Column(TypeName = "numeric")]
        public Decimal Complaince { get; set; }
        [Required, Display(Name = "Others"), Column(TypeName = "numeric")]
        public Decimal Others { get; set; }
        [Required, Display(Name = "Remarks"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string OtherRemarks { get; set; }
    }
}