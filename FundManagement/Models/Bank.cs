﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class Bank : EntityBase, IControllerHooks, IFIMSContext
    {
        public Bank()
        {
            BankBranchs = new List<BankBranch>();
            BankAccounts = new List<BankAccount>();
            SanctionMains = new List<SanctionMain>();
        }

        public Bank(IFIMSContext viewModel)
        {
            BankBranchs = new List<BankBranch>();
            BankAccounts = new List<BankAccount>();
            SanctionMains = new List<SanctionMain>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("Bank", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short BankId { get; set; }
        [NotMapped, Display(Name = "Code"), MaxLength(5), Column(TypeName = "VARCHAR")]
        public string BankCode { get; set; }
        [Required, Display(Name = "Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        [Index("IX_BankName", 1, IsUnique = true)]
        public string BankName { get; set; }
        [Required, Display(Name = "Short Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        [Index("IX_ShortName", 1, IsUnique = true)]
        public string ShortName { get; set; }
        [Required, Display(Name = "Type"), DefaultValue(0)]
        public short BankType { get; set; }
        [Required, Display(Name = "Latest IR"), Column(TypeName = "numeric")]
        public decimal LatestInterestRate { get; set; }
        [Required, Display(Name = "Latest OD IR"), Column(TypeName = "numeric")]
        public decimal LatestODInterestRate { get; set; }
        [Required, Display(Name = "Early Settle Rate"), Column(TypeName = "numeric")]
        public decimal EarlySettlementRate { get; set; }
        [Required, Display(Name = "LC Com Rate"), Column(TypeName = "numeric")]
        public decimal LCCommissionRate { get; set; }
        [Required, Display(Name = "Accept Com Rate"), Column(TypeName = "numeric")]
        public decimal AcceptanceCommissionRate { get; set; }
        [Display(Name = "Old LC Bank Id")]
        public short LCMBankId { get; set; }

        [Display(Name = "Sales Bank Id")]
        public short SalesBankId { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }

        public virtual ICollection<BankBranch> BankBranchs { get; set; }
        public virtual ICollection<BankAccount> BankAccounts { get; set; }
        public virtual ICollection<SanctionMain> SanctionMains { get; set; }

        [InverseProperty("IssuerBank")]
        public virtual ICollection<LCMain> LCIssuerBank { get; set; }
        //[InverseProperty("NegotiationBank")]
        //public virtual ICollection<LCMain> LCNegotiationBank { get; set; }
        //[InverseProperty("SupplierBank")]
        //public virtual ICollection<LCMain> LCSupplierBank { get; set; }
    }
}