﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class Wharfrent : EntityBase, IFIMSContext, IControllerHooks
    {
        public Wharfrent()
        { 
        
        }

        public Wharfrent(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

            foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
                else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("Wharfrent", pi.Name, pi.PropertyType);
                    if (obj != null)
                        pi.SetValue(this, obj);
                }
            }
        }

        public int WharfrentId { get; set; }

        [ForeignKey("LCMain")]
        [Required(ErrorMessage ="Please Select LcNo")]
        [Display(Name ="LcNo:")]
        public int LcFileId { get; set; }

        [Required(ErrorMessage = "Please Select LotNo")]
        [Display(Name = "LotNo:")]
        public string LotNo { get; set; }

        [Required(ErrorMessage = "Please Select BillDate")]
        [Display(Name = "C&F Billing Date :")]
        [DisplayFormat(DataFormatString = "{0:MM-yyyy}", ApplyFormatInEditMode = true)]
        public DateTime BillDate { get; set; }

        [Required(ErrorMessage = "Please Select Breathing Date")]
        [Display(Name = "Berthing Date:")]
        public DateTime BrethingDate { get; set; }

        [Required(ErrorMessage = "Please Select Port Landing   Date")]
        [Display(Name = "Port Landing Date:")]
        public DateTime PortLandingDate { get; set; }

        [Required(ErrorMessage = "Please Select Delivery  Date")]
        [Display(Name = "Delivery Date:")]
        public DateTime DeliveryDate { get; set; }

        [Required(ErrorMessage = "Please Select WhrafRent From Date")]
        [Display(Name = "From Date:")]
        public DateTime WhrafRentFromDate { get; set; }

        [Required(ErrorMessage = "Please Select WhrafRent To  Date")]
        [Display(Name = "To Date:")]
        public DateTime WhrafRentToDate { get; set; }

        [Required(ErrorMessage = "Please Select Amount")]
        [Display(Name = "Amount:")]
        public decimal WhrafRentAmount { get; set; }

        [Required(ErrorMessage = "Please Select Detention Charge")]
        [Display(Name = "Amount")]
        public decimal ContainerDetentionCharge { get; set; }

        [Required(ErrorMessage = "Please Select Detention From Date")]
        [Display(Name = "From Date:")]
        public DateTime DetentionFromDate { get; set; }

        [Required(ErrorMessage = "Please Select Detention To  Date")]
        [Display(Name = "To Date:")]
        public DateTime DetentionToDate { get; set; }

        [Required(ErrorMessage = "Please Select Detention To  Date")]
        [Display(Name = "Return Date:")]
        public DateTime ContainerReturnDate { get; set; }

        public string FilePath { get; set; }

        public string Remarks { get; set; }

        public virtual LCMain LCMain { get; set; }
             





    }
}