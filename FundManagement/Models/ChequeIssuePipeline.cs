﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class ChequeIssuePipeline : IFIMSContext
    {
        public ChequeIssuePipeline()
        {
            //InstructionDate = clsMain.getCurrentTime();
            //ChequeDate = InstructionDate;
            //ApprovedTime = DateTime.Parse("01-Jan-1900");
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public ChequeIssuePipeline(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("ChequeIssuePipeline", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int InstructionPipelineId { get; set; }

        [Required, Display(Name = "ChequeIssueInstruction")]
        [ForeignKey("ChequeIssueSub")]
        public int InstructionSubId { get; set; }

        [Required, Display(Name = "ChequeIssueInstruction")]
        [ForeignKey("ChequeIssueInstruction")]
        public int InstructionId { get; set; }

        [Required, Display(Name = "Bill Forward Id")]
        public int LPBillForwardId { get; set; }

        [Required, Display(Name = "Bill Forward Id")]
        public int LPBillForwardDetailsId { get; set; }

        [Required, Display(Name = "Tax"), Column(TypeName = "numeric")]
        public Decimal Tax { get; set; }

        [Required, Display(Name = "VAT"), Column(TypeName = "numeric")]
        public Decimal VAT { get; set; }

        public virtual ChequeIssueInstruction ChequeIssueInstruction { get; set; }
        public virtual ChequeIssueSub ChequeIssueSub { get; set; }
    }
}