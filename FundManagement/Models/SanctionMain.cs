﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class SanctionMain : EntityBase, IControllerHooks, IFIMSContext
    {
        public SanctionMain()
        {
            SanctionDetails = new List<SanctionDetails>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public SanctionMain(IFIMSContext viewModel)
        {
            SanctionDetails = new List<SanctionDetails>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("SanctionMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int SanctionId { get; set; }

        [Required, Display(Name = "Bank")]
        [ForeignKey("Bank")]
        public short BankId { get; set; }

        [Required, Display(Name = "Branch")]
        [ForeignKey("BankBranch")]
        public short BranchId { get; set; }

        [Required, Display(Name = "Ref No"), DefaultValue("-"), MaxLength(50), Column(TypeName = "VARCHAR")]
        [DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SanctionNo { get; set; }

        [Required, Display(Name = "Tenure")]
        public short SanctionYear { get; set; }

        [Required, Display(Name = "Expire Date"), Column(TypeName = "smalldatetime")]
        public DateTime ExpireDate { get; set; }

        [NotMapped, Display(Name = "Bank")]
        public string BankName { get; set; }

        [NotMapped, Display(Name = "Branch")]
        public string BranchName { get; set; }

        [NotMapped, Display(Name = "Account No"), DefaultValue("-"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string AccountNo { get; set; }

        public virtual Bank Bank { get; set; }
        public virtual BankBranch BankBranch { get; set; }
        public virtual ICollection<SanctionDetails> SanctionDetails { get; set; }
    }
}