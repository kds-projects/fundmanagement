﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public partial class CustomType : EntityBase, IFIMSContext, IControllerHooks
    {
        public CustomType()
        {
            //LCSTenureTypes = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();
        }

        public CustomType(IFIMSContext viewModel)
        {
            //LCSubs = new List<LCSub>();
            //LCWisePIs = new List<LCWisePI>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("CustomType", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public short TypeId { get; set; }
        [Required, Display(Name = "Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        [Index("IX_TypeName", 1, IsUnique = true)]
        public string TypeName { get; set; }
        [Required, Display(Name = "Short Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string ShortName { get; set; }
        [Required, Display(Name = "Flag"), MaxLength(50), Column(TypeName = "VARCHAR")]
        [Index("IX_TypeName", 2, IsUnique = true)]
        public string Flag { get; set; }
        [Display(Name = "Group"), MaxLength(50), Column(TypeName = "VARCHAR"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string GroupName { get; set; }
        [Display(Name = "Category"), MaxLength(150), Column(TypeName = "VARCHAR"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string Category { get; set; }
        [Display(Name = "Sub-Category"), MaxLength(150), Column(TypeName = "VARCHAR"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string SubCategory { get; set; }
        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
        [Required, Display(Name = "SL #")]
        public short RankId { get; set; }

        [InverseProperty("STenureType")]
        public virtual ICollection<LCMain> LCSTenureTypes { get; set; }
        [InverseProperty("BTenureType")]
        public virtual ICollection<LCMain> LCBTenureTypes { get; set; }
        [InverseProperty("Currency")]
        public virtual ICollection<LCMain> LCCurrencies { get; set; }

        [InverseProperty("CategoryType")]
        public virtual ICollection<BankAccount> BACategoryTypes { get; set; }
        [InverseProperty("AccountType")]
        public virtual ICollection<BankAccount> BAccountTypes { get; set; }
        [InverseProperty("CurrencyType")]
        public virtual ICollection<BankAccount> BAccountCurrencies { get; set; }
        public virtual ICollection<LCSub> LCAmendmentTypes { get; set; }
        public virtual ICollection<LCAcceptance> AcceptanceCurrencies { get; set; }
        public virtual ICollection<SanctionDetails> SanctionDetails { get; set; }
    }
}