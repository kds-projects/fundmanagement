﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class PIMain : EntityBase, IControllerHooks, IFIMSContext
    {
        public PIMain()
        {
            PISubs = new List<PISub>();
        }

        public PIMain(IFIMSContext viewModel)
        {
            PISubs = new List<PISub>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("PIMain", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public int PIFileId { get; set; }

        [Required, Display(Name = "File #"), MaxLength(15), Column(TypeName = "VARCHAR")]
        public string PIFileNo { get; set; }

        [Required, Display(Name = "PI No"), MaxLength(75), Column(TypeName = "VARCHAR")]
        public string PINo { get; set; }

        [Required, Display(Name = "PI Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime PIDate { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Month")]
        public Nullable<int> Month { get { return PIDate.Month; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "Year")]
        public Nullable<int> Year { get { return PIDate.Year; } protected set { } }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Display(Name = "SL #")]
        public Nullable<int> SerialNo { get { return short.Parse(PIFileNo.Substring(2, 4)); } protected set { } }

        [Required, Display(Name = "Applicant")]
        public short BeneficiaryId { get; set; }

        [Required, Display(Name = "Supplier")]
        public short SupplierId { get; set; }

        [Required, Display(Name = "Agent")]
        public short LocalAgentId { get; set; }

        [Required, Display(Name = "Country of Origin")]
        public short OriginCountryId { get; set; }

        [Required, Display(Name = "Port of Loading")]
        public short POLId { get; set; }

        [Required, Display(Name = "Esst. Despatch Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ETD { get; set; }

        [Required, Display(Name = "Esst. Arrival Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ETA { get; set; }

        [Required, Display(Name = "Esst. Ship. Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ETS { get; set; }

        [Required, Display(Name = "Esst. Prod. Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ETP { get; set; }

        [Required, Display(Name = "Requisition Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime RequisitionDate { get; set; }

        [Required, Display(Name = "Order Confirm Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime BookingConfirmDate { get; set; }

        [Required, Display(Name = "Receive From Sourcing"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime PIReceiveDateFromSourcing { get; set; }

        [Required, Display(Name = "LC Shipment Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ShipmentDate { get; set; }

        [Required, Display(Name = "Expire Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ExpireDate { get; set; }

        [Required, Display(Name = "Port of Loading")]
        public short LeadTime { get; set; }

        [Required, Display(Name = "Port of Loading")]
        public short Tenure { get; set; }

        [Required, Display(Name = "Payment Term"), MaxLength(250), Column(TypeName = "VARCHAR")]
        public string PaymentTerms { get; set; }

        [Required, Display(Name = "PI Amount"), Column(TypeName = "numeric")]
        public decimal PIAmount { get; set; }

        [Required, Display(Name = "Currency")]
        [ForeignKey("Currency")]
        public short CurrencyId { get; set; }

        [NotMapped, Display(Name = "Currency")]
        public string CurrencyType { get; set; }

        [Required, Display(Name = "BDT Rate"), Column(TypeName = "numeric")]
        public decimal BDTRate { get; set; }

        [Required, Display(Name = "USD Rate"), Column(TypeName = "numeric")]
        public decimal USDRate { get; set; }

        [Display(Name = "USD Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInUSD { get { return PIAmount * USDRate; } protected set { } }

        [Display(Name = "BDT Amount"), Column(TypeName = "numeric"), DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        public Nullable<decimal> AmountInBDT { get { return PIAmount * BDTRate; } protected set { } }

        [Required, Display(Name = "Approval Ref. #"), MaxLength(500), Column(TypeName = "VARCHAR")]
        public string ApprovalRef { get; set; }

        [Required, Display(Name = "CS Done?")]
        public byte IsCSDone { get; set; }

        [Required, Display(Name = "CS Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime CSDate { get; set; }

        [Required, Display(Name = "Bank Forward?")]
        public byte IsForwardToBank { get; set; }

        [Required, Display(Name = "Forward Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime ForwardToBankDate { get; set; }

        [Required, Display(Name = "Forwarding Bank")]
        public short ForwardBankId { get; set; }

        [Required, Display(Name = "Forwarding Branch")]
        public short ForwardBranchId { get; set; }

        [Required, Display(Name = "Forwarding User")]
        public short ForwardUserId { get; set; }

        [Required, Display(Name = "LC Open?")]
        public byte IsLCOpen { get; set; }

        [Required, Display(Name = "LC Open Date"), Column(TypeName = "smalldatetime"), DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public System.DateTime LCOpenDate { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Delay Reason"), MaxLength(250), Column(TypeName = "VARCHAR"), DisplayFormat(ConvertEmptyStringToNull = false)]
        public string DelayReason { get; set; }

        [Required(AllowEmptyStrings = true), Display(Name = "Remarks"), MaxLength(500), Column(TypeName = "VARCHAR"), DataType(DataType.MultilineText)]
        public string Remarks { get; set; }

        [DatabaseGenerated(DatabaseGeneratedOption.Computed)]
        [Required, Display(Name = "Status")]
        public string Status { get { return IsLCOpen == 1 ? "LC OPEN" : IsForwardToBank == 1 ? "FORWARDED TO BANK" : "IN HAND"; } set { } }

        [NotMapped]
        public short _editStatus { get; set; }

        [NotMapped]
        public string RequistionNo
        {
            get
            {
                if (PIFileId == 0)
                    return "";
                using (var ctx = new FundManagement.Models.DBFirst.IMSContext())
                {
                    var ids = (new FundContext()).PIMains.Find(PIFileId).PISubs.Select(c => c.PRSubId).DefaultIfEmpty(0).ToList();
                    ids = ctx.PRSubs.Where(l => ids.Contains(l.PRSubId)).Select(c => c.PRId).ToList();
                    var prnos = ctx.PRMains.Where(l => ids.Contains(l.PRId)).Select(c => c.PRNo).Distinct().ToList();
                    var prno = prnos.Count == 0 ? "" : prnos.Aggregate((i, j) => i + ", " + j);

                    return prno;
                }
            }
        }

        [NotMapped]
        public string PRDate
        {
            get
            {
                if (PIFileId == 0)
                    return "";
                using (var ctx = new FundManagement.Models.DBFirst.IMSContext())
                {
                    var ids = PISubs.Select(c => c.PRSubId).DefaultIfEmpty(0).ToList();
                    ids = ctx.PRSubs.Where(l => ids.Contains(l.PRSubId)).Select(c => c.PRId).ToList();
                    var prnos = ctx.PRMains.Where(l => ids.Contains(l.PRId)).Select(c => c.PRDate).Distinct().ToList().Select(c => c.ToString("dd-MMM-yyyy")).ToList();
                    var prno = prnos.Count == 0 ? "" : prnos.Aggregate((i, j) => i + ", " + j);

                    return prno;
                }
            }
        }

        [NotMapped]
        public FundManagement.Models.DBFirst.Supplier Supplier
        {
            get
            {
                using (var ctx = new FundManagement.Models.DBFirst.IMSContext())
                {
                    return ctx.Suppliers.Find(this.SupplierId) ?? new FundManagement.Models.DBFirst.Supplier();
                }
            }
        }

        [NotMapped]
        public FundManagement.Models.DBFirst.Company Company
        {
            get
            {
                using (var ctx = new FundManagement.Models.DBFirst.ProductContext())
                {
                    return ctx.Companies.Find(this.BeneficiaryId) ?? new FundManagement.Models.DBFirst.Company();
                }   
            }
        }

        [NotMapped]
        public CSMain CSMain
        {
            get
            {
                using (var ctx = new FundContext())
                {
                    return ctx.CSMains.AsEnumerable().Where(c => c.PIFileId == this.PIFileId).First() ?? new CSMain();
                }
            }
        }

        public virtual ICollection<PISub> PISubs { get; set; }

        public virtual CustomType Currency { get; set; }
    }
}