﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class ChequeIssueInstructionTDSVDSVM
    {
        public ChequeIssueInstruction ChequeIssueInstruction { get; set; }

        [Required(ErrorMessage ="Company Name Is Required")]
        [Display(Name ="Company")]
        public int GroupId { get; set; }

        [Display(Name = "From Date")]
        [DataType(DataType.DateTime)]
        [Required, DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime? FromDate { get; set; }

        [Display(Name = "To Date")]
        [DataType(DataType.Date)]
        [Required, DisplayFormat(DataFormatString = "{0:dd-MMM-yyyy}")]
        public DateTime? ToDate { get; set; }

        [Display(Name ="Instruction For:")]
        public int TypeOfTDSVDS { get; set; }

    }
}