﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FundManagement.Models
{
    public class BondVM
    {
        //bb.BondId,bb.BondCode,bb.LicenseNumber
        public Int16 BondId { get; set; }
        public string BondCode { get; set; }

        public string LicenseNumber { get; set; }
    }
}