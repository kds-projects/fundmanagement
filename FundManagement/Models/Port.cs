﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FundManagement.Models
{
    public class Port : EntityBase, IControllerHooks, IFIMSContext
    {
        public Port()
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();
        }

        public Port(IFIMSContext viewModel)
        {
            //BankBranchs = new List<BankBranch>();
            //BankAccounts = new List<BankAccount>();
            //SanctionDetails = new List<SanctionDetails>();

    		foreach (PropertyInfo pi in this.GetType().GetProperties())
            {
                if (viewModel.GetType().GetProperty(pi.Name) != null && viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) != null)
                {
                    pi.SetValue(this, viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel));
                }
    			else if (viewModel.GetType().GetProperty(pi.Name) == null || viewModel.GetType().GetProperty(pi.Name).GetValue(viewModel) == null)
                {
                    object obj = clsMain.getDefaultValue("Port", pi.Name, pi.PropertyType);
                    if (obj != null)
    					pi.SetValue(this, obj);
                }
            }
        }

        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        [Display(Name = "ID")]
        public short PortId { get; set; }

        [Required, Display(Name = "Country")]
        public short CountryId { get; set; }

        [NotMapped, Display(Name = "Code"), MaxLength(5), Column(TypeName = "VARCHAR")]
        public string PortCode { get; set; }

        [Required, Display(Name = "Short Name"), MaxLength(50), Column(TypeName = "VARCHAR")]
        public string ShortName { get; set; }

        [Required, Display(Name = "Name"), MaxLength(150), Column(TypeName = "VARCHAR")]
        public string PortName { get; set; }

        [Required, Display(Name = "Activity")]
        public ActivityType? Activity { get; set; }
    }
}